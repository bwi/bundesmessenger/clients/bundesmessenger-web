<div align="center">
  <img src="https://gitlab.opencode.de/bwi/bundesmessenger/info/-/raw/main/images/logo.png?inline=false" alt="BundesMessenger Logo" width="128" height="128">
</div>

<div align="center">
  <h2 align="center">BundesMessenger-Web</h2>
</div>

---

Wir freuen uns, dass Du Dich für den BundesMessenger interessierst.
BundesMessenger-Web ist ein Matrix Client basierend auf [Element Web](https://github.com/vector-im/element-web)
von [Element Software](https://element.io/) und Teil des [BundesMessenger Backends](https://gitlab.opencode.de/bwi/bundesmessenger/backend)

Allgemeine Infos zum Thema BundesMessenger und was dahinter steckt findet ihr [hier](https://gitlab.opencode.de/bwi/bundesmessenger/info).

## Grundsätzliches

BundesMessenger ist ein Artefakt, welches durch die BWI im Herstellungsprozess für den BwMessenger alle 4 Wochen hergestellt wird.

Hierzu durchlaufen wir folgenden Prozess:

<p align="center">
  <img src="docs/img/element-bum-bwm.png" width="600">
</p>

Aufgrund der starken Bindung an _Element Web_ ist es aktuell nicht vorgesehen, dass ihr über das Repository Einfluss auf den BundesMessenger nehmen könnt. Wenn ihr euch beteiligen wollt, müsst ihr eure Contribution direkt in [Element Web](https://github.com/vector-im/element-web) einfließen lassen. Diese werden in der Regel im Anschluss in den BundesMessenger übernommen.

Wenn ihr euch unsicher seid, haltet hierzu gerne [Rücksprache mit uns](#kontakt).

> Übrigens: Diesen Prozess leben wir selber auch für bestimmte Features.

**Warum veröffentlichen wir hier den Quellcode?**

Wir möchten 100% transparent sein und euch die Möglichkeit geben den Source Code einzusehen.

Wir freuen uns über euer Feedback. Öffnet gerne neue Issues für eure Fragen oder Probleme hier im GitLab.

## Repo

https://gitlab.opencode.de/bwi/bundesmessenger/clients/bundesmessenger-web

## Fehler und Verbesserungsvorschläge

https://gitlab.opencode.de/bwi/bundesmessenger/clients/bundesmessenger-web/-/issues

## Abhängigkeiten

[Element Web](https://github.com/vector-im/element-web)

[BundesMessenger Web - Config and Assets](https://gitlab.opencode.de/bwi/bundesmessenger/clients/bundesmessenger-web-config-and-assets)

[BundesMessenger Web - Matrix SDK](https://gitlab.opencode.de/bwi/bundesmessenger/clients/bundesmessenger-web-matrix-sdk)

[BundesMessenger Web - React SDK](https://gitlab.opencode.de/bwi/bundesmessenger/clients/bundesmessenger-web-react-sdk)

## Updates

In der Regel werden wir hier alle 4 Wochen ein Update veröffentlichen. In etwa zeitgleich erfolgt das Update der Apps in den App Stores.

Sollte es zu Sicherheitsvorfällen kommen, stellen wir hier und im App Store auch kurzfristig Hotfixes zur Verfügung.

## Forks

Du hast die Möglichkeit einen Fork von diesem Projekt zu machen. Bitte beachte, dass die [kontinuierliche Pflege](#updates) sehr viel Aufwand und Entwicklungsressourcen benötigt.

Wir nehmen euch diese Arbeit ab, da wir dies für den BwMessenger ohnehin machen müssen. Daher empfehlen wir euch für den produktiven Einsatz in der öffentlichen Verwaltung die gepflegten BundesMessenger Apps aus den App Stores zu verwenden. Ziel ist es nicht euch auszuschließen, sondern eine stetige hohe Qualität und Sicherheit zur Verfügung zu stellen.

| :warning: Wichtig: Dieses Repository ist ein Mirror eines internen BWI Repos. Die aktive Entwicklung findet nicht in diesem Repository statt. Es müssen BWI spezifische Build Anteile entfernt werden (z.B. GitLab CI Informationen). Um dies zu gewährleisten wird die Git History beim Mirroring neu geschrieben. Wenn ihr einen Fork von diesem Repository erstellt, müsst ihr mit erhöhten Aufwänden bei Updates rechnen. |
| ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |

## Nutzung

Um den BundesMessenger Web nutzen zu können, müsst ihr euer eigenes BundesMessenger Backend betreiben und registrieren lassen. Weitere Infos dazu [hier](https://messenger.bwi.de/ich-will-bum).

Wenn Du Dein Backend noch nicht erfolgreich aufgebaut hast, aber trotzdem schon einen Blick in die App werfen möchtest, bieten wir Dir eine Demo Umgebung an. Bitte kontaktiere uns per [Email](mailto:bundesmessenger@bwi.de&subject=Ich%20will%20testen).

## Für Entwickler

Um den BundesMessenger Webclient weiterzuentwickeln werden zusätzlich die Repositories Config and Assets, Matrix SDK und React SDK benötigt.
Wir empfehlen alle 4 Projekte auszuchecken und übergeordnet eine makefile mit folgendem Inhalt zu erstellen:

```
LINK_FOLDER := ../.yarn/links

init:
@echo "clean up links"
rm -rf ./.yarn/links
@echo "Setting up matrix-js-sdk ..."
cd bundesmessenger-web-matrix-sdk && yarn install && yarn build && yarn link --link-folder ${LINK_FOLDER}
@echo "Setting up matrix-react-sdk ..."
cd bundesmessenger-web-react-sdk && yarn install && yarn link matrix-js-sdk --link-folder ${LINK_FOLDER} && yarn build && yarn link --link-folder ${LINK_FOLDER}
@echo "Setting up bundesmessenger-web ..."
cd bundesmessenger-web && yarn install && yarn link matrix-js-sdk --link-folder ${LINK_FOLDER} && yarn link matrix-react-sdk --link-folder ${LINK_FOLDER}
```

Führt man nun die makefile aus, werden die Repos untereinander verlinkt und die Dependencies geladen.
Der Client kann nun aus dem BwMessenger Web Ordner heraus mit yarn start lokal gestartet und über yarn build gebaut werden.
Initial muss eine config.json Datei im BwMessenger Web Ordner erstellt werden.
Hierzu als Referenz die base.json aus config und assets hernehmen, als config.json ablegen und an die eigene Umgebung anpassen.

Wichtig sind hier vorallem folgende Punkte, die mit der eigenen homeserver Adresse editiert werden müssen:

```
    "m.homeserver": {
        "base_url": "https://meinbackend.de",
        "server_name": "meinbackend.de"
    }
```

```
  "enable_presence_by_hs_url": {
    "https://meinbackend.de": false
  },
```

Weitere Informationen können auch dem [Element Web](https://github.com/vector-im/element-web) Github Repo entnommen werden.

## Rechtliches

**BundesMessenger** ist sowohl als Logo- und als Textmarke durch die [BWI GmbH](https://www.bwi.de) geschützt.

Eine Nutzung derselben ist nur mit Freigabe durch die BWI möglich.

| :warning: Wichtig: Falls ihr einen Fork erstellt, müssen alle Verweise (Texte & Bilder) auf BundesMessenger und die BWI zwingend entfernt werden! Das betrifft nicht die Git History, sondern die Releases der jeweiligen neuen Apps. |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |

Die Lizenz des BundesMessenger Web ist die [AGPLv3](./LICENSE).

### Copyright

- [BWI GmbH](https://messenger.bwi.de/copyright)
- [Element](https://element.io/copyright)

## Kontakt

Für den Austausch zum BundesMessenger haben wir einen [Matrix Raum](https://matrix.to/#/#opencodebum:matrix.org) erstellt.

<div align="center">
  <img src="https://gitlab.opencode.de/bwi/bundesmessenger/info/-/raw/main/images/qr_matrix_room.png" alt="QR Code Matrix" width="128" height="128"/>
</div>

Kein Matrix Client zur Hand, dann auch gerne über unser [Email Postfach](mailto:bundesmessenger@bwi.de).

# Wir freuen uns auf den Austausch.
