# Naming:

Version without hotfix like e.g. 1.14.json

# Format:

{
date: "month year"
version: "Version without hotfix e.g. 1.14"
features:
{
"name": "",
"description": "",
"image":""
}
improvements:
{
"name": "",
"description": "",
"image":""
}
bugfixes:
{
"name": "",
"description": "",
"image":""
}
}

# Possible Extensions

What’s new
Resolved issues
Known issues
