// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { MediaEventContent } from "matrix-js-sdk/src/types";

import { presentableTextForFile } from "../../src/utils/FileUtils";

describe("Voice message", () => {
    // BWI(MESSENGER-4974) Translate "Voice message" body
    // It is a bit stupid to test this
    it("should translate Voice Message", () => {
        expect(
            presentableTextForFile({
                body: "Voice message",
            } as MediaEventContent),
        ).toEqual("Voice Message");
    });
});
