// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { EventType as MxEventType } from "matrix-js-sdk/src/matrix";
import { M_POLL_RESPONSE } from "matrix-events-sdk";

import { shouldSuppressNotification, EventType } from "../../src/bwi/helper/Notification";
import * as TestUtils from "../../test/test-utils";
import { generateRoomId } from "../mock/MockRoom";
import { MatrixClientPeg } from "../../src/MatrixClientPeg";
import { Notifier } from "../../src/Notifier";
import { mockPlatformPeg } from "../../test/test-utils";
import SdkConfig from "../../src/SdkConfig";

describe("Notification", () => {
    TestUtils.stubClient();
    const client = MatrixClientPeg.safeGet();

    const mockRoom = TestUtils.mkStubRoom(generateRoomId(), "testroom", client);
    const mockMember = {
        userId: "test@localhost",
    };

    describe("shouldSuppressNotification", () => {
        const events = [
            {
                type: EventType.Redact,
                ev: TestUtils.mkEvent({
                    type: MxEventType.RoomRedaction,
                    event: true,
                    room: mockRoom.roomId,
                    user: mockMember.userId,
                    content: {},
                }),
            },
            {
                type: EventType.Reaction,
                ev: TestUtils.mkEvent({
                    type: MxEventType.Reaction,
                    event: true,
                    room: mockRoom.roomId,
                    user: mockMember.userId,
                    content: {},
                }),
            },
            {
                type: EventType.Edit,
                ev: TestUtils.mkEvent({
                    type: MxEventType.RoomMessage,
                    event: true,
                    room: mockRoom.roomId,
                    user: mockMember.userId,
                    content: {
                        "m.new_content": "test",
                    },
                }),
            },
            {
                type: EventType.PollResponse,
                ev: TestUtils.mkEvent({
                    type: M_POLL_RESPONSE.name,
                    event: true,
                    room: mockRoom.roomId,
                    user: mockMember.userId,
                    content: {},
                }),
            },
        ];

        beforeEach(() => {
            SdkConfig.put({
                brand: "testbrand",
            });
        });

        describe("it should not suppress events by default", () => {
            test.each(events)("Event: $ev.event.type", ({ ev }) => {
                expect(shouldSuppressNotification(ev)).toEqual(false);
            });
        });

        describe("it should suppress events when config is enabled", () => {
            test.each(events)("Event: $ev.event.type", ({ type, ev }) => {
                SdkConfig.add({
                    suppressNotification: [type],
                });
                expect(shouldSuppressNotification(ev)).toEqual(true);
            });
        });

        describe("nv implementation", () => {
            const platformSpy = jest.fn();
            mockPlatformPeg({ loudNotification: platformSpy });

            MatrixClientPeg.safeGet().getPushActionsForEvent = jest.fn().mockReturnValue({
                notify: true,
                tweaks: {
                    sound: true,
                },
            });

            describe("should not suppress by default", () => {
                test.each(events)("Event: $ev.event.type", ({ ev }) => {
                    Notifier.evaluateEvent(ev);
                    expect(platformSpy).toHaveBeenCalledTimes(1);
                    platformSpy.mockClear();
                });
            });

            describe("should suppress events when config is enabled", () => {
                test.each(events)("Event: $ev.event.type", ({ type, ev }) => {
                    SdkConfig.add({
                        suppressNotification: [type],
                    });
                    Notifier.evaluateEvent(ev);
                    expect(platformSpy).toHaveBeenCalledTimes(0);
                });
            });
        });
    });

    afterAll(() => {
        jest.clearAllMocks();
        jest.resetModules();
    });
});
