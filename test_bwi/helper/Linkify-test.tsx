// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { _linkifyElement } from "../../src/linkify-matrix";

const stringsToLinkify = [
    "notes://test",
    "onenote://test",
    "outlook://test",
    "file://test",
    "ftp://test",
    "http://localhost",
    "https://localhost",
];

describe("Linkify", () => {
    it.each(stringsToLinkify)("should linkify certain custom protocols", (link) => {
        const node = document.createElement("span");
        node.textContent = link;
        _linkifyElement(node);
        const anchorElement = node.querySelector("a");
        expect(anchorElement).toBeTruthy();
        expect(anchorElement?.href).toMatch(link);
    });
});
