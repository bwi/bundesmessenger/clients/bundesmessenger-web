// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { RoomMember } from "matrix-js-sdk/src/matrix";

import { canLeaveRoom } from "../../src/bwi/helper/Room";
import { getMockClientWithEventEmitter, mockClientMethodsUser, unmockClientPeg } from "../../test/test-utils";
import { generateRoomId, mockRoomWithEvents } from "../mock/MockRoom";

const ROOM_ID = generateRoomId();
function mockRoomMember(userId: string, powerLevel: number, membership: string): RoomMember {
    const member = new RoomMember(ROOM_ID, userId);
    member.powerLevel = powerLevel;
    member.membership = membership;
    return member;
}

describe("Room", () => {
    const me = mockRoomMember("admin@localhost", 100, "join");
    const member = mockRoomMember("member@localhost", 10, "join");
    const member2 = mockRoomMember("member1@localhost", 10, "join");
    const inviteMember = mockRoomMember("invitemember@localhost", 10, "invite");
    const inviteAdmin = mockRoomMember("inviteadmin@localhost", 100, "invite");

    getMockClientWithEventEmitter({
        ...mockClientMethodsUser("@name:server.org"),
    });

    const room = mockRoomWithEvents({
        partials: {
            roomId: "my_room",
            myUserId: me.userId,
        },
    });

    const getMemberSpy = jest.spyOn(room, "getMembers");

    afterAll(() => {
        unmockClientPeg();
    });

    it("if admin is alone in a room, he should be able to leave it", () => {
        getMemberSpy.mockReturnValueOnce([me]);
        expect(canLeaveRoom(room)).toEqual(true);
    });

    it("last admin shouldnt be able to leave the room while another member is in", () => {
        getMemberSpy.mockReturnValueOnce([me, member]);
        expect(canLeaveRoom(room)).toEqual(false);
    });

    it("broken room without admin, everyone should be able to leave", () => {
        getMemberSpy.mockReturnValueOnce([member, member2]);
        expect(canLeaveRoom(room)).toEqual(true);
    });

    it("admin wants to leave a room with an invited member, shouldnt be able to leave", () => {
        getMemberSpy.mockReturnValueOnce([me, inviteMember]);
        expect(canLeaveRoom(room)).toEqual(false);
    });

    it("admin wants to leave a room with an invited admin, should be able to leave", () => {
        getMemberSpy.mockReturnValueOnce([me, inviteAdmin]);
        expect(canLeaveRoom(room)).toEqual(true);
    });

    it("admin wants to leave a room with an invited admin and an invited member, should be able to leave", () => {
        getMemberSpy.mockReturnValueOnce([me, inviteAdmin, inviteMember]);
        expect(canLeaveRoom(room)).toEqual(true);
    });
});
