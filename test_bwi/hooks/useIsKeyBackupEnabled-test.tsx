// Copyright 2025 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";
import { act, renderHook, RenderHookResult } from "jest-matrix-react";
import { MatrixClient } from "matrix-js-sdk/src/matrix";
import { CryptoApi } from "matrix-js-sdk/src/crypto-api";

import {
    CROSS_SIGNING_COMPLETED,
    SECRET_IS_CLOSED,
    useIsKeyBackupEnabled,
} from "../../src/bwi/hooks/useIsKeyBackupEnabled.ts";
import MatrixClientContext from "../../src/contexts/MatrixClientContext";
import { createTestClient } from "../../test/test-utils";

describe("useIsKeyBackupEnabled", () => {
    let client: MatrixClient;

    beforeEach(() => {
        client = createTestClient();
    });

    afterEach(() => jest.clearAllMocks());
    beforeAll(() => jest.useFakeTimers());
    afterAll(() => jest.useRealTimers());

    const wrapper = ({ children }: { children: React.ReactNode }): React.ReactNode => (
        <MatrixClientContext.Provider value={client}>{children}</MatrixClientContext.Provider>
    );

    function render(): RenderHookResult<any, any> {
        // @ts-ignore
        return renderHook(() => useIsKeyBackupEnabled(), { wrapper: wrapper });
    }

    it("no input, isKeyBackupEnabled false", () => {
        const result = render();
        jest.advanceTimersByTime(1);
        expect(result.result.current).toBe(false);
    });

    it("crossSigning done, isKeyBackupEnabled true", async () => {
        const event = new Event(CROSS_SIGNING_COMPLETED);
        const crypto = client.getCrypto() as unknown as CryptoApi;
        crypto.getUserVerificationStatus = jest.fn().mockResolvedValue({
            isVerified: () => true,
        });
        let result: RenderHookResult<any, any> = {} as unknown as RenderHookResult<any, any>;
        await act(async () => {
            result = render();
        });
        await act(async () => {
            window.dispatchEvent(event);
        });

        jest.advanceTimersByTime(1);
        expect(result.result.current).toBe(true);
    });

    it("keybackup done, isKeyBackupEnabled true", async () => {
        const event = new Event(SECRET_IS_CLOSED);
        const crypto = client.getCrypto() as CryptoApi;
        crypto.getActiveSessionBackupVersion = jest.fn().mockResolvedValue("");
        let result: RenderHookResult<any, any> = {} as unknown as RenderHookResult<any, any>;
        await act(async () => {
            result = render();
        });
        await act(async () => {
            window.dispatchEvent(event);
        });

        jest.advanceTimersByTime(1);
        expect(result.result.current).toBe(true);
    });
});
