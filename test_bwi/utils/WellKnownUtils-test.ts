// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { MockedObject } from "jest-mock";
import { IClientWellKnown, MatrixClient } from "matrix-js-sdk/src/matrix";

import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import {
    getSecureBackupSetupMethods,
    IE2EEWellKnown,
    isSecureBackupRequired,
    SecureBackupSetupMethod,
} from "../../src/utils/WellKnownUtils";
import { getMockClientWithEventEmitter } from "../../test/test-utils";
import { mockFeatureFlag } from "../mock/MockSettingsStore";

const noE2EEWellKnown: IClientWellKnown = {
    "m.homeserver": {
        base_url: "mock.org",
    },
};

const E2EEDisabledPartOfWellKnown: IE2EEWellKnown = {
    secure_backup_required: false,
    secure_backup_setup_methods: [],
};

const E2EEEnabledPartOfWellKnown: IE2EEWellKnown = {
    secure_backup_required: true,
    secure_backup_setup_methods: [SecureBackupSetupMethod.Passphrase, SecureBackupSetupMethod.Key],
};

const E2EEEnabledPartPassphraseOnly: IE2EEWellKnown = {
    secure_backup_required: true,
    secure_backup_setup_methods: [SecureBackupSetupMethod.Passphrase],
};

const E2EEDisabledWellKnown: IClientWellKnown = {
    "m.homeserver": {
        base_url: "mock.org",
    },
    "io.element.e2ee": E2EEDisabledPartOfWellKnown,
};

const E2EEEnabledWellKnown: IClientWellKnown = {
    "m.homeserver": {
        base_url: "mock.org",
    },
    "io.element.e2ee": E2EEEnabledPartOfWellKnown,
};

const E2EEEnabledPasPhraseOnlyWellKnown: IClientWellKnown = {
    "m.homeserver": {
        base_url: "mock.org",
    },
    "io.element.e2ee": E2EEEnabledPartPassphraseOnly,
};

describe("WellKnownUtils", () => {
    let client: MockedObject<MatrixClient>;

    beforeAll(() => {
        client = getMockClientWithEventEmitter({
            getClientWellKnown: jest.fn(),
        });
    });

    afterAll(() => {
        jest.resetModules();
    });

    describe("isSecureBackupRequired", () => {
        const checkRequired = (wellKnown: IClientWellKnown | undefined, shouldBeRequired: boolean): void => {
            client.getClientWellKnown.mockImplementation(() => wellKnown);
            if (shouldBeRequired) expect(isSecureBackupRequired(client)).toBe(true);
            else expect(isSecureBackupRequired(client)).toBeFalsy();
        };

        describe("feature flag off", () => {
            it("client wellknown undefined", () => {
                checkRequired(undefined, false);
            });
            it("client wellknown without E2EE flag", () => {
                checkRequired(noE2EEWellKnown, false);
            });
            it("client wellknown with E2EE flag disabled", () => {
                checkRequired(E2EEDisabledWellKnown, false);
            });

            it("client wellknown with E2EE flag enabled", () => {
                checkRequired(E2EEEnabledWellKnown, true);
            });
        });

        describe("feature flag on", () => {
            it("client wellknown undefined", () => {
                const mandatorySpy = mockFeatureFlag(BwiFeature.MandatoryCrossSigning, true);
                checkRequired(undefined, true);
                mandatorySpy.mockRestore();
            });
            it("client wellknown without E2EE flag", () => {
                const mandatorySpy = mockFeatureFlag(BwiFeature.MandatoryCrossSigning, true);
                checkRequired(noE2EEWellKnown, true);
                mandatorySpy.mockRestore();
            });
            it("client wellknown with E2EE flag disabled", () => {
                const mandatorySpy = mockFeatureFlag(BwiFeature.MandatoryCrossSigning, true);
                checkRequired(E2EEDisabledWellKnown, false);
                mandatorySpy.mockRestore();
            });

            it("client wellknown with E2EE flag enabled", () => {
                const mandatorySpy = mockFeatureFlag(BwiFeature.MandatoryCrossSigning, true);
                checkRequired(E2EEEnabledWellKnown, true);
                mandatorySpy.mockRestore();
            });
        });
    });
    describe("getSecureBackupSetupMethods", () => {
        const allMethods = [SecureBackupSetupMethod.Passphrase, SecureBackupSetupMethod.Key];
        const onlyPassPhrase = [SecureBackupSetupMethod.Passphrase];

        const checkMethods = (
            wellKnown: IClientWellKnown | undefined,
            expectedMethods: SecureBackupSetupMethod[] | undefined,
        ): void => {
            client.getClientWellKnown.mockImplementation(() => wellKnown);
            expect(getSecureBackupSetupMethods(client)).toEqual(expectedMethods);
        };

        describe("feature flag off", () => {
            it("client wellknown undefined", () => {
                checkMethods(undefined, allMethods);
            });
            it("client wellknown without methods", () => {
                checkMethods(noE2EEWellKnown, allMethods);
            });
            it("client wellknown with both methods", () => {
                checkMethods(E2EEEnabledWellKnown, allMethods);
            });

            it("client wellknown with only passphrase", () => {
                checkMethods(E2EEEnabledPasPhraseOnlyWellKnown, onlyPassPhrase);
            });
        });

        describe("feature flag on", () => {
            it("client wellknown undefined", () => {
                const mandatorySpy = mockFeatureFlag(BwiFeature.MandatoryCrossSigning, true);
                checkMethods(undefined, onlyPassPhrase);
                mandatorySpy.mockRestore();
            });
            it("client wellknown without methods", () => {
                const mandatorySpy = mockFeatureFlag(BwiFeature.MandatoryCrossSigning, true);
                checkMethods(noE2EEWellKnown, onlyPassPhrase);
                mandatorySpy.mockRestore();
            });
            it("client wellknown with both methods", () => {
                const mandatorySpy = mockFeatureFlag(BwiFeature.MandatoryCrossSigning, true);
                checkMethods(E2EEEnabledWellKnown, allMethods);
                mandatorySpy.mockRestore();
            });

            it("client wellknown with only passphrase", () => {
                const mandatorySpy = mockFeatureFlag(BwiFeature.MandatoryCrossSigning, true);
                checkMethods(E2EEEnabledPasPhraseOnlyWellKnown, onlyPassPhrase);
                mandatorySpy.mockRestore();
            });
        });
    });
});
