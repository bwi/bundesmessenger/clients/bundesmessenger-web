// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";
import { act, fireEvent, render, screen, waitFor, within } from "jest-matrix-react";
import fetchMockJest from "fetch-mock-jest";

import { BwiA11yDeclaration } from "../../src/bwi/components/A11yDeclaration";
import { mockFeatureFlag } from "../mock/MockSettingsStore";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import SdkConfig from "../../src/SdkConfig";
import Modal from "../../src/Modal";

describe("A11yDeclaration", () => {
    beforeEach(() => {
        fetchMockJest.restore();

        fetchMockJest.get("/a11y/de.html", { status: 404 });
        fetchMockJest.get("/a11y/en.html", "<h2>My a11y declaration</h2>");

        SdkConfig.put({ brand: "MyApp" });
    });

    it("should display nothing if the feature is not configured", () => {
        const { container } = render(<BwiA11yDeclaration />);

        expect(container).toBeEmptyDOMElement();
    });

    it("should display the title if feature is configured", () => {
        const flag = mockFeatureFlag(BwiFeature.A11yDeclaration, "/a11y/{lang}.html");

        render(<BwiA11yDeclaration />);

        expect(screen.queryByRole("button", { name: "Accessibility statement" })).toBeInTheDocument();

        flag.mockRestore();
    });

    it("should open and load the a11y declaration", async () => {
        const flag = mockFeatureFlag(BwiFeature.A11yDeclaration, "/a11y/{lang}.html");

        render(<BwiA11yDeclaration />);

        await act(async () => {
            fireEvent.click(screen.getByRole("button", { name: "Accessibility statement" }));
        });

        await waitFor(() => {
            expect(document.querySelector(".mx_Dialog")).toBeInTheDocument();
        });

        const dialog = within(document.querySelector(".mx_Dialog")!);
        expect(dialog.queryByText("My a11y declaration")).toBeInTheDocument();

        flag.mockRestore();
    });

    it("should not open if the a11y declaration could not be loaded", async () => {
        const flag = mockFeatureFlag(BwiFeature.A11yDeclaration, "/a11y/de.html");

        render(<BwiA11yDeclaration />);

        const modalSpy = jest.spyOn(Modal, "createDialog");

        await act(async () => {
            fireEvent.click(screen.getByRole("button", { name: "Accessibility statement" }));
        });

        expect(modalSpy).not.toHaveBeenCalled();

        flag.mockRestore();
    });
});
