// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { fireEvent, render, screen } from "jest-matrix-react";
import React from "react";
import { act } from "react-dom/test-utils"; // eslint-disable-line deprecate/import

import { InputTemplate, InputWithTemplates } from "../../../src/bwi/components/structures/InputWithTemplates";
import {
    getMockClientWithEventEmitter,
    mockPlatformPeg,
    unmockClientPeg,
    unmockPlatformPeg,
} from "../../../test/test-utils";

async function selectTemplate(templateLabel: string): Promise<void> {
    const dropdown = screen.getByLabelText("mockInput", {
        selector: "div",
    });
    act(() => {
        fireEvent.click(dropdown, {});
    });

    const option = screen.getByText(templateLabel);
    act(() => {
        fireEvent.click(option, {});
    });
}

describe("InputWithTemplate", () => {
    const mockInputTemplates: InputTemplate[] = [
        { description: "mock1desc", key: "mock1", label: "mock1label" },
        { description: "mock2desc", key: "mock2", label: "mock2label" },
    ];

    beforeEach(() => {
        getMockClientWithEventEmitter({});
        mockPlatformPeg();
    });

    afterAll(() => {
        unmockClientPeg();
        unmockPlatformPeg();
    });

    it("should render input without control buttons", () => {
        render(<InputWithTemplates label="mockInput" placeholder="mockplaceholder" templates={mockInputTemplates} />);

        expect(screen.getByText("Templates")).toBeTruthy();
        expect(screen.queryByText("Cancel")).toBeNull();
        expect(screen.queryByText("Save")).toBeNull();
    });

    it("should render control buttons on input change", () => {
        render(<InputWithTemplates label="mockInput" placeholder="mockplaceholder" templates={mockInputTemplates} />);

        const input: HTMLInputElement = screen.getByTestId("template-input");

        act(() => {
            fireEvent.change(input, { target: { value: "random value" } });
        });

        expect(screen.getByText("Cancel")).toBeTruthy();
        expect(screen.getByText("Save")).toBeTruthy();
    });

    it("should show counter and limit", () => {
        const maxChars = 100;
        const initialValue = "Mock";
        render(
            <InputWithTemplates
                label="mockInput"
                placeholder="mockplaceholder"
                showCounter
                templates={mockInputTemplates}
                maxChars={maxChars}
                initialValue={initialValue}
            />,
        );

        expect(screen.queryByText(new RegExp(`${initialValue.length}\\s/\\s${maxChars}`))).toBeInTheDocument();
    });

    it("should render dropdown with template options", () => {
        render(
            <InputWithTemplates
                label="mockInput"
                placeholder="mockplaceholder"
                showCounter
                templates={mockInputTemplates}
            />,
        );

        const dropdown = screen.getByLabelText("mockInput", {
            selector: "div",
        });
        act(() => {
            fireEvent.click(dropdown, {});
        });

        expect(screen.queryByText(mockInputTemplates[0].label)).toBeInTheDocument();
        expect(screen.queryByText(mockInputTemplates[1].label)).toBeInTheDocument();
    });

    it("should set template value on template change", () => {
        render(
            <InputWithTemplates
                label="mockInput"
                placeholder="mockplaceholder"
                showCounter
                templates={mockInputTemplates}
            />,
        );

        selectTemplate(mockInputTemplates[0].label);

        const input: HTMLInputElement = screen.getByTestId("template-input");
        expect(input.value).toEqual(mockInputTemplates[0].description);
    });

    it("should reset input value and template on cancel click", () => {
        const callback = jest.fn();
        render(
            <InputWithTemplates
                label="mockInput"
                placeholder="mockplaceholder"
                showCounter
                templates={mockInputTemplates}
                cancelCallback={callback}
            />,
        );

        const input: HTMLInputElement = screen.getByTestId("template-input");

        selectTemplate(mockInputTemplates[0].label);
        // ensure the input vlaue is set to the previous selected template description
        expect(screen.queryByDisplayValue(mockInputTemplates[0].description)).toBeInTheDocument();

        const cancelButton = screen.getByText("Cancel");
        act(() => {
            fireEvent.click(cancelButton);
        });

        // input value
        expect(input.value).toEqual("");
        // dropdown value
        expect(screen.queryByText("Templates")).toBeInTheDocument();
        // callback
        expect(callback).toHaveBeenCalled();
    });

    it("should emit save callback on save click", () => {
        const callback = jest.fn();
        render(
            <InputWithTemplates
                label="mockInput"
                placeholder="mockplaceholder"
                showCounter
                templates={mockInputTemplates}
                saveCallback={callback}
            />,
        );

        // make some changes to mark the field as dirty
        const input: HTMLInputElement = screen.getByTestId("template-input");
        act(() => {
            fireEvent.change(input, { target: { value: "random value" } });
        });

        const saveButton = screen.getByText("Save");
        act(() => {
            fireEvent.click(saveButton);
        });

        expect(callback).toHaveBeenCalled();
    });
});
