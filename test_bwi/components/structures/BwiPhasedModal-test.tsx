// Copyright 2024 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";
import { act, fireEvent, render, screen } from "jest-matrix-react";

import { PhasedModal, usePhasedModel } from "../../../src/bwi/components/structures/BwiPhasedModal";
import { _t } from "../../../src/languageHandler";

// @ts-ignore TS2709
function TestComponent({ initialPhase }: { initialPhase: "one" | "two" | "three" }): PhasedModal {
    const modal = usePhasedModel({
        initialPhase: initialPhase,
        phaseOrder: ["one", "two", "three"],
    });

    return (
        <PhasedModal modal={modal}>
            <PhasedModal.Phase phase="one">
                <p>Phase 1</p>
            </PhasedModal.Phase>
            <PhasedModal.Phase phase="two">
                <p>Phase 2</p>
            </PhasedModal.Phase>
            <PhasedModal.Phase phase="three">
                <p>Phase 3</p>
            </PhasedModal.Phase>
        </PhasedModal>
    );
}

describe("<BwiPhasedModel />", () => {
    it("should show all components", () => {
        render(<TestComponent initialPhase="two" />);

        // expect correct phase to be shown
        expect(screen.queryByText("Phase 2")).toBeInTheDocument();

        // expect counter to show the correct phase page
        expect(screen.queryByText("2/3")).toBeInTheDocument();
    });

    it("should navigate to the next phase", () => {
        render(<TestComponent initialPhase="two" />);

        act(() => {
            fireEvent.click(screen.getByText(_t("action|next")));
        });
        // expect phase 3 to be active
        expect(screen.queryByText("Phase 3")).toBeInTheDocument();
        expect(screen.queryByText("3/3")).toBeInTheDocument();
        // expect 'Go back' button to be visible
        expect(screen.queryByText(_t("action|go_back"))).toBeInTheDocument();
        // expect 'Next' button to be hidden because we are in the final phase
        expect(screen.queryByText(_t("action|next"))).not.toBeInTheDocument();
    });

    it("should navigate to the previous phase", () => {
        render(<TestComponent initialPhase="two" />);

        act(() => {
            fireEvent.click(screen.getByText(_t("action|go_back")));
        });
        // expect phase 1 to be active
        expect(screen.queryByText("Phase 1")).toBeInTheDocument();
        expect(screen.queryByText("1/3")).toBeInTheDocument();
        // expect 'Go back' button to be hidden because we are in the final phase
        expect(screen.queryByText(_t("action|go_back"))).not.toBeInTheDocument();
        // expect 'Next' button to be visible
        expect(screen.queryByText(_t("action|next"))).toBeInTheDocument();
    });
});
