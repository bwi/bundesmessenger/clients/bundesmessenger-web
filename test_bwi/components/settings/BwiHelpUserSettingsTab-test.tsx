// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";
import { cleanup, render, screen } from "jest-matrix-react";
import { act } from "react-dom/test-utils"; // eslint-disable-line deprecate/import

import { BwiConfigOptions } from "../../../src/bwi/settings/SdkConfigOptions";
import { BwiHelpUserSettingsTab } from "../../../src/bwi/settings/tabs/user/HelpUserSettingsTab";
import { IConfigOptions } from "../../../src/IConfigOptions";
import SdkConfig from "../../../src/SdkConfig";
import * as TestUtils from "../../../test/test-utils";
import {
    getMockClientWithEventEmitter,
    mockPlatformPeg,
    unmockClientPeg,
    unmockPlatformPeg,
} from "../../../test/test-utils";

jest.mock("../../../src/bwi/BwiPlatform", () => ({
    getElementVersion: jest.fn().mockResolvedValue("1"),
}));

const WrappedBwiHelpUserSettingsTab = TestUtils.wrapInMatrixClientContext(BwiHelpUserSettingsTab);

describe("HelpUserSettingsTab", () => {
    beforeAll(() => {
        getMockClientWithEventEmitter({});
        mockPlatformPeg({
            getAppVersion: jest.fn().mockResolvedValue("1"),
            canSelfUpdate: jest.fn().mockResolvedValue(true),
        });

        SdkConfig.put({
            brand: "Test",
        });
    });

    afterAll(() => {
        unmockClientPeg();
        unmockPlatformPeg();
    });

    afterEach(cleanup);

    it("should render helpdesk infos", async () => {
        const config: Pick<BwiConfigOptions, "helpdesk"> = {
            helpdesk: {
                name: "TestHelpdesk",
                telephone: [
                    {
                        type: "internal",
                        number: "+491337",
                    },
                ],
            },
        };
        SdkConfig.put(config);

        await act(async () => {
            render(<WrappedBwiHelpUserSettingsTab />);
        });

        expect(screen.getByText(config.helpdesk!.name)).toBeTruthy();
        expect(screen.getByText(config.helpdesk!.telephone[0].number)).toBeTruthy();
    });

    it("should render faq url", async () => {
        const config: Pick<BwiConfigOptions, "faq_url"> = {
            faq_url: "http://localhost/",
        };
        SdkConfig.put(config);

        await act(async () => {
            render(<WrappedBwiHelpUserSettingsTab />);
        });

        const faqLink = screen.getByText(config.faq_url!);
        expect(faqLink).toBeTruthy();
        expect((faqLink as HTMLAnchorElement).href).toEqual(config.faq_url);
    });

    it("should render wiki url", async () => {
        const config: Pick<BwiConfigOptions, "wiki_url"> = {
            wiki_url: "http://localhost/",
        };
        SdkConfig.put(config);

        await act(async () => {
            render(<WrappedBwiHelpUserSettingsTab />);
        });

        const wikiLink = screen.getByText("Wiki", { selector: "a" });
        expect(wikiLink).toBeTruthy();
        expect((wikiLink as HTMLAnchorElement).href).toEqual(config.wiki_url);
    });

    it("should render community support hint", async () => {
        const config: Pick<IConfigOptions, "community_support"> = {
            community_support: true,
        };
        SdkConfig.put(config);

        await act(async () => {
            render(<WrappedBwiHelpUserSettingsTab />);
        });

        expect(screen.queryByText("Community Support", { selector: "b" })).toBeInTheDocument();
    });
});
