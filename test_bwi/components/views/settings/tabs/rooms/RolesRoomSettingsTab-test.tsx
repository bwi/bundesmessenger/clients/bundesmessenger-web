/*
Copyright 2022 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import { fireEvent, render, RenderResult, screen, waitFor } from "jest-matrix-react";
import { MatrixClient, EventType, IContent, Room, User, MatrixEvent, RoomMember } from "matrix-js-sdk/src/matrix";
import React from "react";

import RolesRoomSettingsTab from "../../../../../../src/components/views/settings/tabs/room/RolesRoomSettingsTab";
import { MatrixClientPeg } from "../../../../../../src/MatrixClientPeg";
import { ElementCall } from "../../../../../../src/models/Call";
import SettingsStore from "../../../../../../src/settings/SettingsStore";
import { mkStubRoom, stubClient } from "../../../../../../test/test-utils";
import { userToRoomMember } from "../../../../../test-utils/user";
import MatrixClientContext from "../../../../../../src/contexts/MatrixClientContext";
import { resetPowerLevels } from "../../../../../../src/bwi/functions";
import SdkConfig from "../../../../../../src/SdkConfig";

describe("RolesRoomSettingsTab", () => {
    const roomId = "!room:example.com";
    let cli: MatrixClient;
    let room: Room;

    // @ts-ignore
    const mockPowerEvent = ({ userA, usersDefault, prevDefault, users, prevUsers }): MatrixEvent => {
        const mxEvent = new MatrixEvent({
            type: EventType.RoomPowerLevels,
            sender: userA.userId,
            state_key: "",
            content: {
                users_default: usersDefault,
                users,
            },
            unsigned: {
                prev_content: {
                    users: prevUsers,
                    users_default: prevDefault,
                },
            },
        });
        mxEvent.sender = { name: userA.name } as RoomMember;
        return mxEvent;
    };

    const renderTab = async (rm: Room = room): Promise<RenderResult> => {
        const renderResult = render(
            <MatrixClientContext.Provider value={cli}>
                <RolesRoomSettingsTab room={rm} />
            </MatrixClientContext.Provider>,
        );
        await waitFor(() => expect(screen.getByText("Permissions")).toBeInTheDocument());
        return renderResult;
    };

    beforeEach(() => {
        stubClient();
        cli = MatrixClientPeg.safeGet();
        room = mkStubRoom(roomId, "test room", cli);
    });

    // Override for matrix-react-sdk/test/components/views/settings/tabs/room/RolesRoomSettingsTab-test.tsx >
    // RolesRoomSettingsTab › Element Call
    describe("Element Call", () => {
        const setGroupCallsEnabled = (val: boolean): void => {
            // @ts-ignore
            jest.spyOn(SettingsStore, "getValue").mockImplementation((name: string) => {
                if (name === "feature_group_calls") return val;
            });
        };

        const getStartCallSelect = (tab: RenderResult): Element | null => {
            return tab.container.querySelector("select[label='Start Element Call calls']");
        };

        const getStartCallSelectedOption = (tab: RenderResult): Element | null => {
            return tab.container.querySelector("select[label='Start Element Call calls'] option:checked");
        };

        const getJoinCallSelect = (tab: RenderResult): Element | null => {
            return tab.container.querySelector("select[label='Join Element Call calls']");
        };

        const getJoinCallSelectedOption = (tab: RenderResult): Element | null => {
            return tab.container.querySelector("select[label='Join Element Call calls'] option:checked");
        };

        describe("Element Call enabled", () => {
            beforeAll(() => {
                SdkConfig.put({
                    element_call: {
                        url: "https://mockety.mock.io",
                        use_exclusively: true,
                        participant_limit: 8,
                        brand: "Element Call",
                    },
                });
            });

            afterAll(() => {
                SdkConfig.reset();
            });

            beforeEach(() => {
                setGroupCallsEnabled(true);
            });

            describe("Join Element calls", () => {
                it("defaults to moderator for joining calls", async () => {
                    const tab = await renderTab();
                    expect(getJoinCallSelectedOption(tab)?.textContent).toBe("Moderator");
                });

                it("can change joining calls power level", async () => {
                    const tab = await renderTab();

                    console.log("fired event");
                    fireEvent.change(getJoinCallSelect(tab)!, {
                        target: { value: 0 },
                    });

                    expect(getJoinCallSelectedOption(tab)?.textContent).toBe("Member");
                    expect(cli.sendStateEvent).toHaveBeenCalledWith(roomId, EventType.RoomPowerLevels, {
                        events: {
                            [ElementCall.MEMBER_EVENT_TYPE.name]: 0,
                        },
                    });
                });
            });

            describe("Start Element calls", () => {
                it("defaults to moderator for starting calls", async () => {
                    const tab = await renderTab();
                    expect(getStartCallSelectedOption(tab)?.textContent).toBe("Moderator");
                });

                it("can change starting calls power level", async () => {
                    const tab = await renderTab();

                    fireEvent.change(getStartCallSelect(tab)!, {
                        target: { value: 0 },
                    });

                    expect(getStartCallSelectedOption(tab)?.textContent).toBe("Member");
                    expect(cli.sendStateEvent).toHaveBeenCalledWith(roomId, EventType.RoomPowerLevels, {
                        events: {
                            [ElementCall.CALL_EVENT_TYPE.name]: 0,
                        },
                    });
                });
            });
        });

        it("hides when group calls disabled", async () => {
            setGroupCallsEnabled(false);

            const tab = await renderTab();

            expect(getStartCallSelect(tab)).toBeFalsy();
            expect(getStartCallSelectedOption(tab)).toBeFalsy();

            expect(getJoinCallSelect(tab)).toBeFalsy();
            expect(getJoinCallSelectedOption(tab)).toBeFalsy();
        });
    });

    describe("Privileged Users", () => {
        it("should not be able change my powerlevel as last remaining admin", () => {
            const alice = userToRoomMember(new User("@alice:server.org"));
            alice.powerLevel = 100;
            const bob = userToRoomMember(new User("@bob:server.org"));
            bob.powerLevel = 50;

            const room = new Room("!my_room:server.org", cli, alice.userId);
            const event = mockPowerEvent({
                userA: alice,
                users: {
                    [alice.userId]: 100,
                    [bob.userId]: 50,
                },
                usersDefault: null,
                prevDefault: null,
                prevUsers: null,
            });
            room.currentState.getStateEvents = jest.fn().mockReturnValue(event);

            jest.spyOn(room, "getMembers").mockReturnValue([alice, bob]);

            renderTab(room);

            expect(screen.getByDisplayValue("Admin")).toHaveAttribute("disabled", "");
        });
    });

    describe("reset power levels", () => {
        it("should reset bwi override to bwi default", () => {
            const plContent = {
                events: {
                    [EventType.RoomAvatar]: 100,
                    [EventType.RoomRedaction]: 100,
                },
            } as IContent;

            resetPowerLevels(
                {
                    [EventType.RoomAvatar]: { bwiDefault: 50 },
                    [EventType.RoomRedaction]: {},
                },
                plContent,
                (key, content, value) => {
                    content.events[key.slice("event_levels_".length)] = value;
                },
            );

            expect(plContent.events).toEqual({
                [EventType.RoomAvatar]: 50,
                [EventType.RoomRedaction]: 100,
            });
        });
    });
});
