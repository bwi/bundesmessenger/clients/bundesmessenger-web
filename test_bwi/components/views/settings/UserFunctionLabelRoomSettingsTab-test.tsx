// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";
import { EventType, ISendEventResponse, RoomStateEvent, User } from "matrix-js-sdk/src/matrix";
import { act, cleanup, fireEvent, render, screen } from "jest-matrix-react";
import { defer } from "matrix-js-sdk/src/utils";

import * as TestUtils from "../../../../test/test-utils";
import { UserFunctionLabelRoomSettingsTab } from "../../../../src/bwi/components/views/settings/tabs/UserFunctionLabelRoomSettingsTab";
import * as UserFunctionLabelHelper from "../../../../src/bwi/helper/events/UserFunctionLabel";
import { getBaseClient, getClientUserMocks } from "../../../mock/MockClient";
import { generateRoomId, mockRoomWithEvents } from "../../../mock/MockRoom";
import { userToRoomMember } from "../../../test-utils/user";

describe("UserFunctionLabelRoomSettingsTab", () => {
    const roomId = generateRoomId();
    const mockLabel = "testlabel";
    const member = userToRoomMember(new User("test@localhost"));
    const event = TestUtils.mkEvent({
        type: EventType.BwiUserFunctionLabels,
        event: true,
        content: {
            [member.userId]: mockLabel,
        },
        room: roomId,
        user: member.userId,
        skey: "",
    });
    const powerLevels = TestUtils.mkEvent({
        type: EventType.RoomPowerLevels,
        event: true,
        content: {
            events: {
                [EventType.BwiUserFunctionLabels]: 100,
            },
        },
        room: roomId,
        user: member.userId,
    });

    getBaseClient({
        ...getClientUserMocks(),
        sendStateEvent: () => Promise.resolve(),
    });
    const room = mockRoomWithEvents({
        partials: {
            roomId,
            name: "1231241241",
            currentState: {
                maySendStateEvent: jest.fn().mockReturnValue(true),
            },
            getMember: () => member,
        },
        events: [event, powerLevels],
    });

    const unsetUserFunctionLabelSpy = jest.spyOn(UserFunctionLabelHelper, "unsetUserFunctionLabel");
    const setUserFunctionLabelSpy = jest.spyOn(UserFunctionLabelHelper, "setUserFunctionLabel");

    beforeEach(() => {
        room.currentState.setStateEvents([event, powerLevels]);
        unsetUserFunctionLabelSpy.mockClear();
        setUserFunctionLabelSpy.mockClear();
        room.currentState.maySendStateEvent.mockClear();
    });

    afterAll(() => {
        jest.clearAllMocks();
        jest.restoreAllMocks();
        jest.resetModules();
    });

    // TODO: This is not the best practice, the buttons should have more meaningful labels. Then we could query them way better.
    // Currently 'Apply' occures multiple times
    function getEditorApplyButton(): HTMLElement {
        return screen.getByTestId("editor-apply");
    }
    // TODO: Same as apply
    function getEditorCancelButton(): HTMLElement {
        return screen.getByTestId("editor-abort");
    }

    const inputLabel = `${member.rawDisplayName} (${member.userId})`;
    function changeInputValue(value: string): {
        clickSave(): Promise<void>;
        clickCancel(): Promise<void>;
        inputElement: any;
    } {
        const inputElement = screen.getByLabelText(inputLabel);

        act(() => {
            fireEvent.change(inputElement, {
                target: { value },
            });
        });

        return {
            async clickSave() {
                await act(async () => {
                    fireEvent.click(getEditorApplyButton());
                });
            },
            async clickCancel() {
                await act(async () => {
                    fireEvent.click(getEditorCancelButton());
                });
            },
            inputElement,
        };
    }

    it('should properly subscribe to "RoomState.events" and unsubscribe when unmounting', () => {
        const eventListenerSpy = jest.spyOn(room.currentState, "on");
        const removeListenerSpy = jest.spyOn(room.currentState, "removeListener");

        render(<UserFunctionLabelRoomSettingsTab room={room} />);

        expect(eventListenerSpy).toHaveBeenCalledWith("RoomState.events", expect.any(Function));
        const eventListenerFunction = eventListenerSpy.mock.calls[0][1];

        cleanup();

        expect(removeListenerSpy).toHaveBeenCalledWith("RoomState.events", eventListenerFunction);
    });

    it("should reflect the current users function label", () => {
        render(<UserFunctionLabelRoomSettingsTab room={room} />);
        expect(screen.queryByDisplayValue(mockLabel)).toBeInTheDocument();
    });

    it("abort should restore current label", async () => {
        render(<UserFunctionLabelRoomSettingsTab room={room} />);

        const newLabel = "Esel";

        const { clickCancel } = changeInputValue(newLabel);

        await clickCancel();

        expect(screen.queryByDisplayValue(mockLabel)).toBeInTheDocument();
    });

    it("save should call onSave and lock inputs while waiting", async () => {
        const newLabel = "🐎";
        const deffered = defer<ISendEventResponse>();

        setUserFunctionLabelSpy.mockReturnValueOnce(deffered.promise);

        render(<UserFunctionLabelRoomSettingsTab room={room} />);

        const { clickSave, inputElement } = changeInputValue(newLabel);

        await clickSave();

        // .toBeDisabled does not accept disabled=""
        function assertDisabled(element: HTMLElement, disabled: boolean): void {
            if (disabled) {
                expect(element).toHaveAttribute("disabled", "");
            } else {
                expect(element).not.toHaveAttribute("disabled", "");
            }
        }

        assertDisabled(inputElement, true);
        assertDisabled(getEditorApplyButton() as HTMLElement, true);
        assertDisabled(getEditorCancelButton() as HTMLElement, true);

        await act(async () => {
            deffered.resolve({} as ISendEventResponse);
        });

        assertDisabled(inputElement, false);
        assertDisabled(getEditorApplyButton(), false);
        assertDisabled(getEditorCancelButton(), false);
    });

    it("delete should properly unset users function label", async () => {
        render(<UserFunctionLabelRoomSettingsTab room={room} />);

        await act(async () => {
            fireEvent.click(screen.getByRole("button", { name: "Delete" }));
        });

        expect(unsetUserFunctionLabelSpy).toHaveBeenCalled();
    });

    it("saving an empty value should unset instead of saving it", async () => {
        render(<UserFunctionLabelRoomSettingsTab room={room} />);

        const { clickSave } = changeInputValue("");
        await clickSave();

        expect(unsetUserFunctionLabelSpy).toHaveBeenCalled();
        expect(setUserFunctionLabelSpy).not.toHaveBeenCalled();
    });

    it("if current roomstate changes we should update the label accordingly", async () => {
        render(<UserFunctionLabelRoomSettingsTab room={room} />);

        expect(screen.queryByLabelText(inputLabel)).toBeInTheDocument();

        await act(async () => {
            room.currentState.emit(
                RoomStateEvent.Events,
                TestUtils.mkEvent({
                    type: EventType.BwiUserFunctionLabels,
                    event: true,
                    content: {},
                    room: room.roomId,
                    user: member.userId,
                }),
                undefined!,
                undefined!,
            );
        });

        expect(screen.queryByLabelText(inputLabel)).not.toBeInTheDocument();
    });

    it("roomstate emitting any other event shouldnt do anything", async () => {
        render(<UserFunctionLabelRoomSettingsTab room={room} />);

        expect(screen.getByDisplayValue(mockLabel)).toBeInTheDocument();

        await act(async () => {
            room.currentState.emit(
                RoomStateEvent.Events,
                TestUtils.mkEvent({
                    type: EventType.Reaction,
                    event: true,
                    content: {},
                    room: room.roomId,
                    user: member.userId,
                }),
                undefined!,
                undefined!,
            );
        });

        expect(screen.getByDisplayValue(mockLabel)).toBeInTheDocument();
    });

    it("should map escape / enter keys to abort / onSave", async () => {
        render(<UserFunctionLabelRoomSettingsTab room={room} />);

        let changeRef = changeInputValue("changed label");

        // When: 'Esc' pressed
        await act(async () => {
            fireEvent.keyDown(changeRef.inputElement, { key: "Escape" });
        });

        // Expect: The old value should be restored
        expect(screen.queryByDisplayValue(mockLabel)).toBeInTheDocument();
        expect(setUserFunctionLabelSpy).not.toHaveBeenCalled();

        const newLabel = "newlabeltest";
        changeRef = changeInputValue(newLabel);

        // When: 'Enter' pressed
        await act(async () => {
            fireEvent.keyDown(changeRef.inputElement, { key: "Enter" });
        });

        // Expect: The new value should be saved
        expect(screen.queryByDisplayValue(newLabel)).toBeInTheDocument();
        expect(setUserFunctionLabelSpy).toHaveBeenCalled();
    });
});
