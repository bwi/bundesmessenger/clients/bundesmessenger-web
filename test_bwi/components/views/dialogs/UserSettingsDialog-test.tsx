// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";
import { render, screen } from "jest-matrix-react";
import { MatrixClient } from "matrix-js-sdk/src/matrix";

import SdkConfig from "../../../../src/SdkConfig";
import UserSettingsDialog from "../../../../src/components/views/dialogs/UserSettingsDialog";
import * as TestUtils from "../../../../test/test-utils";
import { getBaseClient, getClientUserMocks } from "../../../mock/MockClient";
import { SdkContextClass } from "../../../../src/contexts/SDKContext";

// HINT: The errors in this test 'Can't call setState on a component that is not yet mounted' comes from GeneralUserSettingsTab.
// There is a bug, where setState is called by the ctor

describe("SdkConfig: show_labs_settings", () => {
    let sdkContext: SdkContextClass;
    beforeAll(() => {
        TestUtils.mockPlatformPeg();

        const client = getBaseClient({
            ...getClientUserMocks(),
            async waitForClientWellKnown() {
                return {};
            },
            getCapabilities() {
                return {};
            },
        }) as MatrixClient;

        sdkContext = new SdkContextClass();
        sdkContext.client = client;

        SdkConfig.put({
            brand: "Test",
            validated_server_config: {} as any,
        });
    });

    afterAll(() => {
        jest.clearAllMocks();
        jest.resetModules();
    });

    it("should render labs if flag is enabled", async () => {
        SdkConfig.add({
            show_labs_settings: true,
        });

        render(<UserSettingsDialog onFinished={jest.fn()} sdkContext={sdkContext} />);

        expect(screen.queryByText("Labs")).toBeInTheDocument();
    });

    it("should not render labs tab if flag is disabled", async () => {
        SdkConfig.add({
            show_labs_settings: false,
        });

        render(<UserSettingsDialog onFinished={jest.fn()} sdkContext={sdkContext} />);

        expect(screen.queryByText("Labs")).not.toBeInTheDocument();
    });
});
