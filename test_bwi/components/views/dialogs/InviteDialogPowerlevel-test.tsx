// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { act, fireEvent, render, screen } from "jest-matrix-react";
import { EventType, MatrixEvent, MatrixClient } from "matrix-js-sdk/src/matrix";
import React from "react";

import { MatrixClientPeg } from "../../../../src/MatrixClientPeg";
import {
    InviteDialogPowerLevel,
    InviteDialogPowerLevelRef,
} from "../../../../src/bwi/components/views/dialogs/InviteDialogPowerLevel";
import MatrixClientContext from "../../../../src/contexts/MatrixClientContext";
import * as TestUtils from "../../../../test/test-utils";
import { getBaseClient, getClientUserMocks } from "../../../mock/MockClient";
import { generateRoomId, mockRoomWithEvents } from "../../../mock/MockRoom";

describe("InviteDialogPowerlevel", () => {
    const WrappedInviteDialogPowerLevel = TestUtils.wrapInMatrixClientContext(InviteDialogPowerLevel);

    const client = getBaseClient({
        ...getClientUserMocks(),
        getRoom: jest.fn(),
        setPowerLevel: jest.fn(),
    }) as MatrixClient;
    const room = mockRoomWithEvents({});

    const goButton: JSX.Element = <button className="gobutton">gogo</button>;
    const setWarningMock = jest.fn();
    const getWarningMock = jest.fn();

    beforeEach(() => {
        (client.getRoom as unknown as jest.SpyInstance).mockReturnValue(room);
    });

    function assertValue(value: string): void {
        const powerSelector = screen.getByLabelText<HTMLSelectElement>("Assign Role");
        expect(powerSelector.value).toEqual(value);
    }

    it("should set powerlevel 0 as default while there is no room", async () => {
        (client.getRoom as unknown as jest.SpyInstance).mockReturnValue(null);

        render(<WrappedInviteDialogPowerLevel roomId={room.roomId} goButton={goButton} />);

        assertValue("0");
    });

    it("should set powerlevel 0 as default if room does not provide the state event", async () => {
        render(<WrappedInviteDialogPowerLevel roomId={room.roomId} goButton={goButton} />);

        assertValue("0");
    });

    it("should set the rooms default powerlevel if room provides the state event", async () => {
        jest.spyOn(room.currentState, "getStateEvents").mockReturnValue({
            getContent: () => ({ users_default: 100 }),
        } as MatrixEvent);

        await act(async () => {
            render(<WrappedInviteDialogPowerLevel roomId={room.roomId} goButton={goButton} />);
        });

        assertValue("100");
    });

    it("should set a warning if powerlevel 100 is choosen from powerlevel dropdown", async () => {
        getWarningMock.mockClear();
        setWarningMock.mockClear();

        render(
            <WrappedInviteDialogPowerLevel
                roomId={room.roomId}
                goButton={goButton}
                setWarning={setWarningMock}
                getWarning={getWarningMock}
            />,
        );

        act(() => {
            fireEvent.change(screen.getByLabelText("Assign Role"), {
                target: {
                    value: "100",
                },
            });
        });

        expect(getWarningMock).toHaveBeenCalled();
        expect(setWarningMock).toHaveBeenCalledWith(
            "You won't be able to revoke the granted administrator permissions!",
        );
    });

    it("should clear the warning if powerlevel below 100 is choosen from powerlevel dropdown", async () => {
        getWarningMock.mockClear();
        setWarningMock.mockClear();

        getWarningMock.mockReturnValue("You won't be able to revoke the granted administrator permissions!");
        render(
            <WrappedInviteDialogPowerLevel
                roomId={room.roomId}
                goButton={goButton}
                setWarning={setWarningMock}
                getWarning={getWarningMock}
            />,
        );

        act(() => {
            fireEvent.change(screen.getByLabelText("Assign Role"), {
                target: {
                    value: "50",
                },
            });
        });

        expect(getWarningMock).toHaveBeenCalled();
        expect(setWarningMock).toHaveBeenCalledWith(null);

        getWarningMock.mockRestore();
    });

    it("should not adjust the powerlevel if the room is null", async () => {
        const ref = React.createRef<InviteDialogPowerLevelRef>();
        (client.getRoom as unknown as jest.SpyInstance).mockReturnValue(null);

        await act(async () => {
            render(
                <MatrixClientContext.Provider value={MatrixClientPeg.safeGet()}>
                    <InviteDialogPowerLevel ref={ref} roomId="unknown_room" goButton={goButton} />
                </MatrixClientContext.Provider>,
            );
        });

        expect(await ref.current?.adjustPowerLevels([])).toBe(false);
    });

    it("should not adjust the powerlevel if power level event is missing", async () => {
        const testRoom = mockRoomWithEvents({});
        (client.getRoom as unknown as jest.SpyInstance).mockReturnValue(testRoom);

        const ref = React.createRef<InviteDialogPowerLevelRef>();

        await act(async () => {
            render(
                <MatrixClientContext.Provider value={MatrixClientPeg.safeGet()}>
                    <InviteDialogPowerLevel ref={ref} roomId={testRoom.roomId} goButton={goButton} />
                </MatrixClientContext.Provider>,
            );
        });

        expect(await ref.current?.adjustPowerLevels([])).toBe(false);
    });

    it("should adjust the powerlevel for users", async () => {
        const roomId = generateRoomId();
        const testRoom = mockRoomWithEvents({
            events: [
                TestUtils.mkEvent({
                    event: true,
                    type: EventType.RoomPowerLevels,
                    user: "",
                    content: {
                        users: {
                            a: 10,
                            b: 20,
                        },
                    },
                    room: roomId,
                    skey: "",
                }),
            ],
            partials: {
                roomId,
            },
        });
        (client.getRoom as unknown as jest.SpyInstance).mockReturnValue(testRoom);

        const ref = React.createRef<InviteDialogPowerLevelRef>();

        await act(async () => {
            render(
                <MatrixClientContext.Provider value={MatrixClientPeg.safeGet()}>
                    <InviteDialogPowerLevel ref={ref} roomId={testRoom.roomId} goButton={goButton} />
                </MatrixClientContext.Provider>,
            );
        });

        await act(async () => {
            fireEvent.change(screen.getByLabelText("Assign Role"), { target: { value: 50 } });
        });

        const setPowerLevelSpy = jest.spyOn(client, "setPowerLevel").mockResolvedValue(undefined!);

        expect(await ref.current?.adjustPowerLevels(["a", "b"])).toBe(true);

        expect(setPowerLevelSpy).toHaveBeenCalledWith(roomId, ["a", "b"], 50);
    });
});
