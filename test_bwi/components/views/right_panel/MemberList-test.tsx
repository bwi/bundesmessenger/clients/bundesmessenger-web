// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
import { act, cleanup, render, RenderResult } from "jest-matrix-react";
import React from "react";
import { Room } from "matrix-js-sdk/src/matrix";

import MemberList from "../../../../src/components/views/rooms/MemberList";
import { getMockClientWithEventEmitter, unmockClientPeg } from "../../../../test/test-utils/client";
import { generateRoomId, mockRoomWithEvents } from "../../../mock/MockRoom";
import { mockClientMethodsUser } from "../../../../test/test-utils";
import { SDKContext, SdkContextClass } from "../../../../src/contexts/SDKContext";

describe("RightPanel > MemberList", () => {
    let room: Room = null!;
    const client = getMockClientWithEventEmitter({
        ...mockClientMethodsUser(),
        getRoom: () => room,
    });

    const roomId = generateRoomId();
    room = mockRoomWithEvents({
        opts: {},
        partials: {
            roomId,
            getMyMembership: () => "join",
        },
    });

    jest.spyOn(client, "getRoom").mockReturnValue(room);

    afterEach(cleanup);

    afterAll(() => {
        unmockClientPeg();
    });

    async function renderComponent(): Promise<RenderResult> {
        const context = SdkContextClass.instance;
        let result: RenderResult;
        await act(async () => {
            result = render(
                <SDKContext.Provider value={context}>
                    <MemberList onClose={() => {}} onSearchQueryChanged={() => {}} roomId={roomId} searchQuery="" />
                </SDKContext.Provider>,
            );
        });
        return result!;
    }

    it("should have a reversed layout", async () => {
        const { asFragment } = await renderComponent();
        expect(asFragment()).toMatchSnapshot();
    });
});
