// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { cleanup, fireEvent, render, screen } from "jest-matrix-react";
import React from "react";
import { act } from "react-dom/test-utils"; // eslint-disable-line deprecate/import

import { BwiFeature } from "../../../../src/bwi/settings/BwiFeature";
import PasswordLogin from "../../../../src/components/views/auth/PasswordLogin";
import Modal from "../../../../src/Modal";
import { ValidatedServerConfig } from "../../../../src/utils/ValidatedServerConfig";
import { getMockClientWithEventEmitter } from "../../../../test/test-utils";
import { mockFeatureFlag } from "../../../mock/MockSettingsStore";

const serverConfig: ValidatedServerConfig = {
    hsName: "",
    hsNameIsDifferent: false,
    hsUrl: "",
    isDefault: true,
    isUrl: "",
    isNameResolvable: false,
    warning: "",
};

describe("PasswordLogin", () => {
    beforeAll(() => {
        getMockClientWithEventEmitter({});
    });

    afterEach(cleanup);

    describe("BwiFeature.passwordResetHint", () => {
        it("should not show password hint by default", () => {
            render(
                <PasswordLogin
                    serverConfig={serverConfig}
                    onSubmit={() => {}}
                    phoneCountry="+49"
                    phoneNumber="123419412"
                    username="alice@app.com"
                />,
            );

            expect(screen.queryByText("Forgot password?")).toBeNull();
        });

        it("should show password hint with feature flag enabled and open forgot password dialog", () => {
            const featureFlag = mockFeatureFlag(BwiFeature.PasswordResetHint, true);

            render(
                <PasswordLogin
                    serverConfig={serverConfig}
                    onSubmit={() => {}}
                    phoneCountry="+49"
                    phoneNumber="123419412"
                    username="alice@app.com"
                />,
            );

            const forgotPaswordEl = screen.getByText("Forgot password?");
            expect(forgotPaswordEl).toBeInTheDocument();

            const modalSpy = jest.spyOn(Modal, "createDialog");

            act(() => {
                fireEvent.click(forgotPaswordEl);
            });

            expect(modalSpy).toHaveBeenCalled();

            featureFlag.mockRestore();
        });
    });
});
