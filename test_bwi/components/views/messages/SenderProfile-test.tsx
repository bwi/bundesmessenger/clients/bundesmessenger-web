// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";
import { EventType } from "matrix-js-sdk/src/matrix";
import { render, screen } from "jest-matrix-react";

import * as TestUtils from "../../../../test/test-utils";
import * as UserFunctionLabelHelper from "../../../../src/bwi/helper/events/UserFunctionLabel";
import { BwiFeature } from "../../../../src/bwi/settings/BwiFeature";
import _SenderProfile from "../../../../src/components/views/messages/SenderProfile";
import RoomContext from "../../../../src/contexts/RoomContext";
import { getRoomContext } from "../../../../test/test-utils";
import { generateRoomId } from "../../../mock/MockRoom";
import { mockFeatureFlag } from "../../../mock/MockSettingsStore";

jest.mock("../../../../src/stores/room-list/RoomListStore", () => ({
    instance: {
        on: () => true,
    },
}));

jest.mock("../../../../src/stores/spaces/SpaceStore", () => ({
    spacesEnabled: false,
}));

describe("SenderProfile", () => {
    const mockRoom = TestUtils.mkStubRoom(generateRoomId(), "1231241241", undefined);
    const mockMember = {
        userId: "test@localhost",
    };

    mockRoom.getMember = jest.fn().mockReturnValue(mockMember);

    const mockLabel = "testlabel";
    const event = TestUtils.mkEvent({
        type: EventType.BwiUserFunctionLabels,
        event: true,
        content: {
            [mockMember.userId]: mockLabel,
        },
        room: mockRoom.roomId,
        user: mockMember.userId,
    });
    jest.spyOn(event, "getTs").mockReturnValue(Date.now() - 3600);
    jest.spyOn(mockRoom.currentState, "getStateEvents").mockReturnValue(event);

    const msg = TestUtils.mkEvent({
        type: "m.room.message",
        event: true,
        room: mockRoom.roomId,
        user: mockMember.userId,
        content: {
            msgtype: "m.text",
            body: "test",
        },
    });
    jest.spyOn(msg, "getTs").mockReturnValue(Date.now());

    jest.spyOn(UserFunctionLabelHelper, "getUserFunctionLabelEvent").mockReturnValue(event.getContent());
    const SenderProfile = TestUtils.wrapInMatrixClientContext(_SenderProfile);

    beforeEach(() => {
        TestUtils.stubClient();
    });

    afterAll(() => {
        jest.clearAllMocks();
        jest.restoreAllMocks();
        jest.resetModules();
    });

    it("should not show users function label by default even if its set", () => {
        render(
            <RoomContext.Provider value={getRoomContext(mockRoom, {})}>
                <SenderProfile mxEvent={msg} />
            </RoomContext.Provider>,
        );
        expect(screen.queryByText(mockMember.userId)).toBeInTheDocument();
    });

    it("should show users function label if set and feature flag enabled", () => {
        const ff = mockFeatureFlag(BwiFeature.UserFunctionLabels, true);

        render(
            <RoomContext.Provider value={getRoomContext(mockRoom, {})}>
                <SenderProfile mxEvent={msg} />
            </RoomContext.Provider>,
        );
        expect(screen.queryByText(`${mockMember.userId} [${mockLabel}]`)).toBeInTheDocument();

        ff.mockRestore();
    });
});
