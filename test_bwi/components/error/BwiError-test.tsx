// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { render, screen, cleanup } from "jest-matrix-react";
import React from "react";

import BwiError from "../../../src/bwi/components/error/BwiError";

// eslint-disable-next-line max-len
const MESSAGE_TRANSLATION_KEY =
    "%(appName)s was unable to load. Please try again. If %(appName)s is still not running, contact your local User HelpDesk.";
const translations: Record<string, string> = {
    "Failed to start": "Unable to load title",
    // eslint-disable-next-line max-len
    ["%(appName)s was unable to load. Please try again. If %(appName)s is still not running, contact your local User HelpDesk."]:
        "Unable to load message",
};

jest.mock("../../../src/vector/init.tsx", () => ({
    loadLanguage: () => Promise.resolve(),
    _t: (key: string) => translations[key] || key,
}));

describe("BwiError", () => {
    function assertTextNodes(...texts: string[]): Promise<Awaited<void>[]> {
        return Promise.all(
            texts.map(async (text) => {
                const el = await screen.findByText(text);
                expect(el).toBeTruthy();
            }),
        );
    }

    afterAll(cleanup);

    // if the configuration is corrupted element is not able to load the translations.
    // We try to recover from this
    it("should recover translations after a corrupt configuration", async () => {
        const title = "missing translation|Some random title";
        const messages = ["missing translation|Some random text"];

        render(<BwiError title={title} messages={messages} />);

        // ignore title and expect fallback title
        await assertTextNodes(translations["Failed to start"]);

        // ignore messages and expect fallback message
        await assertTextNodes(translations[MESSAGE_TRANSLATION_KEY]);
    });

    it("should use the passed title and messages if no fallback is necessary", async () => {
        const title = "Some random title";
        const messages = ["Some random text"];

        render(<BwiError title={title} messages={messages} />);

        await assertTextNodes(title);
        await assertTextNodes(...messages);
    });
});
