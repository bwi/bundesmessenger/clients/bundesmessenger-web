// Copyright 2024 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";
import { render, waitFor } from "jest-matrix-react";
import { MatrixClient } from "matrix-js-sdk/src/matrix";

import { PersonalNotesRoomManager } from "../../src/bwi/managers/PersonalNotesRoomManager";
import { PersonalNotesRoomCreator } from "../../src/bwi/components/PersonalNotesRoomCreator";
import * as TestUtils from "../../test/test-utils";
import { MatrixClientContextProvider } from "../../src/components/structures/MatrixClientContextProvider";

describe("<PersonalNotesRoomCreator />", () => {
    let managePersonalNotesSpy: jest.SpyInstance<Promise<void>, [], any>;

    let cli: MatrixClient;
    function HelperComp(): React.JSX.Element {
        return (
            <MatrixClientContextProvider client={cli}>
                <PersonalNotesRoomCreator />
            </MatrixClientContextProvider>
        );
    }

    beforeEach(() => {
        const crypto = {
            getUserVerificationStatus: async () => {
                return { isCrossSigningVerified: () => true, isVerified: () => true };
            },
            getActiveSessionBackupVersion: () => "not null",
        };
        cli = TestUtils.getMockClientWithEventEmitter({
            getCrypto: () => crypto,
            getUserId: () => "userId",
            credentials: {
                userId: "userId",
            },
        });
        managePersonalNotesSpy = jest.spyOn(PersonalNotesRoomManager.instance, "managePersonalNotes");
    });
    afterEach(() => {
        jest.resetAllMocks();
    });

    it("should call PersonalNotesRoomManager.instance.managePersonalNotes() only once", async () => {
        const { rerender } = render(<HelperComp />);
        rerender(<HelperComp />);
        rerender(<HelperComp />);
        rerender(<HelperComp />);
        rerender(<HelperComp />);

        await waitFor(() => expect(managePersonalNotesSpy).toHaveBeenCalledTimes(1));
    });
});
