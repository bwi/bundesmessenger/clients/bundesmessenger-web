// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";
import { render, cleanup } from "jest-matrix-react";

import SdkConfig from "../../../src/SdkConfig";
import BwiAuthPage from "../../../src/bwi/components/auth/BwiAuthPage";

describe("BwiAuthPage", () => {
    beforeEach(() => {
        jest.spyOn(global.Math, "random").mockReturnValue(1);
    });
    afterEach(() => {
        cleanup();
        jest.spyOn(global.Math, "random").mockRestore();
    });

    function assertBackgroundStyle(img: string): void {
        const { container } = render(<BwiAuthPage />);
        const page = container.firstChild as HTMLDivElement;
        expect(page).toHaveStyle({ background: `center/cover fixed url(${img})` });
    }

    it("should display the configured background image", () => {
        const img = "my/welcome/image";
        SdkConfig.put({
            branding: {
                welcome_background_url: img,
            },
        } as any);

        assertBackgroundStyle(img);
    });

    it("should pick a random image if config provides an array", () => {
        const images = ["my/welcome/image", "my/second/welcome/image"];
        SdkConfig.put({
            branding: {
                welcome_background_url: images,
            },
        } as any);

        assertBackgroundStyle(images[1]);
    });

    it("should fallback to the default background image if no config is provided", () => {
        const defaultImg = "themes/element/img/backgrounds/lake.jpg";
        SdkConfig.put({
            branding: {
                welcome_background_url: undefined,
            },
        } as any);

        assertBackgroundStyle(defaultImg);
    });
});
