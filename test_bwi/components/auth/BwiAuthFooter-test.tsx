// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";
import { render, cleanup, screen, act } from "jest-matrix-react";
import fetchMockJest from "fetch-mock-jest";

import SdkConfig from "../../../src/SdkConfig";
import { setCurrentHomeServer } from "../../../src/bwi/functions";
import BwiAuthFooter from "../../../src/bwi/components/auth/BwiAuthFooter";

describe("BwiAuthFooter", () => {
    afterEach(cleanup);

    it("should render all configured links", () => {
        const links = [
            {
                text: "foo",
                url: "foo",
            },
            {
                text: "bar",
                url: "bar",
            },
        ];
        SdkConfig.put({
            branding: {
                auth_footer_links: links,
                home_byline: "",
                home_title: "",
            },
        } as any);

        const { container } = render(<BwiAuthFooter />);

        const aTags = Array.from(container.getElementsByTagName("a"));
        aTags.forEach((anchor, index) => {
            const { href, target, rel, text } = anchor;
            expect(text).toBe(links[index].text);
            expect(href).toMatch(`${links[index].url}`);
            expect(target).toBe("_blank");
            expect(rel).toBe("noreferrer noopener");
        });
    });
    it("should fallback to the default links", () => {
        SdkConfig.put({
            branding: {
                auth_footer_links: undefined,
                home_byline: "",
                home_title: "",
            },
        } as any);

        const { container } = render(<BwiAuthFooter />);
        const aTags = container.getElementsByTagName("a");
        expect(aTags).toHaveLength(3);
    });
    it("should load the data privacy url from the wellknown", async () => {
        fetchMockJest.getOnce("https://hs.de/.well-known/matrix/client", {
            ["de.bwi"]: {
                data_privacy_url: "https://privacy.com",
            },
        });
        setCurrentHomeServer("https://hs.de");

        await act(async () => {
            render(<BwiAuthFooter />);
        });

        expect(screen.queryByRole("link", { name: "Privacy Policy" })).toHaveAttribute("href", "https://privacy.com");

        cleanup();
        setCurrentHomeServer("");
    });
});
