// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import fetchMock from "fetch-mock-jest";

import BwiPlatform from "../../src/bwi/platform/BwiPlatform";

fetchMock.config.overwriteRoutes = true;

describe("BwiPlatform", () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    it("returns human readable name", () => {
        const platform = new BwiPlatform();
        expect(platform.getHumanReadableName()).toEqual("BWI Platform");
    });

    describe("app version", () => {
        const envVersion = process.env.VERSION;
        const prodVersion = "1.10.13";

        afterAll(() => {
            process.env.VERSION = envVersion;
        });

        it("getAppVersion empty string unsuccessful request", async () => {
            process.env.VERSION = prodVersion;
            fetchMock.getOnce("/version", { throws: { status: "error" } });
            const platform = new BwiPlatform();

            const version = await platform.getAppVersion();
            expect(version).toEqual("");
        });

        it("getAppVersion successful request -> version", async () => {
            process.env.VERSION = prodVersion;
            fetchMock.getOnce("/version", prodVersion);
            const platform = new BwiPlatform();
            const version = await platform.getAppVersion();
            expect(version).toEqual(prodVersion);
        });
    });
});
