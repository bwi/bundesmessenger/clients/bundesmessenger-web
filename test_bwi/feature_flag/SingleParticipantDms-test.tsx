// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { act, cleanup, fireEvent, render, screen } from "jest-matrix-react";
import { EventType, Room, MatrixClient } from "matrix-js-sdk/src/matrix";
import React from "react";

import Modal from "../../src/Modal";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import InviteDialog from "../../src/components/views/dialogs/InviteDialog";
import { InviteKind } from "../../src/components/views/dialogs/InviteDialogTypes";
import _RoomTopic from "../../src/components/views/elements/RoomTopic";
import SettingsStore from "../../src/settings/SettingsStore";
import { UIFeature } from "../../src/settings/UIFeature";
import DMRoomMap from "../../src/utils/DMRoomMap";
import * as testUtils from "../../test/test-utils";
import { getBaseClient, getClientUserMocks } from "../mock/MockClient";
import { mockRoomWithEvents } from "../mock/MockRoom";
import { mockFeatureFlag } from "../mock/MockSettingsStore";
import { mockUser } from "../mock/MockUser";
import { SdkContextClass } from "../../src/contexts/SDKContext";

describe("BwiFeature.singleParticipantDms", () => {
    afterAll(() => {
        jest.clearAllMocks();
        jest.resetModules();
    });

    describe("InviteDialog", () => {
        beforeAll(() => {
            const client = getBaseClient({
                isRoomEncrypted: () => Promise.resolve(true),
                ...getClientUserMocks(mockUser),
                doesServerSupportUnstableFeature: () => false,
                getRoomDirectoryVisibility: () =>
                    Promise.resolve({
                        visibility: "public",
                    }),
                getRooms: () => [],
                getRoom: jest.fn(),
                searchUserDirectory: () =>
                    Promise.resolve({
                        limited: false,
                        results: [
                            {
                                user_id: "@alice:server.org",
                                display_name: "Alice",
                            },
                            {
                                user_id: "@bob:server.org",
                                display_name: "Bob",
                            },
                        ],
                    }),
                invite: () => Promise.resolve(),
            }) as MatrixClient;
            DMRoomMap.makeShared(client);
            SdkContextClass.instance.client = client;
        });

        afterEach(() => {
            jest.spyOn(SettingsStore, "getValue").mockReset();
        });

        afterAll(() => {
            testUtils.unmockClientPeg();
        });

        it("should be able to invite multiple people to a room by default", async () => {
            mockFeatureFlag(
                new Map<string, unknown>([
                    [BwiFeature.SingleParticipantDms, false],
                    [UIFeature.IdentityServer, false],
                ]),
            );

            await act(async () => {
                render(<InviteDialog kind={InviteKind.Dm} onFinished={() => {}} initialText="Alice" />);
            });

            await act(async () => {
                fireEvent.click(screen.getByText("Alice"));
            });

            await act(async () => {
                fireEvent.change(screen.getByTestId("invite-dialog-input"), { target: { value: "Bob" } });
            });

            await act(async () => {
                fireEvent.click(screen.getByText("Bob"));
            });

            // Expect: No error message
            expect(screen.queryByText("Only one user can be selected")).not.toBeInTheDocument();
            // Expect: The 'Go' button is enabled
            expect(screen.queryByText("Go")).toBeEnabled();

            // Wait to stabilize the component. Otherwise it will throw 'Can't perform state update on unmounted component'.
            // This is due to the debounce of the updateSuggestions
            await new Promise((done) => setTimeout(done, 150));
        });

        it("should only allow 1 person to be added to a room if feature flag is enabled", async () => {
            mockFeatureFlag(
                new Map<string, unknown>([
                    [BwiFeature.SingleParticipantDms, true],
                    [UIFeature.IdentityServer, false],
                ]),
            );

            await act(async () => {
                render(<InviteDialog kind={InviteKind.Dm} onFinished={() => {}} initialText="Alice" />);
            });

            await act(async () => {
                fireEvent.click(screen.getByText("Alice"));
            });

            await act(async () => {
                fireEvent.change(screen.getByTestId("invite-dialog-input"), { target: { value: "Bob" } });
            });

            await act(async () => {
                fireEvent.click(screen.getByText("Bob"));
            });

            // Expect: To show an error warning
            expect(screen.queryByText("Only one user can be invited")).toBeInTheDocument();
            // Expect: The 'Go' button is disabled
            expect(screen.queryByText("Go")).toHaveAttribute("disabled", "");

            // Wait to stabilize the component. Otherwise it will throw 'Can't perform state update on unmounted component'.
            // This is due to the debounce of the updateSuggestions
            await new Promise((done) => setTimeout(done, 150));
        });
    });

    describe("RoomTopic", () => {
        const roomId = "!room:server.org";
        let room: Room;
        const modalSpy = jest.spyOn(Modal, "createDialog");
        beforeAll(() => {
            const client = getBaseClient({
                ...getClientUserMocks(mockUser),
                searchUserDirectory: () =>
                    Promise.resolve({
                        limited: false,
                        results: [
                            {
                                user_id: "@alice:server.org",
                                display_name: "Alice",
                            },
                            {
                                user_id: "@bob:server.org",
                                display_name: "Bob",
                            },
                        ],
                    }),
            }) as MatrixClient;

            DMRoomMap.makeShared(client);

            room = mockRoomWithEvents({
                partials: {
                    roomId: roomId,
                    name: "testroom",
                    canInvite: () => false,
                    getJoinRule: () => "private",
                },
                events: [
                    testUtils.mkEvent({
                        event: true,
                        type: EventType.RoomTopic,
                        content: {
                            topic: "My Secret Room",
                        },
                        user: mockUser.userId,
                        room: roomId,
                    }),
                ],
            });

            jest.spyOn(room.currentState, "maySendStateEvent").mockReturnValue(true);
            jest.spyOn(DMRoomMap.prototype, "getUserIdForRoomId").mockReturnValue(mockUser.userId);
        });

        const RoomTopic = testUtils.wrapInMatrixClientContext(_RoomTopic);

        beforeEach(() => {
            jest.spyOn(Modal, "createDialog").mockClear();
        });

        afterEach(() => {
            cleanup();
            document.getElementById("mx_Dialog_Container")?.remove();
        });

        it("should show edit topic when user is admin", async () => {
            render(<RoomTopic room={room} />);

            await act(async () => {
                fireEvent.click(screen.getByText("My Secret Room"));
            });

            // wait for the dispatcher to send the action 'Action.ShowRoomTopic'
            await new Promise((done) => setTimeout(done, 0));

            // wait for Dialog to be rendered, which takes ages!
            await screen.findByText("testroom");

            expect(modalSpy).toHaveBeenCalled();
            expect(screen.queryByText("Edit topic")).toBeInTheDocument();
        });

        it("should not show edit topic when user is admin and singleparticipant dms is enabled", async () => {
            const ff = mockFeatureFlag(BwiFeature.SingleParticipantDms, true);

            render(<RoomTopic room={room} />);

            await act(async () => {
                fireEvent.click(screen.getByText("My Secret Room"));
            });

            // wait for the dispatcher to send the action 'Action.ShowRoomTopic'
            await new Promise((done) => setTimeout(done, 0));

            // wait for Dialog to be rendered, which takes ages!
            await screen.findByText("My Secret Room");

            expect(modalSpy).toHaveBeenCalled();
            expect(screen.queryByText("Edit topic")).not.toBeInTheDocument();

            ff.mockRestore();
        });
    });
});
