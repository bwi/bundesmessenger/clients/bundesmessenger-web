// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { render, screen } from "jest-matrix-react";
import React from "react";

import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import PowerSelector from "../../src/components/views/elements/PowerSelector";
import { mockFeatureFlag } from "../mock/MockSettingsStore";

describe("BwiFeature.DisableCustomPowerlevel", () => {
    it("should render custom level by default", () => {
        render(<PowerSelector />);
        expect(screen.queryByRole("option", { name: "Custom level" })).toBeInTheDocument();
    });

    it("should not render custom level when feature flag is enabled", () => {
        const ff = mockFeatureFlag(BwiFeature.DisableCustomPowerlevel, true);
        render(<PowerSelector />);
        expect(screen.queryByRole("option", { name: "Custom level" })).not.toBeInTheDocument();
        ff.mockRestore();
    });
});
