// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";
import { act, fireEvent, render, screen } from "jest-matrix-react";
import { JoinRule, Room, Visibility, MatrixClient } from "matrix-js-sdk/src/matrix";

import { adjustRoomCreateOpts } from "../../src/bwi/helper/Room";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import AliasSettings from "../../src/components/views/room_settings/AliasSettings";
import MatrixClientContext from "../../src/contexts/MatrixClientContext";
import { IOpts as RoomCreateOpts } from "../../src/createRoom";
import { getBaseClient, getClientUserMocks } from "../mock/MockClient";
import { mockRoomWithEvents } from "../mock/MockRoom";
import { mockFeatureFlag } from "../mock/MockSettingsStore";

describe("BwiFeature.roomAutoPublish", () => {
    type JoinParam = {
        join_rule: JoinRule;
    };

    afterAll(() => {
        jest.clearAllMocks();
        jest.resetModules();
    });

    describe("AliasSettings", () => {
        let room: Room | null = null;
        const domain = "local.server.org";
        const client = getBaseClient({
            doesServerSupportUnstableFeature: () => Promise.resolve(false),
            getRoomDirectoryVisibility: () => Promise.resolve({ visibility: Visibility.Private }),
            getLocalAliases: () => Promise.resolve([]),
            getRoom: () => room,
            ...getClientUserMocks(),
            getDomain: () => domain,
            sendStateEvent: jest.fn(),
            setRoomDirectoryVisibility: jest.fn(),
        }) as MatrixClient;
        room = mockRoomWithEvents({});

        afterEach(() => {
            jest.clearAllMocks();
        });

        const switchLabel = `Publish this room to the public in ${domain}'s room directory?`;

        async function togglePublishSetting(): Promise<void> {
            await act(async () => {
                fireEvent.click(screen.getByRole("switch", { name: switchLabel }));
            });
        }

        async function renderComponent(props?: Partial<ConstructorParameters<typeof AliasSettings>[0]>): Promise<void> {
            await act(async () => {
                render(
                    <MatrixClientContext.Provider value={client}>
                        <AliasSettings
                            roomId={room!.roomId}
                            canSetCanonicalAlias={false}
                            canSetAliases={false}
                            {...props}
                        />
                    </MatrixClientContext.Provider>,
                );
            });
        }

        it("should render more than the public/private switch by default", async () => {
            await renderComponent();
            expect(screen.queryByText("Published Addresses")).toBeInTheDocument();
        });

        it("should render more than the public/private switch if flag is disabled", async () => {
            const ff = mockFeatureFlag(BwiFeature.RoomAutoPublish, false);
            await renderComponent();
            expect(screen.queryByText("Published Addresses")).toBeInTheDocument();

            ff.mockRestore();
        });

        it("should render only the public/private switch if flag is enabled", async () => {
            const ff = mockFeatureFlag(BwiFeature.RoomAutoPublish, true);
            await renderComponent();

            expect(screen.queryByText(switchLabel)).toBeInTheDocument();
            expect(screen.queryByText("Published Addresses")).not.toBeInTheDocument();

            ff.mockRestore();
        });

        it("should set the room directory visibility & m.room.join_rules accordingly", async () => {
            const ff = mockFeatureFlag(BwiFeature.RoomAutoPublish, true);

            const setRoomDirectoryVisibilityMock = (
                client.setRoomDirectoryVisibility as unknown as jest.SpyInstance
            ).mockResolvedValue(true);
            const sendStateEventMock = (client.sendStateEvent as unknown as jest.SpyInstance).mockResolvedValue(
                {} as any,
            );

            await renderComponent({ canSetCanonicalAlias: true });

            // set public
            await togglePublishSetting();
            const firstCall = sendStateEventMock.mock.calls[0];
            const firstCallParam = firstCall[2] as JoinParam;
            expect(firstCall[1]).toBe("m.room.join_rules");
            expect(firstCallParam.join_rule).toBe(JoinRule.Public);
            expect(setRoomDirectoryVisibilityMock.mock.calls[0][1]).toBe(Visibility.Public);

            // set private
            await togglePublishSetting();

            const secondCall = sendStateEventMock.mock.calls[1];
            const secondCallParam = secondCall[2] as JoinParam;
            expect(secondCall[1]).toBe("m.room.join_rules");
            expect(secondCallParam.join_rule).toBe(JoinRule.Invite);
            expect(setRoomDirectoryVisibilityMock.mock.calls[1][1]).toBe(Visibility.Private);

            ff.mockRestore();
        });
    });

    describe("RoomCreation", () => {
        function getOpts(): Partial<RoomCreateOpts> {
            return {
                createOpts: {
                    name: "test",
                },
            };
        }

        it("should not create alias by default", () => {
            const ff = mockFeatureFlag(BwiFeature.RoomAutoPublish, false);

            const opts = adjustRoomCreateOpts(getOpts());
            expect(opts.createOpts?.room_alias_name).toEqual(undefined);

            ff.mockRestore();
        });

        it("should create alias when feature flag is enabled", () => {
            const ff = mockFeatureFlag(BwiFeature.RoomAutoPublish, true);

            const aliasRegex = new RegExp(`${getOpts().createOpts!.name}\\d+`, "g");

            const opts = adjustRoomCreateOpts(getOpts());
            expect(opts.createOpts!.room_alias_name).toMatch(/^[a-zA-Z0-9]*$/);
            expect(opts.createOpts!.room_alias_name).toMatch(aliasRegex);

            ff.mockRestore();
        });

        it("should not create alias when feature flag is enabled but name is empty", () => {
            const ff = mockFeatureFlag(BwiFeature.RoomAutoPublish, true);

            const emptyName: RoomCreateOpts = {
                createOpts: {
                    name: "",
                },
            };

            const opts = adjustRoomCreateOpts(emptyName);
            expect(opts.createOpts!.room_alias_name).toEqual(undefined);

            ff.mockRestore();
        });

        it("should not create alias when feature flag is enabled but resulting alias is empty", () => {
            const ff = mockFeatureFlag(BwiFeature.RoomAutoPublish, true);

            const emptyName: RoomCreateOpts = {
                createOpts: {
                    name: "$$$$$",
                },
            };

            const opts = adjustRoomCreateOpts(emptyName);
            expect(opts.createOpts!.room_alias_name).toEqual(undefined);

            ff.mockRestore();
        });
    });
});
