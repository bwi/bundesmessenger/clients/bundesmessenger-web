// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
import React from "react";
import { render, screen } from "jest-matrix-react";

import LabsUserSettingsTab from "../../src/components/views/settings/tabs/user/LabsUserSettingsTab";
import SdkConfig from "../../src/SdkConfig";
import {
    getMockClientWithEventEmitter,
    unmockClientPeg,
    mockClientMethodsServer,
    mockClientMethodsUser,
} from "../../test/test-utils";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import { mockFeatureFlag } from "../mock/MockSettingsStore";

describe("BwiFeature.selectedLabSettings", () => {
    const sdkConfigSpy = jest.spyOn(SdkConfig, "get");

    const userId = "@alice:server.org";
    getMockClientWithEventEmitter({
        ...mockClientMethodsUser(userId),
        ...mockClientMethodsServer(),
    });

    describe("LabUserSettingsTab", () => {
        afterAll(() => {
            unmockClientPeg();
        });

        const defaultProps = {
            closeSettingsFn: jest.fn(),
        };
        const validLabFeatures = ["BwiFeature.federation", "feature_msc3531_hide_messages_pending_moderation"];
        const invalidLabFeatures = ["mockedimockmock", "feature_does_not_exist"];
        const getComponent = (): React.JSX.Element => <LabsUserSettingsTab {...defaultProps} />;

        const runTest = (featureSet: string[], numLabSections: number, hasExperimentalText: boolean): void => {
            sdkConfigSpy.mockImplementation((configName) => configName === "show_labs_settings");
            const flag = mockFeatureFlag(BwiFeature.SelectedLabSettings, featureSet);
            const { container } = render(getComponent());
            const labsSections = container.getElementsByClassName("mx_SettingsSubsection");
            const experimentalText = screen.queryByText("Feeling experimental?", { exact: false });
            expect(labsSections.length).toEqual(numLabSections);
            if (hasExperimentalText) expect(experimentalText).toBeDefined();
            else expect(experimentalText).toBeNull();
            flag.mockRestore();
        };

        it("two valid settings in selectedLabSettings, only show these two", () => {
            runTest(validLabFeatures, 2, false);
        });
        it("invalidSettings in selectedLabSettings, show nothing", () => {
            runTest(invalidLabFeatures, 0, false);
        });
        it("empty array, default, show all lab features", () => {
            // numLabSections same as in NV LabUserSettingsTab-test default case
            runTest([], 10, true);
        });
    });
});
