// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { act, fireEvent, render, screen, waitFor, within } from "jest-matrix-react";
import { renderHook } from "@testing-library/react-hooks";
import { MockInstance } from "jest-mock";
import {
    Direction,
    EventTimeline,
    EventType,
    IContent,
    JoinRule,
    KNOWN_SAFE_ROOM_VERSION,
    MatrixError,
    MatrixEvent,
    PendingEventOrdering,
    Preset,
    Room,
    RoomEvent,
    RoomMember,
    MatrixClient,
} from "matrix-js-sdk/src/matrix";
import React from "react";

import Modal from "../../src/Modal";
import { getRoomMemberDisplayname } from "../../src/TextForEvent";
import {
    BwiAnalyticsCategories,
    FederationAnalyticsActions,
    FederationAnalyticsEvents,
} from "../../src/bwi/analytics/BwiAnalyticsEvents";
import BwiAnalyticsManager from "../../src/bwi/analytics/BwiAnalyticsManager";
import { federationTracker } from "../../src/bwi/analytics/BwiAnalyticsSender";
import * as openFederatedAdminsNotAllowedModalModule from "../../src/bwi/components/federation/AdminsNotAllowedModal";
import { BwiDecoratedAvatar } from "../../src/bwi/components/federation/DecoratedAvatar";
import { FederationDecisionDialog } from "../../src/bwi/components/federation/FederationDecisionDialog";
import {
    BwiRoomFederationSettings,
    ConfirmDisableFederationDialog,
} from "../../src/bwi/components/federation/RoomFederationSettings";
import * as dms from "../../src/bwi/components/views/rooms/SingleParticipantDms";
import * as notes from "../../src/bwi/functions/PersonalNotes";
import * as federation from "../../src/bwi/functions/federation";
import { useRoomFederationState } from "../../src/bwi/hooks/useRoomFederationState";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import { IRoomState } from "../../src/components/structures/RoomView";
import DecoratedRoomAvatar from "../../src/components/views/avatars/DecoratedRoomAvatar";
import CreateRoomDialog from "../../src/components/views/dialogs/CreateRoomDialog";
import RovingInviteDialog from "../../src/components/views/dialogs/InviteDialog";
import { InviteKind } from "../../src/components/views/dialogs/InviteDialogTypes";
import RoomSummaryCard from "../../src/components/views/right_panel/RoomSummaryCard";
import { PowerLevelEditor } from "../../src/components/views/right_panel/UserInfo";
import { UnwrappedEventTile } from "../../src/components/views/rooms/EventTile";
import MemberTile from "../../src/components/views/rooms/MemberTile";
import GeneralRoomSettingsTab from "../../src/components/views/settings/tabs/room/GeneralRoomSettingsTab";
import RolesRoomSettingsTab from "../../src/components/views/settings/tabs/room/RolesRoomSettingsTab";
import MatrixClientContext from "../../src/contexts/MatrixClientContext";
import RoomContext from "../../src/contexts/RoomContext";
import { SdkContextClass } from "../../src/contexts/SDKContext";
import createRoom, { IOpts as RoomCreateOpts } from "../../src/createRoom";
import { _t } from "../../src/languageHandler";
import DMRoomMap from "../../src/utils/DMRoomMap";
import { Member } from "../../src/utils/direct-messages";
import { getRoomContext, MakeEventProps, mkEvent, mkRoomMember, mkStubRoom } from "../../test/test-utils";
import { getBaseClient, getClientUserMocks } from "../mock/MockClient";
import { generateRoomId, mockRoomWithEvents } from "../mock/MockRoom";
import { mockFeatureFlag } from "../mock/MockSettingsStore";
import { openBwiFederationOnboarding } from "../../src/bwi/components/notifications/BwiFederationOnboarding";
import * as wellknown from "../../src/bwi/functions/WellKnown";
import { getBwiOnboardingDialog } from "../../src/bwi/functions";

/**
 * DISCLAIMER:
 *
 * This file gets pretty large. But for performance reasons caused by (necessary) excessive mocking,
 * it's better (faster) to keep this one large test file instead of splitting.
 */

const HOME_SERVER = "server.org";
const FEDERATED_HOME_SERVER = "bobs.server.org";
const USER_ID = `@alice:${HOME_SERVER}`;
const FEDERATED_USER_ID = "@bob:bobs.server.org";

describe("BwiFeature.Federation", () => {
    const { client, makeRoom, makeDm } = mockTests();
    SdkContextClass.instance.client = client;

    beforeEach(() => {
        mockFeatureFlag(BwiFeature.Federation, true);
        DMRoomMap.makeShared(client);
    });

    describe("<DecoratedRoomAvatar />", () => {
        const renderAvatar = (room: Room): void => {
            render(<DecoratedRoomAvatar room={room} size="32px" />);
        };

        it("should remove public room icon", () => {
            // When room is public and feature is enabled
            const room = makeRoom({
                type: EventType.RoomJoinRules,
                content: {
                    join_rule: JoinRule.Public,
                },
                skey: "",
            });

            renderAvatar(room);
            // Expect the global icon not to be rendered
            expect(document.querySelector(".mx_DecoratedRoomAvatar_icon_globe")).not.toBeInTheDocument();
        });

        it("should display federation icon in a federated room", () => {
            // When room is public(optional), federated and the feature is enabled
            const room = makeRoom(
                {
                    type: EventType.RoomJoinRules,
                    content: {
                        join_rule: JoinRule.Public,
                    },
                    skey: "",
                },
                {
                    type: EventType.RoomServerAcl,
                    content: {
                        allow: ["*"],
                    },
                    skey: "",
                },
            );

            renderAvatar(room);

            // Expect the federation icon to be rendered
            expect(document.querySelector(".bwi_FederationIcon")).toBeInTheDocument();
        });

        it("should update federation icon on acl event", async () => {
            // When room is not federated
            const room = makeRoom(
                {
                    type: EventType.RoomServerAcl,
                    content: {
                        allow: ["my.server.org"],
                    },
                    skey: "",
                },
                {
                    type: EventType.RoomJoinRules,
                    content: {
                        join_rule: JoinRule.Public,
                    },
                    skey: "",
                },
            );

            renderAvatar(room);

            // Expect the federation icon not to be rendered
            expect(document.querySelector(".bwi_FederationIcon")).not.toBeInTheDocument();

            // When receiving a server acl event
            await act(async () => {
                emitRoomFederationEvent(room, true);
            });

            // Expect the federation icon to be rendered
            expect(document.querySelector(".bwi_FederationIcon")).toBeInTheDocument();
        });
    });

    describe("<RoomSummaryCard />", () => {
        function renderRoomSummaryCard(room: Room): void {
            render(
                <MatrixClientContext.Provider value={client}>
                    <RoomContext.Provider value={getRoomContext(room, {})}>
                        <RoomSummaryCard room={room} permalinkCreator={null!} />
                    </RoomContext.Provider>
                </MatrixClientContext.Provider>,
            );
        }

        describe("<FederationPill />", () => {
            it("should render the federation badge", () => {
                // When room is federated and feature is enabled
                const room = makeRoom(
                    {
                        type: EventType.RoomJoinRules,
                        content: {
                            join_rule: JoinRule.Public,
                        },
                        skey: "",
                    },
                    {
                        type: EventType.RoomServerAcl,
                        content: {
                            allow: ["*"],
                        },
                        skey: "",
                    },
                );

                renderRoomSummaryCard(room);

                // Expect the federation pill to be rendered
                expect(document.querySelector(".mx_RoomSummaryCard_badges .bwi_Federation_pill")).toBeInTheDocument();
            });

            it("should not render the federation badge if the room is not federated", () => {
                // When room is federated and feature is enabled
                const room = makeRoom(
                    {
                        type: EventType.RoomCreate,
                        content: {
                            creator: USER_ID,
                            room_version: KNOWN_SAFE_ROOM_VERSION,
                            ["m.federated"]: false,
                        },
                        skey: "",
                    },
                    {
                        type: EventType.RoomJoinRules,
                        content: {
                            join_rule: JoinRule.Public,
                        },
                        skey: "",
                    },
                );

                renderRoomSummaryCard(room);

                // Expect the federation pill to be rendered
                expect(
                    document.querySelector(".mx_RoomSummaryCard_badges .bwi_Federation_pill"),
                ).not.toBeInTheDocument();
            });
        });

        it("should decorate the room avatar with the federation icon", () => {
            // When room is federated and feature is enabled
            const room = makeRoom(
                {
                    type: EventType.RoomJoinRules,
                    content: {
                        join_rule: JoinRule.Public,
                    },
                    skey: "",
                },
                {
                    type: EventType.RoomServerAcl,
                    content: {
                        allow: ["*"],
                    },
                    skey: "",
                },
            );

            renderRoomSummaryCard(room);
            // Expect room avatar to be decorated with federation icon
            expect(document.querySelector(".mx_DecoratedRoomAvatar .bwi_FederationIcon")).toBeInTheDocument();
        });
    });

    describe("bwiIsRoomFederated()", () => {
        describe("m.federated > enabled", () => {
            it("should allow federation with m.federated = true and without acl event", () => {
                const result = federation.bwiIsRoomFederated(
                    makeRoom({
                        type: EventType.RoomCreate,
                        content: {
                            creator: USER_ID,
                            room_version: KNOWN_SAFE_ROOM_VERSION,
                            ["m.federated"]: true,
                        },
                        skey: "",
                    }),
                );

                expect(result).toBe(true);
            });
            it("should allow federation with m.federated = true and a valid acl event", () => {
                const result = federation.bwiIsRoomFederated(
                    makeRoom(
                        {
                            type: EventType.RoomCreate,
                            content: {
                                creator: USER_ID,
                                room_version: KNOWN_SAFE_ROOM_VERSION,
                                ["m.federated"]: true,
                            },
                            skey: "",
                        },
                        {
                            type: EventType.RoomServerAcl,
                            content: {
                                allow: ["*"],
                            },
                            skey: "",
                        },
                    ),
                );

                expect(result).toBe(true);
            });
            it("should allow federation with m.federated = undefined and a valid acl event", () => {
                const result = federation.bwiIsRoomFederated(
                    makeRoom({
                        type: EventType.RoomServerAcl,
                        content: {
                            allow: ["*"],
                        },
                        skey: "",
                    }),
                );

                expect(result).toBe(true);
            });
            it("should indicate federation if room is a DM and their home server is different from ours", () => {
                // create dm with bob from bobs_server.org
                const dm = makeDm(`@bob:bobs_server.org`);

                expect(federation.bwiIsRoomFederated(dm)).toBe(true);
            });
            it("should not indicate federation if room is a DM and their home server is the same as ours", () => {
                // create dm with bob
                const dm = makeDm(`@bob:${HOME_SERVER}`);

                expect(federation.bwiIsRoomFederated(dm)).toBe(false);
            });
        });
        describe("m.federated > disabled", () => {
            it("should not allow federation with m.federated = false", () => {
                const result = federation.bwiIsRoomFederated(
                    makeRoom({
                        type: EventType.RoomCreate,
                        content: {
                            creator: USER_ID,
                            room_version: KNOWN_SAFE_ROOM_VERSION,
                            ["m.federated"]: false,
                        },
                        skey: "",
                    }),
                );

                expect(result).toBe(false);
            });
            it("should not allow federation with m.federated = false and valid acl event", () => {
                const result = federation.bwiIsRoomFederated(
                    makeRoom(
                        {
                            type: EventType.RoomCreate,
                            content: {
                                creator: USER_ID,
                                room_version: KNOWN_SAFE_ROOM_VERSION,
                                ["m.federated"]: false,
                            },
                            skey: "",
                        },
                        {
                            type: EventType.RoomServerAcl,
                            content: {
                                allow: ["*"],
                            },
                            skey: "",
                        },
                    ),
                );

                expect(result).toBe(false);
            });
            it("should not allow federation with m.federated = undefined and invalid acl event", () => {
                const result = federation.bwiIsRoomFederated(
                    makeRoom({
                        type: EventType.RoomServerAcl,
                        content: {
                            allow: ["my.server.org"],
                        },
                        skey: "",
                    }),
                );

                expect(result).toBe(false);
            });
            it("should not indicate federation for the personal notes room", () => {
                const room = makeRoom(
                    {
                        type: EventType.RoomServerAcl,
                        content: {
                            allow: ["*"],
                        },
                        skey: "",
                    },
                    {
                        type: notes.NOTES_DATA,
                        content: {},
                    },
                );
                jest.spyOn(client, "getAccountData").mockReturnValueOnce(
                    mkEvent({
                        event: true,
                        type: notes.NOTES_DATA,
                        content: {
                            room_id: room.roomId,
                        },
                        user: USER_ID,
                        room: room.roomId,
                    }),
                );

                const result = federation.bwiIsRoomFederated(room);

                expect(result).toBe(false);
            });
        });
    });

    describe("isFederatedByACLs", () => {
        it("should return true if server matches the first acl", () => {
            // Expect the presence of a second server to result in true
            expect(federation.isFederatedByACLs({ allow: ["*"] })).toBe(true);
            expect(federation.isFederatedByACLs({ allow: ["*.server.org"] })).toBe(true);
        });

        it("should return false if server does not match the first acl", () => {
            // Expect invalid wildcard placement not to be allowed
            expect(federation.isFederatedByACLs({ allow: ["app.*.server"] })).toBe(false);

            // Expect empty acl not be allowed
            expect(federation.isFederatedByACLs({ allow: [] })).toBe(false);
        });
    });

    describe("useRoomFederationState()", () => {
        it("should return the correct federation state", async () => {
            // When room is not federated
            const room = makeRoom({
                type: EventType.RoomServerAcl,
                skey: "",
                content: {
                    allow: ["my.server.org"],
                },
            });

            const hook = renderHook(() => useRoomFederationState(room), {
                // @ts-ignore
                wrapper: ({ children }) => (
                    <RoomContext.Provider value={getRoomContext(room, {})}>{children}</RoomContext.Provider>
                ),
            });

            // Expect hook to return false
            expect(hook.result.current).toBe(false);

            // When room received federation event
            await act(async () => {
                emitRoomFederationEvent(room, true);
            });

            // Expect hook to return true
            expect(hook.result.current).toBe(true);
        });
    });

    describe("bwiIsUserFederated()", () => {
        it("should return true if their home server is different from ours", () => {
            expect(federation.bwiIsUserFederated("@bob:bobs_server.org", "@alice:alice_server.org")).toBe(true);
        });
        it("should return false if their home server is the same as ours", () => {
            expect(federation.bwiIsUserFederated("@bob:bobs_server.org", "@alice:bobs_server.org")).toBe(false);
        });
        it("should return false if one user id has an invalid signature", () => {
            expect(federation.bwiIsUserFederated("@bob", "@alice:bobs_server.org")).toBe(false);
            expect(federation.bwiIsUserFederated("@bob:bobs_server.org", "@alice.bobs_server.org")).toBe(false);
        });
    });

    describe("<MemberTile />", () => {
        it("should show federation icon if their home server is different from ours", () => {
            const room = makeRoom();
            const alice = new RoomMember(room.roomId, "@alice:alice_server.org");
            render(<MemberTile member={alice} bwiShowFederation={true} />);

            expect(document.querySelector(".bwi_FederationIcon")).toBeInTheDocument();
        });
        it("should not show federation icon if their home server is the same as ours", () => {
            const room = makeRoom();
            const bob = new RoomMember(room.roomId, "@bob:server.org");
            render(<MemberTile member={bob} bwiShowFederation={true} />);

            expect(document.querySelector(".bwi_FederationIcon")).not.toBeInTheDocument();
        });
    });

    describe("createRoom()", () => {
        const createRoomSpy = jest.spyOn(client, "createRoom");

        beforeEach(() => {
            createRoomSpy.mockClear();
        });

        function getAclEvent([createOpts]: Parameters<typeof client.createRoom>): any {
            return createOpts.initial_state?.find((s) => s.type === EventType.RoomServerAcl);
        }

        it("should set acls to  '*' if federation is desired", async () => {
            await createRoom(client, {
                bwiEnableFederation: true,
                inlineErrors: false,
            });

            expect(createRoomSpy).toHaveBeenCalled();

            const aclEvent = getAclEvent(createRoomSpy.mock.calls[0]);

            // Expect acl event to only allow the home server, federation is enabled
            expect(aclEvent).toBeTruthy();
            expect(aclEvent?.content.allow).toEqual(["*"]);
        });

        it("should set acls to homeserver host if federation is not desired", async () => {
            await createRoom(client, {
                bwiEnableFederation: false,
                inlineErrors: false,
            });

            const aclEvent = getAclEvent(createRoomSpy.mock.calls[0]);

            // Expect acl event to only allow the home server, federation is disabled
            expect(aclEvent).toBeTruthy();
            expect(aclEvent?.content.allow).toEqual([HOME_SERVER]);
        });

        it("should not set acls if federation is desired but the feature is disabled", async () => {
            mockFeatureFlag(BwiFeature.Federation, false);

            await createRoom(client, {
                bwiEnableFederation: true,
                inlineErrors: false,
            });

            const aclEvent = getAclEvent(createRoomSpy.mock.calls[0]);

            expect(aclEvent).toBeUndefined();
        });

        it("should not set acls if room is a DM", async () => {
            // when room is a DM
            await createRoom(client, {
                bwiEnableFederation: true,
                inlineErrors: false,
                dmUserId: FEDERATED_USER_ID,
            });

            const aclEvent = getAclEvent(createRoomSpy.mock.calls[0]);

            // expect acl event not to be set
            expect(aclEvent).toBeUndefined();
        });
    });
    describe("<CreateRoomDialog />", () => {
        it("should show federation switch", async () => {
            await act(async () => {
                render(<CreateRoomDialog onFinished={() => {}} defaultEncrypted={true} />);
            });

            expect(screen.queryByText("Allow federation")).toBeInTheDocument();
        });
    });
    describe("<BwiRoomFederationSetting />", () => {
        const modalSpy = jest.spyOn(Modal, "createDialog");
        const sendStateEventSpy = jest.spyOn(client, "sendStateEvent");

        beforeEach(() => {
            modalSpy.mockReset();
            modalSpy.mockReturnValue({
                finished: Promise.resolve([true]),
                close() {},
            });
        });

        afterAll(() => {
            modalSpy.mockRestore();
            sendStateEventSpy.mockRestore();
        });

        it("should enable room federation", async () => {
            const room = makeRoom({
                type: EventType.RoomServerAcl,
                skey: "",
                content: {
                    allow: [HOME_SERVER],
                },
            });
            render(<BwiRoomFederationSettings room={room} />);
            const switchElement = screen.getByRole("switch");

            // When room is federated, expect switch value to be true
            expect(switchElement).toHaveAttribute("aria-checked", "false");

            // Click switch and enable federation
            await act(async () => {
                fireEvent.click(switchElement);
                // userEvent.click(switchElement);
            });

            // Expect acl event to be sent and federation to be enabled
            expect(sendStateEventSpy).toHaveBeenCalledWith(room.roomId, EventType.RoomServerAcl, {
                allow: ["*"],
                allow_ip_literals: false,
            });

            // Expect switch to be unchecked
            expect(switchElement).toHaveAttribute("aria-checked", "true");
        });

        it("should disable room federation", async () => {
            const room = makeRoom({
                type: EventType.RoomServerAcl,
                skey: "",
                content: {
                    allow: ["*"],
                },
            });
            render(<BwiRoomFederationSettings room={room} />);
            const switchElement = screen.getByRole("switch");

            // When room is federated, expect switch value to be true
            expect(switchElement).toHaveAttribute("aria-checked", "true");

            // Click switch and disable federation
            await act(async () => {
                fireEvent.click(switchElement);
                // userEvent.click(switchElement);
            });

            // Expect confirmation request to be made.
            // We accepted in advance by mocking finished = [true]
            expect(modalSpy).toHaveBeenCalledWith(ConfirmDisableFederationDialog);

            // Expect acl event to be sent and federation to be disabled
            expect(sendStateEventSpy).toHaveBeenCalledWith(room.roomId, EventType.RoomServerAcl, {
                allow: [HOME_SERVER],
                allow_ip_literals: false,
            });

            // Expect switch to be unchecked
            expect(switchElement).toHaveAttribute("aria-checked", "false");
        });

        it("should be disabled if user lacks permission", () => {
            const room = makeRoom({
                type: EventType.RoomServerAcl,
                skey: "",
                content: {
                    allow: ["*"],
                },
            });

            const roomState = room.getLiveTimeline().getState(Direction.Forward);
            jest.spyOn(roomState!, "mayClientSendStateEvent").mockReturnValue(false);

            render(<BwiRoomFederationSettings room={room} />);

            const switchElement = screen.getByRole("switch");
            expect(switchElement).toHaveAttribute("aria-disabled", "true");
        });

        describe("<ConfirmDisableFederationDialog />", () => {
            it("should return false on cancel", () => {
                render(
                    <ConfirmDisableFederationDialog
                        onFinished={(proceed) => {
                            expect(proceed).toBe(false);
                        }}
                    />,
                );
                fireEvent.click(screen.getByText(_t("action|cancel")));
            });
            it("should return true on confirm", () => {
                render(
                    <ConfirmDisableFederationDialog
                        onFinished={(proceed) => {
                            expect(proceed).toBe(true);
                        }}
                    />,
                );
                fireEvent.click(screen.getByText(_t("Yes, withdraw")));
            });
        });

        describe("<GeneralRoomSettingsTab />", () => {
            const room = makeRoom();

            function renderGeneralRoomSettingsTab(): void {
                render(
                    <MatrixClientContext.Provider value={client}>
                        <GeneralRoomSettingsTab room={room} />
                    </MatrixClientContext.Provider>,
                );
            }

            it("should show BwiRoomFederationSetting if feature is enabled", () => {
                renderGeneralRoomSettingsTab();
                expect(screen.queryByRole("heading", { name: "Federation" })).toBeInTheDocument();
            });

            it("should hide BwiRoomFederationSetting if feature is disabled", () => {
                mockFeatureFlag(BwiFeature.Federation, false);

                renderGeneralRoomSettingsTab();
                expect(screen.queryByRole("heading", { name: "Federation" })).not.toBeInTheDocument();
            });
        });
    });

    describe("<BwiDecoratedAvatar />", () => {
        describe("user-federation", () => {
            it("should show the federation icon for a federated user", () => {
                render(
                    <BwiDecoratedAvatar type="user-federation" userId={FEDERATED_USER_ID}>
                        <p>Bobs avatar</p>
                    </BwiDecoratedAvatar>,
                );
                // Expect bobs avatar to be decorated with the federation icon
                expect(document.querySelector(".bwi_FederationIcon")).toBeInTheDocument();
            });
            it("must not show the federation icon for a non federated user", () => {
                render(
                    <BwiDecoratedAvatar type="user-federation" userId={`@bob:${HOME_SERVER}`}>
                        <p>Bobs avatar</p>
                    </BwiDecoratedAvatar>,
                );
                // Expect bobs avatar not to be decorated with the federation icon
                expect(document.querySelector(".bwi_FederationIcon")).not.toBeInTheDocument();
            });
            it("must not show the federation icon for a federated user if feature is disabled", () => {
                mockFeatureFlag(BwiFeature.Federation, false);
                render(
                    <BwiDecoratedAvatar type="user-federation" userId={FEDERATED_USER_ID}>
                        <p>Bobs avatar</p>
                    </BwiDecoratedAvatar>,
                );
                // Expect bobs avatar not to be decorated with the federation icon
                expect(document.querySelector(".bwi_FederationIcon")).not.toBeInTheDocument();
            });
        });

        describe("props", () => {
            it("hideDecoration should hide the decoration", () => {
                render(
                    <BwiDecoratedAvatar type="user-federation" userId={FEDERATED_USER_ID} hideDecoration={true}>
                        <p>Bobs avatar</p>
                    </BwiDecoratedAvatar>,
                );

                // Expect bobs avatar not to be decorated with the federation icon
                expect(document.querySelector(".bwi_FederationIcon")).not.toBeInTheDocument();
            });
        });
    });

    describe("<FederationDecisionDialog />", () => {
        it("dialog has all components", () => {
            const room = makeRoom();
            render(<FederationDecisionDialog room={room} onFinished={jest.fn} />);
            const title = screen.getByTestId("FederationDecisionDialog_title");
            expect(title).toBeInTheDocument();
            expect(title).toHaveTextContent(room.name);
            expect(
                screen.getByText(
                    "This will enable external organizations to access the room. It can be reversed in the room settings afterwards.",
                ),
            ).toBeInTheDocument();
            expect(screen.getByTestId("FederationDecisionDialog_buttons").children).toHaveLength(2);
        });

        it("pushing allow button sends acl allow state event and closes dialog", async () => {
            const room = makeRoom();
            const eventSpy = jest.spyOn(room.client, "sendStateEvent");
            const dateSpy = (Date.now = jest.fn().mockReturnValue(1706600374892));
            const callback = jest.fn();
            render(<FederationDecisionDialog room={room} onFinished={callback} />);
            const button = screen.getByText("Federate room");
            expect(button).toBeInTheDocument();
            await act(async () => {
                button.click();
            });
            expect(eventSpy).toHaveBeenCalledWith(room.roomId, EventType.RoomServerAcl, {
                allow: ["*"],
                allow_ip_literals: false,
            });
            // Expect success stage
            await act(async () => {
                fireEvent.click(
                    within(document.querySelector(".mx_FederationDecisionDialog_buttons")!).getByText("OK"),
                );
            });
            expect(callback).toHaveBeenCalledWith(true);
            expect(room.client.createAlias).toHaveBeenCalled();
            expect(eventSpy).toHaveBeenCalledWith(room.roomId, EventType.RoomCanonicalAlias, {
                alias: federation.roomNameToAlias(room.name, room.client),
            });
            eventSpy.mockRestore();
            dateSpy.mockRestore();
            room.client.createAlias.mockReset();
        });

        it("pushing forbid button sends acl forbid state event and closes dialog", async () => {
            const room = makeRoom();
            const eventSpy = jest.spyOn(room.client, "sendStateEvent");
            const dateSpy = (Date.now = jest.fn().mockReturnValue(1706600374892));
            const callback = jest.fn();
            render(<FederationDecisionDialog room={room} onFinished={callback} />);
            const button = screen.getByText("Keep room internal");
            expect(button).toBeInTheDocument();
            await act(async () => {
                button.click();
            });
            expect(eventSpy).toHaveBeenCalledWith(room.roomId, EventType.RoomServerAcl, {
                allow: ["server.org"],
                allow_ip_literals: false,
            });
            expect(callback).toHaveBeenCalledWith(true);
            expect(room.client.createAlias).toHaveBeenCalled();
            expect(eventSpy).toHaveBeenCalledWith(room.roomId, EventType.RoomCanonicalAlias, {
                alias: federation.roomNameToAlias(room.name, room.client),
            });
            eventSpy.mockRestore();
            dateSpy.mockRestore();
            room.client.createAlias.mockReset();
        });

        it("alias already set, don't create alias", async () => {
            jest.spyOn(client, "getLocalAliases").mockResolvedValueOnce({ aliases: ["mockitymock"] }) as unknown as any;
            const room = makeRoom();
            const eventSpy = jest.spyOn(room.client, "sendStateEvent");
            const callback = jest.fn();
            render(<FederationDecisionDialog room={room} onFinished={callback} />);
            const button = screen.getByText("Keep room internal");
            expect(button).toBeInTheDocument();
            await act(async () => {
                button.click();
            });
            expect(eventSpy).toHaveBeenCalledWith(room.roomId, "m.room.server_acl", {
                allow: ["server.org"],
                allow_ip_literals: false,
            });
            expect(callback).toHaveBeenCalledWith(true);
            expect(room.client.createAlias).not.toHaveBeenCalled();
            eventSpy.mockRestore();
            room.client.createAlias.mockReset();
        });

        it("should show an error message if something went wrong", async () => {
            const room = makeRoom();

            const callback = jest.fn();
            render(<FederationDecisionDialog room={room} onFinished={callback} />);

            // mock response value
            jest.spyOn(federation, "updateRoomAcl").mockImplementationOnce(() => {
                throw new Error("Upsi");
            });

            // After click on any option
            await act(async () => {
                fireEvent.click(screen.getByText("Keep room internal"), {});
            });

            // Expect the state to be error
            expect(screen.queryByText(_t("bwi|federation|decision_failed_title"))).toBeInTheDocument();
            expect(screen.queryByText(_t("bwi|federation|decision_failed_message"))).toBeInTheDocument();
        });
    });

    describe("shouldShowFederationDecisionDialog", () => {
        let ff: jest.SpyInstance;

        afterEach(() => {
            ff.mockRestore();
        });

        const prepareSuccessCase = (room: Room): void => {
            const member = new RoomMember(room.roomId, room.myUserId);
            member.powerLevel = 100;
            room.getMembers = jest.fn().mockReturnValue([member]);
            jest.spyOn(notes, "isPersonalNotesRoom").mockReturnValue(false);
            jest.spyOn(dms, "isRoomDm").mockReturnValue(false);

            room.getLiveTimeline().getState(EventTimeline.FORWARDS)!.getStateEvents = jest
                .fn()
                .mockImplementation((eventType: EventType) => {
                    if (eventType === EventType.RoomCreate) {
                        return {
                            getContent: jest.fn().mockReturnValue({
                                "m.federated": true,
                            }),
                        };
                    }
                    if (eventType === EventType.RoomServerAcl) {
                        return {
                            getContent: jest.fn().mockReturnValue(undefined),
                        };
                    } else
                        return {
                            getContent: jest.fn(),
                        };
                });

            ff = mockFeatureFlag(BwiFeature.Federation, true);
        };

        it("all requirements fulfilled, show dialog", () => {
            const room = makeRoom();
            prepareSuccessCase(room);

            const result = federation.shouldShowFederationDecisionDialog(room);
            expect(result).toBeTruthy();
            ff.mockRestore();
        });

        it("ff not set, don't show dialog", () => {
            const room = makeRoom();
            prepareSuccessCase(room);
            ff = mockFeatureFlag(BwiFeature.Federation, false);

            const result = federation.shouldShowFederationDecisionDialog(room);
            expect(result).toBeFalsy();
            ff.mockRestore();
        });

        it("user not admin, don't show dialog", () => {
            const room = makeRoom();
            prepareSuccessCase(room);
            const member = new RoomMember(room.roomId, room.myUserId);
            member.powerLevel = 50;
            room.getMembers = jest.fn().mockReturnValue([member]) as unknown as MockInstance<
                () => RoomMember[]
            > & {} & (() => RoomMember[]);

            const result = federation.shouldShowFederationDecisionDialog(room);
            expect(result).toBeFalsy();
            ff.mockRestore();
        });

        it("personal notes room, don't show dialog", () => {
            const room = makeRoom();
            prepareSuccessCase(room);
            jest.spyOn(notes, "isPersonalNotesRoom").mockReturnValue(true);

            const result = federation.shouldShowFederationDecisionDialog(room);
            expect(result).toBeFalsy();
            ff.mockRestore();
        });

        it("dm room, don't show dialog", () => {
            const room = makeRoom();
            prepareSuccessCase(room);
            jest.spyOn(dms, "isRoomDm").mockReturnValue(true);

            const result = federation.shouldShowFederationDecisionDialog(room);
            expect(result).toBeFalsy();
            ff.mockRestore();
        });

        it("m.federated false, don't show dialog", () => {
            const room = makeRoom();
            prepareSuccessCase(room);
            room.getLiveTimeline().getState(EventTimeline.FORWARDS)!.getStateEvents = jest
                .fn()
                .mockImplementation((eventType: EventType) => {
                    if (eventType === EventType.RoomCreate) {
                        return {
                            getContent: jest.fn().mockReturnValue({
                                "m.federated": false,
                            }),
                        };
                    }
                    if (eventType === EventType.RoomServerAcl) {
                        return {
                            getContent: jest.fn().mockReturnValue(undefined),
                        };
                    } else
                        return {
                            getContent: jest.fn(),
                        };
                });

            const result = federation.shouldShowFederationDecisionDialog(room);
            expect(result).toBeFalsy();
            ff.mockRestore();
        });

        it("acl already set to allow, don't show dialog", () => {
            const room = makeRoom();
            prepareSuccessCase(room);
            room.getLiveTimeline().getState(EventTimeline.FORWARDS)!.getStateEvents = jest
                .fn()
                .mockImplementation((eventType: EventType) => {
                    if (eventType === EventType.RoomCreate) {
                        return {
                            getContent: jest.fn().mockReturnValue({
                                "m.federated": true,
                            }),
                        };
                    }
                    if (eventType === EventType.RoomServerAcl) {
                        return {
                            getContent: jest.fn().mockReturnValue({
                                allow: ["*"],
                            }),
                        };
                    } else
                        return {
                            getContent: jest.fn(),
                        };
                });

            const result = federation.shouldShowFederationDecisionDialog(room);
            expect(result).toBeFalsy();
            ff.mockRestore();
        });

        it("acl already set to deny, don't show dialog", () => {
            const room = makeRoom();
            prepareSuccessCase(room);
            room.getLiveTimeline().getState(EventTimeline.FORWARDS)!.getStateEvents = jest
                .fn()
                .mockImplementation((eventType: EventType) => {
                    if (eventType === EventType.RoomCreate) {
                        return {
                            getContent: jest.fn().mockReturnValue({
                                "m.federated": true,
                            }),
                        };
                    }
                    if (eventType === EventType.RoomServerAcl) {
                        return {
                            getContent: jest.fn().mockReturnValue({
                                allow: ["server.org"],
                            }),
                        };
                    } else
                        return {
                            getContent: jest.fn(),
                        };
                });

            const result = federation.shouldShowFederationDecisionDialog(room);
            expect(result).toBeFalsy();
            ff.mockRestore();
        });

        it("workaround for when room state is incorrect after creating room, enabled federation in room creation, don't show dialog", () => {
            const roomCreateOpts: RoomCreateOpts = {
                bwiEnableFederation: true,
            };
            const room = makeRoom();
            prepareSuccessCase(room);

            const result = federation.shouldShowFederationDecisionDialog(room, roomCreateOpts);
            expect(result).toBeFalsy();
            ff.mockRestore();
        });

        it("workaround for when room state is incorrect after creating room, disable federation in room creation, don't show dialog", () => {
            const roomCreateOpts: RoomCreateOpts = {
                bwiEnableFederation: false,
            };
            const room = makeRoom();
            prepareSuccessCase(room);

            const result = federation.shouldShowFederationDecisionDialog(room, roomCreateOpts);
            expect(result).toBeFalsy();
            ff.mockRestore();
        });
    });

    describe("<UnwrappedEventTile />", () => {
        function renderEventTile(event: MatrixEvent, room: Room): void {
            render(
                <RoomContext.Provider
                    value={
                        {
                            ...RoomContext,
                            room,
                        } as unknown as IRoomState
                    }
                >
                    <UnwrappedEventTile mxEvent={event} />
                </RoomContext.Provider>,
            );
        }

        describe("avatar icon", () => {
            it("should render the avatar federation icon if the sender is federated", () => {
                const room = makeRoom();
                const event = mkEvent({
                    event: true,
                    type: EventType.RoomMessage,
                    content: {
                        body: "My message :)",
                    },
                    user: FEDERATED_USER_ID,
                });
                event.sender = new RoomMember(room.roomId, FEDERATED_USER_ID);

                renderEventTile(event, room);

                // Expect bobs avatar not to be decorated with the federation icon
                expect(document.querySelector(".bwi_FederationIcon")).toBeInTheDocument();
            });

            it("should hide decoration for smaller (state) events", () => {
                const room = makeRoom({
                    type: EventType.RoomMember,
                    skey: FEDERATED_USER_ID,
                    content: {
                        membership: "join",
                        displayname: "Bob Ross",
                    },
                });

                const joinEvent = room
                    .getLiveTimeline()
                    .getState(Direction.Forward)!
                    .getStateEvents(EventType.RoomMember, FEDERATED_USER_ID)!;

                joinEvent.sender = joinEvent.target = new RoomMember(room.roomId, FEDERATED_USER_ID);

                renderEventTile(joinEvent, room);

                // expect join event not to be decorated
                expect(document.querySelector(".bwi_FederationIcon")).not.toBeInTheDocument();
            });
        });

        describe("federation state event", () => {
            const room = makeRoom();
            it("should replace the avatar with an icon", () => {
                renderEventTile(
                    mkEvent({
                        event: true,
                        type: EventType.RoomServerAcl,
                        skey: "",
                        user: USER_ID,
                        content: {
                            allow: ["*"],
                        },
                    }),
                    room,
                );

                expect(document.querySelector(".bwi_FederationStateEventIcon")).toBeInTheDocument();
            });
            it("should show decline message for first event", () => {
                renderEventTile(
                    mkEvent({
                        event: true,
                        type: EventType.RoomServerAcl,
                        skey: "",
                        user: USER_ID,
                        content: {
                            allow: [HOME_SERVER],
                        },
                    }),
                    room,
                );

                expect(screen.queryByText(_t("bwi|federation|state_event|declined"))).toBeInTheDocument();
            });
            it("should show allow message for first event", () => {
                renderEventTile(
                    mkEvent({
                        event: true,
                        type: EventType.RoomServerAcl,
                        skey: "",
                        user: USER_ID,
                        content: {
                            allow: ["*"],
                        },
                    }),
                    room,
                );

                expect(screen.queryByText(_t("bwi|federation|state_event|allowed"))).toBeInTheDocument();
            });
            it("should show revoked message for follow up event", () => {
                const firstEvent = mkEvent({
                    event: true,
                    type: EventType.RoomServerAcl,
                    skey: "",
                    user: USER_ID,
                    content: {
                        allow: ["*"],
                    },
                });
                const secondEvent = mkEvent({
                    event: true,
                    type: EventType.RoomServerAcl,
                    skey: "",
                    user: USER_ID,
                    content: {
                        allow: [HOME_SERVER],
                    },
                    prev_content: firstEvent.getContent(),
                });

                renderEventTile(secondEvent, room);

                expect(screen.queryByText(_t("bwi|federation|state_event|revoked"))).toBeInTheDocument();
            });
            it("should show allowed message for follow up event", () => {
                const firstEvent = mkEvent({
                    event: true,
                    type: EventType.RoomServerAcl,
                    skey: "",
                    user: USER_ID,
                    content: {
                        allow: [HOME_SERVER],
                    },
                });
                const secondEvent = mkEvent({
                    event: true,
                    type: EventType.RoomServerAcl,
                    skey: "",
                    user: USER_ID,
                    content: {
                        allow: ["*"],
                    },
                    prev_content: firstEvent.getContent(),
                });

                renderEventTile(secondEvent, room);

                expect(screen.queryByText(_t("bwi|federation|state_event|allowed"))).toBeInTheDocument();
            });
        });
    });

    describe("validateFederationInvites()", () => {
        describe("federation pending", () => {
            const room = makeRoom();
            it("should allow internal users", () => {
                const result = federation.validateFederationInvites(
                    [
                        {
                            userId: `@local1:${HOME_SERVER}`,
                        } as Member,
                        {
                            userId: `@local2:${HOME_SERVER}`,
                        } as Member,
                    ],
                    room,
                );

                expect(result).toBe(null);
            });
            it("should block external users", () => {
                const result = federation.validateFederationInvites(
                    [
                        {
                            userId: `@local1:${HOME_SERVER}`,
                        } as Member,
                        {
                            userId: `@federated1:${FEDERATED_HOME_SERVER}`,
                        } as Member,
                    ],
                    room,
                );

                expect(result).toBe(_t("bwi|invite_dialog|federation_pending"));
            });
        });
        describe("federation disabled", () => {
            const room = makeRoom({
                type: EventType.RoomServerAcl,
                skey: "",
                content: {
                    allow: [HOME_SERVER],
                },
            });
            it("should allow internal users", () => {
                const result = federation.validateFederationInvites(
                    [
                        {
                            userId: `@local1:${HOME_SERVER}`,
                        } as Member,
                        {
                            userId: `@local2:${HOME_SERVER}`,
                        } as Member,
                    ],
                    room,
                );

                expect(result).toBe(null);
            });
            it("should block external users", () => {
                const result = federation.validateFederationInvites(
                    [
                        {
                            userId: `@local1:${HOME_SERVER}`,
                        } as Member,
                        {
                            userId: `@federated1:${FEDERATED_HOME_SERVER}`,
                        } as Member,
                    ],
                    room,
                );

                expect(result).toBe(_t("bwi|invite_dialog|federation_disabled"));
            });
        });
        describe("federation enabled", () => {
            const room = makeRoom({
                type: EventType.RoomServerAcl,
                skey: "",
                content: {
                    allow: ["*"],
                },
            });
            it("should allow internal users", () => {
                const result = federation.validateFederationInvites(
                    [
                        {
                            userId: `@local1:${HOME_SERVER}`,
                        } as Member,
                        {
                            userId: `@local2:${HOME_SERVER}`,
                        } as Member,
                    ],
                    room,
                );

                expect(result).toBe(null);
            });
            it("should allow external users", () => {
                const result = federation.validateFederationInvites(
                    [
                        {
                            userId: `@local1:${HOME_SERVER}`,
                        } as Member,
                        {
                            userId: `@federated1:${FEDERATED_HOME_SERVER}`,
                        } as Member,
                    ],
                    room,
                );

                expect(result).toBe(null);
            });
            it("should not allow federated admins", () => {
                const result = federation.validateFederationInvites(
                    [
                        {
                            userId: `@local1:${HOME_SERVER}`,
                        } as Member,
                        {
                            userId: `@federated1:${FEDERATED_HOME_SERVER}`,
                        } as Member,
                    ],
                    room,
                    100,
                );
                expect(result).toBe(_t("bwi|federation|admin_power_level_forbidden"));
            });
        });
    });

    describe("<InviteDialog />", () => {
        it("should show error notification from validation", async () => {
            const room = makeRoom();
            jest.spyOn(client, "searchUserDirectory").mockResolvedValue({
                results: [
                    {
                        user_id: `@bob:${FEDERATED_HOME_SERVER}`,
                        display_name: "Bob Ross",
                    },
                ],
                limited: false,
            });
            render(<RovingInviteDialog kind={InviteKind.Invite} roomId={room.roomId} onFinished={() => {}} />);
            const input = screen.getByTestId("invite-dialog-input");
            await act(async () => {
                fireEvent.change(input, { target: { value: "Bob Ross" } });
            });
            const bobElement = await screen.findByText("Bob Ross");
            await act(async () => {
                fireEvent.click(bobElement);
            });

            expect(screen.getByText(_t("bwi|invite_dialog|federation_pending"))).toBeInTheDocument();
        });
    });

    describe("getRoomMemberDisplayname()", () => {
        it("should suffix the user name with the server name", () => {
            const room = makeRoom(
                {
                    type: EventType.RoomServerAcl,
                    skey: "",
                    content: {
                        allow: [HOME_SERVER],
                    },
                },
                {
                    type: EventType.RoomMember,
                    skey: FEDERATED_USER_ID,
                    content: {
                        membership: "invite",
                        displayname: "Bob Ross",
                    },
                },
            );

            const inviteEvent = room
                .getLiveTimeline()
                .getState(Direction.Forward)!
                .getStateEvents(EventType.RoomMember, FEDERATED_USER_ID)!;

            const displayName = getRoomMemberDisplayname(client, inviteEvent, FEDERATED_USER_ID);

            expect(displayName).toEqual(`Bob Ross (server.org)`);
        });
    });

    describe("formatServerName()", () => {
        it("should format my.server.org to server.org", () => {
            expect(federation.formatServerName("my.server.org")).toBe("server.org");
        });
        it("should fallback if the server name is 'invalid'", () => {
            expect(federation.formatServerName("server")).toBe("server");
        });
        it("should fallback if the server name is undefined", () => {
            expect(federation.formatServerName(undefined)).toBe("");
        });
    });

    describe("Inviting or promoting admins is not allowed", () => {
        function makeFederatedRoomWithPermissions(): { room: Room; federatedUser: RoomMember } {
            const room = makeRoom(
                {
                    type: EventType.RoomPowerLevels,
                    skey: "",
                    content: {
                        users: {
                            [USER_ID]: 100,
                            [FEDERATED_USER_ID]: 50,
                        },
                    },
                },
                {
                    type: EventType.RoomMember,
                    skey: USER_ID,
                    content: {
                        membership: "join",
                    },
                },
                {
                    type: EventType.RoomMember,
                    skey: FEDERATED_USER_ID,
                    content: {
                        membership: "join",
                    },
                },
            );

            const federatedUser = new RoomMember(room.roomId, FEDERATED_USER_ID);
            federatedUser.rawDisplayName = "Bob";
            federatedUser.setPowerLevelEvent(
                room.getLiveTimeline().getState(Direction.Forward)!.getStateEvents(EventType.RoomPowerLevels, "")!,
            );

            return { room, federatedUser };
        }

        describe("<UserInfo.PowerLevelEditor />", () => {
            it("should not be allowed to promote a federated user to admin", async () => {
                const { room, federatedUser } = makeFederatedRoomWithPermissions();

                await act(async () => {
                    render(
                        <MatrixClientContext.Provider value={client}>
                            <PowerLevelEditor
                                room={room}
                                user={federatedUser}
                                roomPermissions={{
                                    modifyLevelMax: 100,
                                    canEdit: true,
                                    canInvite: false,
                                }}
                            />
                        </MatrixClientContext.Provider>,
                    );
                });

                const warningSpy = jest.spyOn(
                    openFederatedAdminsNotAllowedModalModule,
                    "openFederatedAdminsNotAllowedModal",
                );

                // select admin
                await act(async () => {
                    fireEvent.change(screen.getByPlaceholderText("Power level"), { target: { value: 100 } });
                });

                expect(warningSpy).toHaveBeenCalled();
            });
        });

        // Untestable due to the untestable AutocompleteInput
        // eslint-disable-next-line jest/no-commented-out-tests
        // describe("<AddPrivilegedUsers />", () => { });

        describe("<RolesRoomSettingsTab />", () => {
            it("show a warning modal if attempting to make a federated user admin", async () => {
                const { room } = makeFederatedRoomWithPermissions();

                await act(async () => {
                    render(
                        <MatrixClientContext.Provider value={client}>
                            <RolesRoomSettingsTab room={room} />
                        </MatrixClientContext.Provider>,
                    );
                });

                const warningSpy = jest.spyOn(
                    openFederatedAdminsNotAllowedModalModule,
                    "openFederatedAdminsNotAllowedModal",
                );
                // change bob to admin
                await act(async () => {
                    fireEvent.change(screen.getByPlaceholderText("@bob:bobs.server.org"), {
                        target: { value: 100 },
                    });
                });
                expect(warningSpy).toHaveBeenCalled();
            });
        });
    });

    describe("BeiAnalytics tracking", () => {
        const trackSpy = jest.spyOn(BwiAnalyticsManager, "trackEvent");
        beforeEach(() => {
            trackSpy.mockReset();
        });

        describe("decide federation state", () => {
            it("should track accept", async () => {
                const room = makeRoom();
                render(<FederationDecisionDialog room={room} onFinished={() => {}} />);
                await act(async () => {
                    fireEvent.click(screen.getByText(_t("bwi|federation|decision_allow")));
                });

                // tracking is not awaited so we have to wait for it here
                await waitFor(() => {
                    expect(trackSpy).toHaveBeenCalledWith(
                        BwiAnalyticsCategories.Federation,
                        FederationAnalyticsActions.ShowSettingFederateRoom,
                        FederationAnalyticsEvents.AcceptFederation,
                        0,
                    );
                });
            });

            it("should track decline", async () => {
                const room = makeRoom();
                render(<FederationDecisionDialog room={room} onFinished={() => {}} />);
                await act(async () => {
                    fireEvent.click(screen.getByText(_t("bwi|federation|decision_forbid")));
                });

                // tracking is not awaited so we have to wait for it here
                await waitFor(() => {
                    expect(trackSpy).toHaveBeenCalledWith(
                        BwiAnalyticsCategories.Federation,
                        FederationAnalyticsActions.ShowSettingFederateRoom,
                        FederationAnalyticsEvents.DeclineFederation,
                        0,
                    );
                });
            });
        });
        describe("create room or dm", () => {
            const createRoomMock = jest.spyOn(client, "createRoom");
            beforeAll(() => {
                createRoomMock.mockImplementation(async (opts) => {
                    const room = makeRoom();
                    return {
                        room_id: room.roomId,
                    };
                });
            });
            afterAll(() => {
                createRoomMock.mockRestore();
            });

            it("should track a local room", async () => {
                await createRoom(client, {});

                expect(trackSpy).toHaveBeenCalledWith(
                    BwiAnalyticsCategories.Federation,
                    FederationAnalyticsActions.CreateRoom,
                    FederationAnalyticsEvents.LocalRoom,
                );
            });
            it("should track a federated room", async () => {
                await createRoom(client, {
                    bwiEnableFederation: true,
                });

                expect(trackSpy).toHaveBeenCalledWith(
                    BwiAnalyticsCategories.Federation,
                    FederationAnalyticsActions.CreateRoom,
                    FederationAnalyticsEvents.FederatedRoom,
                );
            });
            it("should track a local dm", async () => {
                await createRoom(client, {
                    dmUserId: USER_ID,
                });

                expect(trackSpy).toHaveBeenCalledWith(
                    BwiAnalyticsCategories.Federation,
                    FederationAnalyticsActions.CreateDM,
                    FederationAnalyticsEvents.LocalRoom,
                );
            });
            it("should track a federated dm", async () => {
                await createRoom(client, {
                    dmUserId: FEDERATED_USER_ID,
                    bwiEnableFederation: true,
                });

                expect(trackSpy).toHaveBeenCalledWith(
                    BwiAnalyticsCategories.Federation,
                    FederationAnalyticsActions.CreateDM,
                    FederationAnalyticsEvents.FederatedRoom,
                );
            });
        });
        describe("invite to federated room", () => {
            // It's not worth to test the full path starting at the RoomView invite event because mocking RoomView is insane.
            it("should track accepting an invite to a private room", async () => {
                const room = makeRoom();
                jest.spyOn(room, "getJoinRule").mockReturnValue(JoinRule.Invite);
                await federationTracker.sendJoinRoom(room);

                expect(trackSpy).toHaveBeenCalledWith(
                    BwiAnalyticsCategories.Federation,
                    FederationAnalyticsActions.JoinRoom,
                    FederationAnalyticsEvents.JoinFederatedRoom,
                );
            });
            it("should track accepting an invite to a public room", async () => {
                const room = makeRoom();
                jest.spyOn(room, "getJoinRule").mockReturnValue(JoinRule.Public);
                await federationTracker.sendJoinRoom(room);

                expect(trackSpy).toHaveBeenCalledWith(
                    BwiAnalyticsCategories.Federation,
                    FederationAnalyticsActions.JoinRoom,
                    FederationAnalyticsEvents.JoinPublicFederatedRoom,
                );
            });
        });
    });

    describe("<BwiFederationOnboarding />", () => {
        it("should not allow the modal the be closed", async () => {
            await act(async () => {
                openBwiFederationOnboarding();
            });

            await waitFor(() => {
                expect(document.querySelector(".bwi_FederationOnboarding_dialog")).toBeInTheDocument();
            });

            screen.debug();

            // when click on background
            await act(async () => {
                fireEvent.click(document.querySelector(".mx_Dialog_background")!);
            });

            // expect dialog not to be close
            expect(document.querySelector(".bwi_FederationOnboarding_dialog")).toBeInTheDocument();

            // clean up modal container
            document.querySelector(".mx_Dialog_StaticContainer")?.remove();
            document.querySelector(".mx_Dialog_Container")?.remove();
        });

        it("should return the onboarding dialog if enabled via well-known", async () => {
            jest.spyOn(wellknown, "getWellKnownConfig").mockResolvedValueOnce({
                federation: {
                    show_introduction: true,
                },
            });

            expect(await getBwiOnboardingDialog()).toBeTruthy();
        });

        it("should not return the onboarding dialog if disabled via well-known", async () => {
            jest.spyOn(wellknown, "getWellKnownConfig").mockResolvedValueOnce({
                federation: {
                    show_introduction: false,
                },
            });

            expect(await getBwiOnboardingDialog()).toBeNull();
        });
    });
});

function mockTests(): {
    client: MatrixClient;
    makeRoom: (...events: Omit<MakeEventProps, "user" | "room" | "event">[]) => any;
    makeDm: (theirId: string) => Room;
} {
    const rooms: Room[] = [];
    const client = getBaseClient({
        ...getClientUserMocks(USER_ID),
        isGuest() {
            return false;
        },
        getDomain() {
            return HOME_SERVER;
        },
        async getLocalAliases() {
            return { aliases: [] };
        },
        async getRoomDirectoryVisibility(roomId: string) {
            return {
                visibility: "public",
            };
        },
        store: {
            async getPendingEvents() {
                return [];
            },
        },
        getEventMapper() {
            return {};
        },
        getRoom(roomId: string) {
            return rooms.find((r) => r.roomId === roomId);
        },
        isRoomEncrypted() {
            return false;
        },
        async createRoom() {
            // It's much easier to just throw an error instead of mocking everything needed for a successful room create
            // Sadly we can not suppress the error
            throw new MatrixError({
                error: "noop",
            });
        },
        getHomeserverUrl() {
            return `https://${HOME_SERVER}`;
        },
        async doesServerForceEncryptionForPreset(preset: Preset) {
            return preset === "private_chat";
        },
        async sendStateEvent(roomId: string, eventType: EventType, content: IContent) {
            const room = rooms.find((room) => room.roomId === roomId);
            if (!room) return undefined;
            const state = room.getLiveTimeline().getState(Direction.Forward);

            // emit event to the room
            if (eventType === EventType.RoomServerAcl) {
                const event = mkEvent({
                    event: true,
                    skey: "",
                    room: roomId,
                    type: eventType,
                    user: USER_ID,
                    content,
                });

                state!.setStateEvents([event]);

                room.emit(RoomEvent.Timeline, event, room, false, false, {
                    timeline: room.getLiveTimeline(),
                });
            }
        },
        getPushActionsForEvent() {
            return null;
        },
        async decryptEventIfNeeded() {},
        createAlias: jest.fn().mockResolvedValue({}),
        async searchUserDirectory() {
            return { results: [] };
        },
        getRooms() {
            return rooms;
        },
        supportsThreads() {
            return false;
        },
        async setPowerLevel() {},
        async setAccountData() {},
    }) as MatrixClient;

    function makeRoom(...events: Omit<Parameters<typeof mkEvent>[0], "user" | "room" | "event">[]): Room {
        const roomId = generateRoomId();
        const room = mockRoomWithEvents(
            {
                partials: {
                    roomId,
                },
                opts: {
                    pendingEventOrdering: PendingEventOrdering.Detached,
                },
                events: events.map((e) =>
                    mkEvent({
                        ...e,
                        event: true,
                        user: USER_ID,
                        room: roomId,
                    }),
                ),
            },
            client,
        );
        rooms.push(room);
        return room;
    }

    function makeDm(theirId: string): Room {
        const roomId = generateRoomId();
        const ourId = `@lea:${HOME_SERVER}`;
        const roomMember1 = mkRoomMember(roomId, ourId);
        const roomMember2 = mkRoomMember(roomId, theirId);
        const room = mkStubRoom(roomId, roomId, client);
        room.currentState.members = { [ourId]: roomMember1, [theirId]: roomMember2 };

        (client.getAccountData as unknown as jest.SpyInstance).mockReturnValueOnce(
            mkEvent({
                event: true,
                user: "",
                type: EventType.Direct,
                content: {
                    [theirId]: [room.roomId],
                },
            }),
        );
        DMRoomMap.makeShared(client);

        return room;
    }

    DMRoomMap.makeShared(client);

    return { client, makeRoom, makeDm };
}

function emitRoomFederationEvent(room: Room, federated: boolean): void {
    const state = room.getLiveTimeline().getState(Direction.Forward);

    const event = mkEvent({
        event: true,
        user: USER_ID,
        room: room.roomId,
        type: EventType.RoomServerAcl,
        skey: "",
        content: {
            allow: ["*"],
        },
    });
    state!.setStateEvents([event]);

    // re emit event because setStateEvents does not emit via RoomEvent.Timeline
    room.emit(RoomEvent.Timeline, event, room, false, false, {
        timeline: room.getLiveTimeline(),
    });
}
