// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { MatrixClientPeg } from "../../src/MatrixClientPeg";
import Modal from "../../src/Modal";
import * as testUtils from "../../test/test-utils";

jest.mock("../../src/stores/room-list/RoomListStore", () => ({
    instance: {
        on: () => true,
    },
}));

// FIXME: The tests are throwing 'Can't perform a React state update on an unmounted component' due to the Field component.
// Investigate why and fix it. The benefit is to suppress the error messages

// FIXME: ChangePassword is no longer exposing the key export. There are currently no tests for this feature
describe.skip("BwiFeature.disableKeyExport", () => {
    afterEach(() => {
        jest.clearAllMocks();
        jest.resetModules();
    });

    // async function fillForm() {
    //     async function emitChange(input: string, value: string) {
    //         await act(async () => {
    //             fireEvent.change(screen.getByPlaceholderText(input), { target: { value } });
    //         });
    //     }

    //     await emitChange("Current password", "atest");
    //     await emitChange("New Password", "thisisanewpassword");
    //     await emitChange("Confirm password", "thisisanewpassword");

    //     await act(async () => {
    //         fireEvent.click(screen.getByText("Change Password"));
    //     });
    // }

    const modalSpy = jest.spyOn(Modal, "createDialog");

    beforeEach(() => {
        testUtils.stubClient();
        MatrixClientPeg.safeGet().getUserIdLocalpart = jest.fn().mockResolvedValue("localhost");
        MatrixClientPeg.safeGet().getDevices = jest.fn().mockResolvedValue({ devices: [1, 2] });

        modalSpy.mockClear().mockReturnValue({
            finished: Promise.resolve([false]),
            close() {},
        });
    });
});
