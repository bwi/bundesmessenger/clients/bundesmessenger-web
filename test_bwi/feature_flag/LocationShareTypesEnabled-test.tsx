// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";
import { act, render, screen } from "jest-matrix-react";
import { MatrixClient, RoomMember } from "matrix-js-sdk/src/matrix";

import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import { ChevronFace } from "../../src/components/structures/ContextMenu";
import LocationShareMenu from "../../src/components/views/location/LocationShareMenu";
import { LocationShareType } from "../../src/components/views/location/shareLocation";
import MatrixClientContext from "../../src/contexts/MatrixClientContext";
import { MatrixClientPeg } from "../../src/MatrixClientPeg";
import { getBaseClient, getClientUserMocks } from "../mock/MockClient";
import { mockFeatureFlag } from "../mock/MockSettingsStore";

jest.mock("../../src/stores/OwnProfileStore", () => ({
    OwnProfileStore: {
        instance: {
            displayName: "Ernie",
            getHttpAvatarUrl: jest.fn().mockReturnValue("image.com/img"),
        },
    },
}));

describe("BwiFeature.locationShareTypesEnabled", () => {
    const userId = "@ernie:server.org";
    const mockClient = getBaseClient({
        ...getClientUserMocks(userId),
        getClientWellKnown: jest.fn().mockResolvedValue({
            map_style_url: "maps.com",
        }),
        sendMessage: jest.fn(),
        unstable_createLiveBeacon: jest.fn().mockResolvedValue({ event_id: "1" }),
        unstable_setLiveBeacon: jest.fn().mockResolvedValue({ event_id: "1" }),
        getVisibleRooms: jest.fn().mockReturnValue([]),
        isGuest: jest.fn().mockReturnValue(false),
    }) as MatrixClient;
    const defaultProps = {
        menuPosition: {
            top: 1,
            left: 1,
            chevronFace: ChevronFace.Bottom,
        },
        onFinished: jest.fn(),
        openMenu: jest.fn(),
        roomId: "!room:server.org",
        sender: new RoomMember("!room:server.org", userId),
    };

    beforeAll(() => {
        jest.spyOn(MatrixClientPeg, "get").mockReturnValue(mockClient as unknown as MatrixClient);
    });

    afterAll(() => {
        jest.clearAllMocks();
        jest.resetModules();
    });

    function renderComponent(): void {
        render(
            <MatrixClientContext.Provider value={mockClient}>
                <LocationShareMenu {...defaultProps} />
            </MatrixClientContext.Provider>,
        );
    }

    describe("LocationShareMenu", () => {
        it('should only display location share "Pin" option, should not ask for geolocation permission', async () => {
            const mockedFeatureFlags = mockFeatureFlag(
                new Map<string, any>([
                    [BwiFeature.LocationShareTypesEnabled, [LocationShareType.Pin]],
                    [BwiFeature.DisableGeolocation, true],
                ]),
            );

            await act(async () => {
                renderComponent();
            });

            // Expect: Map is instantly opened in 'Pin" mode. No options given
            expect(screen.queryByText("Unable to load map")).toBeInTheDocument();

            mockedFeatureFlags.mockRestore();
        });

        it("should display all configured location share types", async () => {
            await act(async () => {
                renderComponent();
            });

            // Expect: To open the selection for the share type
            expect(screen.queryByRole("button", { name: /My current location/ })).toBeInTheDocument();
            expect(screen.queryByRole("button", { name: "Drop a Pin" })).toBeInTheDocument();
            expect(screen.queryByRole("button", { name: "My live location" })).toBeInTheDocument();
        });
    });
});
