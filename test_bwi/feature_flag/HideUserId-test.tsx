// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { render, screen } from "jest-matrix-react";
import { User } from "matrix-js-sdk/src/matrix";
import React from "react";

import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import MemberTile from "../../src/components/views/rooms/MemberTile";
import * as TestUtils from "../../test/test-utils";
import { getRoomClientSpy } from "../mock/MockClient";
import { generateRoomId } from "../mock/MockRoom";
import { mockFeatureFlag } from "../mock/MockSettingsStore";
import { userToRoomMember } from "../test-utils/user";

describe("BwiFeature.HideUserId", () => {
    getRoomClientSpy(TestUtils.mkStubRoom(generateRoomId(), "roomName", undefined));

    afterAll(function () {
        TestUtils.unmockClientPeg();
    });

    function renderComponent(rawName: string, name: string): void {
        const user = new User("@user:server.org");
        const member = userToRoomMember(user);
        member.powerLevel = member.powerLevelNorm = 100;
        member.name = name;
        member.rawDisplayName = rawName;

        render(<MemberTile member={member} />);
    }

    describe("Default without feature flag", () => {
        it("regular user without id in name, just return name", () => {
            renderComponent("Alice", "Alice");

            expect(screen.queryByText(/^Alice$/)).toBeInTheDocument();
        });
    });

    describe("Feature Flag enabled", () => {
        let featureFlag: jest.SpyInstance;

        beforeAll(function () {
            featureFlag = mockFeatureFlag(BwiFeature.HideUserId, true);
        });

        afterAll(function () {
            featureFlag.mockRestore();
        });

        it("regular user without id in name, just return name", () => {
            renderComponent("Alice", "Alice");

            expect(screen.queryByText(/^Alice$/)).toBeInTheDocument();
        });

        it("regular user with id in name, just return name", () => {
            renderComponent("Alice", "Alice (@alice:server.org)");

            expect(screen.queryByText(/^Alice$/)).toBeInTheDocument();
        });
    });
});
