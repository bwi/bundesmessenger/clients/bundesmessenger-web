// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { act, render } from "jest-matrix-react";
import { UserVerificationStatus } from "matrix-js-sdk/src/crypto-api";
import { PendingEventOrdering, Room, RoomMember, MatrixClient } from "matrix-js-sdk/src/matrix";
import React from "react";

import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import RoomSummaryCard from "../../src/components/views/right_panel/RoomSummaryCard";
import UserInfo from "../../src/components/views/right_panel/UserInfo";
import MemberTile from "../../src/components/views/rooms/MemberTile";
import MatrixClientContext from "../../src/contexts/MatrixClientContext";
import { RightPanelPhases } from "../../src/stores/right-panel/RightPanelStorePhases";
import DMRoomMap from "../../src/utils/DMRoomMap";
import { getBaseClient, getClientUserMocks } from "../mock/MockClient";
import { mockDevice, mockDeviceMap, mockDeviceVerificationStatus } from "../mock/MockDevice";
import { mockRoomWithEvents } from "../mock/MockRoom";
import { mockFeatureFlag } from "../mock/MockSettingsStore";

const ROOM_ID = "#room:server.org";
const USER_ID = "@alice:server.org";

describe("BwiFeature.HideE2Eshields", () => {
    const { client, room, member } = mockTest();

    // Enable feature for all tests
    mockFeatureFlag(BwiFeature.HideE2EShields, true);

    describe("<MemberTile />", () => {
        it("should hide the e2e shield for a verified member in an encrypted room", async () => {
            // When feature is enabled
            await act(async () => {
                render(<MemberTile member={member} />);
            });

            // Expect the shield icon not to be rendered
            expect(document.querySelector(".mx_E2EIcon")).not.toBeInTheDocument();
        });
    });

    describe("<RoomSummaryCard />", () => {
        it("should hide the e2e shield", () => {
            // When feature is enabled
            render(
                <MatrixClientContext.Provider value={client as MatrixClient}>
                    <RoomSummaryCard room={room} permalinkCreator={undefined!} />
                </MatrixClientContext.Provider>,
            );

            // Expect the shield icon not to be rendered
            expect(document.querySelector(".mx_RoomSummaryCard_e2ee")).not.toBeInTheDocument();
        });
    });

    describe("<UserInfo />", () => {
        it("should hide the e2e shield", async () => {
            // When feature is enabled
            await act(async () => {
                render(
                    <MatrixClientContext.Provider value={client as MatrixClient}>
                        <UserInfo room={room} phase={RightPanelPhases.MemberInfo} user={member} onClose={() => {}} />
                    </MatrixClientContext.Provider>,
                );
            });

            // Expect the shield icon not to be rendered
            expect(document.querySelector(".mx_UserInfo_profile .mx_E2EIcon")).not.toBeInTheDocument();
        });
    });
});

function mockTest(): { client: Partial<MatrixClient>; room: Room; member: RoomMember } {
    // eslint-disable-next-line prefer-const
    let room: Room;
    const client = getBaseClient({
        ...getClientUserMocks(),
        isRoomEncrypted() {
            return true;
        },
        checkUserTrust() {
            return new UserVerificationStatus(true, true, true);
        },
        getRoom() {
            return room;
        },
        getCrypto() {
            return {
                async getUserDeviceInfo() {
                    return mockDeviceMap(USER_ID, mockDevice(USER_ID));
                },
                async getDeviceVerificationStatus() {
                    return mockDeviceVerificationStatus(true);
                },
                async getUserVerificationStatus() {
                    return new UserVerificationStatus(true, true, true);
                },
                async isEncryptionEnabledInRoom() {
                    return true;
                },
            };
        },
        store: {
            async getPendingEvents() {
                return [];
            },
        },
        getEventMapper() {
            return {};
        },
        async doesServerSupportExtendedProfiles() {
            return false;
        },
    });
    room = mockRoomWithEvents(
        {
            partials: { roomId: ROOM_ID },
            opts: {
                pendingEventOrdering: PendingEventOrdering.Detached,
            },
        },
        client as MatrixClient,
    );
    DMRoomMap.makeShared(client as MatrixClient);

    return { client, room, member: new RoomMember(ROOM_ID, "@alice:server.org") };
}
