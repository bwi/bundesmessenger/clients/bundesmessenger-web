// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";
import { act, cleanup, fireEvent, render, screen, waitForElementToBeRemoved } from "jest-matrix-react";
import { Mocked } from "jest-mock";
import { User, Device, IMyDevice, TypedEventEmitter } from "matrix-js-sdk/src/matrix";
import { CryptoApi, VerificationRequest, VerificationRequestEvent } from "matrix-js-sdk/src/crypto-api";

import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import SetupEncryptionBody from "../../src/components/structures/auth/SetupEncryptionBody";
import VerificationRequestDialog from "../../src/components/views/dialogs/VerificationRequestDialog";
import { mockFeatureFlag } from "../mock/MockSettingsStore";
import { MockUser } from "../mock/MockUser";
import { SetupEncryptionStore } from "../../src/stores/SetupEncryptionStore";
import { unmockClientPeg } from "../../test/test-utils";
import { getVerificationSpy } from "../mock/MockClient";

describe("BwiFeature.MandatoryCrossSigning", () => {
    const client = getVerificationSpy();

    (client.secretStorage?.isStored as unknown as jest.SpyInstance).mockResolvedValue(null);
    jest.spyOn(SetupEncryptionStore.prototype, "lostKeys").mockReturnValue(false);

    const otherDeviceId = "other_device";
    const myOtherDevice: IMyDevice = { device_id: otherDeviceId, last_seen_ip: "1.1.1.1" };
    (client.getDevice as unknown as jest.SpyInstance).mockResolvedValue(myOtherDevice);

    const otherDeviceInfo = new Device({
        algorithms: [],
        keys: new Map([[`curve25519:${otherDeviceId}`, "secure"]]),
        userId: "",
        deviceId: otherDeviceId,
        displayName: "my other device",
    });
    const deviceMap = new Map([[client.getSafeUserId?.(), new Map([[otherDeviceId, otherDeviceInfo]])]]);

    const mockCrypto = {
        getDeviceVerificationStatus: jest.fn().mockResolvedValue({
            crossSigningVerified: false,
            signedByOwner: true,
        }),
        getCrossSigningKeyId: jest.fn(),
        getUserDeviceInfo: jest.fn().mockResolvedValue(deviceMap),
        isCrossSigningReady: jest.fn().mockResolvedValue(true),
        isSecretStorageReady: jest.fn().mockResolvedValue(true),
        getVerificationRequestsToDeviceInProgress: () => [],
        requestOwnUserVerification: jest.fn(),
    } as unknown as Mocked<CryptoApi>;

    (client.getCrypto as unknown as jest.SpyInstance).mockReturnValue(mockCrypto);

    const user: User = new User(MockUser.userId);

    afterAll(() => {
        jest.clearAllMocks();
        jest.resetModules();
        unmockClientPeg();
    });

    describe("VerificationRequestDialog", () => {
        afterEach(() => {
            cleanup();
            document.body.querySelector("#mx_Dialog_StaticContainer")?.remove();
            document.body.querySelector("#mx_Dialog_Container")?.remove();
        });
        const checkCanCancelDialog = (shouldBeCancellable: boolean): void => {
            const mockVerificationRequest = makeMockVerificationRequest({
                isSelfVerification: true,
                otherDeviceId,
                cancel: async () => {},
                initiatedByMe: true,
            });

            const { container } = render(
                <VerificationRequestDialog
                    verificationRequest={mockVerificationRequest}
                    verificationRequestPromise={new Promise<VerificationRequest>(jest.fn())}
                    onFinished={jest.fn()}
                    member={user}
                />,
            );

            screen.debug();

            if (shouldBeCancellable) {
                expect(container.getElementsByClassName("mx_Dialog_cancelButton").length).toBe(1);
                expect(container.getElementsByClassName("bwi_VerificationRequest_cancel").length).toBe(0);
            } else {
                expect(container.getElementsByClassName("mx_Dialog_cancelButton").length).toBe(0);
                expect(container.getElementsByClassName("bwi_VerificationRequest_cancel").length).toBe(1);
            }
        };

        it("feature flag off, dialog has cancel button", () => {
            checkCanCancelDialog(true);
        });
        it("feature flag on, dialog should have no cancel button", () => {
            const featureFlag = mockFeatureFlag(BwiFeature.MandatoryCrossSigning, true);
            checkCanCancelDialog(false);
            featureFlag.mockRestore();
        });
    });

    describe("SetupEncryptionBody", () => {
        const prepareDialog = async (): Promise<void> => {
            const request = makeMockVerificationRequest({
                isSelfVerification: true,
                otherDeviceId,
                cancel: async () => {},
                initiatedByMe: true,
            });

            mockCrypto.requestOwnUserVerification.mockResolvedValue(request);

            await act(async () => {
                render(<SetupEncryptionBody onFinished={jest.fn()} />);
            });
            const verifyButton = await screen.findByTestId(
                "encryptionVerify",
                {},
                {
                    timeout: 1000,
                },
            );
            // Click verify button to open VerificationRequestDialog
            act(() => {
                fireEvent.click(verifyButton);
            });
        };

        const checkDialogCancelButton = async (shouldBeCancellable: boolean): Promise<void> => {
            const infoDialog = await screen.findByTestId("encryptionbody_fixture");
            expect(infoDialog).toBeDefined();
            if (shouldBeCancellable) expect(await screen.queryByLabelText("Close dialog")).toBeInTheDocument();
            else expect(await screen.queryByLabelText("Close dialog")).not.toBeInTheDocument();
        };

        const clickBackground = async (): Promise<void> => {
            const background = await screen.findByTestId("dialog-background");
            // Click background to check if dialog is mandatory
            act(() => {
                fireEvent.click(background);
            });
        };

        it("feature flag off, can skip the dialog", async () => {
            await prepareDialog();
            await checkDialogCancelButton(true);
            await clickBackground();
            // dialog is not mandatory so check if dialog closes
            await waitForElementToBeRemoved(() => screen.getByRole("dialog"));
        });

        it("feature flag on, no cancel button, can't skip dialog", async () => {
            const featureFlag = mockFeatureFlag(BwiFeature.MandatoryCrossSigning, true);
            await prepareDialog();
            await checkDialogCancelButton(false);
            await clickBackground();
            await waitForElementToBeRemoved(() => screen.getByRole("dialog")).catch((e) =>
                expect(e).toBeInstanceOf(Error),
            );
            featureFlag.mockRestore();
        });
    });
});

function makeMockVerificationRequest(props: Partial<VerificationRequest> = {}): Mocked<VerificationRequest> {
    const request = new TypedEventEmitter<VerificationRequestEvent, any>();
    Object.assign(request, {
        ...props,
    });
    return request as unknown as Mocked<VerificationRequest>;
}
