// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";
import { render, screen, RenderResult } from "jest-matrix-react";
import { mocked } from "jest-mock";

import SecureBackupPanel from "../../src/components/views/settings/SecureBackupPanel";
import { accessSecretStorage } from "../../src/SecurityManager";
import {
    flushPromises,
    getMockClientWithEventEmitter,
    mockClientMethodsCrypto,
    mockClientMethodsUser,
} from "../../test/test-utils";
import { mockFeatureFlag } from "../mock/MockSettingsStore";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";

jest.mock("../../src/SecurityManager", () => ({
    accessSecretStorage: jest.fn(),
}));

// extension of SecureBackupPanel-test.tsx from NV
describe("BwiFeature.SecureBackupSettings", () => {
    const userId = "@alice:server.org";
    const client = getMockClientWithEventEmitter({
        ...mockClientMethodsUser(userId),
        ...mockClientMethodsCrypto(),
        getKeyBackupEnabled: jest.fn(),
        getKeyBackupVersion: jest.fn().mockReturnValue("1"),
        isKeyBackupTrusted: jest.fn().mockResolvedValue(true),
        getClientWellKnown: jest.fn(),
        deleteKeyBackupVersion: jest.fn(),
        isKeyBackupKeyStored: () => Promise.resolve(true),
    });

    const getComponent = (): RenderResult => render(<SecureBackupPanel />);
    const altDescription =
        "If you have any issues with non-decryptable messages, it can help, to fetch your keys again. To do that, click on the button and wait until all your keys are restored. The app has to stay active during the process.";

    beforeEach(() => {
        const crypto = mocked(client.getCrypto()!);
        crypto.checkKeyBackupAndEnable.mockResolvedValue({
            backupInfo: {
                version: "1",
                algorithm: "test",
                auth_data: {
                    public_key: "1234",
                },
            },
            trustInfo: {
                matchesDecryptionKey: true,
                trusted: true,
            },
        });

        crypto.isKeyBackupTrusted.mockResolvedValue({
            trusted: true,
            matchesDecryptionKey: true,
        });

        crypto.getActiveSessionBackupVersion.mockResolvedValue("1");

        crypto.getSessionBackupPrivateKey.mockResolvedValue(new Uint8Array());

        mocked(client.secretStorage.hasKey).mockClear().mockResolvedValue(false);
        client.deleteKeyBackupVersion.mockClear().mockResolvedValue();
        client.getKeyBackupVersion.mockClear();
        client.isKeyBackupTrusted.mockClear();

        mocked(accessSecretStorage).mockClear().mockResolvedValue();
    });

    const checkRenderingWithFF = (): void => {
        expect(screen.getByText(altDescription)).toBeInTheDocument();
        expect(screen.queryByText("Algorithm:")).not.toBeInTheDocument();
        expect(screen.queryByText("Delete Backup")).not.toBeInTheDocument();
        expect(screen.queryByText("Reset")).not.toBeInTheDocument();
    };

    it("FF enabled, no backup, hide elements and alternative texts", async () => {
        const ff = mockFeatureFlag(BwiFeature.SecureBackupSettings, true);
        getComponent();
        await flushPromises();
        checkRenderingWithFF();
        ff.mockRestore();
    });

    it("FF enabled, backup available, hide elements and alternative texts", async () => {
        mocked(client.secretStorage.hasKey).mockClear().mockResolvedValue(true);
        const ff = mockFeatureFlag(BwiFeature.SecureBackupSettings, true);
        getComponent();
        await flushPromises();
        checkRenderingWithFF();
        ff.mockRestore();
    });
});
