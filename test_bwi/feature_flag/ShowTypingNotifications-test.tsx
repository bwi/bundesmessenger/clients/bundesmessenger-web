// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { SETTINGS } from "../../src/settings/Settings";

describe("ShowTypingNotifications", () => {
    it("Should be always enabled and not editable by not supporting any levels", () => {
        Object.entries(SETTINGS).filter(([key, value]) => {
            if (key === "showTypingNotifications") {
                expect(value.supportedLevels).toMatchObject([]);
                expect(value.default).toBe(true);
            }
        });
    });
});
