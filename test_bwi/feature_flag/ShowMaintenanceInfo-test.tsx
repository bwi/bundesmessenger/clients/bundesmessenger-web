// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { act, fireEvent, render, screen } from "jest-matrix-react";
import React, { ReactNode } from "react";

import { MatrixClientPeg } from "../../src/MatrixClientPeg";
import MediaDeviceHandler from "../../src/MediaDeviceHandler";
import { MaintenanceManager } from "../../src/bwi/managers/MaintenanceManager";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import LoggedInView from "../../src/components/structures/LoggedInView";
import UserMenu from "../../src/components/structures/UserMenu";
import Login from "../../src/components/structures/auth/Login";
import UserSettingsDialog from "../../src/components/views/dialogs/UserSettingsDialog";
import { SDKContext, SdkContextClass } from "../../src/contexts/SDKContext";
import ResizeNotifier from "../../src/utils/ResizeNotifier";
import { TestSdkContext } from "../../test/unit-tests/TestSdkContext";
import { mkServerConfig, mockPlatformPeg, unmockClientPeg, unmockPlatformPeg } from "../../test/test-utils";
import { getLoggedInViewClientSpy, getRequestClientSpy, getTempClientSpy } from "../mock/MockClient";
import {
    DowntimeEndedMock,
    OngoingMaintenanceMock,
    RegularMaintenanceMock,
} from "../../src/bwi/components/views/maintenance/MockMaintenanceInfo";
import { mockFeatureFlag } from "../mock/MockSettingsStore";
import SdkConfig from "../../src/SdkConfig";
import { ValidatedServerConfig } from "../../src/utils/ValidatedServerConfig";

jest.spyOn(MediaDeviceHandler, "loadDevices").mockResolvedValue();

jest.mock("../../src/components/structures/PipContainer", () => ({
    __esModule: true,
    PipContainer: () => ({
        render: () => <div data-testid="pipcontainer" />,
    }),
    default: () => ({
        render: () => <div data-testid="pipcontainer" />,
    }),
}));

function TestWrapper({ children }: { children: ReactNode }): React.JSX.Element {
    const context = new TestSdkContext();
    return <SDKContext.Provider value={context}>{children}</SDKContext.Provider>;
}

describe("BwiFeature.showMaintenanceInfo", () => {
    beforeAll(() => {
        Object.defineProperty(window, "matchMedia", {
            writable: true,
            value: jest.fn().mockImplementation((query) => ({
                matches: false,
                media: query,
                onchange: null,
                addListener: jest.fn(),
                removeListener: jest.fn(),
                addEventListener: jest.fn(),
                removeEventListener: jest.fn(),
                dispatchEvent: jest.fn(),
            })),
        });
    });

    afterEach(() => {
        MaintenanceManager.resetManager();
        jest.clearAllMocks();
        jest.resetModules();
    });

    describe("UserSettingsDialog", () => {
        beforeEach(() => {
            mockPlatformPeg();
            SdkConfig.add({
                default_country_code: "GB",
                validated_server_config: {
                    isUrl: "",
                } as ValidatedServerConfig,
            });
        });

        afterEach(() => {
            unmockClientPeg();
            unmockPlatformPeg();
        });

        async function renderComponent(): Promise<void> {
            const sdkContext = new SdkContextClass();
            sdkContext.client = MatrixClientPeg.safeGet();
            await act(async () => {
                render(<UserSettingsDialog onFinished={jest.fn()} sdkContext={sdkContext} />);
            });
        }

        it("valid maintenance info, feature flag true, should render", async () => {
            const featureFlag = mockFeatureFlag(
                new Map([
                    ["UIFeature.widgets", false],
                    [BwiFeature.ShowMaintenanceInfo, true],
                ]),
            );

            getRequestClientSpy(RegularMaintenanceMock);

            await MaintenanceManager.instance.getDowntimeInfo();

            await renderComponent();
            const content = screen.getByTestId("BwiMaintenance_Warning_Content");
            expect(content).toBeInTheDocument();
            expect(content.textContent).toContain("Messages may not be sent or received during that time.");
            expect(
                screen.queryByText("General", { selector: ".mx_TabbedView_Notification_pulse" }),
            ).not.toBeInTheDocument();

            featureFlag.mockRestore();
        });

        it("valid maintenance info, feature flag false, should not render", async () => {
            const featureFlag = mockFeatureFlag(
                new Map([
                    ["UIFeature.widgets", false],
                    [BwiFeature.ShowMaintenanceInfo, false],
                ]),
            );

            getRequestClientSpy(RegularMaintenanceMock);

            await MaintenanceManager.instance.getDowntimeInfo();

            await renderComponent();

            expect(screen.queryByTestId("BwiMaintenance_Warning_Content")).not.toBeInTheDocument();
            expect(
                screen.queryByText("General", { selector: ".mx_TabbedView_Notification_pulse" }),
            ).not.toBeInTheDocument();

            featureFlag.mockRestore();
        });

        it("invalid maintenance info, feature flag true, should not render", async () => {
            const featureFlag = mockFeatureFlag(
                new Map([
                    ["UIFeature.widgets", false],
                    [BwiFeature.ShowMaintenanceInfo, true],
                ]),
            );

            getRequestClientSpy(DowntimeEndedMock);

            await MaintenanceManager.instance.getDowntimeInfo();

            await renderComponent();

            expect(screen.queryByTestId("BwiMaintenance_Warning_Content")).not.toBeInTheDocument();
            expect(
                screen.queryByText("General", { selector: ".mx_TabbedView_Notification_pulse" }),
            ).not.toBeInTheDocument();

            featureFlag.mockRestore();
        });

        it(`valid maintenance info, feature flag true, banner already seen,
            should render banner but not notification`, async () => {
            const featureFlag = mockFeatureFlag(
                new Map([
                    ["UIFeature.widgets", false],
                    [BwiFeature.ShowMaintenanceInfo, true],
                ]),
            );

            getRequestClientSpy(RegularMaintenanceMock);
            const setSeenDowntimeSpy = jest
                .spyOn(MaintenanceManager.instance, "shouldSeeDowntimeNotification")
                .mockReturnValue(false);

            await MaintenanceManager.instance.getDowntimeInfo();

            await renderComponent();
            expect(screen.getByTestId("BwiMaintenance_Warning_Content")).toBeInTheDocument();
            expect(
                screen.queryByText("General", { selector: ".mx_TabbedView_Notification_pulse" }),
            ).not.toBeInTheDocument();

            featureFlag.mockRestore();
            setSeenDowntimeSpy.mockRestore();
        });
    });

    describe("UserMenu", () => {
        afterEach(() => {
            unmockClientPeg();
        });

        async function renderComponent(): Promise<void> {
            await act(async () => {
                render(
                    <TestWrapper>
                        <UserMenu isPanelCollapsed={false} />
                    </TestWrapper>,
                );
            });
        }

        async function openContextMenu(): Promise<void> {
            await act(async () => {
                fireEvent.click(screen.getByLabelText("User menu"), {
                    target: {
                        getBoundingClientRect: () => ({ width: 100, height: 100 }),
                    },
                });
            });
        }

        it("valid maintenance info, feature flag false, should not render", async () => {
            const featureFlag = mockFeatureFlag(BwiFeature.ShowMaintenanceInfo, false);
            getRequestClientSpy(RegularMaintenanceMock);

            await MaintenanceManager.instance.getDowntimeInfo();

            await renderComponent();

            await openContextMenu();

            expect(
                screen.queryByLabelText("All settings", { selector: ".mx_TabbedView_Notification_pulse" }),
            ).not.toBeInTheDocument();
            expect(document.querySelector(".mx_UserMenu_headerNotification")).not.toBeInTheDocument();

            featureFlag.mockRestore();
        });
        it("valid maintenance info, feature flag true, should render", async () => {
            const featureFlag = mockFeatureFlag(BwiFeature.ShowMaintenanceInfo, true);
            getRequestClientSpy(RegularMaintenanceMock);
            const setSeenSpy = jest.spyOn(MaintenanceManager.instance, "setSeenDowntime").mockReturnValue();

            await MaintenanceManager.instance.getDowntimeInfo();

            await renderComponent();

            await openContextMenu();
            const notificationMenu = screen.queryByLabelText("All settings", {
                selector: ".mx_TabbedView_Notification_pulse",
            });
            expect(notificationMenu).toBeInTheDocument();
            expect(document.querySelector(".mx_UserMenu_headerNotification_pulse")).toBeInTheDocument();

            act(() => {
                fireEvent.click(notificationMenu!);
            });

            expect(setSeenSpy).toHaveBeenCalledTimes(1);

            featureFlag.mockRestore();
            setSeenSpy.mockRestore();
        });

        it("invalid maintenance info, feature flag true, should not render", async () => {
            const featureFlag = mockFeatureFlag(BwiFeature.ShowMaintenanceInfo, true);
            getRequestClientSpy(DowntimeEndedMock);

            await MaintenanceManager.instance.getDowntimeInfo();

            await renderComponent();

            await openContextMenu();

            expect(
                screen.queryByLabelText("All settings", { selector: ".mx_TabbedView_Notification_pulse" }),
            ).not.toBeInTheDocument();
            expect(document.querySelector(".mx_UserMenu_headerNotification")).not.toBeInTheDocument();

            featureFlag.mockRestore();
        });
    });

    describe("Login", () => {
        async function renderComponent(): Promise<void> {
            await act(async () => {
                render(
                    <Login
                        serverConfig={mkServerConfig("https://mock.com", "https://idsrv.mock.com")}
                        onLoggedIn={jest.fn()}
                        onRegisterClick={jest.fn()}
                        onServerConfigChange={jest.fn()}
                    />,
                );
            });
        }
        it("valid maintenance info, feature flag false, should not render", async () => {
            const featureFlag = mockFeatureFlag(BwiFeature.ShowMaintenanceInfo, false);
            const clientSpy = getTempClientSpy(OngoingMaintenanceMock);

            await renderComponent();

            expect(screen.queryByText(/Messages may not be sent or received during that time/)).not.toBeInTheDocument();

            featureFlag.mockRestore();
            clientSpy.mockRestore();
        });

        it("valid maintenance info, feature flag true, should render", async () => {
            const featureFlag = mockFeatureFlag(BwiFeature.ShowMaintenanceInfo, true);
            const clientSpy = getTempClientSpy(OngoingMaintenanceMock);

            await renderComponent();

            expect(screen.queryByText(/Messages may not be sent or received during that time/)).toBeInTheDocument();

            featureFlag.mockRestore();
            clientSpy.mockRestore();
        });

        it("invalid maintenance info, feature flag true, should not render", async () => {
            const featureFlag = mockFeatureFlag(BwiFeature.ShowMaintenanceInfo, true);
            const clientSpy = getTempClientSpy(RegularMaintenanceMock);

            await renderComponent();

            expect(screen.queryByText(/Messages may not be sent or received during that time/)).not.toBeInTheDocument();

            featureFlag.mockRestore();
            clientSpy.mockRestore();
        });
    });

    describe("LoggedinView", () => {
        const config = {
            privacy_policy_url: "",
            brand: "Brand",
            element_call: {},
        };
        const resizeNotifier = new ResizeNotifier();

        async function renderComponent(): Promise<void> {
            const client = MatrixClientPeg.safeGet();
            await act(async () => {
                render(
                    <TestWrapper>
                        <LoggedInView
                            currentRoomId="!:1:room"
                            currentUserId={client.getUserId()}
                            config={config}
                            matrixClient={client}
                            onRegistered={jest.fn()}
                            collapseLhs={false}
                            hideToSRUsers={false}
                            resizeNotifier={resizeNotifier}
                        />
                    </TestWrapper>,
                );
            });
        }

        afterEach(() => {
            unmockClientPeg();
        });

        it("valid maintenance info, feature flag false, should not render", async () => {
            const featureFlag = mockFeatureFlag(BwiFeature.ShowMaintenanceInfo, false);
            getLoggedInViewClientSpy(OngoingMaintenanceMock);
            MaintenanceManager.instance;

            await renderComponent();
            expect(screen.queryByText(/Messages may not be sent or received during that time/)).not.toBeInTheDocument();

            featureFlag.mockRestore();
        });

        it("valid maintenance info, feature flag true, should render", async () => {
            const featureFlag = mockFeatureFlag(BwiFeature.ShowMaintenanceInfo, true);
            getLoggedInViewClientSpy(OngoingMaintenanceMock);

            await renderComponent();
            expect(screen.queryByText(/Messages may not be sent or received during that time/)).toBeInTheDocument();

            featureFlag.mockRestore();
        });

        it("invalid maintenance info, feature flag true, should not render", async () => {
            const featureFlag = mockFeatureFlag(BwiFeature.ShowMaintenanceInfo, true);
            getLoggedInViewClientSpy(RegularMaintenanceMock);

            await renderComponent();
            expect(screen.queryByText(/Messages may not be sent or received during that time/)).not.toBeInTheDocument();

            featureFlag.mockRestore();
        });
    });
});
