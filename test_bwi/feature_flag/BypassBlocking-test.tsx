// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { act, fireEvent, render, screen, waitFor } from "jest-matrix-react";
import React from "react";

import { mockPlatformPeg, unmockClientPeg, unmockPlatformPeg } from "../../test/test-utils";
import { mockFeatureFlag } from "../mock/MockSettingsStore";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import { getLoggedInViewClientSpy } from "../mock/MockClient";
import {
    BlockingMaintenanceMock,
    OngoingMaintenanceMock,
    RegularMaintenanceMock,
} from "../../src/bwi/components/views/maintenance/MockMaintenanceInfo";
import { ActiveMaintenance } from "../../src/bwi/components/views/maintenance/ActiveMaintenance";
import { MaintenanceManager, ServerMaintenanceInfo } from "../../src/bwi/managers/MaintenanceManager";
import { MatrixClientPeg } from "../../src/MatrixClientPeg";

describe("BypassBlocking", () => {
    let featureFlag: jest.SpyInstance;

    beforeEach(() => {
        mockPlatformPeg();
        if (featureFlag) featureFlag.mockRestore();
    });

    afterEach(() => {
        unmockClientPeg();
        unmockPlatformPeg();
        MaintenanceManager.resetManager();
        jest.clearAllMocks();
        jest.resetModules();
    });

    async function renderComponent(): Promise<void> {
        await act(async () => {
            render(<ActiveMaintenance destination="LoggedIn" />);
        });
    }

    async function testComponent(
        bypassBlocking: boolean,
        maintenanceMock: ServerMaintenanceInfo | undefined,
        isActive: boolean,
        isBlocking: boolean,
    ): Promise<void> {
        const features = new Map([
            ["UIFeature.widgets", false],
            [BwiFeature.ShowMaintenanceInfo, true],
        ]);
        if (bypassBlocking) features.set(BwiFeature.BypassBlockingButton, true);

        featureFlag = mockFeatureFlag(features);
        getLoggedInViewClientSpy(maintenanceMock);

        await renderComponent();

        await waitFor(() => {
            const activeWarning = screen.queryByTestId("BwiMaintenance_Active");

            if (isActive) expect(activeWarning).toBeInTheDocument();
            else expect(activeWarning).not.toBeInTheDocument();

            if (!isActive) return;

            if (isBlocking) {
                expect(activeWarning).toHaveClass("mx_BwiMaintenance_Active_Blocking");
                expect(activeWarning).not.toHaveClass("mx_BwiMaintenance_Active_NonBlocking");
            } else {
                expect(activeWarning).not.toHaveClass("mx_BwiMaintenance_Active_Blocking");
                expect(activeWarning).toHaveClass("mx_BwiMaintenance_Active_NonBlocking");
            }

            if (bypassBlocking) expect(screen.queryByText("Ignore")).toBeInTheDocument();
            else expect(screen.queryByText("Ignore")).not.toBeInTheDocument();
        });
    }

    it("no maintenance, show nothing", async () => {
        await testComponent(false, undefined, false, false);
    });

    it("not yet started maintenance, show nothing", async () => {
        await testComponent(false, RegularMaintenanceMock, false, false);
    });

    it("ongoing maintenance, show warning, non-blocking", async () => {
        await testComponent(false, OngoingMaintenanceMock, true, false);
    });

    it("blocking maintenance, show warning, blocking no bypass feature", async () => {
        await testComponent(false, BlockingMaintenanceMock, true, true);
    });

    it("blocking maintenance, show warning, blocking and bypass button", async () => {
        await testComponent(true, BlockingMaintenanceMock, true, true);
    });

    it("blocking maintenance, show warning, blocking and bypass button, press bypass", async () => {
        await testComponent(true, BlockingMaintenanceMock, true, true);

        fireEvent.click(screen.getByText("Ignore"));
        expect(MatrixClientPeg.get()?.http.disableBlocking).toHaveBeenCalledWith();
    });
});
