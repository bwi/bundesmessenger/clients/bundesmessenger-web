// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { render, screen } from "jest-matrix-react";
import { RoomMember, MatrixClient } from "matrix-js-sdk/src/matrix";
import React from "react";

import { AdminTool } from "../../src/bwi/AdminTools";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import { RoomAdminToolsContainer } from "../../src/components/views/right_panel/UserInfo";
import MatrixClientContext from "../../src/contexts/MatrixClientContext";
import { getBaseClient, getClientUserMocks } from "../mock/MockClient";
import { mockRoomWithEvents } from "../mock/MockRoom";
import { mockFeatureFlag } from "../mock/MockSettingsStore";

jest.mock("../../src/stores/room-list/RoomListStore", () => ({
    instance: {
        on: () => true,
    },
}));

jest.mock("../../src/stores/spaces/SpaceStore", () => ({
    spacesEnabled: false,
}));

describe("BwiFeature.adminToolsEnabled", () => {
    const roomId = "!room:server.org";

    const client = getBaseClient({
        ...getClientUserMocks(),
    }) as MatrixClient;

    const room = mockRoomWithEvents({
        partials: {
            roomId,
            getMember: () => {
                const member = new RoomMember(roomId, "@1337:server.org");
                member.powerLevel = 100;
                member.membership = "join";
                return member;
            },
        },
    });

    const otherMemeber = new RoomMember(roomId, "@1338:server.org");
    otherMemeber.powerLevel = 50;
    otherMemeber.membership = "join";

    function renderComponent(): void {
        render(
            <MatrixClientContext.Provider value={client}>
                <RoomAdminToolsContainer
                    powerLevels={{}}
                    room={room}
                    member={otherMemeber}
                    startUpdating={() => {}}
                    stopUpdating={() => {}}
                    isUpdating={false}
                />
            </MatrixClientContext.Provider>,
        );
    }

    afterAll(() => {
        jest.clearAllMocks();
        jest.resetModules();
    });

    it("should render all admintools by default", () => {
        renderComponent();
        expect(screen.queryByText("Remove from room")).toBeInTheDocument();
        expect(screen.queryByText("Ban from room")).toBeInTheDocument();
        expect(screen.queryByText("Remove messages")).toBeInTheDocument();
    });

    it("should render only ban if AdminToolsEnabled is set to ban only", () => {
        const flag = mockFeatureFlag(new Map<any, any>([[BwiFeature.AdminToolsEnabled, [AdminTool.Ban]]]));

        renderComponent();

        expect(screen.queryByText("Remove from room")).not.toBeInTheDocument();
        expect(screen.queryByText("Ban from room")).toBeInTheDocument();
        expect(screen.queryByText("Remove messages")).not.toBeInTheDocument();

        flag.mockRestore();
    });

    it("should render only ban & kick if AdminToolsEnabled is set to ban & kick only", () => {
        const flag = mockFeatureFlag(
            new Map<any, any>([[BwiFeature.AdminToolsEnabled, [AdminTool.Ban, AdminTool.Kick]]]),
        );

        renderComponent();

        expect(screen.queryByText("Remove from room")).toBeInTheDocument();
        expect(screen.queryByText("Ban from room")).toBeInTheDocument();
        expect(screen.queryByText("Remove recent messages")).not.toBeInTheDocument();

        flag.mockRestore();
    });
});
