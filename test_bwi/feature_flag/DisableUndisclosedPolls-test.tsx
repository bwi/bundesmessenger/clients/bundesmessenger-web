// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { Room } from "matrix-js-sdk/src/matrix";
import React from "react";
import { act, fireEvent, render, screen } from "jest-matrix-react";
import { PollStartEvent } from "matrix-js-sdk/src/extensible_events_v1/PollStartEvent";

import { MatrixClientPeg } from "../../src/MatrixClientPeg";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import PollCreateDialog from "../../src/components/views/elements/PollCreateDialog";
import MatrixClientContext from "../../src/contexts/MatrixClientContext";
import { getMockClientWithEventEmitter } from "../../test/test-utils";
import { mockFeatureFlag } from "../mock/MockSettingsStore";

describe("BwiFeature.disableUndisclosedPolls", () => {
    const mockClient = getMockClientWithEventEmitter({
        sendEvent: jest.fn().mockResolvedValue({ event_id: "1" }),
    });

    beforeEach(() => {
        mockClient.sendEvent.mockClear();
    });

    function renderComponent(props?: { onFinished?: (value: boolean) => void }): void {
        render(
            <MatrixClientContext.Provider value={mockClient}>
                <PollCreateDialog room={createRoom()} onFinished={() => {}} {...props} />
            </MatrixClientContext.Provider>,
        );
    }

    describe("PollCreateDialog", () => {
        it("should render poll type dropdown by default", () => {
            renderComponent();

            expect(screen.queryByText("Poll type")).toBeInTheDocument();
        });

        it("should not render poll type dropdown with feature flag enabled, default should be disclosed", async () => {
            mockFeatureFlag(BwiFeature.DisableUndisclosedPolls, true);
            const submitSpy = jest.fn();
            const createPollDialogSpy = jest.spyOn(PollStartEvent, "from");

            renderComponent({ onFinished: submitSpy });

            // Expect: Poll Type to be hidden
            expect(screen.queryByText("Poll type")).not.toBeInTheDocument();

            act(() => {
                fireEvent.change(screen.getByLabelText("Question or topic"), { target: { value: "Ananas on Pizza?" } });
            });
            act(() => {
                fireEvent.change(screen.getByLabelText("Option 1"), { target: { value: "Yes" } });
            });
            act(() => {
                fireEvent.change(screen.getByLabelText("Option 2"), { target: { value: "No" } });
            });

            await act(async () => {
                fireEvent.click(screen.getByText("Create Poll"));
            });

            // Expect: Poll to be submitted and of
            expect(submitSpy).toHaveBeenCalled();
            // Expect: Poll is disclosed-
            expect(createPollDialogSpy).toHaveBeenCalledWith(
                expect.anything(),
                expect.anything(),
                "org.matrix.msc3381.poll.disclosed",
                1,
                false,
            );
        });
    });
});

function createRoom(): Room {
    return new Room("roomid", MatrixClientPeg.safeGet(), "@name:example.com", {});
}
