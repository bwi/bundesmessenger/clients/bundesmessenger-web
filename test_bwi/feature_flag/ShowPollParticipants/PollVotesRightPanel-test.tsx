// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { MatrixClient, MatrixEvent, User, Room } from "matrix-js-sdk/src/matrix";
import { M_POLL_END, M_POLL_RESPONSE, M_POLL_START, M_TEXT, REFERENCE_RELATION } from "matrix-events-sdk";
import { RenderResult, act, fireEvent, render, screen, within } from "jest-matrix-react";
import React from "react";

import { getBaseClient, getClientUserMocks } from "../../mock/MockClient";
import { mockRoomWithEvents } from "../../mock/MockRoom";
import { mkEvent } from "../../../test/test-utils";
import BwiPollVotesRightPanel from "../../../src/bwi/components/right_panels/PollVotesRightPanel";
import { userToRoomMember } from "../../test-utils/user";
import dispatcher from "../../../src/dispatcher/dispatcher";
import { Action } from "../../../src/dispatcher/actions";

const roomId = "!room:server.org";

const POLL_START_ID = "pollstart";

function mockPollStartEvent(showParticipants?: boolean): MatrixEvent {
    return mkEvent({
        id: POLL_START_ID,
        event: true,
        type: M_POLL_START.name,
        user: "@alice:server.org",
        room: roomId,
        content: {
            [M_POLL_START.name]: {
                question: {
                    [M_TEXT.name]: "Favorite pet?",
                },
                kind: "m.poll.disclosed",
                max_selections: 1,
                answers: [
                    { id: "m.cat", [M_TEXT.name]: "Cat 🐈" },
                    { id: "m.dog", [M_TEXT.name]: "Dog 🐕" },
                    { id: "m.fish", [M_TEXT.name]: "Fish 🐟" },
                ],
            },
            ...(showParticipants !== undefined ? { show_participants: showParticipants } : {}),
        },
    });
}

function mockResponseEvent(user: User, answer: "m.cat" | "m.dog" | "m.fish", ts?: number): MatrixEvent {
    const event = mkEvent({
        event: true,
        type: M_POLL_RESPONSE.name,
        user: user.userId,
        room: roomId,
        content: {
            [M_POLL_RESPONSE.name]: {
                answers: [answer],
            },
            "m.relates_to": {
                event_id: POLL_START_ID,
                rel_type: REFERENCE_RELATION.name,
            },
        },
        ts,
    });

    if (ts) {
        event.getTs = () => ts;
    }

    return event;
}

function mockEndEvent(): MatrixEvent {
    return mkEvent({
        event: true,
        type: M_POLL_END.name,
        user: "@alice:server.org",
        content: {
            "m.relates_to": {
                event_id: POLL_START_ID,
                rel_type: REFERENCE_RELATION.name,
            },
            [M_POLL_END.name]: {},
            [M_TEXT.name]: "Poll ended",
        },
        room: roomId,
    });
}

const users = {
    alice: new User("@alice:server.org"),
    bob: new User("@bob:server.org"),
    julia: new User("@julia.server.org"),
    frederic: new User("@frederic:server.org"),
    maria: new User("@maria:server.org"),
    tim: new User("@tom:server.org"),
    linus: new User("@linus:server.org"),
    sara: new User("@sara:server.org"),
};

describe("PollVotesRightPanel", () => {
    let client: MatrixClient;

    async function setupRoom(events: MatrixEvent[], extraEvents: MatrixEvent[] = []): Promise<Room> {
        const members = Object.values(users).map((user) => userToRoomMember(user, roomId));
        const room = mockRoomWithEvents({
            events,
            partials: {
                roomId,
                getMember: (id: string) => members.find((m) => m.userId === id),
                getMembers: () => members,
                getJoinedMembers: () => members,
                getJoinedMemberCount: () => members.length,
            },
        });
        (client.getRoom as unknown as jest.SpyInstance).mockReturnValue(room);
        (client.relations as unknown as jest.SpyInstance).mockResolvedValue({
            events: [
                ...events.filter((event) => [M_POLL_RESPONSE.name, M_POLL_END.name].includes(event.getType() as any)),
                ...extraEvents,
            ],
            originalEvent: events.find((event) => event.getId() === POLL_START_ID),
        });
        await room.processPollEvents(events);
        return room;
    }

    beforeAll(() => {
        client = getBaseClient({
            ...getClientUserMocks(),
            getRoom: jest.fn(),
            decryptEventIfNeeded: () => Promise.resolve(),
            relations: jest.fn(),
        }) as MatrixClient;
    });

    beforeEach(() => {
        jest.spyOn(dispatcher, "dispatch").mockReset();
    });

    it("should render the view", async () => {
        const startEvent = mockPollStartEvent(true);
        await setupRoom([
            startEvent,
            mockResponseEvent(users.julia, "m.cat"),
            mockResponseEvent(users.alice, "m.dog"),
            mockResponseEvent(users.bob, "m.dog"),
            mockResponseEvent(users.frederic, "m.fish"),
        ]);

        await act(async () => {
            render(<BwiPollVotesRightPanel event={startEvent} />);
        });

        expect(document.body.innerHTML).toMatchSnapshot();
    });

    it("should show all votes for an answer after clicked on show all", async () => {
        const startEvent = mockPollStartEvent(true);
        const testsUsers = [users.julia, users.alice, users.frederic, users.bob, users.linus, users.tim];

        await setupRoom([startEvent, ...testsUsers.map((user) => mockResponseEvent(user, "m.cat"))]);

        await act(async () => {
            render(<BwiPollVotesRightPanel event={startEvent} />);
        });

        // Expect: Only the fitst 5 should be visible
        testsUsers.slice(0, 5).forEach((user) => {
            expect(screen.queryByText(user.userId)).toBeInTheDocument();
        });
        expect(screen.queryByText(testsUsers[5].userId)).not.toBeInTheDocument();

        // When: View all clicked
        act(() => {
            fireEvent.click(screen.getByText("View all (1 more)"));
        });

        // Expect: All 6 user votes should be visible
        testsUsers.forEach((user) => {
            expect(screen.queryByText(user.userId)).toBeInTheDocument();
        });
    });

    it("should only count the last user vote", async () => {
        const startEvent = mockPollStartEvent(true);
        await setupRoom([
            startEvent,
            mockResponseEvent(users.julia, "m.cat", 1),
            mockResponseEvent(users.julia, "m.dog", 2),
            mockResponseEvent(users.julia, "m.fish", 3),
            mockResponseEvent(users.frederic, "m.fish", 4),
        ]);

        let wrapper: RenderResult;
        await act(async () => {
            wrapper = render(<BwiPollVotesRightPanel event={startEvent} />);
        });

        const answerContainers: HTMLDivElement[] = Array.from(
            wrapper!.container.querySelectorAll(".bwi_PollVotesPanel_answer"),
        );
        const fishAnswerContainer = answerContainers.find((c) => !!within(c).queryByText("Fish 🐟"))!;

        // Expect Julias counted vote to be fish
        expect(within(fishAnswerContainer).queryByText("@julia.server.org")).toBeInTheDocument();
    });

    it("should still show results of unknown users", async () => {
        const startEvent = mockPollStartEvent(true);
        const carl = new User("@carl:server.org");
        carl.displayName = "";
        carl.rawDisplayName = "";

        await setupRoom([
            startEvent,
            mockResponseEvent(users.julia, "m.cat"),
            mockResponseEvent(users.alice, "m.dog"),
            mockResponseEvent(users.bob, "m.dog"),
            mockResponseEvent(users.frederic, "m.fish"),
            mockResponseEvent(new User("@ghost:server.org"), "m.fish"),
            mockResponseEvent(carl, "m.cat"),
        ]);

        await act(async () => {
            render(<BwiPollVotesRightPanel event={startEvent} />);
        });

        // Expect: Unknown user should still be visible
        expect(screen.queryByText("@ghost:server.org")).toBeInTheDocument();
        expect(screen.queryByText("@carl:server.org")).toBeInTheDocument();
    });

    it("should show the trophy icon for the winner after the poll has ended", async () => {
        const startEvent = mockPollStartEvent(true);
        await setupRoom([
            startEvent,
            mockResponseEvent(users.julia, "m.cat"),
            mockResponseEvent(users.alice, "m.dog"),
            mockResponseEvent(users.bob, "m.dog"),
            mockResponseEvent(users.frederic, "m.cat"),
            mockEndEvent(),
        ]);

        let wrapper: RenderResult;
        await act(async () => {
            wrapper = render(<BwiPollVotesRightPanel event={startEvent} />);
        });

        expect(wrapper!.container.querySelector(".mx_PollOption_winnerIcon")).toBeInTheDocument();
    });

    it("should dispatch ViewUser action", async () => {
        const startEvent = mockPollStartEvent(true);
        await setupRoom([
            startEvent,
            mockResponseEvent(users.julia, "m.cat"),
            mockResponseEvent(users.alice, "m.dog"),
            mockResponseEvent(users.bob, "m.dog"),
            mockResponseEvent(users.frederic, "m.fish"),
        ]);

        await act(async () => {
            render(<BwiPollVotesRightPanel event={startEvent} />);
        });

        const dispatchSpy = jest.spyOn(dispatcher, "dispatch");

        await act(async () => {
            fireEvent.click(screen.getByText("@julia.server.org"));
        });

        expect(dispatchSpy).toHaveBeenCalledWith({
            action: Action.ViewUser,
            push: true,
            member: expect.objectContaining({
                userId: "@julia.server.org",
            }),
        });
    });

    it("should format dates properly", async () => {
        const startEvent = mockPollStartEvent(true);
        const now = +new Date("2023-11-10:13:00");
        await setupRoom([
            startEvent,
            mockResponseEvent(users.julia, "m.cat", now - 8.64e7 * 2),
            mockResponseEvent(users.alice, "m.dog", now),
        ]);

        jest.useFakeTimers();
        jest.setSystemTime(now);

        await act(async () => {
            render(<BwiPollVotesRightPanel event={startEvent} />);
        });

        // Expect cat vote (two days before) to be fully formatted
        expect(screen.queryByText("Wed, Nov 8, 2023, 13:00")).toBeInTheDocument();

        // Expect dog vote (now) to be formatted as today
        expect(screen.queryByText("Today, 13:00")).toBeInTheDocument();

        jest.useRealTimers();
    });
});
