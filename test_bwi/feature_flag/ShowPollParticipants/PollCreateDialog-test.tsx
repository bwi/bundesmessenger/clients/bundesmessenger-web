// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { act, fireEvent, render, screen } from "jest-matrix-react";
import { MockedObject } from "jest-mock";
import { MatrixClient, Room, MatrixEvent } from "matrix-js-sdk/src/matrix";
import React from "react";
import { M_POLL_START, M_TEXT } from "matrix-events-sdk";
import { PollStartEvent } from "matrix-js-sdk/src/extensible_events_v1/PollStartEvent";

import { BwiFeature } from "../../../src/bwi/settings/BwiFeature";
import SettingsStore from "../../../src/settings/SettingsStore";
import { mockFeatureFlag } from "../../mock/MockSettingsStore";
import PollCreateDialog from "../../../src/components/views/elements/PollCreateDialog";
import { getBaseClient, getClientUserMocks } from "../../mock/MockClient";
import { mockRoomWithEvents } from "../../mock/MockRoom";
import { mkEvent, unmockClientPeg } from "../../../test/test-utils";
import Modal from "../../../src/Modal";

const roomId = "!room:server.org";

function mockEvent(showParticipants?: boolean): MatrixEvent {
    return mkEvent({
        event: true,
        type: M_POLL_START.name,
        user: "@alice:server.org",
        room: roomId,
        content: {
            [M_POLL_START.name]: {
                question: {
                    [M_TEXT.name]: "Favorite pet?",
                },
                kind: "m.poll.disclosed",
                max_selections: 1,
                answers: [
                    { id: "m.cat", [M_TEXT.name]: "Cat 🐈" },
                    { id: "m.dog", [M_TEXT.name]: "Dog 🐕" },
                    { id: "m.fish", [M_TEXT.name]: "Fish 🐟" },
                ],
                ...(showParticipants !== undefined ? { show_participants: showParticipants } : {}),
            },
        },
    });
}

describe("ShowPollParticipants", () => {
    describe("PollCreateDialog", () => {
        let client: MatrixClient;
        let room: MockedObject<Room>;

        beforeAll(() => {
            mockFeatureFlag(BwiFeature.ShowPollParticipants, true);

            client = getBaseClient({
                ...getClientUserMocks(),
                getRoom: () => room,
                sendEvent: jest.fn().mockResolvedValue(null),
            }) as MatrixClient;

            room = mockRoomWithEvents({ partials: { roomId } });
        });

        beforeEach(() => {
            (client.sendEvent as unknown as jest.SpyInstance).mockClear();
            document.querySelector(".mx_Dialog_StaticContainer")?.remove();
        });

        afterAll(() => {
            jest.spyOn(SettingsStore, "getValue").mockRestore();
            unmockClientPeg();
        });

        it("should show the toggle if the feature is enabled", () => {
            render(<PollCreateDialog onFinished={() => {}} room={room} />);

            expect(screen.queryByLabelText("Show who voted for which option")).toBeInTheDocument();
        });

        it("should create the event with show_participants set to true", async () => {
            Modal.createDialog(
                PollCreateDialog,
                {
                    room: room,
                },
                "mx_CompoundDialog",
                false,
                true,
            );

            // Wait for the modal to be open
            await screen.findByText("Create poll");

            // Check show participants to true
            act(() => {
                fireEvent.click(screen.getByLabelText("Show who voted for which option"));
            });

            // Fill in question and possible answers
            act(() => {
                fireEvent.change(screen.getByLabelText("Question or topic"), { target: { value: "Favorite pet?" } });
            });
            act(() => {
                fireEvent.change(screen.getByLabelText("Option 1"), { target: { value: "Cat 🐈" } });
            });
            act(() => {
                fireEvent.change(screen.getByLabelText("Option 2"), { target: { value: "Dog 🐕" } });
            });

            await act(async () => {
                fireEvent.click(screen.getByRole("button", { name: "Create Poll" }));
            });

            expect(client.sendEvent).toHaveBeenCalledWith(
                expect.anything(),
                null,
                expect.anything(),
                expect.objectContaining({
                    [M_POLL_START.name]: expect.objectContaining({
                        show_participants: true,
                    }),
                }),
            );
        });
    });
    describe("PollStartEvent", () => {
        describe("parse", () => {
            it("should parse all values from the event", () => {
                const pollStartEvent = new PollStartEvent(mockEvent(true).event as any);

                expect(pollStartEvent.bwiShowParticipants).toBe(true);
            });
            it("should fallback to fals if event data is empty", () => {
                const pollStartEvent = new PollStartEvent(mockEvent().event as any);
                expect(pollStartEvent.bwiShowParticipants).toBe(false);
            });
        });
        describe("serialize", () => {
            it("should carry all values in the event content", () => {
                const pollStartEvent = new PollStartEvent(mockEvent(true).event as any);
                const content = pollStartEvent.serialize() as any;

                expect(content.content[M_POLL_START.name]).toEqual(
                    expect.objectContaining({
                        show_participants: true,
                    }),
                );
            });
        });
    });
});
