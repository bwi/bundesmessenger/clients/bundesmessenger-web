// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { ICreateRoomOpts as ICreateRoomRequest, MatrixClient } from "matrix-js-sdk/src/matrix";
import { EventType } from "matrix-events-sdk";

import { mockFeatureFlag } from "../mock/MockSettingsStore";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import { adjustRoomCreateOpts } from "../../src/bwi/helper/Room";
import createRoom, { IOpts as ICreateRoomOpts } from "../../src/createRoom";
import * as testUtils from "../../test/test-utils";
import { MatrixClientPeg } from "../../src/MatrixClientPeg";

describe("HuddleCreationForAll", () => {
    const expectedBwiCallPowerlevels = {
        ["m.call"]: 0,
        ["m.call.member"]: 0,
        ["org.matrix.msc3401.call"]: 0,
        ["org.matrix.msc3401.call.member"]: 0,
    };

    const expectedNvCallPowerlevels = {
        ["org.matrix.msc3401.call"]: 100,
        ["org.matrix.msc3401.call.member"]: 0,
    };

    function getOpts(): Partial<ICreateRoomOpts> {
        return {
            createOpts: {
                name: "test",
            },
        };
    }

    it("should not change power levels by default", () => {
        const ff = mockFeatureFlag(BwiFeature.HuddleCreationForAll, false);

        const opts = adjustRoomCreateOpts(getOpts());
        expect(opts.createOpts?.power_level_content_override).toEqual(undefined);

        ff.mockRestore();
    });

    it("should add call power levels when ff is on", () => {
        const ff = mockFeatureFlag(BwiFeature.HuddleCreationForAll, true);

        const opts = adjustRoomCreateOpts(getOpts());

        expect(opts.createOpts!.power_level_content_override!.events).toEqual(
            expect.objectContaining(expectedBwiCallPowerlevels),
        );

        ff.mockRestore();
    });

    describe("createRoom", () => {
        let client: MatrixClient;

        const createRoomMock = jest.fn().mockResolvedValue(false);
        beforeAll(() => {
            testUtils.stubClient();
            client = MatrixClientPeg.safeGet();
            client.createRoom = createRoomMock;
            jest.spyOn(MatrixClientPeg, "get").mockReturnValue(client);
        });

        beforeEach(() => {
            createRoomMock.mockClear();
        });

        it("without any ffs, should not alter call power levels", async () => {
            const createOpts: ICreateRoomOpts = getOpts();

            await createRoom(client, createOpts);
            expect(createRoomMock.mock.calls[0][0].power_level_content_override).toEqual(undefined);
        });

        it("only huddle creation flag, should set power levels to 0 for all call events", async () => {
            const createOpts: ICreateRoomOpts = getOpts();

            const featureFlag = mockFeatureFlag(BwiFeature.HuddleCreationForAll, true);
            await createRoom(client, createOpts);
            expect(getPowerLevelEventsFromCreateRequest(createRoomMock.mock.calls[0][0])).toEqual(
                expect.objectContaining(expectedBwiCallPowerlevels),
            );
            featureFlag.mockRestore();
        });

        it("only group call flag, should set power levels to 100 as it is nv default", async () => {
            const createOpts: ICreateRoomOpts = getOpts();

            const featureFlag = mockFeatureFlag("feature_group_calls", true);
            await createRoom(client, createOpts);
            expect(getPowerLevelEventsFromCreateRequest(createRoomMock.mock.calls[0][0])).toEqual(
                expect.objectContaining(expectedNvCallPowerlevels),
            );
            featureFlag.mockRestore();
        });

        it("group call and huddle flags, huddle flag power levels take precedence", async () => {
            const createOpts: ICreateRoomOpts = getOpts();

            const featureFlagNv = mockFeatureFlag("feature_group_calls", true);
            const featureFlagBwi = mockFeatureFlag(BwiFeature.HuddleCreationForAll, true);
            await createRoom(client, createOpts);
            expect(getPowerLevelEventsFromCreateRequest(createRoomMock.mock.calls[0][0])).toEqual(
                expect.objectContaining(expectedBwiCallPowerlevels),
            );
            featureFlagNv.mockRestore();
            featureFlagBwi.mockRestore();
        });

        const getPowerLevelEventsFromCreateRequest = (
            opts: ICreateRoomRequest,
            // @ts-ignore TS2344
        ): Record<EventType | string, number> | undefined => {
            return opts.power_level_content_override!.events;
        };
    });
});
