// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { act, cleanup, fireEvent, render, screen } from "jest-matrix-react";
import React from "react";
import { MatrixClient } from "matrix-js-sdk/src/matrix";
import { Mocked, mocked } from "jest-mock";
import { CryptoApi } from "matrix-js-sdk/src/crypto-api";

import DeviceListener from "../../src/DeviceListener";
import { crossSigningCallbacks } from "../../src/SecurityManager";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import CompleteSecurity from "../../src/components/structures/auth/CompleteSecurity";
import { SetupEncryptionStore } from "../../src/stores/SetupEncryptionStore";
import * as WellKnownUtils from "../../src/utils/WellKnownUtils";
import { getEncryptionClient } from "../mock/MockClient";
import { KeyInfo } from "../mock/MockEncryption";
import { mockFeatureFlag } from "../mock/MockSettingsStore";

function cleanDOM(): void {
    document.querySelector("#mx_Dialog_StaticContainer")?.remove();
    document.querySelector("#mx_Dialog_Container")?.remove();
    cleanup();
}

describe("BwiFeature.EnforcePassphrase", () => {
    let client: MatrixClient;

    beforeEach(() => {
        client = getEncryptionClient(false) as MatrixClient;
        const mockCrypto = {
            getDeviceVerificationStatus: jest.fn().mockResolvedValue({
                crossSigningVerified: false,
            }),
            getCrossSigningKeyId: jest.fn(),
            getUserDeviceInfo: jest.fn().mockResolvedValue(new Map()),
            isCrossSigningReady: jest.fn().mockResolvedValue(true),
            isSecretStorageReady: jest.fn().mockResolvedValue(false),
            getVerificationRequestsToDeviceInProgress: jest.fn().mockReturnValue([]),
            signedByOwner: true,
            userHasCrossSigningKeys: () => Promise.resolve(true),
            getCrossSigningStatus: async () => ({
                publicKeysOnDevice: true,
                privateKeysInSecretStorage: true,
                privateKeysCachedLocally: {
                    masterKey: true,
                    selfSigningKey: true,
                    userSigningKey: true,
                },
            }),
            getKeyBackupInfo: jest.fn().mockResolvedValue({}),
            isEncryptionEnabledInRoom: jest.fn().mockResolvedValue(true),
        } as unknown as Mocked<CryptoApi>;

        client.getCrypto = jest.fn().mockReturnValue(mockCrypto);
        mocked(client.secretStorage.isStored).mockResolvedValue(null);

        jest.spyOn(WellKnownUtils, "isSecureBackupRequired").mockReturnValue(true);
        jest.spyOn(SetupEncryptionStore.prototype, "lostKeys").mockReturnValue(false);
    });

    afterAll(() => {
        jest.clearAllMocks();
        jest.resetModules();
    });

    async function waitForDialog(): Promise<void> {
        await new Promise((done) => setTimeout(done, 200));
    }

    describe("DeviceListener", () => {
        beforeEach(() => {
            cleanDOM();
            jest.resetModules();
        });

        it("feature flag true, dialog shown, no cancel button, backgroundclick doesnt close", async () => {
            const featureFlag = mockFeatureFlag(BwiFeature.EnforcePassphrase, true);
            const deviceListener = new DeviceListener();
            deviceListener.start(client);

            await waitForDialog();

            expect(document.querySelector(".mx_SetupEncryptionDialog")).toBeInTheDocument();

            await act(async () => {
                fireEvent.click(screen.getByTestId("dialog-background"));
            });

            expect(document.querySelector(".mx_SetupEncryptionDialog")).toBeInTheDocument();
            expect(document.querySelector(".mx_QuestionDialog")).not.toBeInTheDocument();

            featureFlag.mockRestore();
            deviceListener.stop();
        });
    });

    describe("SecurityManager", () => {
        beforeEach(() => {
            jest.resetModules();
        });

        it("default, no feature flag set, backgroundClick closes dialog", async () => {
            await act(async () => {
                crossSigningCallbacks.getSecretStorageKey!({ keys: KeyInfo }, "key");
            });
            await waitForDialog();
            screen.debug();
            expect(document.querySelector(".mx_AccessSecretStorageDialog")).toBeInTheDocument();
            await act(async () => {
                fireEvent.click(document.querySelector(".mx_Dialog_background")!);
            });

            await waitForDialog();

            expect(document.querySelector(".mx_QuestionDialog")).toBeInTheDocument();
        });

        it("feature flag true, background click doesn't close", async () => {
            const featureFlag = mockFeatureFlag(BwiFeature.EnforcePassphrase, true);

            await act(async () => {
                crossSigningCallbacks.getSecretStorageKey!({ keys: KeyInfo }, "key");
            });
            await waitForDialog();

            expect(document.querySelector(".mx_AccessSecretStorageDialog")).toBeInTheDocument();

            await act(async () => {
                fireEvent.click(document.querySelector(".mx_Dialog_background")!);
            });

            await waitForDialog();

            expect(document.querySelector(".mx_SetupEncryptionDialog")).toBeInTheDocument();
            expect(document.querySelector(".mx_QuestionDialog")).not.toBeInTheDocument();

            featureFlag.mockRestore();
        });
    });

    describe("CompleteSecurity", () => {
        it("default, no feature flag set, skip button shown", async () => {
            const featureFlag = mockFeatureFlag(BwiFeature.EnforcePassphrase, false);
            await act(async () => {
                render(<CompleteSecurity onFinished={() => {}} />);
            });
            expect(screen.queryByLabelText("Skip verification for now")).toBeInTheDocument();
            featureFlag.mockRestore();
        });

        it("feature flag true, no skip button", async () => {
            const featureFlag = mockFeatureFlag(BwiFeature.EnforcePassphrase, true);
            await act(async () => {
                render(<CompleteSecurity onFinished={() => {}} />);
            });

            expect(screen.queryByLabelText("Skip verification for now")).not.toBeInTheDocument();
            featureFlag.mockRestore();
        });
    });
});
