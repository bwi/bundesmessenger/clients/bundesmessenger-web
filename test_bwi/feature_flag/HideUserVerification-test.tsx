// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { act, render, screen } from "jest-matrix-react";
import React from "react";
import { Room, RoomMember, MatrixClient } from "matrix-js-sdk/src/matrix";
import { UserVerificationStatus } from "matrix-js-sdk/src/crypto-api";

import UserInfo from "../../src/components/views/right_panel/UserInfo";
import { RightPanelPhases } from "../../src/stores/right-panel/RightPanelStorePhases";
import { getBaseClient, getClientUserMocks } from "../mock/MockClient";
import { mockRoomWithEvents } from "../mock/MockRoom";
import { mockDevice, mockDeviceMap } from "../mock/MockDevice";
import MatrixClientContext from "../../src/contexts/MatrixClientContext";
import DMRoomMap from "../../src/utils/DMRoomMap";
import { _t } from "../../src/languageHandler";
import { mockFeatureFlag } from "../mock/MockSettingsStore";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";

const ME_ID = "@me:server.org";
const USER_ID = "@alice:server.org";
const ROOM_ID = "#room:server.org";

describe("BwiFeature.HideUserVerification", () => {
    const { room, client } = mockTest();

    // Enable feature for all tests
    mockFeatureFlag(BwiFeature.HideUserVerification, true);

    describe("<UserInfo />", () => {
        it("should hide user verification", async () => {
            await act(async () => {
                render(
                    <MatrixClientContext.Provider value={client as MatrixClient}>
                        <UserInfo
                            phase={RightPanelPhases.MemberInfo}
                            user={new RoomMember(ROOM_ID, USER_ID)}
                            room={room}
                            onClose={() => {}}
                        />
                    </MatrixClientContext.Provider>,
                );
            });

            // Expect the 'Verify' button not to be rendered
            expect(screen.queryByRole("button", { name: _t("action|verify") })).not.toBeInTheDocument();
        });
    });
});

function mockTest(): { client: Partial<MatrixClient>; room: Room } {
    // eslint-disable-next-line prefer-const
    let room: Room;
    const client = getBaseClient({
        ...getClientUserMocks(ME_ID),
        getRoom() {
            return room;
        },
        // enable crypto and devices
        isCryptoEnabled() {
            return true;
        },
        isRoomEncrypted() {
            return true;
        },
        async doesServerSupportUnstableFeature(feature: string) {
            return feature === "org.matrix.e2e_cross_signing";
        },
        getCrypto() {
            return {
                async getUserDeviceInfo() {
                    return mockDeviceMap(USER_ID, mockDevice(USER_ID));
                },
                async getUserVerificationStatus() {
                    return new UserVerificationStatus(false, false, false);
                },
                async getDeviceVerificationStatus() {
                    return new UserVerificationStatus(false, false, false);
                },
                async userHasCrossSigningKeys() {
                    return true;
                },
                async isEncryptionEnabledInRoom() {
                    return true;
                },
            };
        },
        // mark user as unverified/untrusted
        checkUserTrust() {
            return new UserVerificationStatus(false, false, false);
        },
        async doesServerSupportExtendedProfiles() {
            return false;
        },
    });
    room = mockRoomWithEvents({
        partials: { roomId: ROOM_ID },
    });

    DMRoomMap.makeShared(client as MatrixClient);

    return { client, room };
}
