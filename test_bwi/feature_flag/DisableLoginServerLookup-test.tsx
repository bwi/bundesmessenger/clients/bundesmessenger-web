// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { fireEvent, render, screen } from "jest-matrix-react";
import fetchMockJest from "fetch-mock-jest";
import React from "react";
import { act } from "react-dom/test-utils"; // eslint-disable-line deprecate/import

import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import Login from "../../src/components/structures/auth/Login";
import * as testUtils from "../../test/test-utils";
import { getLoginSpy } from "../mock/MockClient";
import { mockFeatureFlag } from "../mock/MockSettingsStore";

describe("BwiFeature.DisableLoginServerLookup", () => {
    beforeAll(() => {
        getLoginSpy();
    });

    beforeEach(() => {
        fetchMockJest.restore();

        fetchMockJest.get("https://matrix.org/_matrix/client/versions", {
            unstable_features: {},
            versions: ["v1.1"],
        });

        fetchMockJest.get("https://vector.im/_matrix/identity/v2", {});

        fetchMockJest.get("https://matrix.org/_matrix/client/v3/login", {
            flows: [
                {
                    type: "m.login.password",
                },
            ],
        });

        fetchMockJest.get("https://loltrix.org/.well-known/matrix/client", {
            status: 500,
        });
    });

    afterAll(() => {
        testUtils.unmockClientPeg();
    });

    describe("Login", () => {
        it("should show the default server picker", async () => {
            await act(async () => {
                render(
                    <Login
                        serverConfig={testUtils.mkServerConfig("https://matrix.org", "https://vector.im")}
                        onLoggedIn={() => {}}
                        onRegisterClick={() => {}}
                        onServerConfigChange={() => {}}
                    />,
                );
            });

            expect(screen.queryByText("Homeserver")).toBeInTheDocument();

            await act(async () => {
                fireEvent.blur(screen.getByLabelText("Username"), {
                    target: {
                        value: "@alice:loltrix.org",
                    },
                });
            });

            expect(fetchMockJest.lastCall()?.[0]).toBe("https://loltrix.org/.well-known/matrix/client");
        });

        it("feature flag set, serverlookup is not executed,´serverpicker is hidden", async () => {
            const featureFlag = mockFeatureFlag(BwiFeature.DisableLoginServerLookup, true);

            await act(async () => {
                render(
                    <Login
                        serverConfig={testUtils.mkServerConfig("https://matrix.org", "https://vector.im")}
                        onLoggedIn={() => {}}
                        onRegisterClick={() => {}}
                        onServerConfigChange={() => {}}
                    />,
                );
            });

            expect(screen.queryByText("Homeserver")).not.toBeInTheDocument();

            await act(async () => {
                fireEvent.blur(screen.getByLabelText("Username"), {
                    target: {
                        value: "@alice:loltrix.org",
                    },
                });
            });

            expect(fetchMockJest.lastCall()?.[0]).not.toBe("https://loltrix.org/.well-known/matrix/client");

            featureFlag.mockReset();
        });
    });
});
