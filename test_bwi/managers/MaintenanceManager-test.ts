// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { ClientEvent, Method, SyncState } from "matrix-js-sdk/src/matrix";
import { renderHook } from "@testing-library/react-hooks";
import { act } from "jest-matrix-react";

import { MatrixClientPeg } from "../../src/MatrixClientPeg";
import { timeStampToISO } from "../../src/bwi/components/views/maintenance/MaintenanceHelper";
import {
    BlockingMock,
    BlockingNotActiveMock,
    DowntimeClosestMock,
    DowntimeEndedMock,
    DowntimeOngoingMock,
    DowntimeWarningMock,
    RegularMaintenanceMock,
    RegularMaintenanceParsedMock,
    InvalidWarningGTEndingMock,
    InvalidWarningGTStartMock,
    InvalidStartingGTEnding,
} from "../../src/bwi/components/views/maintenance/MockMaintenanceInfo";
import {
    ActiveDowntimeInfo,
    DowntimeInfo,
    MaintenanceManager,
    REFRESH_INTERVAL,
    ServerDowntimeInfo,
    ServerMaintenanceInfo,
    useMaintenanceManager,
} from "../../src/bwi/managers/MaintenanceManager";
import { unmockClientPeg } from "../../test/test-utils";
import { getBlockingClientSpy, getRequestClientSpy } from "../mock/MockClient";

function makeServerInfo(downtime: Partial<ServerDowntimeInfo>): ServerMaintenanceInfo {
    return {
        ...RegularMaintenanceMock,
        downtime: [{ ...DowntimeWarningMock, ...downtime }],
    };
}

describe("MaintenanceManager", () => {
    const dtToServerDt = (downtime: DowntimeInfo): ServerDowntimeInfo => ({
        end_time: timeStampToISO(downtime.end_time),
        start_time: timeStampToISO(downtime.start_time),
        warning_start_time: timeStampToISO(downtime.warning_start_time),
        description: downtime.description,
        type: downtime.type,
    });

    afterAll(() => {
        jest.clearAllMocks();
        jest.resetModules();
    });

    afterEach(() => {
        MaintenanceManager.resetManager();
    });

    describe("findClosestDownTime", () => {
        afterEach(() => {
            unmockClientPeg();
        });

        it("array with one regular downtime, DowntimeWarningMock", () => {
            getRequestClientSpy(RegularMaintenanceMock);

            const maintenanceInfo = { ...RegularMaintenanceMock };
            maintenanceInfo.downtime = [DowntimeWarningMock];

            const result = MaintenanceManager.findClosestDowntime(maintenanceInfo, false)!;
            expect(dtToServerDt(result)).toEqual(DowntimeWarningMock);
            expect(dtToServerDt(result)).not.toEqual(DowntimeEndedMock);
        });

        it("array with two downtimes, DowntimeWarningMock and DownTimeEndedMock", () => {
            const maintenanceInfo = { ...RegularMaintenanceMock };
            maintenanceInfo.downtime = [DowntimeWarningMock, DowntimeEndedMock];
            const result = MaintenanceManager.findClosestDowntime(maintenanceInfo, false)!;
            expect(dtToServerDt(result)).toEqual(DowntimeWarningMock);
            expect(dtToServerDt(result)).not.toEqual(DowntimeEndedMock);

            maintenanceInfo.downtime = [DowntimeEndedMock, DowntimeWarningMock];
            const resultReverse = MaintenanceManager.findClosestDowntime(maintenanceInfo, false)!;
            expect(dtToServerDt(resultReverse)).toEqual(DowntimeWarningMock);
            expect(dtToServerDt(resultReverse)).not.toEqual(DowntimeEndedMock);
        });

        it(`array with two valid downtimes, DowntimeWarningMock
            and DowntimeClosestMock, DowntimeClosest should be chosen`, () => {
            const maintenanceInfo = { ...RegularMaintenanceMock };
            maintenanceInfo.downtime = [DowntimeWarningMock, DowntimeClosestMock];
            const result = MaintenanceManager.findClosestDowntime(maintenanceInfo, false)!;
            expect(dtToServerDt(result)).toEqual(DowntimeClosestMock);
            expect(dtToServerDt(result)).not.toEqual(DowntimeWarningMock);

            maintenanceInfo.downtime = [DowntimeClosestMock, DowntimeWarningMock];
            const resultReverse = MaintenanceManager.findClosestDowntime(maintenanceInfo, false)!;
            expect(dtToServerDt(resultReverse)).toEqual(DowntimeClosestMock);
            expect(dtToServerDt(resultReverse)).not.toEqual(DowntimeWarningMock);
        });

        it("array with no valid (downtime expired) downtimes, should return undefined", () => {
            const maintenanceInfo = { ...RegularMaintenanceMock };
            maintenanceInfo.downtime = [DowntimeEndedMock];
            const result = MaintenanceManager.findClosestDowntime(maintenanceInfo, false);
            expect(result).toBeNull();
        });
        it("empty array, should return undefined", () => {
            const maintenanceInfo = { ...RegularMaintenanceMock };
            maintenanceInfo.downtime = [];
            const result = MaintenanceManager.findClosestDowntime(maintenanceInfo, false);
            expect(result).toBeNull();
        });
        it("no array, should return undefined", () => {
            const maintenanceInfo = { ...RegularMaintenanceMock };
            maintenanceInfo.downtime = [];
            const result = MaintenanceManager.findClosestDowntime(maintenanceInfo, false);
            expect(result).toBeNull();
        });
    });

    describe("shouldSeeDowntimeNotification", () => {
        afterEach(() => {
            unmockClientPeg();
        });
        it("no downtime info, endpoint not returning valid downtime, return false", () => {
            getRequestClientSpy(RegularMaintenanceMock);

            const result = MaintenanceManager.instance.shouldSeeDowntimeNotification();
            expect(result).toBe(false);
        });

        it("valid downtime info, nothin in localstorage, return true", async () => {
            getRequestClientSpy(RegularMaintenanceMock);
            await MaintenanceManager.instance.getDowntimeInfo();
            const result = MaintenanceManager.instance.shouldSeeDowntimeNotification();
            expect(result).toBe(true);
        });

        it("valid downtime info, timestamp in localstorage and alreadyseen boolean false, return true", async () => {
            getRequestClientSpy(RegularMaintenanceMock);
            await MaintenanceManager.instance.getDowntimeInfo();
            MaintenanceManager.instance.setSeenDowntime(false);
            const result = MaintenanceManager.instance.shouldSeeDowntimeNotification();
            const resultAlreadySeen = MaintenanceManager.instance.hasSeenDowntimeNotification();

            expect(result).toBe(true);
            expect(resultAlreadySeen).toBe(false);
        });

        it("valid downtime info, timestamp in localstorage and alreadyseen boolean true, return false", async () => {
            getRequestClientSpy(RegularMaintenanceMock);
            await MaintenanceManager.instance.getDowntimeInfo();
            MaintenanceManager.instance.setSeenDowntime(true);
            const result = MaintenanceManager.instance.shouldSeeDowntimeNotification();
            const resultAlreadySeen = MaintenanceManager.instance.hasSeenDowntimeNotification();

            expect(result).toBe(true);
            expect(resultAlreadySeen).toBe(true);
        });

        it("invalid downtime info, timestamp in localstorage and alreadyseen boolean false, return false", async () => {
            const maintenanceInfo = { ...RegularMaintenanceMock };
            maintenanceInfo.downtime = [DowntimeEndedMock];
            getRequestClientSpy(maintenanceInfo);
            await MaintenanceManager.instance.getDowntimeInfo();
            MaintenanceManager.instance.setSeenDowntime(true);
            const result = MaintenanceManager.instance.shouldSeeDowntimeNotification();

            expect(result).toBe(false);
        });

        it("valid downtime info, localstorage not queried and alreadyseen boolean false, return true", async () => {
            getRequestClientSpy(RegularMaintenanceMock);
            await MaintenanceManager.instance.getDowntimeInfo();
            MaintenanceManager.instance.setSeenDowntime(false);
            MaintenanceManager.instance["downtimeSeen"] = {};
            const result = MaintenanceManager.instance.shouldSeeDowntimeNotification();

            expect(result).toBe(true);
        });

        it("valid downtime info, localstorage not queried and alreadyseen boolean true, return false", async () => {
            getRequestClientSpy(RegularMaintenanceMock);
            await MaintenanceManager.instance.getDowntimeInfo();
            MaintenanceManager.instance.setSeenDowntime(true);
            const result = MaintenanceManager.instance.shouldSeeDowntimeNotification();
            const resultAlreadySeen = MaintenanceManager.instance.hasSeenDowntimeNotification();

            expect(result).toBe(true);
            expect(resultAlreadySeen).toBe(true);
        });

        it("invalid downtime info, warning time > start time", async () => {
            getRequestClientSpy(makeServerInfo(InvalidWarningGTStartMock));
            await MaintenanceManager.instance.getDowntimeInfo();
            const result = MaintenanceManager.instance.shouldSeeDowntimeNotification();

            expect(result).toBe(false);
        });

        it("invalid downtime info, warning time > end time", async () => {
            getRequestClientSpy(makeServerInfo(InvalidWarningGTEndingMock));
            await MaintenanceManager.instance.getDowntimeInfo();
            const result = MaintenanceManager.instance.shouldSeeDowntimeNotification();

            expect(result).toBe(false);
        });

        it("invalid downtime info, start_time > end_time", async () => {
            getRequestClientSpy(makeServerInfo(InvalidStartingGTEnding));
            await MaintenanceManager.instance.getDowntimeInfo();
            const result = MaintenanceManager.instance.shouldSeeDowntimeNotification();

            expect(result).toBe(false);
        });
    });

    describe("shouldSeeDowntimeBanner", () => {
        afterEach(() => {
            unmockClientPeg();
        });
        it("valid downtime info, return true", async () => {
            getRequestClientSpy(RegularMaintenanceMock);
            await MaintenanceManager.instance.getDowntimeInfo();
            const result = MaintenanceManager.instance.shouldSeeDowntimeBanner();

            expect(result).toBe(true);
        });

        it("invalid downtime info, return false", async () => {
            const maintenanceInfo = { ...RegularMaintenanceMock };
            maintenanceInfo.downtime = [DowntimeEndedMock];
            getRequestClientSpy(maintenanceInfo);
            await MaintenanceManager.instance.getDowntimeInfo();
            const result = MaintenanceManager.instance.shouldSeeDowntimeBanner();

            expect(result).toBe(false);
        });

        it("invalid api request or no request at all, return false", () => {
            getRequestClientSpy(RegularMaintenanceMock);
            const result = MaintenanceManager.instance.shouldSeeDowntimeBanner();

            expect(result).toBe(false);
        });
    });

    describe("serverNotReachable", () => {
        const expectedNotReachableDowntime: DowntimeInfo = {
            type: "UNAVAILABLE",
            start_time: 0,
            end_time: 0,
            warning_start_time: 0,
        };

        beforeEach(() => {
            getRequestClientSpy(RegularMaintenanceMock);
        });

        afterEach(() => {
            unmockClientPeg();
        });

        it("client emits sync state Error, servernotReachable triggered, downtimeInfo object defined", async () => {
            await MaintenanceManager.instance.getDowntimeInfo();
            expect(MaintenanceManager.instance.shouldSeeActiveDowntime()).toEqual<ActiveDowntimeInfo>({
                active: false,
            });
            MatrixClientPeg.safeGet().emit(ClientEvent.Sync, SyncState.Error, SyncState.Catchup);
            expect(MaintenanceManager.instance.shouldSeeActiveDowntime()).toEqual<ActiveDowntimeInfo>({
                active: true,
                downtime: expectedNotReachableDowntime,
            });
        });

        it("client emits sync state Error, servernotReachable triggered, downtimeInfo object undefined", async () => {
            expect(MaintenanceManager.instance.shouldSeeActiveDowntime()).toEqual<ActiveDowntimeInfo>({
                active: false,
            });
            MatrixClientPeg.safeGet().emit(ClientEvent.Sync, SyncState.Error, SyncState.Catchup);
            expect(MaintenanceManager.instance.shouldSeeActiveDowntime()).toEqual<ActiveDowntimeInfo>({
                active: true,
                downtime: expectedNotReachableDowntime,
            });
        });

        it("client emits any other sync should trigger nothing for sanity", async () => {
            await MaintenanceManager.instance.getDowntimeInfo();
            expect(MaintenanceManager.instance.shouldSeeActiveDowntime()).toEqual<ActiveDowntimeInfo>({
                active: false,
            });
            MatrixClientPeg.safeGet().emit(ClientEvent.Sync, SyncState.Syncing, SyncState.Catchup);
            expect(MaintenanceManager.instance.shouldSeeActiveDowntime()).toEqual<ActiveDowntimeInfo>({
                active: false,
            });
        });
    });

    describe("refreshQuery", () => {
        beforeEach(() => {
            getRequestClientSpy(RegularMaintenanceMock);
        });

        afterEach(() => {
            unmockClientPeg();
        });

        it("instance created, query refresh called after REFRESH_INTERVAL, queryApi not called initially", async () => {
            jest.useFakeTimers({
                legacyFakeTimers: true,
            });
            const queryApiSpy = jest.spyOn<any, any>(MaintenanceManager.instance, "queryApi");

            expect(queryApiSpy).toHaveBeenCalledTimes(0);
            jest.advanceTimersByTime(REFRESH_INTERVAL);
            expect(queryApiSpy).toHaveBeenCalledTimes(1);

            queryApiSpy.mockClear();
            queryApiSpy.mockReset();
            jest.useRealTimers();
        });

        it("instance created, query refresh calles after REFRESH_INTERVAL, queryApi called initially", async () => {
            jest.useFakeTimers({
                legacyFakeTimers: true,
            });
            const queryApiSpy = jest.spyOn<any, any>(MaintenanceManager.instance, "queryApi").mockResolvedValue(true);

            await MaintenanceManager.instance.getDowntimeInfo();
            expect(queryApiSpy).toHaveBeenCalledTimes(1);
            jest.advanceTimersByTime(REFRESH_INTERVAL);
            expect(queryApiSpy).toHaveBeenCalledTimes(2);

            queryApiSpy.mockClear();
            queryApiSpy.mockReset();
            jest.useRealTimers();
        });
    });

    describe("blocking", () => {
        afterEach(() => {
            MaintenanceManager.resetManager();
        });
        const setupFetchStub = (data: ServerDowntimeInfo | null): jest.Mock => {
            return function fetchStub(_url) {
                return new Promise((resolve) => {
                    resolve({
                        json: () =>
                            Promise.resolve({
                                downtime: [data],
                            }),
                        ok: true,
                    });
                });
            } as jest.Mock;
        };

        const testBlocking = async (
            mock: ServerDowntimeInfo | null,
            loggedIn: boolean,
            shouldBlock: boolean,
            isBlockedInitially = false,
        ): Promise<void> => {
            const clientSpy = getBlockingClientSpy();
            const fetchSpy = jest.spyOn(global, "fetch").mockImplementation(setupFetchStub(mock));
            const clientImpl = clientSpy?.getMockImplementation();
            if (clientImpl) {
                const client = clientImpl();
                if (isBlockedInitially) client?.http.enableBlocking();

                if (loggedIn) {
                    await MaintenanceManager.instance.getDowntimeInfo();
                } else if (client) {
                    await MaintenanceManager.shouldShowBeforeLogin(client);
                }

                expect(fetchSpy).toHaveBeenCalledTimes(1);

                expect(client?.http.isBlocked()).toBe(shouldBlock);

                client?.http.request(Method.Get, "/mock");
                expect(fetchSpy).toHaveBeenCalledTimes(shouldBlock ? 1 : 2);

                fetchSpy.mockClear();
                fetchSpy.mockReset();
                clientSpy.mockClear();
                clientSpy.mockReset();
            }
        };

        it("logged in, regular maintenance, blocking not set, don't block query", async () => {
            await testBlocking(DowntimeOngoingMock, true, false);
        });

        it("logged in, blocking maintenance, blocking true, downtime not active, don't block query", async () => {
            await testBlocking(BlockingNotActiveMock, true, false);
        });

        it("logged in, blocking maintenance, blocking true, downtime active, block query", async () => {
            await testBlocking(BlockingMock, true, true);
        });

        it("logged in, should unblock after no more active blocking downtime", async () => {
            await testBlocking(BlockingNotActiveMock, true, false, true);
        });

        it("logged in, should unblock after incorrect downtime received", async () => {
            await testBlocking(null, true, false, true);
        });

        it("not logged in, regular maintenance, blocking not set, don't block query", async () => {
            await testBlocking(DowntimeOngoingMock, false, false);
        });

        it("not logged in, blocking maintenance, blocking true, downtime not active, don't block query", async () => {
            await testBlocking(BlockingNotActiveMock, false, false);
        });

        it("not logged in, blocking maintenance, blocking true, downtime active, block query", async () => {
            await testBlocking(BlockingMock, false, true);
        });

        it("not logged in, should unblock after no more active blocking downtime", async () => {
            await testBlocking(BlockingNotActiveMock, false, false, true);
        });

        it("not logged in, should unblock after incorrect downtime received", async () => {
            await testBlocking(null, false, false, true);
        });
    });

    describe("parse", () => {
        describe("add downtime", () => {
            it("is valid", () => {
                const info = MaintenanceManager.parse(RegularMaintenanceMock);
                expect(info).toEqual(RegularMaintenanceParsedMock);
            });
        });

        describe("omit downtime", () => {
            it("has invalid type", () => {
                const info = MaintenanceManager.parse(makeServerInfo({ type: "INVALID_TYPE" as any }));
                expect(info.downtime).toHaveLength(0);
            });

            it("has invalid start time", () => {
                const info = MaintenanceManager.parse(makeServerInfo({ start_time: "invalid_date" }));
                expect(info.downtime).toHaveLength(0);
            });

            it("has invalid end time", () => {
                const info = MaintenanceManager.parse(makeServerInfo({ end_time: "invalid_date" }));
                expect(info.downtime).toHaveLength(0);
            });

            it("has invalid warning time", () => {
                const info = MaintenanceManager.parse(makeServerInfo({ warning_start_time: "invalid_date" }));
                expect(info.downtime).toHaveLength(0);
            });

            it("is a ADHOC_MESSAGE without message content", () => {
                const info = MaintenanceManager.parse(makeServerInfo({ type: "ADHOC_MESSAGE", description: "" }));
                expect(info.downtime).toHaveLength(0);
            });
        });
    });

    describe("useMaintenanceManager()", () => {
        it("should listen to updates from the manager", async () => {
            const manager = new MaintenanceManager();
            const hookResult = renderHook(
                ({ manager }: { manager: MaintenanceManager }) => {
                    return useMaintenanceManager(manager);
                },
                {
                    initialProps: {
                        manager,
                    },
                },
            );

            const downtime: DowntimeInfo = {
                end_time: 0,
                start_time: 0,
                warning_start_time: 0,
                blocking: false,
                description: "Downtime",
                type: "MAINTENANCE",
            };

            // mock downtime
            manager["downtimeInfo"] = downtime;
            jest.spyOn(manager, "getDowntimeInfo").mockResolvedValue(downtime);

            // emit update
            await act(async () => {
                manager.emit("update", manager, downtime);
            });

            // expect hook to have updated result
            expect(hookResult.result.current.downtime).toEqual(downtime);
        });
    });
});
