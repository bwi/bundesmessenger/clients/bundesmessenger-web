// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { MatrixError } from "matrix-js-sdk/src/matrix";

import {
    BwiNotifications,
    BwiNotificationTypes,
    NOTIFICATION_DATA,
} from "../../src/bwi/components/notifications/BwiNotifications";
import { getBwiAnalyticsDialog } from "../../src/bwi/components/notifications/BwiAnalyticsDialog";
import { getBwiReleaseNotes } from "../../src/bwi/components/notifications/BwiReleaseNotes";
import { BwiNotificationManager } from "../../src/bwi/managers/BwiNotificationManager";
import { MatrixClientPeg } from "../../src/MatrixClientPeg";
import { getAccountDataSpy, getAuthRequestClientSpy, getRejectedAccountDataSpy } from "../mock/MockClient";
import {
    MultipleMockOneStillToShow,
    MultipleMockAllSeen,
    NothingSeenMock,
    showPrivacyMock,
    showReleaseNotesMock,
    showReleasePrivacyMock,
    getBwiAnalyticsDialogMockNonCustom,
} from "../mock/MockNotification";
import { BwiNotificationDialog } from "../../src/bwi/components/notifications/BwiNotificationRenderer";
import { AnalyticsSessionsAccountData } from "../../src/bwi/analytics/BwiAnalyticsManager";

describe("NotificationManager", () => {
    let clientSpy: jest.SpyInstance;
    let setDataSpy: jest.SpyInstance;

    afterAll(() => {
        jest.clearAllMocks();
        jest.resetModules();
    });

    afterEach(() => {
        BwiNotificationManager.resetManager();
        if (clientSpy) {
            clientSpy.mockReset();
        }
        if (setDataSpy) {
            setDataSpy.mockRestore();
        }
    });

    const runCheck = async (dialogs: BwiNotificationDialog[]): Promise<BwiNotificationDialog[]> => {
        const manager = BwiNotificationManager.instance;
        return manager.filterAlreadySeen(dialogs);
    };

    const verifyResult = (result: BwiNotificationDialog[], expected: BwiNotificationDialog[]): void => {
        expect(clientSpy).toHaveBeenCalledTimes(1);
        if (setDataSpy) {
            expect(setDataSpy).toHaveBeenCalledTimes(0);
        }

        expect(result).toStrictEqual(expected);
    };

    const prepareSpies = (accountData: BwiNotifications | AnalyticsSessionsAccountData): void => {
        getAccountDataSpy(accountData);
        setDataSpy = jest.spyOn(MatrixClientPeg.safeGet(), "setAccountData");
        clientSpy?.mockClear();
    };

    function getClientSpy(): jest.SpyInstance {
        return jest.spyOn(MatrixClientPeg, "safeGet");
    }

    describe("init and show", () => {
        it("Empty list of notifications, intitialize nothing", async () => {
            getAuthRequestClientSpy(NothingSeenMock);
            clientSpy = getClientSpy();
            const expected: BwiNotificationDialog[] = [];
            const result = await runCheck([]);
            verifyResult(result, expected);
        });

        describe("One notification without version", () => {
            it("nothing in the account data yet, set account data to show notification", async () => {
                prepareSpies({});

                const input = [getBwiAnalyticsDialogMockNonCustom()];
                const expected = await runCheck(input);
                verifyResult(input, expected);
            });

            it("account data call rejected, throws M_NOT_FOUND, show notification", async () => {
                getRejectedAccountDataSpy(new MatrixError({ errcode: "M_NOT_FOUND" }));
                clientSpy = getClientSpy();
                setDataSpy = jest.spyOn(MatrixClientPeg.safeGet(), "setAccountData");
                clientSpy.mockClear();
                const input = [getBwiAnalyticsDialogMockNonCustom()];
                const result = await runCheck(input);
                verifyResult(result, input);
            });

            it("account data call rejected, doesn't throw M_NOT_FOUND, don't show notification", async () => {
                getRejectedAccountDataSpy({ err: "mock" });
                clientSpy = getClientSpy();
                setDataSpy = jest.spyOn(MatrixClientPeg.safeGet(), "setAccountData");
                clientSpy.mockClear();
                const input = [getBwiAnalyticsDialogMockNonCustom()];
                const expected: BwiNotificationDialog[] = [];
                const result = await runCheck(input);
                verifyResult(result, expected);
            });

            it("already shown in the account data, empty result", async () => {
                prepareSpies(showPrivacyMock(false));
                const input = [getBwiAnalyticsDialogMockNonCustom()];
                const expected: BwiNotificationDialog[] = [];
                const result = await runCheck(input);
                verifyResult(result, expected);
            });

            it("already initialized and needs to be shown, should show Notification", async () => {
                prepareSpies(showPrivacyMock(true));

                const input = [getBwiAnalyticsDialogMockNonCustom()];
                const result = await runCheck(input);

                verifyResult(result, input);
            });
        });

        describe("One notification with version", () => {
            it("nothing in the account data yet, set account data to show notification", async () => {
                prepareSpies({});

                const releaseNotes = await getBwiReleaseNotes("mock");
                const input = [releaseNotes!];
                const result = await runCheck(input);

                verifyResult(result, input);
            });

            it("already shown in the account data, empty result", async () => {
                prepareSpies(showReleaseNotesMock(false));

                const releaseNotes = await getBwiReleaseNotes("mock");
                const expected: BwiNotificationDialog[] = [];
                const result = await runCheck([releaseNotes!]);

                verifyResult(result, expected);
            });

            it("already initialized and needs to be shown, should show Notification", async () => {
                prepareSpies(showReleaseNotesMock(true));

                const releaseNotes = await getBwiReleaseNotes("mock");
                const input = [releaseNotes!];
                const result = await runCheck(input!);

                verifyResult(result, input);
            });
        });

        describe("Multiple notifications", () => {
            it("nothing in the account data yet, set account data to show notification", async () => {
                prepareSpies({});

                const releaseNotes = await getBwiReleaseNotes("mock");
                const privacy = getBwiAnalyticsDialogMockNonCustom();
                const input = [releaseNotes!, privacy!];
                const result = await runCheck(input);

                verifyResult(result, input);
            });

            it("already shown in the account data, empty result", async () => {
                prepareSpies(showReleasePrivacyMock(false, false));

                const releaseNotes = await getBwiReleaseNotes("mock");
                const input = [releaseNotes!];
                const expected: BwiNotificationDialog[] = [];
                const result = await runCheck(input);

                verifyResult(result, expected);
            });

            it("already initialized and needs to be shown, should show Notification", async () => {
                prepareSpies(showReleasePrivacyMock(true, true));

                const releaseNotes = await getBwiReleaseNotes("mock");
                const privacy = getBwiAnalyticsDialogMockNonCustom();
                const input = [releaseNotes!, privacy!];
                const result = await runCheck(input);

                verifyResult(result, input);
            });
        });

        describe("Multiple notifications combinations", () => {
            it("non-version show and version not show", async () => {
                prepareSpies(showReleasePrivacyMock(true, false));

                const releaseNotes = await getBwiReleaseNotes("mock");
                const privacy = getBwiAnalyticsDialogMockNonCustom();
                const input = [releaseNotes!, privacy!];
                const expected = [privacy!];
                const result = await runCheck(input);

                verifyResult(result, expected);
            });

            it("version show and non-version not show", async () => {
                prepareSpies(showReleasePrivacyMock(false, true));

                const releaseNotes = await getBwiReleaseNotes("mock");
                const privacy = getBwiAnalyticsDialogMockNonCustom();
                const input = [releaseNotes!, privacy!];
                const expected = [releaseNotes!];
                const result = await runCheck(input);

                verifyResult(result, expected);
            });

            it("release notes seen, privacy still to show, everything except privacy seen", async () => {
                prepareSpies(MultipleMockAllSeen);

                const releaseNotes = await getBwiReleaseNotes("mock");
                const privacy = getBwiAnalyticsDialogMockNonCustom();
                const input = [releaseNotes!, privacy!];
                const expected = [privacy!];
                const result = await runCheck(input);

                verifyResult(result, expected);
            });

            it("release notes seen, privacy still to show, analytics and privacy not seen", async () => {
                prepareSpies(MultipleMockOneStillToShow);

                const releaseNotes = await getBwiReleaseNotes("mock");
                const privacy = getBwiAnalyticsDialogMockNonCustom();
                const input = [releaseNotes!, privacy!];
                const expected = [privacy!];
                const result = await runCheck(input);

                verifyResult(result, expected);
            });
        });

        describe("Notification has custom functions", () => {
            it("analytics custom logic, dialog not yet seen, show dialog", async () => {
                prepareSpies({});
                const input = [getBwiAnalyticsDialog()];
                const result = await runCheck(input);
                expect(clientSpy).toHaveBeenCalledTimes(2);
                if (setDataSpy) {
                    expect(setDataSpy).toHaveBeenCalledTimes(0);
                }
                expect(result).toStrictEqual(input);
            });

            it("analytics custom logic, already seen, empty result", async () => {
                prepareSpies({
                    ABCDEFGHI: {
                        time: 1234656345,
                        platform: "mock",
                        consent: true,
                        app_version: "1.mock",
                    },
                });
                const input = [getBwiAnalyticsDialog()];
                const expected: BwiNotificationDialog[] = [];
                const result = await runCheck(input);
                expect(clientSpy).toHaveBeenCalledTimes(2);
                if (setDataSpy) {
                    expect(setDataSpy).toHaveBeenCalledTimes(0);
                }
                expect(result).toStrictEqual(expected);
            });
            it("analytics custom logic, empty account data, show result", async () => {
                clientSpy = getClientSpy();
                getRejectedAccountDataSpy(new MatrixError({ errcode: "M_NOT_FOUND" }));
                const input = [getBwiAnalyticsDialog()];
                const result = await runCheck(input);
                expect(clientSpy).toHaveBeenCalledTimes(2);
                if (setDataSpy) {
                    expect(setDataSpy).toHaveBeenCalledTimes(0);
                }
                expect(result).toStrictEqual(input);
            });
        });
    });

    describe("finish notification", () => {
        it("finish feature without version", async () => {
            prepareSpies(MultipleMockAllSeen);

            const manager = BwiNotificationManager.instance;
            const privacy = getBwiAnalyticsDialogMockNonCustom();
            await manager.hasSeenNotification(privacy);
            expect(clientSpy).toHaveBeenCalledTimes(2);
            expect(setDataSpy).toHaveBeenCalledTimes(1);
            expect(setDataSpy).toHaveBeenCalledWith(NOTIFICATION_DATA, {
                ...MultipleMockAllSeen,
                [BwiNotificationTypes.HINT_ANALYTICS_NOTIFICATION]: false,
            });
        });

        it("finish feature with version", async () => {
            prepareSpies({});

            const manager = BwiNotificationManager.instance;
            const releaseNotes = await getBwiReleaseNotes("mock");
            await manager.hasSeenNotification(releaseNotes!);

            expect(clientSpy).toHaveBeenCalledTimes(2);
            expect(setDataSpy).toHaveBeenCalledTimes(1);
            expect(setDataSpy).toHaveBeenCalledWith(NOTIFICATION_DATA, {
                [BwiNotificationTypes.HINT_RELEASE_NOTES]: {
                    ["mock"]: false,
                },
            });
        });

        it("finish custom notification, don't change notification account data", async () => {
            prepareSpies({});

            const manager = BwiNotificationManager.instance;
            const privacy = getBwiAnalyticsDialog();
            await manager.hasSeenNotification(privacy);
            expect(setDataSpy).toHaveBeenCalledTimes(0);
        });
    });
});
