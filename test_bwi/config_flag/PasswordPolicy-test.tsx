// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import Modal from "../../src/Modal";
import SdkConfig from "../../src/SdkConfig";
import { PasswordPolicyRules, getPasswordPolicies, isValidPasswordPolicy } from "../../src/bwi/settings/PasswordPolicy";
import { ValidatedServerConfig } from "../../src/utils/ValidatedServerConfig";
import { unmockClientPeg } from "../../test/test-utils";
import { getPlatformSpy } from "../mock/MockPlatform";
import { mockFeatureFlag } from "../mock/MockSettingsStore";

function setupSdkConfig(policy: PasswordPolicyRules): void {
    SdkConfig.put({
        validated_server_config: {
            isUrl: "",
        } as ValidatedServerConfig,
        password_policy: policy,
        default_country_code: "GB",
    });
}

describe("BwiConfig.EnforcePassphrase", () => {
    getPlatformSpy();
    mockFeatureFlag("UIFeature.widgets", false);

    const modalSpy = jest.spyOn(Modal, "createDialog").mockImplementation((() => {}) as any);
    afterEach(() => {
        modalSpy.mockClear();
    });

    afterAll(() => {
        jest.clearAllMocks();
        jest.resetModules();
        unmockClientPeg();
    });

    describe("PasswordPolicy", () => {
        describe("isValidPasswordPolicy", () => {
            it("undefined policy, return false", () => {
                setupSdkConfig(undefined!);
                expect(isValidPasswordPolicy()).toBe(false);
            });

            it("empty object policy, return false", () => {
                setupSdkConfig({});
                expect(isValidPasswordPolicy()).toBe(false);
            });

            it("single value policy, return true", () => {
                setupSdkConfig({ minCaps: 2 });
                expect(isValidPasswordPolicy()).toBe(true);
            });

            it("all value policy, return true", () => {
                const allVals: PasswordPolicyRules = {
                    minCaps: 2,
                    minLength: 20,
                    minLows: 1,
                    minNum: 3,
                    minSpecial: 4,
                };
                setupSdkConfig(allVals);
                expect(isValidPasswordPolicy()).toBe(true);
            });
        });

        describe("getPasswordPolicies", () => {
            it("undefined policy, return undefined", () => {
                setupSdkConfig(undefined!);
                expect(getPasswordPolicies()!).toBe(undefined);
            });

            it("empty object policy, return false", () => {
                setupSdkConfig({});
                expect(getPasswordPolicies()!).toBe(undefined);
            });

            it("single value policy, return label of value that is set and not other", () => {
                setupSdkConfig({ minCaps: 2 });
                expect(getPasswordPolicies()!.caps).toBeDefined();
                expect(getPasswordPolicies()!.length).toBeUndefined();
                expect(getPasswordPolicies()!.lows).toBeUndefined();
                expect(getPasswordPolicies()!.numbers).toBeUndefined();
                expect(getPasswordPolicies()!.specials).toBeUndefined();
            });

            it("all value policy, all entries 1, return label string", () => {
                const allVals: PasswordPolicyRules = {
                    minCaps: 1,
                    minLength: 1,
                    minLows: 1,
                    minNum: 1,
                    minSpecial: 1,
                };
                setupSdkConfig(allVals);
                expect(getPasswordPolicies()!.caps).toBeDefined();
                expect(getPasswordPolicies()!.length).toBeDefined();
                expect(getPasswordPolicies()!.lows).toBeDefined();
                expect(getPasswordPolicies()!.numbers).toBeDefined();
                expect(getPasswordPolicies()!.specials).toBeDefined();
            });

            it("all value policy, all entries > 1, return label string", () => {
                const allVals: PasswordPolicyRules = {
                    minCaps: 2,
                    minLength: 30,
                    minLows: 4,
                    minNum: 5,
                    minSpecial: 6,
                };
                setupSdkConfig(allVals);
                expect(getPasswordPolicies()!.caps).toBeDefined();
                expect(getPasswordPolicies()!.length).toBeDefined();
                expect(getPasswordPolicies()!.lows).toBeDefined();
                expect(getPasswordPolicies()!.numbers).toBeDefined();
                expect(getPasswordPolicies()!.specials).toBeDefined();
            });
        });
    });
});
