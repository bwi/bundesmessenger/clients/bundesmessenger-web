// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { EventType } from "matrix-js-sdk/src/matrix";

import { disableMessageActionBar } from "../../src/bwi/functions";
import { mkEvent } from "../../test/test-utils";
import { mockFeatureFlag } from "../mock/MockSettingsStore";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";

describe("MessageActions", () => {
    describe("disableMessageActionBar", () => {
        it("should return true if the event is not whitelisted", () => {
            const isDisabled = disableMessageActionBar(
                mkEvent({
                    event: true,
                    type: "unknown.event",
                    user: "@alice:server.org",
                    content: {},
                }),
            );
            expect(isDisabled).toBe(true);
        });

        it("should return isRedacted value if event can have an action bar but all action bar items are hidden", () => {
            mockFeatureFlag(
                new Map([
                    [BwiFeature.HideRedactedMessageSharing, true],
                    [BwiFeature.HideDebug, true],
                    [BwiFeature.HideOSMLink, true],
                ]),
            );

            const event = mkEvent({
                event: true,
                type: EventType.RoomMessage,
                content: {},
                user: "",
            });
            const isDisabled = disableMessageActionBar(event);

            expect(isDisabled).toBe(event.isRedacted());
        });

        it("should return true if event can have an action bar and some action bar items can be shown", () => {
            mockFeatureFlag(
                new Map([
                    [BwiFeature.HideRedactedMessageSharing, false],
                    [BwiFeature.HideDebug, false],
                    [BwiFeature.HideOSMLink, true],
                ]),
            );

            const isDisabled = disableMessageActionBar(
                mkEvent({
                    event: true,
                    type: EventType.RoomMessage,
                    content: {},
                    user: "",
                }),
            );

            expect(isDisabled).toBe(false);
        });
    });
});
