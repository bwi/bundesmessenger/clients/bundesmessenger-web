// Copyright 2023 seb
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
import { act, renderHook } from "@testing-library/react-hooks";
import { MockedObject } from "jest-mock";
import { EventType, MatrixClient, Room, RoomMember, RoomStateEvent } from "matrix-js-sdk/src/matrix";
import React from "react";
import { RenderHookResult } from "@testing-library/react-hooks/dom";

import { compareByUserFunctionLabel, useUserFunctionLabel } from "../../src/bwi/functions";
import { IRoomState } from "../../src/components/structures/RoomView";
import RoomContext from "../../src/contexts/RoomContext";
import SettingsStore from "../../src/settings/SettingsStore";
import { getMockClientWithEventEmitter, mockClientMethodsUser, unmockClientPeg } from "../../test/test-utils/client";
import { mkEvent } from "../../test/test-utils/test-utils";
import { mockRoomWithEvents } from "../mock/MockRoom";

describe("useUserFunctionLabel", () => {
    const userId = "@user:bwi.de";
    let room: Room;
    let client: MockedObject<MatrixClient>;

    beforeAll(() => {
        client = getMockClientWithEventEmitter({
            ...mockClientMethodsUser(userId),
        });
    });

    afterAll(() => {
        unmockClientPeg();
    });

    function renderTest(
        functionLabel: string,
        opts: {
            featureEnabled: boolean;
            withRoom: boolean;
            withContent: boolean;
        },
    ): RenderHookResult<any, any> {
        jest.spyOn(SettingsStore, "getValue").mockReturnValue(opts.featureEnabled);
        room = mockRoomWithEvents({}, client);

        const event = mkEvent({
            room: room.roomId,
            type: EventType.BwiUserFunctionLabels,
            user: userId,
            content: opts.withContent
                ? {
                      [userId]: functionLabel,
                  }
                : {},
            event: true,
            skey: "",
        });

        room.currentState.setStateEvents([event]);

        const roomContext = {
            room: opts.withRoom ? room : undefined,
        } as IRoomState;

        return renderHook(() => useUserFunctionLabel(event), {
            wrapper: ({ children }) => <RoomContext.Provider value={roomContext}>{children}</RoomContext.Provider>,
        });
    }

    it("should return the function label from an event", async () => {
        const functionLabel = "Esel";
        const { result } = renderTest(functionLabel, {
            featureEnabled: true,
            withRoom: true,
            withContent: true,
        });

        expect(result.current).toBe(functionLabel);
    });

    it("should return <empty_string> if feature flag is disabled", () => {
        const { result } = renderTest("Esel", {
            featureEnabled: false,
            withRoom: true,
            withContent: true,
        });
        expect(result.current).toBe("");
    });

    it("should return <empty_string> if room is undefined", () => {
        const { result } = renderTest("Esel", {
            featureEnabled: true,
            withRoom: false,
            withContent: true,
        });
        expect(result.current).toBe("");
    });

    it("should return <empty_string> if event content is empty", () => {
        const { result } = renderTest("Esel", {
            featureEnabled: true,
            withRoom: false,
            withContent: false,
        });
        expect(result.current).toBe("");
    });

    it("should listen to room event updates and return new function label", async () => {
        const functionLabel = "Esel";
        const { result } = renderTest(functionLabel, {
            featureEnabled: true,
            withRoom: true,
            withContent: true,
        });

        expect(result.current).toBe(functionLabel);

        let event = mkEvent({
            room: room.roomId,
            type: EventType.BwiUserFunctionLabels,
            user: userId,
            content: {
                [userId]: "Katze",
            },
            event: true,
            skey: "",
        });

        act(() => {
            room.currentState.setStateEvents([event]);
            client.emit(RoomStateEvent.Events, event, room.currentState, event);
        });

        expect(result.current).toEqual("Katze");

        event = mkEvent({
            room: room.roomId,
            type: EventType.BwiUserFunctionLabels,
            user: userId,
            content: {
                some_random_user: "Tigger",
            },
            event: true,
            skey: "",
        });
        act(() => {
            room.currentState.setStateEvents([event]);
            client.emit(RoomStateEvent.Marker, event);
        });
        // expect the hook not to update
        expect(result.current).toEqual("Katze");
    });
});

describe("compareByUserFunctionLabel", () => {
    const userId = "@user:bwi.de";
    const client = getMockClientWithEventEmitter({
        ...mockClientMethodsUser(userId),
    });
    const room = mockRoomWithEvents({}, client);

    const alice = new RoomMember(room.roomId, "alice");
    const bobo = new RoomMember(room.roomId, "bobo");

    afterAll(() => {
        unmockClientPeg();
    });

    it("should return -1 if user a has a function label", () => {
        room.currentState.setStateEvents([
            mkEvent({
                room: room.roomId,
                type: EventType.BwiUserFunctionLabels,
                user: alice.userId,
                content: {
                    [alice.userId]: "Katze",
                    [bobo.userId]: "Hund",
                },
                event: true,
                skey: "",
            }),
        ]);
        expect(compareByUserFunctionLabel(alice, bobo, room)).toBe(-1);
    });

    it("should return 1 if 'user b' has a function label but 'user a' not", () => {
        room.currentState.setStateEvents([
            mkEvent({
                room: room.roomId,
                type: EventType.BwiUserFunctionLabels,
                user: alice.userId,
                content: {
                    [bobo.userId]: "Hund",
                },
                event: true,
                skey: "",
            }),
        ]);
        expect(compareByUserFunctionLabel(alice, bobo, room)).toBe(1);
    });

    it("should return 0 if both users have no function label", () => {
        room.currentState.setStateEvents([
            mkEvent({
                room: room.roomId,
                type: EventType.BwiUserFunctionLabels,
                user: alice.userId,
                content: {},
                event: true,
                skey: "",
            }),
        ]);
        expect(compareByUserFunctionLabel(alice, bobo, room)).toBe(0);
    });

    it("should return 0 if room is null", () => {
        room.currentState.setStateEvents([
            mkEvent({
                room: room.roomId,
                type: EventType.BwiUserFunctionLabels,
                user: alice.userId,
                content: {
                    [alice.userId]: "Katze",
                    [bobo.userId]: "Hund",
                },
                event: true,
                skey: "",
            }),
        ]);
        expect(compareByUserFunctionLabel(alice, bobo, null)).toBe(0);
    });
});
