// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { act, render, screen } from "jest-matrix-react";
import React from "react";

import MFileBody from "../../src/components/views/messages/MFileBody";
import { MediaEventHelper } from "../../src/utils/MediaEventHelper";
import * as TestUtils from "../../test/test-utils";
import { enableContentScanner } from "../mock/ContentScanner";

jest.mock("../../src/stores/room-list/RoomListStore", () => ({
    instance: {
        on: () => true,
    },
}));

jest.mock("../../src/stores/spaces/SpaceStore", () => ({
    spacesEnabled: false,
}));

const scanSourceMock = jest.fn();
const scanThumbnailMock = jest.fn();
const mediaMock = {
    media: {
        scanSource: scanSourceMock,
        scanThumbnail: scanThumbnailMock,
    },
    sourceBlob: {
        value: "thisisablob",
    },
} as unknown as MediaEventHelper;

const ev = TestUtils.mkEvent({
    type: "m.room.message",
    room: "room_id",
    user: "sender",
    content: {
        url: "http://test",
        mxc: "mxc://test",
        file: "",
        info: "",
    },
    event: true,
});

describe("ContentScanner: MFilebody", () => {
    beforeEach(() => {
        TestUtils.stubClient();
        scanSourceMock.mockClear();
        scanThumbnailMock.mockClear();
    });

    afterAll(() => {
        jest.clearAllMocks();
        jest.resetModules();
    });

    async function renderComponent(): Promise<void> {
        await act(async () => {
            render(
                <MFileBody
                    mxEvent={ev}
                    mediaEventHelper={mediaMock}
                    onHeightChanged={() => {}}
                    onMessageAllowed={() => {}}
                    permalinkCreator={undefined}
                />,
            );
        });
    }

    it("should mark file as safe as default", async () => {
        await renderComponent();
        expect(screen.queryAllByText("Attachment")[0]).toBeInTheDocument();
    });

    it("should render unsafe (403) file as unsafe", async () => {
        enableContentScanner();

        scanSourceMock.mockResolvedValue("unsafe");
        scanThumbnailMock.mockResolvedValue("unsafe");

        await renderComponent();

        expect(scanSourceMock).toHaveBeenCalled();

        expect(screen.queryByText("Blocked")).toBeInTheDocument();
    });

    it("should render safe file as safe", async () => {
        enableContentScanner();

        scanSourceMock.mockResolvedValue("safe");
        scanThumbnailMock.mockResolvedValue("safe");

        await renderComponent();

        expect(scanSourceMock).toHaveBeenCalled();
        screen.debug();
        expect(screen.queryAllByText("Attachment")[0]).toBeInTheDocument();
        expect(screen.queryByText("Blocked")).not.toBeInTheDocument();
    });

    it("should render file request-failed (400,403) as request failed", async () => {
        enableContentScanner();

        scanSourceMock.mockResolvedValue("request-failed");
        scanThumbnailMock.mockResolvedValue("request-failed");

        await renderComponent();

        expect(scanSourceMock).toHaveBeenCalled();

        expect(screen.queryByText("Connection error. Please try again later.")).toBeInTheDocument();
    });

    it("should render not-found (404) file as unavailable", async () => {
        enableContentScanner();

        scanSourceMock.mockResolvedValue("not-found");
        scanThumbnailMock.mockResolvedValue("not-found");

        await renderComponent();

        expect(scanSourceMock).toHaveBeenCalled();

        expect(screen.queryByText("File is unavailable")).toBeInTheDocument();
    });

    it("only thumbnail unsafe, show unsafe", async () => {
        enableContentScanner();

        scanSourceMock.mockResolvedValue("safe");
        scanThumbnailMock.mockResolvedValue("unsafe");

        await renderComponent();

        expect(scanSourceMock).toHaveBeenCalled();
        expect(screen.queryByText("Blocked")).toBeInTheDocument();
    });

    it("only source unsafe, show unsafe", async () => {
        enableContentScanner();

        scanSourceMock.mockResolvedValue("unsafe");
        scanThumbnailMock.mockResolvedValue("safe");

        await renderComponent();

        expect(scanSourceMock).toHaveBeenCalled();
        expect(screen.queryByText("Blocked")).toBeInTheDocument();
    });
});
