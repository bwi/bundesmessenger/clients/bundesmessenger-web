// Copyright 2024 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { ContentScanError, ScanResult } from "../../src/bwi/content/ContentScanner";
import { MediaEventHelper } from "../../src/utils/MediaEventHelper";
import { LazyValue } from "../../src/utils/LazyValue";

export const getMediaMock: (scanResult: ScanResult) => MediaEventHelper = (scanResult) => {
    let urlMock: LazyValue<any>;
    const scanSourceMock = jest.fn();
    const scanThumbnailMock = jest.fn();
    let sourceBlob: LazyValue<any>;

    switch (scanResult) {
        case "safe":
            urlMock = new LazyValue(
                jest.fn().mockResolvedValue({
                    arrayBuffer: () => new ArrayBuffer(1),
                }),
            );
            scanSourceMock.mockResolvedValue("safe");
            scanThumbnailMock.mockResolvedValue("safe");
            sourceBlob = new LazyValue(
                jest.fn().mockResolvedValue({
                    arrayBuffer: () => new ArrayBuffer(1),
                }),
            );
            break;
        case "not-found":
            urlMock = new LazyValue(jest.fn().mockRejectedValue(new ContentScanError("not-found")));
            scanSourceMock.mockResolvedValue("not-found");
            scanThumbnailMock.mockResolvedValue("not-found");
            sourceBlob = new LazyValue(jest.fn().mockRejectedValue(new ContentScanError("not-found")));
            break;
        case "request-failed":
            urlMock = new LazyValue(jest.fn().mockRejectedValue(new ContentScanError("request-failed")));
            scanSourceMock.mockResolvedValue("request-failed");
            scanThumbnailMock.mockResolvedValue("request-failed");
            sourceBlob = new LazyValue(jest.fn().mockRejectedValue(new ContentScanError("request-failed")));
            break;
        case "unsafe":
            urlMock = new LazyValue(jest.fn().mockRejectedValue(new ContentScanError("unsafe")));
            scanSourceMock.mockResolvedValue("unsafe");
            scanThumbnailMock.mockResolvedValue("unsafe");
            sourceBlob = new LazyValue(jest.fn().mockRejectedValue(new ContentScanError("unsafe")));
            break;
        default:
            urlMock = new LazyValue(jest.fn().mockResolvedValue("thisIsAUrl"));
            scanSourceMock.mockResolvedValue("safe");
            scanThumbnailMock.mockResolvedValue("safe");
            sourceBlob = new LazyValue(jest.fn().mockResolvedValue({ arrayBuffer: () => new ArrayBuffer(1) }));
    }

    return {
        media: {
            scanSource: scanSourceMock,
            scanThumbnail: scanThumbnailMock,
            isEncrypted: true,
        },
        sourceUrl: urlMock,
        thumbnailUrl: urlMock,
        sourceBlob: sourceBlob,
    } as unknown as MediaEventHelper;
};
