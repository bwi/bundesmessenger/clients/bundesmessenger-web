// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { RenderResult, act, render, screen } from "jest-matrix-react";
import { MatrixClient } from "matrix-js-sdk/src/matrix";
import React from "react";

import { ScanResult } from "../../src/bwi/content/ContentScanner";
import MImageBody from "../../src/components/views/messages/MImageBody";
import MatrixClientContext from "../../src/contexts/MatrixClientContext";
import * as TestUtils from "../../test/test-utils";
import { enableContentScanner } from "../mock/ContentScanner";
import { getBaseClient, getClientUserMocks } from "../mock/MockClient";
import { getMediaMock } from "./ContentScannerHelper";

jest.mock("../../src/stores/room-list/RoomListStore", () => ({
    instance: {
        on: () => true,
    },
}));

jest.mock("../../src/stores/spaces/SpaceStore", () => ({
    spacesEnabled: false,
}));

jest.mock("../../src/components/views/messages/MImageReplyBody", () => <>replybody</>);

jest.mock("../../src/bwi/managers/PersonalNotesRoomManager", () => ({
    isPersonalNotesRoom: jest.fn(),
}));

jest.mock("../../src/bwi/functions/UserFunctionLabel", () => ({
    useUserFunctionLabel: jest.fn(),
}));

const ev = TestUtils.mkEvent({
    type: "m.room.message",
    room: "room_id",
    user: "sender",
    content: {
        url: "http://localhost/test",
        mxc: "mxc://localhost/test",
        file: "",
        info: "",
    },
    event: true,
});

describe("ContentScanner: MImageBody", () => {
    const client = getBaseClient({
        ...getClientUserMocks(),
        getHomeserverUrl: () => "",
    });

    afterAll(() => {
        jest.clearAllMocks();
        jest.resetModules();
    });

    async function renderComponent(scanResult: ScanResult): Promise<RenderResult> {
        let container: RenderResult;
        await act(async () => {
            container = render(
                <MatrixClientContext.Provider value={client as MatrixClient}>
                    <MImageBody
                        mxEvent={ev}
                        mediaEventHelper={getMediaMock(scanResult)}
                        onHeightChanged={() => {}}
                        onMessageAllowed={() => {}}
                        permalinkCreator={undefined}
                    />
                </MatrixClientContext.Provider>,
            );
        });
        return container!;
    }

    it("should render image when file is safe", async () => {
        const { container } = await renderComponent("safe");

        expect(container.querySelector("img")).toBeInTheDocument();
    });

    it("should not render image when content is unsafe", async () => {
        enableContentScanner();

        const { container, queryByText } = await renderComponent("unsafe");

        expect(container.querySelector("img")).not.toBeInTheDocument();
        expect(queryByText("Blocked")).toBeInTheDocument();
    });

    it("should render image when file is safe and content scanner enabled", async () => {
        enableContentScanner();

        const { container } = await renderComponent("safe");
        expect(container.querySelector("img")).toBeInTheDocument();
    });

    it("should render filebody with scan error when contentscanner doesn't find file", async () => {
        enableContentScanner();

        await renderComponent("not-found");

        expect(screen.queryByText("File is unavailable")).toBeInTheDocument();
    });

    it("should render filebody with scan error when contentscanner has network issues", async () => {
        enableContentScanner();

        await renderComponent("request-failed");

        expect(screen.queryByText("Connection error. Please try again later.")).toBeInTheDocument();
    });
});
