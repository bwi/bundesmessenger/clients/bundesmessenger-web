// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { EncryptedFile } from "matrix-js-sdk/src/types";
import fetchMock from "fetch-mock-jest";

import SdkConfig from "../../src/SdkConfig";
import ContentScanner, { ContentScanError, ContentScannerClass, ScanError } from "../../src/bwi/content/ContentScanner";
import { enableContentScanner } from "../mock/ContentScanner";
import { getAuthRequestClientSpy, getBaseClient } from "../mock/MockClient";
import { MatrixClientPeg } from "../../src/MatrixClientPeg.ts";

const setRecipientKeyMock = jest.fn((key) => !!key);
const PkEncryptionMock = jest.fn().mockImplementation(() => {
    return {
        set_recipient_key: setRecipientKeyMock,
        encrypt: jest.fn((file) => file),
    };
});
const olmInitMock = jest.fn().mockResolvedValue(true);

const MxcMock = "12345";

const cleanFileMock = {
    url: "mxc://scanner/cleanfile",
    key: {
        alg: "a",
        key_ops: [],
        kty: "",
        k: "",
        ext: false,
    },
    iv: "",
    hashes: {
        a: "a",
    },
    v: "",
} as EncryptedFile;

const publicKeyResponse = {
    public_key: "a",
};

function mockScanResponse(isOk: boolean, isClean: boolean): void {
    getBaseClient({
        ...getAuthRequestClientSpy({
            ok: isOk,
            json: () => {
                return { clean: isClean };
            },
        }),
        getHomeserverUrl() {
            return "https://scanner";
        },
    });
}
function mockDownloadResponse(isOk: boolean, body: any): void {
    getBaseClient({
        ...getAuthRequestClientSpy({ ok: isOk, text: () => body }),
        getHomeserverUrl() {
            return "https://scanner";
        },
    });
}
function mockError(isOk: boolean, status: number, error: ScanError): void {
    getBaseClient({
        ...getAuthRequestClientSpy({
            ok: isOk,
            status: status,
            json: () => {
                return { reason: error };
            },
        }),
        getHomeserverUrl() {
            return "https://scanner";
        },
    });
}

describe("ContentScanner", () => {
    beforeAll(() => {
        // @ts-ignore
        global.Olm = {};
        global.Olm.PkEncryption = PkEncryptionMock;
        global.Olm.init = olmInitMock;

        getBaseClient({
            getHomeserverUrl() {
                return "https://scanner";
            },
        });
    });

    beforeEach(async () => {
        localStorage.clear();
        fetchMock.mockReset();

        fetchMock.get(/^https?:\/\/scanner.*$/, (url) => {
            if (url.endsWith("/media_proxy/unstable/public_key")) {
                return publicKeyResponse;
            }

            return {};
        });
        enableContentScanner();
    });

    describe("instance", () => {
        it("should have a global instance", () => {
            expect(ContentScanner.instance).toBeInstanceOf(ContentScannerClass);
        });
        it("should be disabled by default", () => {
            SdkConfig.put({});
            expect(ContentScanner.instance.canEnable).toBe(false);
        });
        it("should be enabled by config", () => {
            expect(ContentScanner.instance.canEnable).toBe(true);
        });
    });

    describe("setup", () => {
        it("should load public key before any request", async () => {
            const scanner = new ContentScannerClass();

            mockScanResponse(true, true);

            await scanner.scan(MxcMock, cleanFileMock);

            expect(scanner["key"]).toBeTruthy();
        });
    });

    describe("scan", () => {
        let scanner: ContentScannerClass;
        beforeEach(() => {
            scanner = new ContentScannerClass();
        });

        it("should call mxc if no file is given", async () => {
            mockScanResponse(true, true);

            await scanner.scan(MxcMock, undefined);

            expect(MatrixClientPeg.safeGet().http.fetch).toHaveBeenCalledWith(
                "https://scanner/_matrix/media_proxy/unstable/scan/",
            );
        });

        it("should use authorized fetch for scanning", async () => {
            mockScanResponse(true, true);

            await scanner.scan(MxcMock, cleanFileMock);

            expect(MatrixClientPeg.safeGet().http.fetch).toHaveBeenCalled();
        });

        it("safe", async () => {
            mockScanResponse(true, true);
            const scanResult = await scanner.scan(MxcMock, cleanFileMock);
            expect(scanResult).toBe("safe");
        });
        it("unsafe", async () => {
            mockScanResponse(true, false);
            const scanResult = await scanner.scan(MxcMock, cleanFileMock);
            expect(scanResult).toBe("unsafe");
        });
        it("not-found", async () => {
            mockError(false, 404, ScanError.NotFound);
            const scanResult = await scanner.scan(MxcMock, cleanFileMock);
            expect(scanResult).toBe("not-found");
        });
        it("unknown if unknown response", async () => {
            mockError(false, 505, "unknown reason" as any);
            const scanResult = await scanner.scan(MxcMock, cleanFileMock);
            expect(scanResult).toBe("unknown");
        });
        it.each([
            [400, ScanError.Malformed],
            [400, ScanError.DecryptFailed],
            [403, ScanError.BadDecryption],
            [502, ScanError.RequestFailed],
        ])("request-failed when %s %s", async (status, reason) => {
            mockError(false, status, reason);
            const scanResult = await scanner.scan(MxcMock, cleanFileMock);
            expect(scanResult).toBe("request-failed");
        });
    });

    describe("download", () => {
        let scanner: ContentScannerClass;
        beforeEach(() => {
            scanner = new ContentScannerClass();
        });

        it("should use authorized fetch for downloading", async () => {
            mockDownloadResponse(true, "file content");

            await scanner.download(MxcMock, cleanFileMock);

            expect(MatrixClientPeg.safeGet().http.fetch).toHaveBeenCalled();
        });

        it("should return the response if not issues occurred during scanning", async () => {
            mockDownloadResponse(true, "file content");

            const scanResult = await scanner.download(MxcMock, cleanFileMock);

            expect(await scanResult.text()).toEqual("file content");
        });

        it("should throw a ContentScanError if content scanner response unsuccessfully", async () => {
            mockError(false, 404, ScanError.NotFound);

            await expect(scanner.download(MxcMock, cleanFileMock)).rejects.toThrow(ContentScanError);
        });
    });
});
