// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { RenderResult, act, render } from "jest-matrix-react";
import React from "react";

import ContentScanner, { ScanResult } from "../../src/bwi/content/ContentScanner";
import MVideoBody from "../../src/components/views/messages/MVideoBody";
import * as TestUtils from "../../test/test-utils";
import { enableContentScanner } from "../mock/ContentScanner";
import { getBaseClient, getClientUserMocks } from "../mock/MockClient";
import { getMediaMock } from "./ContentScannerHelper";

jest.mock("../../src/stores/room-list/RoomListStore", () => ({
    instance: {
        on: () => true,
    },
}));

jest.mock("../../src/stores/spaces/SpaceStore", () => ({
    spacesEnabled: false,
}));

const ev = TestUtils.mkEvent({
    type: "m.room.message",
    room: "room_id",
    user: "sender",
    content: {
        url: "http://test",
        mxc: "mxc://test",
        file: "",
        info: "",
    },
    event: true,
});

describe("ContentScanner: MVideoBody", () => {
    getBaseClient({
        ...getClientUserMocks(),
        getHomeserverUrl: () => "",
    });

    afterAll(() => {
        jest.clearAllMocks();
        jest.resetModules();
    });

    async function renderComponent(scanResult: ScanResult): Promise<RenderResult> {
        let container: RenderResult;
        await act(async () => {
            container = render(
                <MVideoBody
                    mxEvent={ev}
                    mediaEventHelper={getMediaMock(scanResult)}
                    onHeightChanged={() => {}}
                    onMessageAllowed={() => {}}
                    permalinkCreator={undefined}
                />,
            );
        });
        return container!;
    }

    it("should mark file as safe as default", async () => {
        const { container } = await renderComponent("safe");
        expect(container.querySelector("video")).toBeInTheDocument();
    });

    it("should not render unsafe video and default to MFileBody", async () => {
        enableContentScanner();

        const spy = jest.spyOn(ContentScanner.instance, "scan").mockResolvedValue("unsafe");

        const { container, queryByText } = await renderComponent("unsafe");

        expect(container.querySelector("video")).not.toBeInTheDocument();
        expect(queryByText("Blocked")).toBeInTheDocument();

        spy.mockRestore();
    });

    it("should render scanned safe video", async () => {
        enableContentScanner();

        const spy = jest.spyOn(ContentScanner.instance, "scan").mockResolvedValue("safe");

        const { container } = await renderComponent("safe");

        expect(container.querySelector("video")).toBeInTheDocument();

        spy.mockRestore();
    });

    it("should render filebody with scan error when contentscanner doesn't find file", async () => {
        enableContentScanner();

        const spy = jest.spyOn(ContentScanner.instance, "scan").mockRejectedValue(new Error());

        const { container, queryByText } = await renderComponent("not-found");

        expect(container.querySelector("video")).not.toBeInTheDocument();
        expect(queryByText("File is unavailable")).toBeInTheDocument();

        spy.mockRestore();
    });

    it("should render filebody with scan error when contentscanner has network issues", async () => {
        enableContentScanner();

        const spy = jest.spyOn(ContentScanner.instance, "scan").mockRejectedValue(new Error());

        const { container, queryByText } = await renderComponent("request-failed");

        expect(container.querySelector("video")).not.toBeInTheDocument();
        expect(queryByText("Connection error. Please try again later.")).toBeInTheDocument();

        spy.mockRestore();
    });
});
