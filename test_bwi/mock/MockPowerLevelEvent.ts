// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { MatrixEvent, EventType, RoomMember, User } from "matrix-js-sdk/src/matrix";

interface PowerEventProps {
    sender: User;
    usersDefault?: number;
    prevDefault?: number;
    users: Record<string, number>;
    prevUsers: Record<string, number>;
}

export const mockPowerLevelEvent = ({
    sender,
    usersDefault,
    prevDefault,
    users,
    prevUsers,
}: PowerEventProps): MatrixEvent => {
    const mxEvent = new MatrixEvent({
        type: EventType.RoomPowerLevels,
        sender: (sender.userId && "" + sender.userId) || "",
        state_key: "",
        content: {
            users_default: usersDefault,
            users,
        },
    });
    mxEvent.sender = { name: sender.displayName } as RoomMember;
    return mxEvent;
};
