// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";

import { BwiNotificationDialog } from "../../src/bwi/components/notifications/BwiNotificationRenderer";
import { BwiNotifications, BwiNotificationTypes } from "../../src/bwi/components/notifications/BwiNotifications";
import { _t } from "../../src/languageHandler";
import SdkConfig from "../../src/SdkConfig";

export const showPrivacyMock = (shouldShow: boolean): BwiNotifications => ({
    should_show_analytics_consent_notification: shouldShow,
});

export const showBirthdayMock = (shouldShow: boolean): BwiNotifications => ({
    should_show_web_birthday_notification: shouldShow,
});

export const showReleaseNotesMock = (shouldShow: boolean): BwiNotifications => ({
    should_show_web_release_notes: {
        mock: shouldShow,
    },
});

export const showReleasePrivacyMock = (showPrivacy: boolean, showRelease: boolean): BwiNotifications => ({
    should_show_web_release_notes: {
        mock: showRelease,
    },
    should_show_analytics_consent_notification: showPrivacy,
});

export const MultipleMockOneStillToShow: BwiNotifications = {
    should_show_feature_hint_notes_room: false,
    should_show_web_birthday_notification: false,
    should_show_analytics_consent_notification: true,
    should_show_web_release_notes: {
        mock: false,
    },
};

export const MultipleMockAllSeen: BwiNotifications = {
    should_show_feature_hint_notes_room: false,
    should_show_web_birthday_notification: false,
    should_show_web_release_notes: {
        mock: false,
    },
};

export const NothingSeenMock: BwiNotifications = {
    should_show_feature_hint_notes_room: true,
    should_show_web_birthday_notification: true,
    should_show_analytics_consent_notification: true,
    should_show_web_release_notes: {
        mock: true,
    },
};

export const getBwiAnalyticsDialogMockNonCustom = (): BwiNotificationDialog => {
    const sdk = SdkConfig.get();

    const dialog: BwiNotificationDialog = {
        notificationType: BwiNotificationTypes.HINT_ANALYTICS_NOTIFICATION,
        title: _t("Error Analytics"),
        confirmButton: <div data-testid="bwiAnalyticsNotificationDialogConfirm">{_t("Agree")}</div>,
        cancelButton: <div data-testid="bwiAnalyticsNotificationDialogCancel">{_t("Disagree")}</div>,
        content: (
            <div className="mx_BwiCelebration_content">
                <p>
                    {_t(
                        "We need your help in finding issues to improve %(brand)s. For this purpose, we would like to collect anonymised diagnostic data.",
                        { brand: sdk?.brand },
                    )}
                </p>
                <p>
                    {_t("No data is given to third parties.")}
                    {sdk?.matomo?.policyLink && (
                        <>
                            {_t("You can find the details in our")}
                            <a href={sdk?.matomo?.policyLink} target="_blank">
                                {" "}
                                {_t("Privacy Policy")}
                            </a>
                            .
                        </>
                    )}
                </p>
                <p>{_t("If you don't want to help anymore, you can always deactivate it again in your settings.")}</p>
                <p>{_t("Do you want to support us in our error analytics?")}</p>
            </div>
        ),
    };
    return dialog;
};
