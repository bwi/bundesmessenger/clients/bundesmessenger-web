// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { act } from "react-dom/test-utils"; // eslint-disable-line deprecate/import

export const waitForTimers = async (): Promise<void> => {
    // eslint-disable-next-line
    await new Promise<void>(async (resolve) => {
        await act(async () => {
            await jest.runOnlyPendingTimers();
            resolve();
        });
    });
};

export const flushPromises = (): Promise<void> => {
    return new Promise((resolve) => setTimeout(resolve));
};
