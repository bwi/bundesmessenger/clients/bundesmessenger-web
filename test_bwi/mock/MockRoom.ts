// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { mocked, MockedObject } from "jest-mock";
import { EventType, MatrixClient, MatrixEvent, PendingEventOrdering, Room } from "matrix-js-sdk/src/matrix";

import { MatrixClientPeg } from "../../src/MatrixClientPeg";
import { mkEvent } from "../../test/test-utils";
import { DeepPartial } from "./utils";

export function generateRoomId(): string {
    return "!" + Math.random().toString().slice(2, 10) + ":domain";
}

export const createRoom = (client: MatrixClient, opts: Partial<Room>): Room => {
    const room = new Room(generateRoomId(), client, client.getUserId() || "", {
        // The room list now uses getPendingEvents(), so we need a detached ordering.
        pendingEventOrdering: PendingEventOrdering.Detached,
    });
    if (opts) {
        Object.assign(room, opts);
    }
    return room;
};

type InferCtor<T> = T extends {
    new (...args: infer U): any;
}
    ? U
    : never;
/**
 * Mock a room with a `m.room.create` event
 * @returns
 */
export function mockRoomWithEvents(
    {
        partials,
        opts,
        events,
    }: {
        opts?: InferCtor<typeof Room>[3];
        partials?: DeepPartial<Room>;
        events?: MatrixEvent[];
    },
    client = MatrixClientPeg.safeGet(),
): MockedObject<Room> {
    const userId = client.getUserId()!;
    const room = new Room(partials?.roomId || generateRoomId(), client, userId, opts);
    const { currentState: currentStatePartials, ...otherPartials } = { currentState: {}, ...partials };
    const hasCreateEvent = events?.some((e) => e.getType() === EventType.RoomCreate) || false;
    const roomEvents: MatrixEvent[] = [];
    if (!hasCreateEvent) {
        roomEvents.push(mockCreateRoomEvent(room.roomId, userId));
    }
    roomEvents.push(...(events || []));
    room.currentState.setStateEvents(roomEvents);
    Object.assign(room, otherPartials);
    if (currentStatePartials) {
        Object.assign(room.currentState, currentStatePartials);
    }
    return mocked(room);
}

export function mockCreateRoomEvent(roomId: string, userId: string): MatrixEvent {
    return mkEvent({
        event: true,
        type: "m.room.create",
        room: roomId,
        user: userId,
        content: {
            creator: userId,
            room_version: "5",
            predecessor: {
                room_id: "!prevroom",
                event_id: "$someevent",
            },
        },
    });
}
