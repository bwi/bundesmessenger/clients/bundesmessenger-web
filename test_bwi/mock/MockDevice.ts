// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { DeviceVerificationStatus } from "matrix-js-sdk/src/crypto-api";
import { Device, DeviceMap, DeviceVerification } from "matrix-js-sdk/src/matrix";

export function mockDevice(userId: string, opts?: Partial<ConstructorParameters<typeof Device>[0]>): Device {
    return new Device({
        algorithms: [],
        deviceId: opts?.deviceId || "device1",
        keys: new Map(),
        userId: userId,
        verified: DeviceVerification.Verified,
        ...opts,
    });
}

export function mockDeviceMap(userId: string, device: Device): DeviceMap {
    return new Map([[userId, new Map([[device.deviceId, device]])]]);
}

export function mockDeviceVerificationStatus(verified = true): DeviceVerificationStatus {
    return new DeviceVerificationStatus({
        localVerified: verified,
    });
}
