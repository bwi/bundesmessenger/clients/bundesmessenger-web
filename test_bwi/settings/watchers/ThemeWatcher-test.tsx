// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import SettingsStore from "../../../src/settings/SettingsStore";
import ThemeWatcher from "../../../src/settings/watchers/ThemeWatcher";
import { SettingLevel } from "../../../src/settings/SettingLevel";
import SdkConfig from "../../../src/SdkConfig";

function makeMatchMedia(values: any) {
    class FakeMediaQueryList {
        // @ts-ignore TS6133
        private matches: false;
        public addListener(): void {}
        public removeListener(): void {}
        public addEventListener(): void {}
        public removeEventListener(): void {}
        public dispatchEvent(): boolean {
            return true;
        }

        public constructor(query: string) {
            this.matches = values[query];
        }
    }

    return function matchMedia(query: string) {
        return new FakeMediaQueryList(query);
    };
}

function makeGetValue(values: any) {
    return function getValue<T = any>(
        settingName: string,
        // @ts-ignore
        _roomId: string = null,
        _excludeDefault = false,
    ): T {
        return values[settingName];
    };
}

function makeGetValueAt(values: any) {
    return function getValueAt(
        _level: SettingLevel,
        settingName: string,
        // @ts-ignore
        _roomId: string = null,
        _explicit = false,
        _excludeDefault = false,
    ): any {
        return values[settingName];
    };
}

describe("ThemeWatcher", function () {
    const config = {
        default_theme: "bwi-light",
        bwi_themes: [
            {
                name: "light",
                default: true,
                is_dark: false,
                colors: {},
                // @ts-ignore
                fonts: [],
            },
            {
                name: "dark",
                is_dark: true,
                default: true,
                colors: {},
                // @ts-ignore
                fonts: [],
            },
        ],
    };

    beforeEach(() => {
        // @ts-ignore
        SdkConfig.put(config);
    });

    it("should choose a light theme by default", () => {
        // Given no system settings
        // @ts-ignore
        global.matchMedia = makeMatchMedia({});

        // Then getEffectiveTheme returns light
        const themeWatcher = new ThemeWatcher();
        expect(themeWatcher.getEffectiveTheme()).toBe(config.default_theme);
    });

    it("should choose default theme if system settings are inconclusive", () => {
        // Given no system settings but we asked to use them
        // @ts-ignore
        global.matchMedia = makeMatchMedia({});
        // @ts-ignore
        SettingsStore.getValue = makeGetValue({
            use_system_theme: true,
            theme: "light",
            custom_themes: [],
        });

        // Then getEffectiveTheme returns light
        const themeWatcher = new ThemeWatcher();
        expect(themeWatcher.getEffectiveTheme()).toBe(config.default_theme);
    });

    it("should choose a light theme if system prefers it (via default)", () => {
        // Given system prefers lightness, even though we did not
        // click "Use system theme" or choose a theme explicitly
        // @ts-ignore
        global.matchMedia = makeMatchMedia({ "(prefers-color-scheme: light)": true });
        // @ts-ignore
        SettingsStore.getValueAt = makeGetValueAt({});
        // @ts-ignore
        SettingsStore.getValue = makeGetValue({ use_system_theme: true });

        // Then getEffectiveTheme returns light
        const themeWatcher = new ThemeWatcher();
        expect(themeWatcher.getEffectiveTheme()).toBe("bwi-light");
    });

    it("should choose a dark theme if system prefers it (via default)", () => {
        // Given system prefers darkness, even though we did not
        // click "Use system theme" or choose a theme explicitly
        // @ts-ignore
        global.matchMedia = makeMatchMedia({ "(prefers-color-scheme: dark)": true });
        // @ts-ignore
        SettingsStore.getValueAt = makeGetValueAt({});
        // @ts-ignore
        SettingsStore.getValue = makeGetValue({ use_system_theme: true });

        // Then getEffectiveTheme returns dark
        const themeWatcher = new ThemeWatcher();
        expect(themeWatcher.getEffectiveTheme()).toBe("bwi-dark");
    });

    it("should choose a light theme if system prefers it (explicit)", () => {
        // Given system prefers lightness
        // @ts-ignore
        global.matchMedia = makeMatchMedia({ "(prefers-color-scheme: light)": true });
        // @ts-ignore
        SettingsStore.getValueAt = makeGetValueAt({ use_system_theme: true });
        // @ts-ignore
        SettingsStore.getValue = makeGetValue({ use_system_theme: true });

        // Then getEffectiveTheme returns light
        const themeWatcher = new ThemeWatcher();
        expect(themeWatcher.getEffectiveTheme()).toBe("bwi-light");
    });

    it("should choose a dark theme if system prefers it (explicit)", () => {
        // Given system prefers darkness
        // @ts-ignore
        global.matchMedia = makeMatchMedia({ "(prefers-color-scheme: dark)": true });
        // @ts-ignore
        SettingsStore.getValueAt = makeGetValueAt({ use_system_theme: true });
        // @ts-ignore
        SettingsStore.getValue = makeGetValue({ use_system_theme: true });

        // Then getEffectiveTheme returns dark
        const themeWatcher = new ThemeWatcher();
        expect(themeWatcher.getEffectiveTheme()).toBe("bwi-dark");
    });

    it("should choose a high-contrast theme if system prefers it", () => {
        // Given system prefers high contrast and light
        // @ts-ignore
        global.matchMedia = makeMatchMedia({
            "(prefers-contrast: more)": true,
            "(prefers-color-scheme: light)": true,
        });
        // @ts-ignore
        SettingsStore.getValueAt = makeGetValueAt({ use_system_theme: true });
        // @ts-ignore
        SettingsStore.getValue = makeGetValue({ use_system_theme: true });

        // Then getEffectiveTheme returns light-high-contrast
        const themeWatcher = new ThemeWatcher();
        expect(themeWatcher.getEffectiveTheme()).toBe("bwi-light");
    });

    it("should not choose a high-contrast theme if not available", () => {
        // Given system prefers high contrast and dark, but we don't (yet)
        // have a high-contrast dark theme
        // @ts-ignore
        global.matchMedia = makeMatchMedia({
            "(prefers-contrast: more)": true,
            "(prefers-color-scheme: dark)": true,
        });
        // @ts-ignore
        SettingsStore.getValueAt = makeGetValueAt({ use_system_theme: true });
        // @ts-ignore
        SettingsStore.getValue = makeGetValue({ use_system_theme: true });

        // Then getEffectiveTheme returns dark
        const themeWatcher = new ThemeWatcher();
        expect(themeWatcher.getEffectiveTheme()).toBe("bwi-dark");
    });
});
