// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { MatrixClient, MatrixEvent } from "matrix-js-sdk/src/matrix";
import { CryptoApi, DecryptionFailureCode, UserVerificationStatus } from "matrix-js-sdk/src/crypto-api";
import { Mocked, mocked, MockedObject } from "jest-mock";

import { DecryptionFailureTracker } from "../../src/DecryptionFailureTracker";
import { BwiAnalyticsClass } from "../../src/bwi/analytics/BwiAnalyticsManager";
import * as Sender from "../../src/bwi/analytics/BwiAnalyticsSender";
import { stubClient } from "../../test/test-utils";

const originalTrackFn = DecryptionFailureTracker.instance["fn"];

describe("BwiDecryptionFailureTracker", () => {
    describe("Instance", () => {
        const senderSpy = jest.spyOn(Sender, "sendAnalyticsEncryptionViewMessage").mockImplementation(jest.fn());

        beforeEach(() => {
            jest.useFakeTimers();
            jest.setSystemTime(0);
            senderSpy.mockClear();
        });
        afterEach(() => {
            jest.useRealTimers();
        });

        it("should send decryption failures to analytics", async () => {
            const tracker = makeTracker(originalTrackFn);
            await tracker.start(mockClient());

            const event1 = getDecryptionEvent("1", false, DecryptionFailureCode.OLM_UNKNOWN_MESSAGE_INDEX);
            const event2 = getDecryptionEvent("2", false, DecryptionFailureCode.MEGOLM_UNKNOWN_INBOUND_SESSION_ID);

            // load in some failed events
            tracker.addVisibleEvent(event1);
            tracker.eventDecrypted(event1, 1);

            tracker.addVisibleEvent(event2);
            tracker.eventDecrypted(event2, 2);

            tracker.checkFailures(600002);
            // advance time by 1 minute so the check interval processes the failed events and the track function was executed
            jest.advanceTimersByTime(60000);

            // expect the two events to be sent to analytics one by one
            expect(senderSpy).toHaveBeenCalledTimes(2);

            await tracker.stop();
        });

        it("should send bundled decryption failures to analytics", async () => {
            const tracker = makeTracker(originalTrackFn);
            await tracker.start(mockClient());

            const event1 = getDecryptionEvent("3", false, DecryptionFailureCode.OLM_UNKNOWN_MESSAGE_INDEX);
            const event2 = getDecryptionEvent("4", false, DecryptionFailureCode.OLM_UNKNOWN_MESSAGE_INDEX);
            const event3 = getDecryptionEvent("5", false, DecryptionFailureCode.MEGOLM_UNKNOWN_INBOUND_SESSION_ID);

            // load in some failed events
            tracker.addVisibleEvent(event1);
            tracker.eventDecrypted(event1, 1);

            tracker.addVisibleEvent(event2);
            tracker.eventDecrypted(event2, 2);

            tracker.addVisibleEvent(event3);
            tracker.eventDecrypted(event3, 3);

            tracker.checkFailures(600002);

            // advance time by 1 minute so the check interval processes the failed events and the track function was executed
            jest.advanceTimersByTime(60000);

            // expect the two events to be sent to analytics one by one
            expect(senderSpy).toHaveBeenCalledTimes(2);

            await tracker.stop();
        });

        it("should exclude events decrypted within the grace period", async () => {
            const tracker = makeTracker(originalTrackFn);
            await tracker.start(mockClient());

            const event1 = getDecryptionEvent("6");
            const event2 = getDecryptionEvent("7");

            // load in some failed events
            tracker.addVisibleEvent(event1);
            tracker.eventDecrypted(event1, 1);

            tracker.addVisibleEvent(event2);
            tracker.eventDecrypted(event2, 2);

            // decrypt event 2
            tracker.eventDecrypted(getDecryptionEvent("6", true), 3000);

            tracker.checkFailures(1000000);

            // expect only event 1 to be tracked
            expect(senderSpy).toHaveBeenCalledTimes(1);

            await tracker.stop();
        });
    });
    describe("E2EEDecryptionTime", () => {
        const senderSpy = jest.spyOn(Sender, "sendE2EEDecryptionTime");

        beforeEach(() => {
            jest.useFakeTimers();
            jest.setSystemTime(0);
            senderSpy.mockClear();
        });
        afterEach(() => {
            jest.useRealTimers();
        });
        it("track e2ee decryption times for failed events", () => {
            const tracker = makeTracker();

            // load in some failed events
            tracker.eventDecrypted(getDecryptionEvent("1"), 1);
            tracker.eventDecrypted(getDecryptionEvent("2"), 2);

            // resolve first event after 1 second
            jest.advanceTimersByTime(1000);
            tracker.eventDecrypted(getDecryptionEvent("1", true), 3);

            // resolve second event after 3 seconds
            jest.advanceTimersByTime(2000);
            tracker.eventDecrypted(getDecryptionEvent("2", true), 4);

            tracker.checkFailures(60000);

            const senderSpy = jest.spyOn(Sender, "sendE2EEDecryptionTime");
            const trackEventSpy = jest.spyOn(BwiAnalyticsClass.sharedInstance, "trackEvent").mockResolvedValue();

            tracker.bwiTrackDecryption();

            // Expect the metrics send to the sender to be correct
            expect(senderSpy).toHaveBeenCalledWith(2, expect.any(Number), expect.any(Number));
            // Expect track function to have been called for each metric
            expect(trackEventSpy).toHaveBeenCalledTimes(3);
        });
    });
});

function getDecryptionEvent(
    eventId: string,
    resolved = false,
    code: DecryptionFailureCode = DecryptionFailureCode.HISTORICAL_MESSAGE_NO_KEY_BACKUP,
): MatrixEvent {
    const event = new MatrixEvent({
        event_id: eventId,
        room_id: "!room",
        content: {
            algorithm: "m.megolm.v1.aes-sha2",
        },
    });

    if (!resolved) {
        jest.spyOn(event, "decryptionFailureReason", "get").mockReturnValue(code);
    }

    return event;
}

function makeTracker(fn: DecryptionFailureTracker["fn"] = () => {}): DecryptionFailureTracker {
    //@ts-ignore
    return new DecryptionFailureTracker(fn, DecryptionFailureTracker.instance.errorCodeMapFn);
}

function mockClient(): MockedObject<MatrixClient> {
    const client = mocked(stubClient());
    const mockCrypto = {
        getVersion: jest.fn().mockReturnValue("Rust SDK 0.7.0 (61b175b), Vodozemac 0.5.1"),
        getUserVerificationStatus: jest.fn().mockResolvedValue(new UserVerificationStatus(false, false, false)),
    } as unknown as Mocked<CryptoApi>;
    client.getCrypto.mockReturnValue(mockCrypto);

    // @ts-ignore
    client.stopClient = jest.fn(() => {});
    // @ts-ignore
    client.removeAllListeners = jest.fn(() => {});

    client.store = { destroy: jest.fn(() => {}) } as any;

    return client;
}
