// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { KNOWN_POLL_KIND, M_POLL_KIND_DISCLOSED, M_POLL_KIND_UNDISCLOSED } from "matrix-events-sdk";
import { DeviceMap, Device, IContent, MatrixClient, MsgType, RoomMember } from "matrix-js-sdk/src/matrix";

import { MatrixClientPeg } from "../../src/MatrixClientPeg";
import {
    HuddleFeatureEvents,
    sendAnalyticsFeatureForwardMessage,
    sendAnalyticsFeatureSendPoll,
    sendAnalyticsFeatureSendVoiceMessage,
    sendAnalyticsGeneralLogout,
    sendHuddleFeatureMessage,
    sendAnalyticsEncryptionViewMessage,
} from "../../src/bwi/analytics/BwiAnalyticsSender";
import * as TestUtils from "../../test/test-utils";
import { createRoom } from "../mock/MockRoom";
import { getPlatformSpy } from "../mock/MockPlatform";
import BwiAnalyticsManager, { Dimension } from "../../src/bwi/analytics/BwiAnalyticsManager";
import { FeatureAnalyticsEvents } from "../../src/bwi/analytics/BwiAnalyticsEvents";

describe("AnalyticsSender", () => {
    let cli: MatrixClient;

    beforeAll(() => {
        TestUtils.stubClient();
        cli = MatrixClientPeg.safeGet();
        cli.getRoom = jest.fn().mockReturnValue(
            createRoom(cli, {
                findEventById: jest.fn().mockReturnValue({ mock: true }),
                getJoinedMembers: jest.fn().mockReturnValue([new RoomMember("#abc", "#mockUser")]),
            }),
        );
        cli.getStoredDevicesForUser = jest.fn().mockReturnValue([{}]);
        getPlatformSpy();
    });

    afterEach(() => {
        // @ts-ignore
        if (window.bwiAnalytics) {
            // @ts-ignore
            window.bwiAnalytics = undefined;
        }
        // @ts-ignore
        MatrixClientPeg.safeGet().getRoom.mockClear();
    });

    describe("sendAnalyticsFeatureSendPoll", () => {
        const checkPoll = async (pollType: KNOWN_POLL_KIND, showParticipants: boolean): Promise<void> => {
            const spy = jest.spyOn(BwiAnalyticsManager, "trackEvent");
            await sendAnalyticsFeatureSendPoll(pollType, 2, cli.getRoom("mock")!.roomId, showParticipants);
            const typeResult = pollType === M_POLL_KIND_DISCLOSED ? "disclosed" : "undisclosed";
            const finalResult = showParticipants ? typeResult + "_show_participants" : typeResult;
            expect(spy).toHaveBeenCalledWith("Feature", "SendPoll", finalResult, 2, []);
            spy.mockClear();
        };

        it("regular disclosed poll in regular room and no dimensions, send disclosed poll event", async () => {
            await checkPoll(M_POLL_KIND_DISCLOSED, false);
        });

        it("regular undisclosed poll in regular room and no dimensions, send undisclosed poll event", async () => {
            await checkPoll(M_POLL_KIND_UNDISCLOSED, false);
        });
        it("show participants disclosed poll in regular room and no dimensions, send disclosed poll event", async () => {
            await checkPoll(M_POLL_KIND_DISCLOSED, true);
        });

        it("show participants undisclosed poll in regular room and no dimensions, send undisclosed poll event", async () => {
            await checkPoll(M_POLL_KIND_UNDISCLOSED, true);
        });
    });

    describe("sendAnalyticsFeatureSendVoiceMessage", () => {
        const checkVoiceMessage = async (length: number, expectedResult: number): Promise<void> => {
            const spy = jest.spyOn(BwiAnalyticsManager, "trackEvent");
            await sendAnalyticsFeatureSendVoiceMessage(length, cli.getRoom("mock")!.roomId);
            expect(spy).toHaveBeenCalledWith(
                "Feature",
                "SendVoiceMessage",
                "send_voice_message_default",
                expectedResult,
                [],
            );
            spy.mockClear();
        };

        it("valid length and no dimensions round down, send voice message event", async () => {
            await checkVoiceMessage(3.4, 3);
        });

        it("valid length and no dimensions round up, send voice message event", async () => {
            await checkVoiceMessage(3.6, 4);
        });

        it("invalid length, send length 0", async () => {
            await checkVoiceMessage(-100, 0);
        });
    });

    describe("sendAnalyticsFeatureForwardMessage", () => {
        const checkForwardMessage = async (contentType: MsgType, result: string, isVoice = false): Promise<void> => {
            const spy = jest.spyOn(BwiAnalyticsManager, "trackEvent");
            let content: IContent = { msgtype: contentType };
            if (isVoice) {
                content = {
                    ...content,
                    "org.matrix.msc2516.voice": {},
                };
            }

            await sendAnalyticsFeatureForwardMessage(content, cli.getRoom("mock")!.roomId);
            expect(spy).toHaveBeenCalledWith("Feature", "ForwardMessage", result, undefined, []);
            spy.mockClear();
        };

        it("text, send text forward message event", async () => {
            await checkForwardMessage(MsgType.Text, "text");
        });

        it("image, send image forward message event", async () => {
            await checkForwardMessage(MsgType.Image, "image");
        });

        it("file, send file forward message event", async () => {
            await checkForwardMessage(MsgType.File, "file");
        });

        it("location, send location forward message event", async () => {
            await checkForwardMessage(MsgType.Location, "location");
        });

        it("video, send video forward message event", async () => {
            await checkForwardMessage(MsgType.Video, "video");
        });

        it("audio, send audio forward message event", async () => {
            await checkForwardMessage(MsgType.Audio, "audio");
        });

        it("voice, send voice forward message event", async () => {
            await checkForwardMessage(MsgType.Audio, "voice_message", true);
        });
    });

    describe("sendAnalyticsGeneralLogout", () => {
        it("regular logout, send logout event", async () => {
            const spy = jest.spyOn(BwiAnalyticsManager, "trackEvent");
            await sendAnalyticsGeneralLogout();
            expect(spy).toHaveBeenCalledWith("General", "Logout", "logout_default");
            spy.mockClear();
        });
    });

    describe("sendHuddleFeatureMessage", () => {
        const checkHuddle = async (event: HuddleFeatureEvents, result: string): Promise<void> => {
            const spy = jest.spyOn(BwiAnalyticsManager, "trackEvent");
            await sendHuddleFeatureMessage(event);
            expect(spy).toHaveBeenCalledWith("Feature", "Huddle", result);
            spy.mockClear();
        };

        it("FeatureAnalyticsEvents.Feature_Huddle_Created", async () => {
            await checkHuddle(FeatureAnalyticsEvents.Huddle_Created, "created_huddle");
        });

        it("FeatureAnalyticsEvents.Feature_Huddle_Joined", async () => {
            await checkHuddle(FeatureAnalyticsEvents.Huddle_Joined, "joined_huddle");
        });

        it("FeatureAnalyticsEvents.Feature_Huddle_Full", async () => {
            await checkHuddle(FeatureAnalyticsEvents.Huddle_Full, "huddle_is_full");
        });
    });

    describe("getRoomDevicesDimension tested with sendAnalyticsEncryptionViewMessage", () => {
        beforeEach(() => {
            BwiAnalyticsManager.configuredDimensions = {
                RoomDevicesCount: 1,
            };
        });

        const createMockDeviceMap = (): DeviceMap => {
            const devicesMap: DeviceMap = new Map<string, Map<string, Device>>();
            const userDevices = new Map<string, Device>();
            const mockDevice = new Device({
                algorithms: [],
                deviceId: cli.deviceId ?? "",
                keys: new Map<string, string>(),
                userId: cli.getUserId() ?? "",
            });
            userDevices.set(cli.deviceId ?? "mockDevice", mockDevice);
            devicesMap.set(cli.getUserId() ?? "mockUser", userDevices);
            return devicesMap;
        };

        const checkDeviceDimension = async (
            dimensions: Dimension[],
            devicesCall: "hasDevices" | "noDevices" | "rejection",
        ): Promise<void> => {
            const resultSpy = jest.spyOn(BwiAnalyticsManager, "trackEvent");
            if (devicesCall === "hasDevices") {
                cli.getCrypto = jest.fn().mockReturnValue({
                    getUserDeviceInfo: jest.fn().mockResolvedValue(createMockDeviceMap()),
                });
            } else if (devicesCall === "noDevices") {
                cli.getCrypto = jest.fn().mockReturnValue({
                    getUserDeviceInfo: jest.fn().mockResolvedValue([]),
                });
            } else {
                cli.getCrypto = jest.fn().mockReturnValue({
                    getUserDeviceInfo: jest.fn().mockRejectedValue([]),
                });
            }
            await sendAnalyticsEncryptionViewMessage("404", 2, cli.getRoom("mockRoom")?.roomId ?? undefined);
            expect(resultSpy).toHaveBeenCalledWith("Encryption", "ViewMessage", "404", 2, dimensions);
            resultSpy.mockClear();
        };

        it("devices can be read, add RoomDevicesCount dimension", async () => {
            const expectedDimension: Dimension = {
                name: "RoomDevicesCount",
                value: "1 bis 10",
                id: 1,
            };

            await checkDeviceDimension([expectedDimension], "hasDevices");
        });

        it("no devices returned, undefined value for dimension", async () => {
            const expectedDimension: Dimension = {
                name: "RoomDevicesCount",
                value: "Undefiniert",
                id: 1,
            };

            await checkDeviceDimension([expectedDimension], "noDevices");
        });

        it("error during device retrieval, catch block", async () => {
            await checkDeviceDimension([], "rejection");
        });
    });
});
