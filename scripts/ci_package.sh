#!/usr/bin/env bash

# Runs package.sh, passing DIST_VERSION determined by git

set -ex

rm dist/element-*.tar.gz || true # rm previous artifacts without failing if it doesn't exist

# Since the deps are fetched from git, we can rev-parse
REACT_SHA=$(cd ../node_modules/matrix-react-sdk; git rev-parse --short=12 HEAD)
JSSDK_SHA=$(cd ../node_modules/matrix-js-sdk; git rev-parse --short=12 HEAD)

CI_PACKAGE=true DIST_VERSION=$DIST_VERSION scripts/package.sh
