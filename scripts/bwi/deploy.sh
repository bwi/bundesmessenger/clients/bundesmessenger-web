#!/bin/bash

set -e

if [ -n "$DIST_VERSION" ]; then
    VERSION=$DIST_VERSION
else
    VERSION=`git describe --tags || echo unknown`
fi

PLATFORM="bwm-$1"
PACKAGE="dist/bundesmessenger-$VERSION-$PLATFORM.tar.gz"

./scripts/bwi/package.sh "devlab/$PLATFORM"

ssh-add <(echo "$DEPLOYMENT_IDENTITY")
ssh -p22 $DEPLOYMENT_USER@$DEPLOYMENT_SERVER "rm -rf /data/www/$1/*"
scp -p22 $PACKAGE $DEPLOYMENT_USER@$DEPLOYMENT_SERVER:/data/www/$1/$1.tar.gz
ssh -p22 $DEPLOYMENT_USER@$DEPLOYMENT_SERVER "cd /data/www/$1/ && tar -xvzf $1.tar.gz --strip-components 1 && sudo service nginx reload && rm /data/www/$1/$1.tar.gz"
echo "Deployed $PACKAGE to $1"
