#!/bin/bash

# BWI (MESSENGER 4215): Exclude files from artifact and ensure they are not included in the build

excluded_files=("welcome.html" "jitsi.html" "jitsi_external_api.min.js")
error=false

for file in "${excluded_files[@]}"
do
    if [ -f "$file" ]; then
        echo "Error! Build contains excluded file '$file'"
        error=true
    fi
done

[ "$error" = true ] && exit 1

echo "Success! No excluded files included"
exit 0
