#!/bin/bash

set -x

ROOT=$(pwd)
LINK_FOLDER=../.yarn/links

"$(dirname $0)/setup_yarn.sh"

# Validation installation
validate() {
    yarn install --prefer-offline --pure-lockfile
}

cd "$ROOT/matrix-js-sdk" && validate && yarn link --link-folder "$LINK_FOLDER"
cd "$ROOT/element-web" && validate && yarn link matrix-js-sdk --link-folder "$LINK_FOLDER"
cd $ROOT
