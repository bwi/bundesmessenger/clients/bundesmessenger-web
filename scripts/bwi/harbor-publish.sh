#!/bin/bash

# Copyright 2022 BWI GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if [ -n "$CI_COMMIT_TAG" ]; then
    defbranch="master"

    cleanTag=`$(dirname $0)/normalize-version.sh ${CI_COMMIT_TAG}`
    if [[ $cleanTag =~ -rc.[0-9]{1,3}$ ]]; then
        releasebranch=$(cut -f1 -d"-" <<< $cleanTag)
        defbranch="release/$releasebranch"
    fi
fi

docker build --build-arg GITLAB_USERNAME=gitlab-ci-token --build-arg GITLAB_PASSWORD=${CI_JOB_TOKEN} \
--build-arg WEB_BRANCH=${defbranch} --build-arg REACT_SDK_BRANCH=${defbranch} --build-arg JS_SDK_BRANCH=${defbranch} \
--build-arg CONFIG_BRANCH=${defbranch} --build-arg JS_REPO=${JS_REPO} --build-arg REACT_REPO=${REACT_REPO} \
--build-arg WEB_REPO=${WEB_REPO} --build-arg WEB_CONFIG_REPO=${WEB_CONFIG_REPO} \
--build-arg GIT_PROJECT_NAMESPACE=${CI_PROJECT_NAMESPACE} --build-arg GIT_PROJECT_HOST=dl-gitlab.example.com \
-t $DOCKER_REGISTRY/$CI_PROJECT_ROOT_NAMESPACE/$CI_PROJECT_NAME:$CI_COMMIT_TAG ./

docker push $DOCKER_REGISTRY/$CI_PROJECT_ROOT_NAMESPACE/$CI_PROJECT_NAME:$CI_COMMIT_TAG
docker rmi $DOCKER_REGISTRY/$CI_PROJECT_ROOT_NAMESPACE/$CI_PROJECT_NAME:$CI_COMMIT_TAG
