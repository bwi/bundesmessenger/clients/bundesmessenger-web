#!/bin/bash

set -x

mkdir element-web
# Filter out:

#   - scripts (Just for convenience so we can execute scripts from the project root)
#   - .yarn (Do not move the mounted .yarn cache)
#   - element-web (Can be prepopulated by the cache element-web/node_modules)
#   - matrix-js-sdk (Same as element-web)

ls -A | grep -v -e .yarn -e element-web -e matrix-js-sdk | xargs mv -t element-web
#   - Copy some script for convenience so we can execute scripts from the project root
mkdir -p scripts/bwi
cp \
    element-web/scripts/bwi/link_workspace.sh \
    element-web/scripts/bwi/setup_yarn.sh \
    element-web/scripts/bwi/fetchdep.sh \
    element-web/scripts/bwi/normalize-version.sh \
    scripts/bwi

"$(dirname $0)/setup_yarn.sh"

YARN_LINK=../.yarn/link

echo "installing..."

# js-sdk
"$(dirname $0)/fetchdep.sh" $CI_PROJECT_NAMESPACE $JS_REPO matrix-js-sdk
pushd matrix-js-sdk
yarn cache clean && yarn install --network-timeout=100000 --pure-lockfile
popd

# element-web
"$(dirname $0)/fetchdep.sh" $CI_PROJECT_NAMESPACE $WEB_REPO element-web
pushd element-web
yarn cache clean && yarn install --network-timeout=100000 --pure-lockfile
popd

"$(dirname $0)/fetchdep.sh" $CI_PROJECT_NAMESPACE $WEB_CONFIG_REPO config-and-assets
