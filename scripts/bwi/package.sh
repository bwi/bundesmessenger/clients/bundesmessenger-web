#!/bin/bash

set -e

if [ -n "$DIST_VERSION" ]; then
    VERSION=$DIST_VERSION
else
    VERSION=`git describe --tags || echo unknown`
fi

# whether we want to build all or choosen platforms
if [ -n "$1" ]; then
    PLATFORMS=$1
else
    PLATFORMS="$(find ../config-and-assets/config/ -maxdepth 1 -name '*.json' -type f | xargs -L1 -I{} basename "{}" .json)"
fi

for PLATFORM in $PLATFORMS
do
    BASECONFIGPATH="../config-and-assets/config/base/base.json"
    CONFIGPATH="../config-and-assets/config/$PLATFORM.json"
    BUILD="messenger"

    if ! test -f "$CONFIGPATH"; then
        echo "ERR: couldnt find config with name '$CONFIG'"
        exit 1
    fi

    # clean build output directory
    if [ -d "$BUILD" ]; then
        rm -r $BUILD
    fi

    mkdir -p $BUILD

    # reset assets
    git reset --hard
    git clean -f

    # copy additional assets if there are any
    PLATFORM_ASSETS="../config-and-assets/assets/$PLATFORM"
    if [ -d "$PLATFORM_ASSETS" ]; then
        echo "copying aditional assets for $PLATFORM"
        cp -r $PLATFORM_ASSETS/* ./res
        cp -r $PLATFORM_ASSETS/* $BUILD
    fi

    # we wanna build for each platform because of the assets :)
    "$(dirname $0)/build.sh"

    PLATFORM="$(basename -- $CONFIGPATH .json)"
    PLATFORM_FOLDER=messenger-$PLATFORM

    # create temp folder for current platform
    echo "creating platform folder: $PLATFORM_FOLDER"
    mkdir -p $PLATFORM_FOLDER

    # copy build
    echo "copying build: $BUILD"
    cp -r $BUILD/* $PLATFORM_FOLDER

    # copy config if $NO_CONFIG isnt set
    if [[ -z $NO_CONFIG ]]; then
        # merge config with base config
        echo "merging platform config: $CONFIGPATH with $BASECONFIGPATH into $PLATFORM_FOLDER/config.json"
        jq -s '.[0] * .[1]' $BASECONFIGPATH $CONFIGPATH | sponge $PLATFORM_FOLDER/config.json

        # fill env vars
        echo "filling env vars"
        envsubst < $PLATFORM_FOLDER/config.json | sponge $PLATFORM_FOLDER/config.json
    fi

    tar chzf dist/$PLATFORM_FOLDER-$VERSION.tar.gz $PLATFORM_FOLDER
    cd dist && sha256sum $PLATFORM_FOLDER-$VERSION.tar.gz > $PLATFORM_FOLDER-$VERSION-hash.txt && cd ..

    echo "Packaged dist/$PLATFORM_FOLDER-$VERSION.tar.gz"
done

echo "done \o/"
