#!/bin/bash
#
# Copyright 2022 BWI GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Converts an svg logo into the various image resources required by
# the various platforms deployments.
#
# We don't need the electron icons for now so those are left out
# replaces icons in bwi/favicon folder
# icon in bwi/img needs to be replaced as well
#
# SVGs are not allowed to have width and height properties -> remove these properties before starting
#
# On debian-based systems you need these deps:
#   apt-get install xmlstarlet python3-cairosvg icnsutils

if [ $# != 1 ]
then
    echo "Usage: $0 <svg file>"
    exit
fi

set -e
set -x

tmpdir=`mktemp -d 2>/dev/null || mktemp -d -t 'icontmp'`

for i in 1024 512 310 256 192 180 152 150 144 128 120 114 96 76 72 70 64 60 57 48 36 32 24 16
do
    #convert -background none -density 1000 -resize $i -extent $i -gravity center "$1" "$tmpdir/$i.png"

    # Above is the imagemagick command to render an svg to png. Unfortunately, its support for SVGs
    # with CSS isn't very good (with rsvg and even moreso the built in renderer) so we use cairosvg.
    # This can be installed with:
    #    pip install cairosvg==1.0.22 # Version 2 doesn't support python 2
    #    pip install tinycss
    #    pip install cssselect # These are necessary for CSS support
    # You'll also need xmlstarlet from your favourite package manager
    #
    # Cairosvg doesn't suport rendering at a specific size (https://github.com/Kozea/CairoSVG/issues/83#issuecomment-215720176)
    # so we have to 'resize the svg' first (add width and height attributes to the svg element) to make it render at the
    # size we need.
    # XXX: This will break if the svg already has width and height attributes
    cp "$1" "$tmpdir/tmp.svg"
    xmlstarlet ed -N x="http://www.w3.org/2000/svg" --insert "/x:svg" --type attr -n width -v $i "$tmpdir/tmp.svg" > "$tmpdir/tmp2.svg"
    xmlstarlet ed -N x="http://www.w3.org/2000/svg" --insert "/x:svg" --type attr -n height -v $i "$tmpdir/tmp2.svg" > "$tmpdir/tmp3.svg"
    cairosvg -f png -o "$tmpdir/$i.png"  "$tmpdir/tmp3.svg"
    rm "$tmpdir/tmp.svg" "$tmpdir/tmp2.svg" "$tmpdir/tmp3.svg"
done

# non square tiles

# 310x150
cp "$1" "$tmpdir/tmp.svg"
xmlstarlet ed -N x="http://www.w3.org/2000/svg" --insert "/x:svg" --type attr -n width -v 310 "$tmpdir/tmp.svg" > "$tmpdir/tmp2.svg"
xmlstarlet ed -N x="http://www.w3.org/2000/svg" --insert "/x:svg" --type attr -n height -v 150 "$tmpdir/tmp2.svg" > "$tmpdir/tmp3.svg"
cairosvg -f png -o "$tmpdir/310x150.png"  "$tmpdir/tmp3.svg"
rm "$tmpdir/tmp.svg" "$tmpdir/tmp2.svg" "$tmpdir/tmp3.svg"

# 620x300
cp "$1" "$tmpdir/tmp.svg"
xmlstarlet ed -N x="http://www.w3.org/2000/svg" --insert "/x:svg" --type attr -n width -v 620 "$tmpdir/tmp.svg" > "$tmpdir/tmp2.svg"
xmlstarlet ed -N x="http://www.w3.org/2000/svg" --insert "/x:svg" --type attr -n height -v 300 "$tmpdir/tmp2.svg" > "$tmpdir/tmp3.svg"
cairosvg -f png -o "$tmpdir/620x300.png"  "$tmpdir/tmp3.svg"
rm "$tmpdir/tmp.svg" "$tmpdir/tmp2.svg" "$tmpdir/tmp3.svg"

# 1240x600
cp "$1" "$tmpdir/tmp.svg"
xmlstarlet ed -N x="http://www.w3.org/2000/svg" --insert "/x:svg" --type attr -n width -v 1240 "$tmpdir/tmp.svg" > "$tmpdir/tmp2.svg"
xmlstarlet ed -N x="http://www.w3.org/2000/svg" --insert "/x:svg" --type attr -n height -v 600 "$tmpdir/tmp2.svg" > "$tmpdir/tmp3.svg"
cairosvg -f png -o "$tmpdir/1240x600.png"  "$tmpdir/tmp3.svg"
rm "$tmpdir/tmp.svg" "$tmpdir/tmp2.svg" "$tmpdir/tmp3.svg"

# end non square tiles

cp "$tmpdir/36.png" "res/bwi/favicon/android-chrome-36.png"
cp "$tmpdir/48.png" "res/bwi/favicon/android-chrome-48.png"
cp "$tmpdir/72.png" "res/bwi/favicon/android-chrome-72.png"
cp "$tmpdir/96.png" "res/bwi/favicon/android-chrome-96.png"
cp "$tmpdir/144.png" "res/bwi/favicon/android-chrome-144.png"
cp "$tmpdir/192.png" "res/bwi/favicon/android-chrome-192.png"
cp "$tmpdir/180.png" "res/bwi/favicon/apple-touch-icon.png"
cp "$tmpdir/180.png" "res/bwi/favicon/apple-touch-icon-precomposed.png"
cp "$tmpdir/57.png" "res/bwi/favicon/apple-touch-icon-57.png"
cp "$tmpdir/60.png" "res/bwi/favicon/apple-touch-icon-60.png"
cp "$tmpdir/72.png" "res/bwi/favicon/apple-touch-icon-72.png"
cp "$tmpdir/76.png" "res/bwi/favicon/apple-touch-icon-76.png"
cp "$tmpdir/114.png" "res/bwi/favicon/apple-touch-icon-114.png"
cp "$tmpdir/120.png" "res/bwi/favicon/apple-touch-icon-120.png"
cp "$tmpdir/144.png" "res/bwi/favicon/apple-touch-icon-144.png"
cp "$tmpdir/152.png" "res/bwi/favicon/apple-touch-icon-152.png"
cp "$tmpdir/180.png" "res/bwi/favicon/apple-touch-icon-180.png"
cp "$tmpdir/16.png" "res/bwi/favicon/favicon-16.png"
cp "$tmpdir/32.png" "res/bwi/favicon/favicon-32.png"
cp "$tmpdir/96.png" "res/bwi/favicon/favicon-96.png"
cp "$tmpdir/70.png" "res/bwi/favicon/mstile-70.png"
cp "$tmpdir/144.png" "res/bwi/favicon/mstile-144.png"
cp "$tmpdir/150.png" "res/bwi/favicon/mstile-150.png"
cp "$tmpdir/310.png" "res/bwi/favicon/mstile-310.png"
cp "$tmpdir/310x150.png" "res/bwi/favicon/mstile-310x150.png"

#old way of generating the favicon
#convert "$tmpdir/16.png" "$tmpdir/32.png" "$tmpdir/64.png" "$tmpdir/128.png"  "$tmpdir/256.png" "res/bwi/favicon/favicon.ico"
inkscape -w 16 -h 16 -e "$tmpdir/16.png" "$1"
inkscape -w 32 -h 32 -e "$tmpdir/32.png" "$1"
inkscape -w 64 -h 64 -e "$tmpdir/64.png" "$1"
inkscape -w 128 -h 128 -e "$tmpdir/128.png" "$1"
convert "$tmpdir/16.png"  "$tmpdir/32.png" "$tmpdir/64.png" "$tmpdir/128.png" "res/bwi/favicon/favicon.ico"

# other custom formats, inkscape needed
inkscape -w 24 -h 24 -e "$tmpdir/24.png" "$1"
inkscape -w 44 -h 44 -e "$tmpdir/44.png" "$1"
inkscape -w 50 -h 50 -e "$tmpdir/50.png" "$1"
inkscape -w 76 -h 76 -e "$tmpdir/76.png" "$1"
inkscape -w 88 -h 88 -e "$tmpdir/88.png" "$1"
inkscape -w 120 -h 120 -e "$tmpdir/120.png" "$1"
inkscape -w 150 -h 150 -e "$tmpdir/150.png" "$1"
inkscape -w 152 -h 152 -e "$tmpdir/152.png" "$1"
inkscape -w 180 -h 180 -e "$tmpdir/180.png" "$1"
inkscape -w 300 -h 300 -e "$tmpdir/300.png" "$1"
inkscape -w 1024 -h 1024 -e "$tmpdir/1024.png" "$1"

cp "$tmpdir/24.png" "res/bwi/favicon/24.png"
cp "$tmpdir/44.png" "res/bwi/favicon/44.png"
cp "$tmpdir/50.png" "res/bwi/favicon/50.png"
cp "$tmpdir/76.png" "res/bwi/favicon/76.png"
cp "$tmpdir/88.png" "res/bwi/favicon/88.png"
cp "$tmpdir/120.png" "res/bwi/favicon/120.png"
cp "$tmpdir/150.png" "res/bwi/favicon/150.png"
cp "$tmpdir/152.png" "res/bwi/favicon/152.png"
cp "$tmpdir/180.png" "res/bwi/favicon/180.png"
cp "$tmpdir/300.png" "res/bwi/favicon/300.png"
cp "$tmpdir/620x300.png" "res/bwi/favicon/620x300.png"
cp "$tmpdir/1024.png" "res/bwi/favicon/1024.png"
cp "$tmpdir/1240x600.png" "res/bwi/favicon/1240x600.png"


rm -r "$tmpdir"
