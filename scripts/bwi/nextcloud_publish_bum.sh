# Copyright 2022 BWI GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

if [[ -z $NEXTCLOUD_BWM_USER || -z $NEXTCLOUD_BWM_PASS ]]; then
  echo 'ERR: user or pass unset'
  exit 1
fi

if [[ -z $NEXTCLOUD_BWM_URL ]]; then
  echo 'ERR: remote unset'
  exit 1
fi

if [[ -z $CI_COMMIT_TAG ]]; then
  echo 'ERR: this script should only run in tags'
  exit 1
fi

remotePath="remote.php/dav/files/$NEXTCLOUD_BWM_USER/BundesMessenger/Releases/Web"

check_response () {
    echo "checking response: $1"
    if [[ "$1" -ne 200 ]] && [[ "$1" -ne 201 ]] && [[ "$1" -ne 204 ]]  ; then
        echo "ERR: failed with status code: $1"
        exit 1
    fi
}

# create folder for tag
status_code_folder=$(curl -sw '%{http_code}' -u $NEXTCLOUD_BWM_USER:$NEXTCLOUD_BWM_PASS -X MKCOL $NEXTCLOUD_BWM_URL/$remotePath/bundesmessenger-$CI_COMMIT_TAG/)
echo "creating folder"
check_response "$status_code_folder"

packages=$(find ./dist/ -maxdepth 1 -name 'messenger-*.tar.gz' -type f)
for package in $packages
do
    packageName="$(basename -- $package .tar.gz)"

    if ! test -f "./dist/$packageName-hash.txt"; then
        echo "ERR: hash for $packageName doesnt exist"
        exit 1
    fi

    status_code_tar=$(curl -sw '%{http_code}' -u $NEXTCLOUD_BWM_USER:$NEXTCLOUD_BWM_PASS -T ./dist/$packageName.tar.gz $NEXTCLOUD_BWM_URL/$remotePath/bundesmessenger-$CI_COMMIT_TAG/$packageName.tar.gz)
    echo "uploading tar"
    check_response "$status_code_tar"

    status_code_hash=$(curl -sw '%{http_code}' -u $NEXTCLOUD_BWM_USER:$NEXTCLOUD_BWM_PASS -T ./dist/$packageName-hash.txt $NEXTCLOUD_BWM_URL/$remotePath/bundesmessenger-$CI_COMMIT_TAG/$packageName-hash.txt)
    echo "uploading hash"
    check_response "$status_code_hash"
done
