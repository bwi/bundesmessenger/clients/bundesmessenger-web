#!/bin/bash

set -e

if [ -n "$DIST_VERSION" ]; then
    VERSION=$DIST_VERSION
else
    VERSION=`git describe --tags || echo unknown`
fi

# building
BUILD="messenger"

mkdir -p $BUILD

yarn clean
yarn build

cp ../config-and-assets/config/base/base.json $BUILD/config.sample.json

cp -r webapp/* $BUILD

# if $version looks like semver with leading v, strip it before writing to file
if [[ ${VERSION} =~ ^v[[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+(-.+)?$ ]]; then
    echo ${VERSION:1} > $BUILD/version
else
    echo ${VERSION} > $BUILD/version
fi

# copy element version
cp elementVersion $BUILD

# prepare packaging
mkdir -p dist
