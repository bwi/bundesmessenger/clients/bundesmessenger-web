if [[ -z $USER || -z $PASS ]]; then
  echo 'ERR: user pass or repo unset'
  exit 1
fi

if [ -n "$CI_COMMIT_TAG" ]; then
    DIST_VERSION=`git describe --tags`
    NEXUS_REPO=$RELEASE_REPO
else
    DIST_VERSION=`npm pkg get version | sed 's/"//g'`-SNAPSHOT.`date +'%s'`
    NEXUS_REPO=$SNAPSHOT_REPO
fi

if [[ -z $NEXUS_REPO ]]; then
  echo 'ERR: repo unset'
  exit 1
fi

AUTH="$(echo -n "$USER:$PASS" | base64)"
AUTHKEY=`echo "$NEXUS_REPO" | sed 's/https\?://'`

echo "always-auth=true" > ~/.npmrc
echo "registry=$NEXUS_REPO" >> ~/.npmrc
echo "$AUTHKEY:_auth=$AUTH" >> ~/.npmrc
echo "email=ci@example.com" >> ~/.npmrc

echo "packaging: $DIST_VERSION"
DIST_VERSION=$DIST_VERSION NO_CONFIG="true" "$(dirname $0)/package.sh"

BUILDS=$(find ./ -maxdepth 1 -name 'messenger-*' -type d)

for BUILD in $BUILDS
do
    if [ -d "$BUILD" ]; then
        BUILD_NAME=$(basename $BUILD)

        # copy over package info
        cp package.json $BUILD

        # move into build directory
        cd $BUILD

        # set version
        npm version $DIST_VERSION --no-git-tag-version

        # private should be set to false for publishing
        jq '.private = false' package.json | sponge package.json

        # patch package name with variant
        jq ".name = \"@bwi/$BUILD_NAME\"" package.json | sponge package.json

        # allow all files to be packed in build dir
        jq 'del(.files)' package.json | sponge package.json

        echo "publishing $BUILD_NAME to $NEXUS_REPO"
        if ! npm publish --verbose
        then
           echo "npm publish failed"
           exit 1
        else
         echo "npm package successfully published"
        fi

        cd ../
    fi
done

# cleanup
rm ~/.npmrc
