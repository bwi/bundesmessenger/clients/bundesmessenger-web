#!/bin/bash

BASECONFIGPATH="../bundesmessenger-web-config-and-assets/config/base/base.json"
CONFIGPATH="../bundesmessenger-web-config-and-assets/config/$1.json"
DEBUG=$2

if ! test -f "$CONFIGPATH"; then
    echo "ERR: couldnt find config with name '$1'"
    exit 1
fi

jq -s '.[0] * .[1]' $BASECONFIGPATH $CONFIGPATH | sponge ./config.json

if [ "$DEBUG" == "true" ]; then
    echo "debug enabled"
    jq -s '.[0] * { "bwiSettingDefaults": { "BwiFeature.hideDebug": false }, "settingDefaults": { "UIFeature.advancedSettings": true } }' ./config.json | sponge ./config.json
fi

echo "done \o/"
