// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { QueryDict } from "matrix-js-sdk/src/utils";

import { MatrixClientPeg } from "../../MatrixClientPeg";
import { getCurrentView, Views } from "../../components/structures/MatrixChat";
import dis from "../../dispatcher/dispatcher";
import { Action } from "../../dispatcher/actions";
import * as Lifecycle from "../../Lifecycle";

export const tryAndRedirectExistingSession = (params: QueryDict): void => {
    /**
     * only try and restore session from login page
     * because nv doesnt support that case
     * we should also check if a login token is set
     * so we dont interfere with token login in general
     */
    if (getCurrentView() === Views.LOGIN) {
        Lifecycle.getStoredSessionVars().then((storedSession) => {
            if (!params?.loginToken && storedSession.hasAccessToken && storedSession.deviceId) {
                Lifecycle.restoreSessionFromStorage({
                    ignoreGuest: false,
                })
                    .then((success) => {
                        if (success) {
                            const cli = MatrixClientPeg.safeGet();
                            const isLoggedOutOrGuest = !cli || cli.isGuest();

                            if (!isLoggedOutOrGuest) {
                                // user is logged in and landing on an auth page which will uproot their session, redirect them home instead
                                dis.dispatch({ action: Action.ViewHomePage });
                                return;
                            }
                        }
                    })
                    .catch(() => console.debug("BWI Session restore failed"));
            }
        });
    }
};
