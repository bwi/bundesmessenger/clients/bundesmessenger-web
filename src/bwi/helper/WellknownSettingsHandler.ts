/*
Copyright 2017 Travis Ralston
Copyright 2019, 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import { ClientEvent, IClientWellKnown, MatrixClient } from "matrix-js-sdk/src/matrix";

import { BwiFeature } from "../../bwi/settings/BwiFeature";
import { SettingLevel } from "../../settings/SettingLevel";
import { WatchManager } from "../../settings/WatchManager";
import MatrixClientBackedSettingsHandler from "../../settings/handlers/MatrixClientBackedSettingsHandler";
import { bwiExtractWellKnownConfig } from "../functions";

/**
 * Gets and sets settings at the "account" level for the current user.
 * This handler does not make use of the roomId parameter.
 */
export default class BwiWellknownSettingsHandler extends MatrixClientBackedSettingsHandler {
    public constructor(public readonly watchers: WatchManager) {
        super();
    }

    public get level(): SettingLevel {
        return SettingLevel.WELLKNOWN;
    }

    public initMatrixClient(oldClient: MatrixClient, newClient: MatrixClient): void {
        oldClient?.off(ClientEvent.ClientWellKnown, this.onWellKnown);
        newClient.on(ClientEvent.ClientWellKnown, this.onWellKnown);
    }

    private onWellKnown = (wellknown: IClientWellKnown): void => {
        const config = bwiExtractWellKnownConfig(wellknown);

        // Notify settings
        this.watchers.notifyUpdate(
            BwiFeature.Federation,
            null,
            SettingLevel.WELLKNOWN,
            config?.federation?.enable ?? false,
        );
    };

    public getValue(settingName: string, roomId: string): any {
        const config = bwiExtractWellKnownConfig(this.client.getClientWellKnown());
        if (settingName === BwiFeature.Federation) {
            return config?.federation?.enable ?? false;
        }

        throw new Error(`${settingName} is not supported by ${SettingLevel.WELLKNOWN}`);
    }
    public setValue(settingName: string, roomId: string, newValue: any): Promise<void> {
        throw new Error(`Can not set config values at ${SettingLevel.WELLKNOWN} level`);
    }

    public canSetValue(settingName: string, roomId: string): boolean {
        // read only config
        return false; // It's their account, so they should be able to
    }

    public isSupported(): boolean {
        return this.client && !!this.client.getClientWellKnown();
    }
}
