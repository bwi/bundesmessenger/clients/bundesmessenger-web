// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import SettingsStore from "../settings/SettingsStore";
import { BwiFeature } from "./settings/BwiFeature";
import { SettingKey } from "../settings/Settings.tsx";

export enum AdminTool {
    Mute = "MUTE",
    Kick = "KICK",
    Ban = "BAN",
    Redact = "REDACT",
}

export const isAdminToolEnabled = (t: AdminTool): boolean => {
    return (SettingsStore.getValue(BwiFeature.AdminToolsEnabled as SettingKey) as unknown as AdminTool[]).includes(t);
};
