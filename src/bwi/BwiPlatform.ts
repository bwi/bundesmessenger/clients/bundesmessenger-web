// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

export class BwiPlatform {
    private elementVersion: string | null = null;
    private elementVersionPending = false;

    private fetchElementVersion = async (): Promise<string> => {
        this.elementVersionPending = true;
        const response = await fetch(`elementVersion?t=${Date.now()}`);

        if (response.status < 200 || response.status >= 300) {
            throw new Error();
        }

        const version = (await response.text()).trim();
        this.elementVersion = version;

        return version;
    };

    public getElementVersion(): Promise<string> {
        if (this.elementVersion !== null || this.elementVersionPending) {
            return Promise.resolve(this.elementVersion || "");
        }
        return this.fetchElementVersion();
    }
}

if (!window.bwiPlatform) {
    window.bwiPlatform = new BwiPlatform();
}
export default window.bwiPlatform;
