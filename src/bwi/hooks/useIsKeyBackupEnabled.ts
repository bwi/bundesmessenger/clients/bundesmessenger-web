// Copyright 2024 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { useEffect, useState } from "react";

import { useMatrixClientContext } from "../../contexts/MatrixClientContext";

export const SECRET_IS_CLOSED = "BWI_SECRET_MODAL_IS_CLOSED";
const secretClosedEvent = new Event(SECRET_IS_CLOSED);
export const CROSS_SIGNING_COMPLETED = "BWI_CROSS_SIGNING_MODAL_IS_CLOSED";
const crossSigningClosedEvent = new Event(CROSS_SIGNING_COMPLETED);

export function emitSecretStorageClosed(): void {
    window.dispatchEvent(secretClosedEvent);
}

export function emitCrossSigningClosed(): void {
    window.dispatchEvent(crossSigningClosedEvent);
}

export function useSecretStorageModalClosed(): boolean {
    const [isModalClosed, setIsModalClosed] = useState(false);
    useEffect(() => {
        const listener = (): void => {
            setIsModalClosed(true);
        };
        window.addEventListener(SECRET_IS_CLOSED, listener);

        return () => {
            window.removeEventListener(SECRET_IS_CLOSED, listener);
        };
    }, []);

    return isModalClosed;
}

export function useCrossSigningModalClosed(): boolean {
    const [isCrossSigningModalClosed, setIsCrossSigningModalClosed] = useState(false);
    useEffect(() => {
        const listener = (): void => {
            setIsCrossSigningModalClosed(true);
        };
        window.addEventListener(CROSS_SIGNING_COMPLETED, listener);

        return () => {
            window.removeEventListener(CROSS_SIGNING_COMPLETED, listener);
        };
    }, []);

    return isCrossSigningModalClosed;
}

export function useIsKeyBackupEnabled(): boolean {
    const cli = useMatrixClientContext();
    const isModalClosed = useSecretStorageModalClosed();
    const isCrossSigningModalClosed = useCrossSigningModalClosed();
    const [isKeyBackupEnabled, setKeyBackupEnabled] = useState(false);

    useEffect(() => {
        async function queryCrypto(): Promise<void> {
            const crypto = cli.getCrypto();
            const userId = cli.getUserId();
            if (crypto && userId) {
                try {
                    const isBackupEnabled = (await crypto.getActiveSessionBackupVersion()) !== null;
                    if (!isCrossSigningModalClosed || isBackupEnabled) {
                        setKeyBackupEnabled(isBackupEnabled);
                    } else {
                        const isCrossSigned = (await crypto.getUserVerificationStatus(userId)).isVerified();
                        setKeyBackupEnabled(isCrossSigned);
                    }
                } catch {}
            }
        }

        queryCrypto();
    }, [cli, isModalClosed, isCrossSigningModalClosed]);

    return isKeyBackupEnabled;
}
