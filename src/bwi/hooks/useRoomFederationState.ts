// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { EventType, MatrixEvent, Room, RoomEvent } from "matrix-js-sdk/src/matrix";
import { useCallback, useState } from "react";

import { useTypedEventEmitter } from "../../hooks/useEventEmitter";
import { bwiIsRoomFederated } from "../functions/federation";

export function useRoomFederationState(room: Room): boolean {
    const [isFederated, setIsFederated] = useState(bwiIsRoomFederated(room));

    const handleTimelineUpdate = useCallback((event: MatrixEvent, updatedRoom: Room) => {
        if (event.getType() === EventType.RoomServerAcl) {
            setIsFederated(bwiIsRoomFederated(updatedRoom));
        }
    }, []);

    useTypedEventEmitter(room, RoomEvent.Timeline, handleTimelineUpdate);

    return isFederated;
}
