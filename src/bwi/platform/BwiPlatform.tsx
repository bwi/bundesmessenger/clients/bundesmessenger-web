// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
import { UpdateCheckStatus, UpdateStatus } from "../..//BasePlatform";
import WebPlatform from "../../vector/platform/WebPlatform";

/*
 * BWI MESSENGER-3670
 * We always rely on the included version file and no env variables.
 * That's why we have our own Platform implementation which caches the api call
 */
export default class BwiPlatform extends WebPlatform {
    private runningVersionPromise?: Promise<string>;

    public getHumanReadableName(): string {
        return "BWI Platform"; // no translation required: only used for analytics
    }

    public getNormalizedAppVersion(version: string): string {
        return version;
    }

    public pollForUpdate = (
        showUpdate: (currentVersion: string, mostRecentVersion: string) => void,
        showNoUpdate?: () => void,
    ): Promise<UpdateStatus> => {
        return super.getMostRecentVersion().then(
            async (mostRecentVersion) => {
                const currentVersion = this.getNormalizedAppVersion(await this.getAppVersion());
                if (currentVersion !== mostRecentVersion) {
                    if (this.shouldShowUpdate(mostRecentVersion)) {
                        console.log("Update available to " + mostRecentVersion + ", will notify user");
                        showUpdate(currentVersion, mostRecentVersion);
                    } else {
                        console.log("Update available to " + mostRecentVersion + " but won't be shown");
                    }
                    return { status: UpdateCheckStatus.Ready };
                } else {
                    console.log("No update available, already on " + mostRecentVersion);
                    showNoUpdate?.();
                }

                return { status: UpdateCheckStatus.NotAvailable };
            },
            (err) => {
                return {
                    status: UpdateCheckStatus.Error,
                    detail: err.message || err.status ? err.status.toString() : "Unknown Error",
                };
            },
        );
    };

    public async getVersion(): Promise<string> {
        return fetch("version", {
            method: "GET",
            cache: "no-cache",
        })
            .then(async (res) => {
                if (res.ok) {
                    const text = await res.text();
                    const ver = text.trim();
                    return ver;
                } else return "";
            })
            .catch(() => {
                return "";
            });
    }

    public getAppVersion(): Promise<string> {
        if (!this.runningVersionPromise) {
            this.runningVersionPromise = this.getVersion();
        }
        return this.runningVersionPromise;
    }
}
