// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { _t } from "../../languageHandler";
import SdkConfig from "../../SdkConfig";

export type PasswordPolicyRules = {
    minLength?: number;
    minCaps?: number;
    minLows?: number;
    minSpecial?: number;
    minNum?: number;
};

export type PasswordPolicies = {
    length?: string;
    caps?: string;
    lows?: string;
    specials?: string;
    numbers?: string;
};

export const getPasswordPolicies = (): PasswordPolicies | undefined => {
    const policy = SdkConfig.get().password_policy;

    if (!policy) return undefined;

    const length = policy.minLength;
    const caps = policy.minCaps;
    const lows = policy.minLows;
    const specials = policy.minSpecial;
    const nums = policy.minNum;

    if (length || caps || lows || specials || nums) {
        const policies: PasswordPolicies = {};
        if (length) {
            policies.length = `${_t("min.")} ${length} ${_t("Character", { count: length })}`;
        }
        if (caps) {
            policies.caps = `${_t("min.")} ${caps} ${_t("Uppercase letter", { count: caps })}`;
        }
        if (lows) {
            policies.lows = `${_t("min.")} ${lows} ${_t("Lowercase letter", { count: lows })}`;
        }
        if (specials) {
            policies.specials = `${_t("min.")} ${specials} ${_t("Special character", { count: specials })}`;
        }
        if (nums) {
            policies.numbers = `${_t("min.")} ${nums} ${_t("Number", { count: nums })}`;
        }
        return policies;
    } else return undefined;
};

export const isValidPasswordPolicy = (): boolean => {
    const policy = SdkConfig.get().password_policy;

    if (policy) {
        const length = policy.minLength;
        const caps = policy.minCaps;
        const lows = policy.minLows;
        const specials = policy.minSpecial;
        const nums = policy.minNum;

        if (length || caps || lows || specials || nums) return true;
    }
    return false;
};
