// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { _td } from "../../languageHandler";
import { SettingLevel } from "../../settings/SettingLevel";
import { LEVELS_UI_FEATURE, LEVELS_ACCOUNT_SETTINGS, LabGroup, Settings } from "../../settings/Settings";
import ReloadOnChangeController from "../../settings/controllers/ReloadOnChangeController";
import { BwiFeature } from "./BwiFeature";

export const BWI_SETTINGS: Settings = {
    /**
     * Closes the chat image viewer when the user triggers a navigate event in the browser
     */
    [BwiFeature.CloseImageViewOnNavigate]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     *  Shows an external password reset url in the login modal
     */
    // @ts-ignore TODO: need to fix typing
    [BwiFeature.ExternalPasswordResetUrl]: {
        supportedLevels: LEVELS_UI_FEATURE,
        // @ts-ignore TODO: need to fix typing
        default: "",
    },
    /**
     * Hides ip addresses in user settings -> security
     */
    [BwiFeature.HideIpAdresses]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * hides low priority rooms & the option to mark a room as low priority from the context menu
     */
    [BwiFeature.HideLowPriorityRoomOption]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * hides certain debug features
     */
    [BwiFeature.HideDebug]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * the user doesn't need to specify a room address when publicizing
     */
    [BwiFeature.RoomAutoPublish]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * hides additional details on room creation
     */
    [BwiFeature.CreateRoomHideDetails]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * since cross signing is disabled by default
     * this one also hides some additional information
     */
    [BwiFeature.RestorePassphraseHideDeviceHint]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: true,
    },
    /**
     * when an update is available the application by default shows the changelog
     * from the matrix GitHub, this flag disables it
     */
    [BwiFeature.SkipUpdateChangesDialog]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * hides the notification button in the room header, its currently empty
     */
    [BwiFeature.HideRoomHeaderNotifsButton]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * hides some additional user settings
     * that are too complicated for the 0815 user
     */
    [BwiFeature.HideNerdyUserSettings]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * hides the room settings -> security tab
     */
    [BwiFeature.RoomSecuritySettings]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: true,
    },
    /**
     * file summary in encrypted rooms doesn't work, so we hide it
     */
    [BwiFeature.RoomSummaryHideFiles]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * whitelists admin features by key
     */
    // @ts-ignore TODO: need to fix typing
    [BwiFeature.AdminToolsEnabled]: {
        supportedLevels: LEVELS_UI_FEATURE,
        // @ts-ignore
        default: ["MUTE", "KICK", "BAN", "REDACT"],
    },
    /**
     * disables the option to export e2e keys
     * in multiple locations
     */
    [BwiFeature.DisableKeyExport]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * hides some additional text when creating a dm
     */
    [BwiFeature.HideDmInviteLink]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * Enable Personal Notes via config and toggle visibility
     */
    [BwiFeature.ShowPersonalNotes]: {
        supportedLevels: LEVELS_ACCOUNT_SETTINGS,
        default: false,
        displayName: _td("Show personal notes room (under favourites)"),
    },
    /**
     *  Disables message history sharing when inviting a user
     */
    [BwiFeature.DisableHistorySharing]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * Hides the voice message icon in the composer
     */
    [BwiFeature.DisableVoiceMessages]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * By default a user can skip the passphrase dialog.
     * If turned on, it is not possible to skip the passphrase
     * dialog and use an unencrypted session
     */
    [BwiFeature.EnforcePassphrase]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * Hides export chat buttons
     */
    [BwiFeature.ExportChat]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: true,
    },
    /**
     * By default Notifications supplied to the NotificationsHandler would not be shown.
     * Enabling this feature supports showing one-time dialogs.
     */
    [BwiFeature.Celebration]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * By default Matomo Error Analytics is disabled.
     * It can be enabled by this feature, if a configuration is supported in the config.json.
     */
    [BwiFeature.Analytics]: {
        supportedLevels: [SettingLevel.ACCOUNT],
        default: false,
        description: _td("Details can be found in the privacy policy."),
    },
    /**
     * Show register button in login screen with additional information
     * on how to receive an account
     */
    [BwiFeature.RegistrationDetails]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * Show extra tab in the User Settings to find the latest release notes (Release Notes Feature) listed
     */
    [BwiFeature.NewFeatures]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * Hides Invite Button for Dm's
     */
    [BwiFeature.SingleParticipantDms]: {
        supportedLevels: [SettingLevel.CONFIG],
        default: false,
    },
    /**
     * Hides the Search Button in the Room Header (because it's not working in the browser for encrypted rooms)
     */
    [BwiFeature.HideSearchButton]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * Hides the option to set the join rules for a room in the Security Room Settings Tab
     */
    [BwiFeature.HideRoomAccessSettings]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * Hides the Button in the Image preview that triggers a download
     */
    [BwiFeature.DisableProfileAvatarDl]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * shows maintenance information to the user
     */
    [BwiFeature.ShowMaintenanceInfo]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * disables nv feedback toast
     */
    [BwiFeature.DisableFeedbackToast]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * hides some buttons to initiate cross signing
     */
    [BwiFeature.HideCrossSigning]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * hides key import
     * when logging in
     */
    [BwiFeature.HideKeyImport]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * shows details on how to reset
     * your password externally
     */
    [BwiFeature.PasswordResetHint]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * disables the custom power level selection
     * in PowerSelector
     */
    [BwiFeature.DisableCustomPowerlevel]: {
        supportedLevels: [SettingLevel.CONFIG],
        default: false,
    },
    /**
     * disables the poll feature
     */
    [BwiFeature.DisablePoll]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * Hide userId when it should not be shown
     * e.g. Memberlist in Room Panel
     */
    [BwiFeature.HideUserId]: {
        supportedLevels: [SettingLevel.CONFIG],
        default: false,
    },
    /**
     * enables user function labels
     */
    [BwiFeature.UserFunctionLabels]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },

    /**
     * disables auto discovery of home server via login username
     */
    [BwiFeature.DisableLoginServerLookup]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * disables undisclosed poll creation
     */
    [BwiFeature.DisableUndisclosedPolls]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * since NV forced spaces, we need our own logic to hide them
     */
    [BwiFeature.HideSpaces]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /*
     *
     */
    [BwiFeature.ComposerTransformLonoSnippetToUrl]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * hides the layout switcher
     */
    [BwiFeature.HideLayoutSwitcher]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * makes Cross Signing mandatory but allows it go back in the process
     */
    [BwiFeature.MandatoryCrossSigning]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * make it possible to limit slash commands to only selected ones in bwi/SlashCommands.ts
     */
    [BwiFeature.WhiteListSlashCommands]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * configures enabled location share types
     */
    // @ts-ignore TODO: need to fix typing
    [BwiFeature.LocationShareTypesEnabled]: {
        supportedLevels: LEVELS_UI_FEATURE,
        // @ts-ignore TODO: need to fix typing
        default: ["Pin", "Own", "Live"],
    },
    /**
     * makes the location sharing config in config.json prioritized over the well-known config
     */
    [BwiFeature.PreferLocationSharingConfig]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * disables everything regarding geolocation sharing
     */
    [BwiFeature.DisableGeolocation]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * hide open street map link
     */
    [BwiFeature.HideOSMLink]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * The new on-boarding experience is displayed via a localStorage flag which is set when a user registers manually.
     * When there is no manual registration process this feature flag can be enabled in order to safe
     * the information whether to display on-boarding or not in the user's account data.
     */
    [BwiFeature.DisplayOnBoardingViaAccountDataFlag]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * disable redacted message sharing
     * hides the MessageActionBar if enabled and message is redacted
     */
    [BwiFeature.HideRedactedMessageSharing]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * hide room permissions we want to hide
     * from RolesRoomSettingsTab
     */
    [BwiFeature.HideRoomPermissions]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * hide notification targets
     * from notification settings
     */
    [BwiFeature.HideNotificationTargets]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * hide individual notification sounds
     * for each room feature
     */
    [BwiFeature.HideIndividualSounds]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * only show selected features
     * in lab section
     */
    // @ts-ignore TODO: need to fix typing
    [BwiFeature.SelectedLabSettings]: {
        supportedLevels: LEVELS_UI_FEATURE,
        // @ts-ignore TODO: need to fix typing
        default: [],
    },
    /**
     * hide thread feature
     */
    [BwiFeature.HideThreads]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * hide login by QR Code feature
     */
    [BwiFeature.HideLoginWithQR]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * feature to show the date of the
     * current message in the timeline
     * while scrolling
     */
    [BwiFeature.FloatingDateIndicator]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * add option to use location picker
     * in fullscreen mode
     */
    [BwiFeature.FullscreenLocationPicker]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * Show participants for polls
     */
    [BwiFeature.ShowPollParticipants]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * Add a11y declaration to login and legal tab
     */
    // @ts-ignore TODO: need to fix typing
    [BwiFeature.A11yDeclaration]: {
        supportedLevels: LEVELS_UI_FEATURE,
        // @ts-ignore TODO: need to fix typing
        default: "",
    },
    /**
     * Skips the zxcvbn validation of passwwords
     */
    [BwiFeature.SkipPasswordFormValidation]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * Requires the dataPrivacyUrl to be configured to log in
     */
    [BwiFeature.MandatoryDataPrivacyStatement]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * Set power levels required for element call, on room creation, to 0
     */
    [BwiFeature.HuddleCreationForAll]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * There are environments which should have the option
     * to manually bypass blocking
     */
    [BwiFeature.BypassBlockingButton]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * Enables an edited version of the secure backup
     * options in the security user settings
     */
    [BwiFeature.SecureBackupSettings]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * Removes all visual shield icons associated with the e2e status of rooms and users
     * Can be useful in environments where e2e is mandatory
     */
    [BwiFeature.HideE2EShields]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * Removes to ability to manually verify a user
     */
    [BwiFeature.HideUserVerification]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * Adds certain federation UI Elements
     */
    // @ts-ignore TODO: need to fix typing
    [BwiFeature.Federation]: {
        // @ts-ignore TODO: need to fix typing
        isFeature: true,
        // @ts-ignore TODO: need to fix typing
        labsGroup: LabGroup.Experimental,
        // @ts-ignore TODO: need to fix typing
        supportedLevels: [SettingLevel.DEVICE, SettingLevel.WELLKNOWN],
        // @ts-ignore TODO: need to fix typing
        default: false,
        // @ts-ignore TODO: need to fix typing
        displayName: _td("Federation"),
        // @ts-ignore TODO: need to fix typing
        controller: new ReloadOnChangeController(),
    },
    /**
     * Hides the UI elements to create permalinks for rooms, messages and users
     */
    [BwiFeature.HidePermalinkCreation]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    /**
     * Disable app download within the app
     */
    [BwiFeature.SkipDownloadForManagedDevices]: {
        supportedLevels: LEVELS_UI_FEATURE,
        default: false,
    },
    // @ts-ignore TODO: need to fix typing
    [BwiFeature.DataVNumber]: {
        supportedLevels: [SettingLevel.CONFIG],
        // @ts-ignore TODO: need to fix typing
        default: null as string | null,
    },
    [BwiFeature.HideWidgets]: {
        supportedLevels: [SettingLevel.CONFIG],
        default: false,
    },
};
