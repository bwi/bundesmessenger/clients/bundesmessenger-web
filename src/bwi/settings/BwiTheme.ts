// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import SdkConfig from "../../SdkConfig";
import { CustomTheme } from "../../theme";

export function getBwiTheme(themeName: string): CustomTheme {
    // set css variables
    const bwiThemes = SdkConfig.get().bwi_themes;
    if (!bwiThemes) {
        throw new Error(`No bwi themes set, can't set bwi theme "${themeName}"`);
    }
    const bwiTheme = bwiThemes.find((t) => t.name === themeName);
    if (!bwiTheme) {
        const knownNames = bwiThemes.map((t) => t.name).join(", ");
        throw new Error(`Can't find bwi custom theme "${themeName}", only know ${knownNames}`);
    }
    return bwiTheme;
}
