// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React, { useState, useEffect } from "react";

import { _t } from "../../../../languageHandler";
import SdkConfig from "../../../../SdkConfig";
import {
    BwiNotificationBuilder,
    BwiNotificationDialog,
} from "../../../components/notifications/BwiNotificationRenderer";
import { getBwiReleaseNotes } from "../../../components/notifications/BwiReleaseNotes";
import { SettingsSection } from "../../../../components/views/settings/shared/SettingsSection";
import { SettingsSubsection } from "../../../../components/views/settings/shared/SettingsSubsection";

export const SHOW_RELEASE_NOTES_FOR = ["2.22.0", "2.21.0", "2.18.0", "2.16.0", "2.15.0"];

export const BwiNewFeatureUserSettingsTab: React.FC = () => {
    const [releaseNotes, setReleaseNotes] = useState<BwiNotificationDialog[]>([]);
    const [abort, setAbort] = useState(false);
    useEffect(() => {
        const createNotes = async (): Promise<void> => {
            const rels: BwiNotificationDialog[] = [];

            for (const version of SHOW_RELEASE_NOTES_FOR) {
                const notes = await getBwiReleaseNotes(version).catch((error) => setAbort(true));
                if (notes) {
                    rels.push(notes);
                }
            }

            // first element is rendered first so order should be descending

            setReleaseNotes(rels);
        };
        if (releaseNotes.length === 0 && !abort && SHOW_RELEASE_NOTES_FOR.length > 0) {
            createNotes();
        }
    }, [abort, releaseNotes]);

    const overviewLink = SdkConfig.get().feature_overview_url;

    return (
        <SettingsSection data-testid="bwi_legalUserSettingsTab" heading={_t("New Features")}>
            {overviewLink && (
                <a href={overviewLink} rel="noreferrer noopener" target="_blank">
                    {_t("Feature Overview")}
                </a>
            )}
            {(releaseNotes as BwiNotificationBuilder[]).map((notes) => (
                <SettingsSubsection key={`${notes.title}${notes.version}`} heading={notes.title}>
                    <div>{notes.content}</div>
                </SettingsSubsection>
            ))}
        </SettingsSection>
    );
};
