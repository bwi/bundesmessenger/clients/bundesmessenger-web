// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { CustomTheme } from "../../theme";
import { BWI_SETTINGS } from "./BwiSettings";
import { PasswordPolicyRules } from "./PasswordPolicy";
import type { EventType } from "../helper/Notification";
import { ConfiguredDimensions } from "../analytics/BwiAnalyticsManager";
import { Member } from "../../components/views/right_panel/UserInfo";

export interface BwiCustomTheme extends CustomTheme {
    default?: boolean;
}

export interface BwiConfigOptions {
    community_support?: boolean;
    helpdesk?: {
        name: string;
        telephone: [
            {
                number: string;
                type: "internal" | "external";
            },
        ];
    };
    wiki_url?: string;
    faq_url?: string;
    bwi_themes?: BwiCustomTheme[];
    bwi_setting_defaults?: typeof BWI_SETTINGS;
    content_scanner?: {
        enabled: boolean;
    };
    bwi_registration_link?: string;
    matomo?: {
        siteId: string;
        url: string;
        policyLink?: string;
        dimensions?: ConfiguredDimensions;
    };
    password_policy?: PasswordPolicyRules;
    suppressNotification?: EventType[];
    enabledLanguages?: string[];
    feature_overview_url?: string;
    map_custom_attribution?: string[] | string;
    external_user_profile?: {
        link: string;
        /* for string replacements in link
            e.g. for rawDisplayName  as dynamicParam and https://link.com/?param=rawDisplayName as link
            rawDisplayName in the link is substituted with the display name of the user
        */
        dynamicParam?: keyof Member;
        buttonLabel?: string;
    };
    bwi_password_login_hint?: Record<string, string>;
}
