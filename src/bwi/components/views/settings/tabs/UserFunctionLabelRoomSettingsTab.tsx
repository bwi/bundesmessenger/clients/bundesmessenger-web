// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React, { useState, useEffect, KeyboardEvent, ChangeEvent } from "react";
import { EventType, ISendEventResponse, MatrixEvent, Room, RoomMember, RoomStateEvent } from "matrix-js-sdk/src/matrix";

import { _t } from "../../../../../languageHandler";
import SettingsFieldset from "../../../../../components/views/settings/SettingsFieldset";
import Field from "../../../../../components/views/elements/Field";
import {
    getUserFunctionLabelEvent,
    setUserFunctionLabel,
    unsetUserFunctionLabel,
    canChangeFunctionLabel,
} from "../../../../helper/events/UserFunctionLabel";
import AccessibleButton from "../../../../../components/views/elements/AccessibleButton";
import Spinner from "../../../../../components/views/elements/Spinner";
import { ICompletion } from "../../../../../autocomplete/Autocompleter";
import UserProvider from "../../../../../autocomplete/UserProvider";
import { AutocompleteInput } from "../../../../../components/structures/AutocompleteInput";
import { SettingsSection } from "../../../../../components/views/settings/shared/SettingsSection";
import SettingsTab from "../../../../../components/views/settings/tabs/SettingsTab";
import { Icon as CancelIcon } from "../../../../../../res/img/bwi/cancel.svg";

interface EditUserFunctionLabelProps {
    room: Room;
    member: RoomMember;
    currentLabel: string;
    onLabelSave: (member: RoomMember | RoomMember[], label: string) => Promise<any>;
    onLabelDelete: (member: RoomMember) => Promise<any>;
    canChange: boolean;
}
export const EditUserFunctionLabel: React.FC<EditUserFunctionLabelProps> = ({
    member,
    currentLabel,
    onLabelSave,
    onLabelDelete,
    canChange,
}) => {
    const [newLabel, setNewLabel] = useState(currentLabel);
    const [busy, setBusy] = useState(false);

    const onAbort = (): void => {
        setNewLabel(currentLabel);
    };

    const onSave = (): void => {
        if (busy || currentLabel === newLabel) {
            return;
        }

        setBusy(true);

        onLabelSave(member, newLabel).finally(() => {
            setBusy(false);
        });
    };

    const handleKeyPress = (event: KeyboardEvent): void => {
        const supress = (e: KeyboardEvent): void => {
            e.preventDefault();
            e.stopPropagation();
        };

        switch (event.key) {
            case "Escape":
                onAbort();
                supress(event);
                break;
            case "Enter":
                onSave();
                supress(event);
                break;
        }
    };

    return (
        <>
            <div className="bwi_UserFunctionLabelRoomSettingsTab_edit">
                <Field
                    data-test-id="userfunctionlabel-input"
                    label={`${member.rawDisplayName} (${member.userId})`}
                    placeholder={_t("Enter function label...")}
                    type="text"
                    onChange={(e: ChangeEvent<HTMLInputElement>) => setNewLabel(e.target.value)}
                    value={newLabel}
                    autoComplete="off"
                    autoFocus={true}
                    disabled={!canChange || busy}
                    onKeyDown={handleKeyPress}
                />

                <AccessibleButton
                    placement="top"
                    title={_t("action|delete")}
                    onClick={() => onLabelDelete(member)}
                    kind="icon"
                    className="bwi_userFunction_deleteButton"
                >
                    <CancelIcon className="bwi_userFunction_deleteButton_icon" />
                </AccessibleButton>
            </div>
            {currentLabel !== newLabel && (
                <div className="bwi_UserFunctionLabelRoomSettingsTab_edit_buttonContainer">
                    <AccessibleButton
                        data-testid="editor-abort"
                        disabled={busy}
                        onClick={() => onAbort()}
                        kind="danger_sm"
                    >
                        {_t("action|cancel")}
                    </AccessibleButton>
                    <AccessibleButton
                        data-testid="editor-apply"
                        disabled={busy}
                        onClick={() => onSave()}
                        kind="primary_sm"
                    >
                        {busy ? <Spinner w={16} h={16} message={_t("Apply")} /> : _t("Apply")}
                    </AccessibleButton>
                </div>
            )}
        </>
    );
};

interface AddUserFunctionLabelProps {
    room: Room;
    canChange: boolean;
    onLabelSave: (member: RoomMember | RoomMember[], label: string) => Promise<any>;
    excludeUids?: string[];
}

export const AddUserFunctionLabel: React.FC<AddUserFunctionLabelProps> = ({
    room,
    canChange,
    onLabelSave,
    excludeUids = [],
}) => {
    const [userProvider] = useState(new UserProvider(room, undefined, true));
    const [label, setLabel] = useState("");
    const [selectedUsers, setSelectedUsers] = useState<ICompletion[]>([]);
    const [busy, setBusy] = useState(false);

    const applyLabel = (): void => {
        setBusy(true);

        const users = selectedUsers
            .map((selection) => room.getMember(selection.completionId || ""))
            .filter((member) => !!member) as RoomMember[];

        onLabelSave(users, label).finally(() => {
            resetForm();
            setBusy(false);
        });
    };

    const resetForm = (): void => {
        setLabel("");
        setSelectedUsers([]);
    };

    return (
        <div>
            <AutocompleteInput
                provider={userProvider}
                placeholder={_t("Search user")}
                onSelectionChange={setSelectedUsers}
                selection={selectedUsers}
                additionalFilter={(user) => !excludeUids.includes(user.completionId || "")}
            />

            <div className="bwi_UserFunctionLabelRoomSettingsTab_add">
                <Field
                    label={_t("Function label")}
                    placeholder={_t("Enter function label...")}
                    type="text"
                    onChange={(e: ChangeEvent<HTMLInputElement>) => setLabel(e.target.value)}
                    value={label}
                    autoComplete="off"
                    autoFocus={true}
                    disabled={!canChange}
                />

                <AccessibleButton
                    disabled={busy || !selectedUsers.length || !label}
                    kind="primary"
                    onClick={() => applyLabel()}
                >
                    {busy ? <Spinner w={16} h={16} message={_t("Apply")} /> : _t("Apply")}
                </AccessibleButton>
            </div>
        </div>
    );
};

interface UserFunctionLabelRoomSettingsTabProps {
    room: Room;
}

export const UserFunctionLabelRoomSettingsTab: React.FC<UserFunctionLabelRoomSettingsTabProps> = ({ room }) => {
    const [event, setEvent] = useState(getUserFunctionLabelEvent(room));
    const [canChange] = useState(canChangeFunctionLabel(room));

    const handleStateEvent = (event: MatrixEvent): void => {
        if (event.getType() !== EventType.BwiUserFunctionLabels) {
            return;
        }

        setEvent(event.getContent() || {});
    };

    useEffect(() => {
        room.currentState.on(RoomStateEvent.Events, handleStateEvent);
        return () => {
            room.currentState.removeListener(RoomStateEvent.Events, handleStateEvent);
        };
        // eslint-disable-next-line
    }, []);

    const deleteLabel = (member: RoomMember): Promise<ISendEventResponse> => {
        return unsetUserFunctionLabel(room, member);
    };

    const setNewLabel = (member: RoomMember | RoomMember[], label: string): Promise<ISendEventResponse> => {
        if (!label) {
            return unsetUserFunctionLabel(room, member);
        }

        return setUserFunctionLabel(room, member, label);
    };

    const eventMembers = Object.keys(event)
        .map((uid) => room.getMember(uid))
        .filter((member) => !!member) as RoomMember[];

    return (
        <SettingsTab>
            <SettingsSection heading={_t("Function labels")}>
                <SettingsFieldset
                    legend={_t("Existing function labels")}
                    description={
                        eventMembers.length === 0 &&
                        _t("No function label has yet been assigned to people in the room.")
                    }
                >
                    {eventMembers.map((member) => {
                        return (
                            <EditUserFunctionLabel
                                canChange={canChange}
                                room={room}
                                member={member}
                                key={member.userId}
                                currentLabel={event[member.userId]}
                                onLabelSave={setNewLabel}
                                onLabelDelete={deleteLabel}
                            />
                        );
                    })}
                </SettingsFieldset>
                <SettingsFieldset
                    legend={_t("Add function label for user")}
                    description={_t(
                        "As an administrator you can add and remove a function label for people in the room. This is visible for all room members.",
                    )}
                >
                    <AddUserFunctionLabel
                        canChange={canChange}
                        room={room}
                        onLabelSave={setNewLabel}
                        excludeUids={Object.keys(event)}
                    />
                </SettingsFieldset>
            </SettingsSection>
        </SettingsTab>
    );
};
