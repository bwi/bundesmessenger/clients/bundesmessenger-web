// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React, { FC } from "react";

import { DowntimeInfo, MaintenanceManager } from "../../../managers/MaintenanceManager";

export const MaintenanceWarning: FC<{ info: DowntimeInfo; isActive: boolean }> = ({ info, isActive }) => {
    const message = MaintenanceManager.getDowntimeMessage(info);

    return (
        <div className="mx_BwiMaintenance_Wrapper">
            <div className="mx_BwiMaintenance_Banner" data-blocking={(isActive && info.blocking) || false}>
                <p data-testid="BwiMaintenance_Warning_Content" className="mx_BwiMaintenance_Content">
                    {message}
                </p>
            </div>
        </div>
    );
};
