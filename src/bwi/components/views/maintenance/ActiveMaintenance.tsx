// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React, { FC, useEffect, useState } from "react";
import classNames from "classnames";

import Login from "../../../../Login";
import { MaintenanceManager, DowntimeInfo } from "../../../managers/MaintenanceManager";
import SettingsStore from "../../../../settings/SettingsStore";
import { BwiFeature } from "../../../settings/BwiFeature";
import { _t } from "../../../../languageHandler";
import AccessibleButton from "../../../../components/views/elements/AccessibleButton";

// 30 seconds when logged in
const loggedInInterval = 1000 * 30;
// 5 minutes when not logged in yet
const loginInterval = 1000 * 60 * 5;

export const ActiveMaintenance: FC<{ destination: "Login" | "LoggedIn"; loginLogic?: Login }> = ({
    destination,
    loginLogic,
}) => {
    const [isActive, setActive] = useState(false);
    const [downtime, setDowntime] = useState<DowntimeInfo>();
    const [bypassBlocking, setBypassBlocking] = useState(MaintenanceManager.getBypassBlocking());

    useEffect(() => {
        const checkDowntime = (): void => {
            if (destination === "LoggedIn") {
                MaintenanceManager.instance.getDowntimeInfo().then(() => {
                    const downtimeState = MaintenanceManager.instance.shouldSeeActiveDowntime();
                    setActive(downtimeState.active);
                    setDowntime(downtimeState.downtime);
                });
            } else if (loginLogic && destination === "Login") {
                MaintenanceManager.shouldShowBeforeLogin(loginLogic.createTemporaryClient()).then((downtimeState) => {
                    setActive(downtimeState.active);
                    setDowntime(downtimeState.downtime);
                });
            }
        };

        checkDowntime();
        const interval = setInterval(
            () => {
                checkDowntime();
            },
            destination === "LoggedIn" ? loggedInInterval : loginInterval,
        );

        return () => clearInterval(interval);
    }, [destination, loginLogic]);

    const onBypassPress = (): void => {
        MaintenanceManager.setBypassBlocking(true);
        setBypassBlocking(MaintenanceManager.getBypassBlocking());
    };

    let dtMessage = "";

    if (isActive && downtime) {
        dtMessage = MaintenanceManager.getDowntimeMessage(downtime);
    }

    let bypassBlockingButton: React.ReactElement | null = null;
    if (SettingsStore.getValue(BwiFeature.BypassBlockingButton) && !bypassBlocking && downtime?.blocking) {
        bypassBlockingButton = (
            <AccessibleButton className="mx_BwiMaintenance_Active_Bypass" onClick={onBypassPress} kind="primary_sm">
                {_t("Ignore")}
            </AccessibleButton>
        );
    }

    return isActive ? (
        <div
            data-testid="BwiMaintenance_Active"
            className={classNames("mx_BwiMaintenance_Active", {
                mx_BwiMaintenance_Active_NonBlocking: !downtime?.blocking,
                mx_BwiMaintenance_Active_Blocking: downtime?.blocking,
            })}
        >
            <p>{dtMessage}</p>
            {bypassBlockingButton}
        </div>
    ) : (
        <></>
    );
};
