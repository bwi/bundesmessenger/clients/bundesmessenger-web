// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";
import { RoomMember } from "matrix-js-sdk/src/matrix";

import SdkConfig from "../../../../SdkConfig";
import AccessibleButton from "../../../../components/views/elements/AccessibleButton";
import { _t } from "../../../../languageHandler";
import { Member } from "../../../../components/views/right_panel/UserInfo";
import { Icon as ExternalLinkIcon } from "../../../../../res/img/external-link.svg";

// BWI MESSENGER-3912
export const ExternalProfile: React.FC<{ member: Member }> = ({ member }) => {
    const configuredExternalProfile = SdkConfig.get("external_user_profile");
    if (!configuredExternalProfile?.link) return null;

    let link = configuredExternalProfile.link;
    const param = configuredExternalProfile.dynamicParam;
    const label = configuredExternalProfile.buttonLabel || "External Profile";
    const replacement = member ? (member as RoomMember)[param || "name"] : null;

    if (param && replacement) {
        link = link.replace(param as string, (replacement as string).trim().replace(/\s+/g, ""));
    }

    return (
        <div className="bwi_UserInfo_externalProfile" data-testid="userinfo_external_profile_container">
            <a data-testid="userinfo_external_profile_link" href={link} target="_blank" rel="noreferrer noopener">
                <AccessibleButton kind="primary_outline" onClick={() => {}}>
                    <ExternalLinkIcon className="bwi_UserInfo_externalProfile_icon" />
                    <span className="bwi_UserInfo_externalProfile_label" data-testid="userinfo_external_profile_label">
                        {_t(label as any)}
                    </span>
                </AccessibleButton>
            </a>
        </div>
    );
};
