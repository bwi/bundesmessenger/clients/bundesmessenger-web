// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React, { useContext, useEffect, useImperativeHandle, useMemo, useState } from "react";
import { MatrixClient, EventTimeline } from "matrix-js-sdk/src/matrix";

import { MatrixClientPeg } from "../../../../MatrixClientPeg";
import Modal from "../../../../Modal";
import { _t } from "../../../../languageHandler";
import MatrixClientContext from "../../../../contexts/MatrixClientContext";
import ErrorDialog from "../../../../components/views/dialogs/ErrorDialog";
import PowerSelector from "../../../../components/views/elements/PowerSelector";

interface InviteDialogPowerLevelProps {
    roomId: string;
    goButton: JSX.Element | null;
    children?: JSX.Element[];
    setWarning?: (text: string | null) => void;
    getWarning?: () => string;
    initialPowerlevel?: number;
    bwiOnPowerLevelChange?(powerLevel: number): void;
}

export interface InviteDialogPowerLevelRef {
    adjustPowerLevels: (users: string[]) => Promise<boolean>;
}

export const InviteDialogPowerLevel = React.forwardRef<InviteDialogPowerLevelRef, InviteDialogPowerLevelProps>(
    (props: InviteDialogPowerLevelProps, ref) => {
        const room = useMemo(() => MatrixClientPeg.safeGet().getRoom(props.roomId), [props.roomId]);
        const cli: MatrixClient = useContext(MatrixClientContext);
        const [powerLevel, setPowerLevel] = useState<number>(0);
        const [defaultLevel, setDefaultLevel] = useState<number>(0);

        const warningMessage = _t("You won't be able to revoke the granted administrator permissions!");

        useEffect(() => {
            if (room) {
                const powerLevelEvent = room.currentState.getStateEvents("m.room.power_levels", "");
                const powerLevelUsersDefault = powerLevelEvent ? Number(powerLevelEvent.getContent().users_default) : 0;
                setPowerLevel(props.initialPowerlevel || powerLevelUsersDefault);
                setDefaultLevel(powerLevelUsersDefault);
            } else {
                setPowerLevel(props.initialPowerlevel || 0);
            }
        }, [room, props.initialPowerlevel]);

        const applyPowerChange = (
            roomId: string,
            targets: string | string[],
            powerLevel: string | number,
        ): Promise<void> => {
            return cli.setPowerLevel(roomId, targets, Number(powerLevel)).then(
                function () {
                    // NO-OP; rely on the m.room.member event coming down else we could
                    // get out of sync if we force setState here!
                    console.log("Power change success");
                },
                function (err) {
                    console.error("Failed to change power level " + err);
                    Modal.createDialog(ErrorDialog, {
                        title: _t("Error"),
                        description: _t("Failed to change power level"),
                    });
                },
            );
        };

        const parseAndSetLevel = useMemo(
            () => (value: number, powerLevelKey: string) => {
                const num = Number(value);
                if (isNaN(num)) setPowerLevel(0);
                else {
                    if (num === 100 && props.setWarning) {
                        props.setWarning(warningMessage);
                    }
                    if (props.getWarning?.() === warningMessage && num !== 100) {
                        props.setWarning?.(null);
                    }
                    setPowerLevel(Number(value));
                }
            },
            [props, warningMessage],
        );

        useImperativeHandle(ref, () => ({
            async adjustPowerLevels(users: string[]): Promise<boolean> {
                if (!room) return false;
                const powerLevelEvent = room
                    .getLiveTimeline()
                    .getState(EventTimeline.FORWARDS)
                    ?.getStateEvents("m.room.power_levels", "");
                if (!powerLevelEvent) return false;
                const oldRules = (powerLevelEvent.event.content?.users || {}) as Record<string, number>;
                const usersToChange = users.filter((user) => !(oldRules[user] === powerLevel));
                if (usersToChange.length > 0) {
                    const change = applyPowerChange(room.roomId, usersToChange, powerLevel);
                    await change;
                }

                return true;
            },
        }));

        // BWI: Access current value from outside by propagating the value change
        const { bwiOnPowerLevelChange } = props;
        useEffect(() => {
            bwiOnPowerLevelChange?.(powerLevel);
        }, [powerLevel, bwiOnPowerLevelChange]);

        return (
            <div className="mx_InviteDialog_powerLevel">
                <PowerSelector
                    label={_t("Assign Role")}
                    value={powerLevel}
                    maxValue={100}
                    usersDefault={defaultLevel}
                    onChange={parseAndSetLevel}
                />
                {props.goButton}
            </div>
        );
    },
);
