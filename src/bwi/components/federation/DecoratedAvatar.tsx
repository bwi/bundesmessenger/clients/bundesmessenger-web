// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React, { ReactNode } from "react";

import { MatrixClientPeg } from "../../../MatrixClientPeg";
import SettingsStore from "../../../settings/SettingsStore";
import { bwiIsUserFederated } from "../../functions/federation";
import { BwiFeature } from "../../settings/BwiFeature";
import { BwiFederationIcon } from "./FederationIcon";

type FederationDecoration = {
    type: "user-federation";
    userId: string;
    hideDecoration?: boolean;
    size?: string;
};

type Props = {
    children: ReactNode;
} & FederationDecoration;

export function BwiDecoratedAvatar({ children, size, hideDecoration = false, ...decoration }: Props): JSX.Element {
    let icon: ReactNode = null;

    // federation icon
    if (
        decoration.type === "user-federation" &&
        SettingsStore.getValue(BwiFeature.Federation) &&
        bwiIsUserFederated(MatrixClientPeg.get()?.getUserId() || "", decoration.userId)
    ) {
        icon = <BwiFederationIcon />;
    }

    return (
        <div
            className="bwi_DecoratedAvatar_container"
            style={
                {
                    "--_federation-icon-size": size || "16px",
                } as React.CSSProperties
            }
        >
            {hideDecoration !== true && <div className="bwi_DecoratedAvatar_decoration">{icon}</div>}
            {children}
        </div>
    );
}
