// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";

import { _t, getUserLanguage } from "../../languageHandler";
import SettingsStore from "../../settings/SettingsStore";
import { BwiFeature } from "../settings/BwiFeature";
import Modal from "../../Modal";
import BaseDialog from "../../components/views/dialogs/BaseDialog";
import { sanitizedHtmlNode } from "../../HtmlUtils";
import SdkConfig from "../../SdkConfig";
import AccessibleButton from "../../components/views/elements/AccessibleButton";
import { SettingKey } from "../../settings/Settings.tsx";

function interpolateUrl(url: string, context: Record<string, string>): string {
    return url.replace(/\{(.*?)\}/gm, (match, token) => {
        return context[token]?.toString() || match;
    });
}

export const BwiA11yDeclaration: React.FC<{ className?: string }> = ({ className }) => {
    const source = SettingsStore.getValue(BwiFeature.A11yDeclaration as SettingKey) as string;

    if (!source) return <></>;

    return (
        <AccessibleButton
            className={className}
            onClick={async () => {
                const lang = getUserLanguage();
                const url = interpolateUrl(source, {
                    lang,
                });
                const content = await fetch(url).then(
                    (res) => res.text(),
                    (reason) => (console.warn(`Unable to load accessibility statement "${url}"`, reason), ""),
                );

                if (!content) return;

                Modal.createDialog(BaseDialog, {
                    title: _t("Accessibility statement %(brand)s", {
                        brand: SdkConfig.get().brand,
                    }),
                    children: <div className="bwi_a11yDeclaration_dialogContent">{sanitizedHtmlNode(content)}</div>,
                });
            }}
        >
            {_t("Accessibility statement")}
        </AccessibleButton>
    );
};
