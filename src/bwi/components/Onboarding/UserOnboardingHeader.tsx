// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
import React from "react";

import AccessibleButton, { ButtonEvent } from "../../../components/views/elements/AccessibleButton";
import Heading from "../../../components/views/typography/Heading";
import defaultDispatcher from "../../../dispatcher/dispatcher";
import { UseCase } from "../../../settings/enums/UseCase";
import SdkConfig from "../../../SdkConfig";
import { useSettingValue } from "../../../hooks/useSettings";
import { _t } from "../../../languageHandler";

const onClickSendDm = (ev: ButtonEvent): void => {
    defaultDispatcher.dispatch({ action: "view_create_chat" });
};

export function UserOnboardingHeader({ useCase }: { useCase: UseCase | null }): JSX.Element {
    const brand = SdkConfig.get("brand");
    const title = _t("onboarding|welcome_to_brand", {
        brand,
    });
    const description = _t("onboarding|description", {
        brand,
    });

    const theme = useSettingValue("theme");
    const image = theme === "bwi-light" ? "bwi/img/BM-Logo_Schriftzug.png" : "bwi/img/BuMLogo-weiß.png";
    const actionLabel = _t("onboarding|personal_messaging_action");

    return (
        <div className="mx_UserOnboardingHeader">
            <div className="mx_UserOnboardingHeader_content">
                <Heading size="1">
                    {title}
                    <span className="mx_UserOnboardingHeader_dot">.</span>
                </Heading>
                <p>{description}</p>
                <AccessibleButton onClick={onClickSendDm} kind="primary">
                    {actionLabel}
                </AccessibleButton>
            </div>
            <img className="mx_UserOnboardingHeader_image" src={image} alt="" />
        </div>
    );
}
