// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import * as React from "react";

// directly import the style here as this layer does not support rethemedex at this time so no matrix-react-sdk
// PostCSS variables will be accessible.
import "../../../../res/bwi/css/ErrorView.pcss";

import { loadLanguage, _t } from "../../../vector/init";
import { name } from "../../../../res/manifest.json";

interface IProps {
    // both of these should already be internationalised (plot twist, they are not!)
    title: string;
    messages?: string[];
}

const ErrorView: React.FC<IProps> = (props) => {
    const [loaded, setLoaded] = React.useState(false);
    React.useEffect(() => {
        loadLanguage().then(() => {
            setLoaded(true);
        });
    }, []);

    const recover = props.title.includes("missing translation");

    // The title is alredy translated,
    // but if the config is corrupted it will always display "missing Translation| ..."
    const title = recover ? "" : props.title;

    const messages = recover
        ? [
              // eslint-disable-next-line max-len
              _t(
                  "%(appName)s was unable to load. Please try again. If %(appName)s is still not running, contact your local User HelpDesk.",
                  {
                      appName: name,
                  },
              ),
          ]
        : props.messages;

    return loaded ? (
        <div className="bwi_ErrorView">
            <div className="bwi_ErrorView_container">
                <div className="bwi_ErrorView_header">
                    <span className="bwi_ErrorView_logo">
                        <img height="80" src="bwi/favicon/152.png" alt="App Logo" />
                    </span>
                    <h1>{_t("Failed to start")}</h1>
                </div>
                <div className="bwi_ErrorView_col">
                    <div className="bwi_ErrorView_row">
                        <div>
                            <h4>{title}</h4>
                            {messages && messages.map((msg) => <p key={msg}>{msg}</p>)}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    ) : null;
};

export default ErrorView;
