// Copyright 2024 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";

import { BwiNotificationDialog } from "./BwiNotificationRenderer";
import { BwiNotificationTypes } from "./BwiNotifications";
import { _t } from "../../../languageHandler";
import BuMLogo from "../../../../res/img/bwi/BuMLogo.svg";

export const getFederationAnnouncement = (): BwiNotificationDialog => {
    const dialog: BwiNotificationDialog = {
        notificationType: BwiNotificationTypes.HINT_FEDERATION_ANNOUNCEMENT,
        title: "",
        confirmButton: <div data-testid="bwiFederationAnnouncementDialogConfirm">{_t("action|got_it")}</div>,
        content: (
            <div className="bwi_FederationAnnouncement_container">
                <div className="bwi_FederationAnnouncement_header">
                    <div className="bwi_FederationAnnouncement_icon" style={{ zIndex: 2 }}>
                        <img src={BuMLogo} alt="Federation BundesMessenger Logo" />
                    </div>
                    <div className="bwi_FederationAnnouncement_icon" style={{ zIndex: 1 }} />
                </div>
                <div className="mx_BwiCelebration_content bwi_FederationAnnouncement_content">
                    <h2>{_t("bwi|federation|announcement_headline")}</h2>
                    <p>
                        {_t("bwi|federation|announcement_description", {}, { Bold: (sub) => <strong>{sub}</strong> })}
                    </p>
                    <p> {_t("bwi|federation|announcement_policy")}</p>
                    <p className="bwi_FederationAnnouncement_subtext">{_t("bwi|federation|announcement_subtext")}</p>
                </div>
            </div>
        ),
        className: "bwi_FederationAnnouncement_dialog",
        fixedWidth: false,
    };

    return dialog;
};
