// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";
import { MatrixError, Method } from "matrix-js-sdk/src/matrix";
import { encodeUri, isNullOrUndefined } from "matrix-js-sdk/src/utils";

import { _t } from "../../../languageHandler";
import { BwiNotificationTypes } from "./BwiNotifications";
import { BwiNotificationDialog } from "./BwiNotificationRenderer";
import SdkConfig from "../../../SdkConfig";
import { SettingLevel } from "../../../settings/SettingLevel";
import { BwiFeature } from "../../settings/BwiFeature";
import SettingsStore from "../../../settings/SettingsStore";
import BwiAnalyticsManager, {
    AnalyticsSessionInfo,
    AnalyticsSessionsAccountData,
    BWI_ANALYTICS_MATRIX_EVENT_TYPE,
} from "../../../bwi/analytics/BwiAnalyticsManager";
import { MatrixClientPeg } from "../../../MatrixClientPeg";

const enableAnalytics = async (): Promise<void> => {
    await SettingsStore.setValue(BwiFeature.Analytics, "popup", SettingLevel.ACCOUNT, true);
};

const disableAnalytics = async (): Promise<void> => {
    await SettingsStore.setValue(BwiFeature.Analytics, "popup", SettingLevel.ACCOUNT, false);
    BwiAnalyticsManager.disable();
};

export const getBwiAnalyticsDialog = (): BwiNotificationDialog => {
    const sdk = SdkConfig.get();

    const dialog: BwiNotificationDialog = {
        notificationType: BwiNotificationTypes.HINT_ANALYTICS_NOTIFICATION,
        title: _t("Error Analytics"),
        confirmButton: (
            <div
                data-testid="bwiAnalyticsNotificationDialogConfirm"
                onClick={enableAnalytics}
                onKeyUp={(event) => {
                    event.preventDefault();
                    if (event.key === "Enter") {
                        enableAnalytics();
                    }
                }}
            >
                {_t("Agree")}
            </div>
        ),
        cancelButton: (
            <div
                data-testid="bwiAnalyticsNotificationDialogCancel"
                onKeyUp={(event) => {
                    event.preventDefault();
                    if (event.key === "Enter") {
                        disableAnalytics();
                    }
                }}
                onClick={disableAnalytics}
            >
                {_t("Disagree")}
            </div>
        ),
        cancelButtonClass: "mx_Dialog_primary",
        content: (
            <div className="mx_BwiCelebration_content">
                <p>
                    {_t(
                        "We need your help in finding issues to improve %(brand)s. For this purpose, we would like to collect anonymised diagnostic data.",
                        { brand: sdk?.brand },
                    )}
                </p>
                <p>
                    {_t("No data is given to third parties.")}
                    {sdk?.matomo?.policyLink && (
                        <>
                            {_t("You can find the details in our")}
                            <a href={sdk?.matomo?.policyLink} target="_blank">
                                {" "}
                                {_t("Privacy Policy")}
                            </a>
                            .
                        </>
                    )}
                </p>
                <p>{_t("If you don't want to help anymore, you can always deactivate it again in your settings.")}</p>
                <p>{_t("Do you want to support us in our error analytics?")}</p>
            </div>
        ),
        customFilterFunction: async () => {
            const client = MatrixClientPeg.safeGet();
            const deviceId = client.getDeviceId() || "";
            const path = encodeUri(`/user/$userId/account_data/$type`, {
                $userId: client.credentials.userId,
                $type: BWI_ANALYTICS_MATRIX_EVENT_TYPE,
            });

            try {
                const response = await client.http.authedRequest<AnalyticsSessionsAccountData>(
                    Method.Get,
                    path,
                    undefined,
                );
                if (response) {
                    const info: AnalyticsSessionInfo = response[deviceId];
                    if (!isNullOrUndefined(info)) return false;
                    else return true;
                }
                return false;
            } catch (err) {
                // no data in notifications stored yet
                if (err instanceof MatrixError && err.data && err.data.errcode === "M_NOT_FOUND") {
                    return true;
                } else return false;
            }
        },
        customFinishFunction: async () => {},
        shouldCloseOnBackdropClick: false,
    };
    return dialog;
};
