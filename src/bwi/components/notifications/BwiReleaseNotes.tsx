// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";

import { _t } from "../../../languageHandler";
import { BwiNotificationTypes } from "./BwiNotifications";
import { BwiNotificationDialog, DialogEffect } from "./BwiNotificationRenderer";

const releaseImgPath = "bwi/img/release-notes/";

type ReleaseNote = {
    name: string;
    description: string;
    image?: string;
};

type ReleaseNotes = {
    features: ReleaseNote[];
    bugfixes: ReleaseNote[];
    improvements: ReleaseNote[];
    version: string;
    date: string;
};

let cachedReleaseNotes: ReleaseNotes | null = null;

async function loadReleaseNotes(version: string): Promise<ReleaseNotes | null> {
    try {
        if (!cachedReleaseNotes || cachedReleaseNotes.version !== version) {
            cachedReleaseNotes = await import(`../../release-notes/${version}.json`);
        }
        return cachedReleaseNotes;
    } catch (e) {
        console.debug(e);
        return null;
    }
}

function SubSection({ title, notes }: { title: string; notes: ReleaseNote[] }): JSX.Element {
    return (
        <div className="mx_BwiReleaseNotes_section">
            <p className="mx_BwiReleaseNotes_header">{title}</p>
            <ul className="mx_BwiReleaseNotes_list">
                {notes.map((note) => (
                    <li key={note.name}>
                        <p className="mx_BwiReleaseNotes_name">{note.name}</p>
                        <p className="mx_BwiReleaseNotes_content">{note.description}</p>
                        {note.image ? (
                            <p>
                                <img
                                    className="mx_BwiReleaseNotes_image"
                                    src={`${releaseImgPath}${note.image}`}
                                    alt={note.name}
                                />
                            </p>
                        ) : null}
                    </li>
                ))}
            </ul>
        </div>
    );
}

export const getBwiReleaseNotes = async (
    version: string,
    dialogEffect?: DialogEffect,
    icon?: string,
): Promise<BwiNotificationDialog | null> => {
    const releaseNotes = await loadReleaseNotes(version);

    if (!releaseNotes) return null;

    const features = releaseNotes.features?.length > 0 && (
        <SubSection title={`${_t("New Features")} 🚀`} notes={releaseNotes.features} />
    );
    const bugfixes = releaseNotes.bugfixes?.length > 0 && (
        <SubSection title={`${_t("Solved Bugs")} 🐞`} notes={releaseNotes.bugfixes} />
    );
    const improvements = releaseNotes.improvements?.length > 0 && (
        <SubSection title={`${_t("Improvements")} ✨`} notes={releaseNotes.improvements} />
    );

    return {
        notificationType: BwiNotificationTypes.HINT_RELEASE_NOTES,
        title: `
                        ${icon || ""}
                        ${_t("Changes in the Web Client")} - Version ${version} ${releaseNotes.date}
                        ${icon || ""}
                    `,
        version: version,
        content: (
            <div className="mx_BwiReleaseNotes">
                {features}
                {improvements}
                {bugfixes}
            </div>
        ),
        triggerEffect: dialogEffect,
        confirmButton: <div data-testid="bwiReleaseNotificationDialogConfirm">{_t("OK")}</div>,
    };
};
