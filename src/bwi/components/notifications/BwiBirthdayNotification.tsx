// Copyright 2025 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";

import { _t } from "../../../languageHandler";
import { BwiNotificationTypes } from "./BwiNotifications";
import { BwiNotificationDialog } from "./BwiNotificationRenderer";

export const getBwiBirthday = (version: string): BwiNotificationDialog => ({
    notificationType: BwiNotificationTypes.HINT_BIRTHDAY_NOTIFICATION,
    title: ``,
    content: (
        <div className="mx_Bwi_Birthday_content">
            <img src="bwi/img/undraw_Birthday_cake_BwM.svg" width="80%" alt="Birthday Present" />
            <div className="mx_Bwi_Birthday_text_content">
                <span className="mx_Bwi_Birthday_custom_title">🥳 HAPPY BIRTHDAY 🥳</span>
                <span className="mx_Bwi_Birthday_custom_subtitle">BundesMessenger</span>
                <p>
                    <span>{_t("congratulate_the_messenger")}</span>
                </p>
            </div>
        </div>
    ),
    triggerEffect: "confetti",
    confirmButton: <div>{_t("Ok")}</div>,
    version: version,
});
