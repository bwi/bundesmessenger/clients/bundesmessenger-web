// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React, { ReactNode, useEffect, useRef, useState } from "react";

import ICanvasEffect from "../../../effects/ICanvasEffect";
import { _t } from "../../../languageHandler";
import UIStore from "../../../stores/UIStore";
import { getBwiDialogs } from "../../functions/NotificationConfig";
import { BwiNotificationManager } from "../../managers/BwiNotificationManager";
import NotificationDialog from "../views/dialogs/BwiNotificationDialog";
import { BwiNotificationValue } from "./BwiNotifications";
import { useIsKeyBackupEnabled } from "../../hooks/useIsKeyBackupEnabled";
import Modal from "../../../Modal.tsx";

export type DialogEffect = "confetti" | "fireworks" | "snowfall" | "spaceinvaders";

export type BwiNotificationBuilder = {
    notificationType: BwiNotificationValue;
    version?: string;
    headerImage?: string;
    customHeaderImage?: ReactNode;
    title: string;
    content: JSX.Element;
    triggerEffect?: DialogEffect;
    confirmButton?: JSX.Element;
    cancelButton?: JSX.Element;
    // The class of the cancel button, only used if a cancel button is
    // enabled
    cancelButtonClass?: string;
    customFilterFunction?: () => Promise<boolean>;
    customFinishFunction?: () => Promise<void>;
    className?: string;
    fixedWidth?: boolean;
    shouldCloseOnBackdropClick?: boolean;
};

export type BwiNotificationCustom = {
    notificationType: BwiNotificationValue;
    createModal(onFinished: () => void): void;
    customFinishFunction?: () => Promise<void>;
    customFilterFunction?: () => Promise<boolean>;
    triggerEffect?: DialogEffect;
    version?: string;
};

export type BwiNotificationDialog = BwiNotificationBuilder | BwiNotificationCustom;

/**
 * Component to render BwiNotificationDialogs and effects
 * Triggered only after Passphrase is set
 */
export function BwiNotifications(): JSX.Element | null {
    const isKeyBackupEnabled = useIsKeyBackupEnabled();

    const [initialized, setInitialized] = useState(false);
    const canvasRef = useRef<HTMLCanvasElement>(null);
    const [hasEffect, setHasEffect] = useState(false);
    const [lastClosed, setLastClosed] = useState(false);

    useEffect(() => {
        if (!initialized && isKeyBackupEnabled) {
            iniNotifications(
                (effect) => {
                    setHasEffect(true);
                    if (canvasRef.current) {
                        effect.start(canvasRef.current);
                    }
                },
                () => {
                    setLastClosed(true);
                },
            ).then(() => {
                setInitialized(true);
            });
        }
    }, [initialized, isKeyBackupEnabled]);

    if (lastClosed) {
        return null;
    }

    return (
        <>
            {hasEffect && isKeyBackupEnabled && (
                <canvas
                    ref={canvasRef}
                    width={UIStore.instance.windowWidth}
                    height={UIStore.instance.windowHeight}
                    style={{
                        display: "block",
                        zIndex: 100,
                        pointerEvents: "none",
                        position: "absolute",
                        top: 0,
                        right: 0,
                    }}
                />
            )}
        </>
    );
}

async function iniNotifications(onEffect: (effect: ICanvasEffect) => void, onLastClosed: () => void): Promise<void> {
    // load dialogs
    const dialogs: BwiNotificationDialog[] = await getBwiDialogs();

    const notificationsFiltered: BwiNotificationDialog[] =
        await BwiNotificationManager.instance.filterAlreadySeen(dialogs);
    const notificationsToShow: BwiNotificationDialog[] = [...notificationsFiltered].reverse();

    if (notificationsFiltered.length === 0) return;

    async function runEffectMaybe(dialog?: BwiNotificationDialog): Promise<void> {
        clearAllEffects();
        const effect: DialogEffect | undefined = dialog?.triggerEffect;
        if (!effect) return;
        const effectModule: ICanvasEffect | null = await lazyLoadEffect(effect);
        if (effectModule) {
            onEffect(effectModule);
        }
    }

    await runEffectMaybe(notificationsFiltered[0]);

    for (const notification of notificationsToShow) {
        const index: number = notificationsToShow.indexOf(notification);

        // eslint-disable-next-line no-inner-declarations
        async function onFinished(): Promise<void> {
            // Uses the first, because modals are added via a stack
            if (0 === index) {
                onLastClosed();
            }
            BwiNotificationManager.instance.hasSeenNotification(notification);
            await runEffectMaybe(notificationsToShow[index - 1]);
        }

        if ("createModal" in notification) {
            notification.createModal(() => {
                onFinished();
            });
        } else {
            renderNotification(notification, notificationsToShow.length, index, onFinished);
        }
    }
}

function renderNotification(
    notification: BwiNotificationBuilder,
    totalNotificationCount: number,
    notificationIndex: number,
    onFinished?: () => Promise<void>,
): void {
    const componentProperties = {
        headerImage: notification.headerImage,
        customHeaderImage: notification.customHeaderImage,
        title: notification.title,
        description: <div>{notification.content}</div>,
        confirmButton: notification.confirmButton,
        cancelButton: notification.cancelButton,
        cancelButtonClass: notification.cancelButtonClass,
        footer: `${_t("Notification")} ${totalNotificationCount - notificationIndex} ${_t("of")} ${totalNotificationCount}`,
        onFinished,
        className: notification.className,
        fixedWidth: notification.fixedWidth,
    };

    const options = {
        async onBeforeClose(reason: string | undefined) {
            console.log("ON BEFORE CLOSE", reason);
            if (reason && reason === "backgroundClick") return !!notification.shouldCloseOnBackdropClick;
            return true;
        },
    };

    Modal.createDialog(NotificationDialog, componentProperties, undefined, false, false, options);
}

const effectsCache = new Map<DialogEffect, ICanvasEffect>();

async function lazyLoadEffect(dialogEffect: DialogEffect): Promise<ICanvasEffect | null> {
    let effect: ICanvasEffect | null = effectsCache.get(dialogEffect) || null;
    if (effect === null) {
        const options = {
            maxCount: 150,
            speed: 3,
            frameInterval: 15,
            alpha: 1.0,
            gradient: false,
        };
        try {
            const { default: Effect } = await import(`../../../effects/${dialogEffect}`);
            effect = new (Effect as new (options?: any) => ICanvasEffect)(options);
            effectsCache.set(dialogEffect, effect);
        } catch (err) {
            console.warn(`Unable to load effect module at '../../effects/${dialogEffect}.`, err);
        }
    }
    return effect;
}

/**
 * Stop and clear all effects
 */
function clearAllEffects(): void {
    effectsCache.forEach((effect) => {
        window.cancelAnimationFrame(effect.animationFrameId || -1);
    });
    effectsCache.clear();
}
