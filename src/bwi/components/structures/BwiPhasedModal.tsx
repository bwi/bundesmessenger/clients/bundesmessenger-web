// Copyright 2024 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React, { ReactNode, createContext, useContext, useState } from "react";
import classNames from "classnames";

import { Icon as ArrowIcon } from "../../../../res/img/bwi/arrow.svg";
import { _t } from "../../../languageHandler";
import AccessibleButton from "../../../components/views/elements/AccessibleButton";

const PhasedModelContext = createContext<UsePhasedModalReturnType>(null!);

export function usePhasedModel({ initialPhase, phaseOrder }: { initialPhase: string; phaseOrder: string[] }): {
    currentPhase: string;
    phaseOrder: string[];
    setPhase: React.Dispatch<React.SetStateAction<string>>;
} {
    const [currentPhase, setPhase] = useState<string>(initialPhase);
    return {
        phaseOrder,
        currentPhase,
        setPhase,
    };
}

type UsePhasedModalReturnType = ReturnType<typeof usePhasedModel>;

export function PhasedModal({
    modal,
    children,
    className,
}: {
    modal: UsePhasedModalReturnType;
    children: ReactNode;
    className?: string;
}): React.JSX.Element {
    const { currentPhase, phaseOrder, setPhase } = modal;
    const currentPhaseIndex = phaseOrder.indexOf(currentPhase);

    return (
        <PhasedModelContext.Provider value={modal}>
            <div className={classNames("bwi_PhasedModal_container", className)}>
                {children}

                <div className="bwi_PhaseModel_controls">
                    {currentPhaseIndex !== 0 && (
                        <AccessibleButton
                            data-area="prev"
                            onClick={() => {
                                setPhase(phaseOrder[currentPhaseIndex - 1]);
                            }}
                        >
                            <ArrowIcon className="bwi_PhaseModel_arrow" />
                            <span>{_t("action|go_back")}</span>
                        </AccessibleButton>
                    )}
                    <div data-area="counter">
                        {currentPhaseIndex + 1}/{3}
                    </div>
                    {currentPhaseIndex !== phaseOrder.length - 1 && (
                        <AccessibleButton
                            onClick={() => {
                                setPhase(phaseOrder[currentPhaseIndex + 1]);
                            }}
                            data-area="next"
                        >
                            <span>{_t("action|next")}</span>
                            <ArrowIcon className="bwi_PhaseModel_arrow" />
                        </AccessibleButton>
                    )}
                </div>
            </div>
        </PhasedModelContext.Provider>
    );
}

PhasedModal.Phase = function ModalPhase({
    phase,
    children,
    className,
}: {
    phase: string;
    children: ReactNode;
    className?: string;
}): React.JSX.Element {
    const { currentPhase } = useContext(PhasedModelContext);
    return (
        <>
            {currentPhase === phase ? (
                <div className={classNames("bwi_PhaseModel_content", className)}>{children}</div>
            ) : null}
        </>
    );
};
