// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React, { ReactElement } from "react";

import { _t } from "../../..//languageHandler";
import { BwiA11yDeclaration } from "../../..//bwi/components/A11yDeclaration";
import bwi from "../../../bwi";
import SdkConfig from "../../../SdkConfig";

const DEFAULT_FOOTER_LINKS = [
    { text: "Blog", url: "https://element.io/blog" },
    { text: "Twitter", url: "https://twitter.com/element_hq" },
    { text: "GitHub", url: "https://github.com/vector-im/element-web" },
];

export default function BwiAuthFooter(): ReactElement {
    const brandingConfig = SdkConfig.getObject("branding");
    const links = [...(brandingConfig?.get("auth_footer_links") || [])];
    const wellKnownConfig = bwi.useBwiWellKnownConfig();

    if (wellKnownConfig?.dataPrivacyUrl) {
        links.push({
            text: _t("Privacy Policy"),
            url: wellKnownConfig?.dataPrivacyUrl,
        });
    }

    if (links.length === 0) {
        links.push(...DEFAULT_FOOTER_LINKS);
    }

    return (
        <footer className="mx_AuthFooter" role="contentinfo">
            {links.map(({ text, url }) => (
                <a key={text} href={url} target="_blank" rel="noreferrer noopener">
                    {text}
                </a>
            ))}
            <BwiA11yDeclaration className="bwi_a11yDeclaration_login" />
        </footer>
    );
}
