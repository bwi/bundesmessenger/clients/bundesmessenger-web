// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React, { CSSProperties, PropsWithChildren, useMemo } from "react";

import SdkConfig from "../../..//SdkConfig";
import { getAuthHeaderTransparency, isTransparencyConfigured } from "../../functions/LoginBackground";
import BwiAuthFooter from "./BwiAuthFooter";

const DEFAULT_BACKGROUND_URL = "themes/element/img/backgrounds/lake.jpg";

const BwiAuthPage: React.FC<PropsWithChildren<{}>> = ({ children }) => {
    const backgroundUrl = useMemo(() => {
        const brandingConfig = SdkConfig.getObject("branding");
        const configuredUrl = brandingConfig?.get("welcome_background_url");

        if (configuredUrl) {
            if (Array.isArray(configuredUrl)) {
                return configuredUrl[Math.floor(Math.random() * configuredUrl.length)];
            } else {
                return configuredUrl;
            }
        }

        return DEFAULT_BACKGROUND_URL;
    }, []);

    const pageStyle = {
        background: `center/cover fixed url(${backgroundUrl})`,
        height: "100%",
    };

    const modalStyle: CSSProperties = {
        position: "relative",
        background: "initial",
    };

    const blurStyle: CSSProperties = {
        position: "absolute",
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        filter: isTransparencyConfigured() ? undefined : "blur(40px)",
        background: pageStyle.background,
    };

    const modalContentStyle: CSSProperties = {
        display: "flex",
        zIndex: 1,
        background: "rgba(255, 255, 255, " + getAuthHeaderTransparency() + ")",
        borderRadius: "8px",
    };
    return (
        <div className="mx_AuthPage" style={pageStyle}>
            <div className="mx_AuthPage_modal" style={modalStyle}>
                <div className="mx_AuthPage_modalBlur" style={blurStyle} />
                <div className="mx_AuthPage_modalContent" style={modalContentStyle}>
                    {children}
                </div>
            </div>
            <BwiAuthFooter />
        </div>
    );
};

export default BwiAuthPage;
