// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React, { useState, useEffect, KeyboardEvent, ChangeEvent } from "react";
import { EventType, Direction, MatrixEvent, Room, RoomStateEvent, RoomMember } from "matrix-js-sdk/src/matrix";
import classNames from "classnames";

import Field from "../../../src/components/views/elements/Field";
import { _t } from "../../languageHandler";
import AccessibleButton from "../../components/views/elements/AccessibleButton";
import { Icon as CloseIcon } from "../../../res/img/bwi/icons-close.svg";
import {
    getUserFunctionLabelEvent,
    setUserFunctionLabel,
    unsetUserFunctionLabel,
    canChangeFunctionLabel,
} from "../helper/events/UserFunctionLabel";

interface UserFunctionLabelDisplayProps {
    onClick: () => void;
    onDelete: () => void;
    canChange: boolean;
    label: string;
}

const UserFunctionLabelDisplay: React.FC<UserFunctionLabelDisplayProps> = ({ canChange, label, onClick, onDelete }) => {
    if (canChange) {
        return (
            <>
                <AccessibleButton
                    placement="top"
                    title={_t("action|edit")}
                    className="bwi_userFunction_edit"
                    onClick={() => onClick()}
                    kind="primary_sm"
                >
                    {label ? label : `+ ${_t("Add user function label")}`}
                </AccessibleButton>

                {label && (
                    <AccessibleButton
                        placement="top"
                        title={_t("action|delete")}
                        onClick={onDelete}
                        className="bwi_userFunction_delete"
                        kind="link_inline"
                    >
                        <CloseIcon width={9} height={9} />
                    </AccessibleButton>
                )}
            </>
        );
    }

    return <span>{label}</span>;
};

interface UserFunctionLabelEditorProps {
    label: string;
    onAbort: () => void;
    onSave: (newLabel: string) => void;
}

const UserFunctionLabelEditor: React.FC<UserFunctionLabelEditorProps> = ({ onSave, onAbort, label }) => {
    const [newLabel, setNewLabel] = useState(label);

    const handleKeyPress = (event: KeyboardEvent): void => {
        const suppress = (e: KeyboardEvent): void => {
            e.preventDefault();
            e.stopPropagation();
        };

        switch (event.key) {
            case "Escape":
                onAbort();
                suppress(event);
                break;
            case "Enter":
                _onSave();
                suppress(event);
                break;
        }
    };

    const _onSave = (): void => {
        if (newLabel === label) {
            return;
        }

        onSave(newLabel);
    };

    return (
        <div className="bwi_userFunction_editor">
            <Field
                label={_t("Function label")}
                placeholder={_t("Enter function label...")}
                type="text"
                onChange={(e: ChangeEvent<HTMLInputElement>) => setNewLabel(e.target.value)}
                value={newLabel}
                autoComplete="off"
                autoFocus={true}
                onKeyDown={handleKeyPress}
            />

            <div className="bwi_userFunction_buttonContainer">
                <AccessibleButton data-test-id="editor-abort" onClick={() => onAbort()} kind="danger_sm">
                    {_t("action|cancel")}
                </AccessibleButton>
                <AccessibleButton
                    data-test-id="editor-apply"
                    disabled={label === newLabel}
                    onClick={() => _onSave()}
                    kind="primary_sm"
                >
                    {_t("Apply")}
                </AccessibleButton>
            </div>
        </div>
    );
};

interface UserFunctionLabelProps {
    room: Room;
    member: RoomMember;
}

export const UserFunctionLabel: React.FC<UserFunctionLabelProps> = ({ room, member }) => {
    const [label, setLabel] = useState("");
    const [canChangeLabel, setCanChangeLabel] = useState(false);
    const [editorShown, setEditorShown] = useState(false);

    useEffect(() => {
        setLabel(getUserFunctionLabelEvent(room)[member.userId] || "");
        setCanChangeLabel(canChangeFunctionLabel(room));
    }, [room, member]);

    useEffect(() => {
        const handleStateEvent = (event: MatrixEvent): void => {
            if (event.getType() !== EventType.BwiUserFunctionLabels) {
                return;
            }

            const newLabel = event.getContent()[member.userId] || "";
            console.log({ newLabel, label });
            if (!editorShown && newLabel !== label) {
                setLabel(newLabel);
            }
        };

        const roomState = room.getLiveTimeline().getState(Direction.Forward);
        roomState?.on(RoomStateEvent.Events, handleStateEvent);
        return () => {
            roomState?.off(RoomStateEvent.Events, handleStateEvent);
        };
    }, [label, editorShown, member.userId, room]);

    const onDelete = (): void => {
        const previousLabel = label;
        setLabel("");
        unsetUserFunctionLabel(room, member).catch(() => {
            setLabel(previousLabel);
        });
    };

    const onSave = (newLabel: string): void => {
        const previousLabel = label;
        setLabel(newLabel);

        if (!newLabel) {
            unsetUserFunctionLabel(room, member).catch(() => {
                setLabel(previousLabel);
            });
        } else {
            setUserFunctionLabel(room, member, newLabel).catch(() => {
                setLabel(previousLabel);
            });
        }

        setEditorShown(false);
    };

    if (!canChangeLabel && !label) return null;

    const wrapperClasses = classNames({
        bwi_userFunction: true,
        bwi_userFunction_canChange: canChangeLabel,
    });

    return (
        <div className="mx_UserInfo_container">
            <h2>{_t("Function label")}</h2>
            {canChangeLabel && (
                <p>
                    {_t(
                        "As an administrator you can add and remove a function label for people in the room. This is visible for all room members.",
                    )}
                </p>
            )}
            <div className={wrapperClasses}>
                {editorShown ? (
                    <UserFunctionLabelEditor label={label} onSave={onSave} onAbort={() => setEditorShown(false)} />
                ) : (
                    <UserFunctionLabelDisplay
                        onClick={() => setEditorShown(true)}
                        onDelete={onDelete}
                        canChange={canChangeLabel}
                        label={label}
                    />
                )}
            </div>
        </div>
    );
};
