// Copyright 2024 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import classNames from "classnames";
import React, { ComponentPropsWithoutRef } from "react";

import { ScanResult } from "./ContentScanner";

interface BwiContentErrorIconProps extends ComponentPropsWithoutRef<"div"> {
    type: ScanResult;
}

export function BwiContentErrorIcon({ type, ...containerProps }: BwiContentErrorIconProps): React.JSX.Element {
    const className = classNames(containerProps.className, "bwi_ContentError_iconContainer");
    return <div {...containerProps} className={className} data-type={type} />;
}
