// Copyright 2024 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { NOT_FOUND, REQUEST_FAILED, SAFE, ScanResult, UNKNOWN, UNSAFE } from "./ContentScanner";
import { _t } from "../../languageHandler";

export const SCANNING = "SCANNING";
export const BLOCKED = "BLOCKED";
export const SUCCESS = "SUCCESS";
export const CONTENT_NOT_AVAILABLE = "CONTENT NOT AVAILABLE";
export const SCANNER_NOT_AVAILABLE = "SCANNER_NOT_AVAILABLE";
export const UNKNOWN_REQ_ERR = "UNKNOWN_REQ_ERR";
export const SCANNING_TYPES = [
    SCANNING,
    SUCCESS,
    CONTENT_NOT_AVAILABLE,
    BLOCKED,
    SCANNER_NOT_AVAILABLE,
    UNKNOWN_REQ_ERR,
] as const;

export type ScanningType = (typeof SCANNING_TYPES)[number];

export class ScanningState {
    private _scanningType: ScanningType;

    public constructor(scanningType: ScanningType) {
        this._scanningType = scanningType;
    }

    public static from(scanResult: ScanResult): ScanningState {
        let scanningType: ScanningType;

        switch (scanResult) {
            case UNSAFE:
                scanningType = BLOCKED;
                break;
            case SAFE:
                scanningType = SUCCESS;
                break;
            case REQUEST_FAILED:
                scanningType = SCANNER_NOT_AVAILABLE;
                break;
            case UNKNOWN:
                scanningType = UNKNOWN_REQ_ERR;
                break;
            case NOT_FOUND:
                scanningType = CONTENT_NOT_AVAILABLE;
                break;
        }

        return new ScanningState(scanningType);
    }

    public static scanning(): ScanningState {
        return new ScanningState(SCANNING);
    }

    public static success(): ScanningState {
        return new ScanningState(SUCCESS);
    }

    public get scanningType(): ScanningType {
        return this._scanningType;
    }

    public getScanMessage(): string | null {
        switch (this._scanningType) {
            case BLOCKED:
                return _t("bwi|timeline|content|unsafe");
            case SUCCESS:
                return null;
            case SCANNER_NOT_AVAILABLE:
                return _t("bwi|timeline|content|request-failed");
            case UNKNOWN_REQ_ERR:
                return _t("bwi|timeline|content|unknown");
            case CONTENT_NOT_AVAILABLE:
                return _t("bwi|timeline|content|not_found");
            case SCANNING:
                return _t("bwi|timeline|content|scanning");
        }
    }
}
