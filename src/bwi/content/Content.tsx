// Copyright 2024 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";

import {
    BLOCKED,
    CONTENT_NOT_AVAILABLE,
    SCANNER_NOT_AVAILABLE,
    SCANNING,
    ScanningState,
    SUCCESS,
    UNKNOWN_REQ_ERR,
} from "./ScanningState";
import AccessibleButton from "../../components/views/elements/AccessibleButton";
import TextWithTooltip from "../../components/views/elements/TextWithTooltip";

export type ContentType = "image" | "video" | "file";

export interface BwiContentLoaderProps {
    fileName: string;
    fileSize: string | null;
    contentType: ContentType;
    scanningState: ScanningState;
    tooltip?: string;
    onClick?: () => void | Promise<void>;
    width?: number;
    height?: number;
}

export function BwiContentLoader({
    contentType,
    fileName,
    scanningState,
    fileSize,
    tooltip = fileName,
    onClick = () => {},
    width = 300,
    height = 100,
}: BwiContentLoaderProps): JSX.Element {
    const styles = {
        "--_bwi-content_width": width + "px",
        "--_bwi-content_height": height + "px",
    } as any;

    let iconTopOrLeft: JSX.Element;

    switch (scanningState.scanningType) {
        case SCANNING:
            iconTopOrLeft = (
                <IconLeftHelper
                    scanningState={scanningState}
                    fileName={fileName}
                    fileSize={fileSize}
                    contentType={contentType}
                />
            );
            break;
        case SUCCESS:
            iconTopOrLeft = (
                <AccessibleButton onClick={onClick}>
                    <TextWithTooltip tooltip={tooltip}>
                        <IconLeftHelper
                            scanningState={scanningState}
                            fileName={fileName}
                            fileSize={fileSize}
                            contentType={contentType}
                        />
                    </TextWithTooltip>
                </AccessibleButton>
            );
            break;
        case BLOCKED:
        case CONTENT_NOT_AVAILABLE:
        case SCANNER_NOT_AVAILABLE:
        case UNKNOWN_REQ_ERR:
            iconTopOrLeft = (
                <IconTopHelper scanningState={scanningState} fileName={fileName} contentType={contentType} />
            );
            break;
    }

    return (
        <div className="bwi_content_container" style={styles} data-bwi_content_type={contentType}>
            <div className="bwi_content_container_helper" data-bwi_scanner_type={scanningState.scanningType}>
                {iconTopOrLeft}
            </div>
        </div>
    );
}

interface IconTopHelperProp {
    scanningState: ScanningState;
    contentType: ContentType;
    fileName: string | null;
}

function IconTopHelper({ scanningState, contentType, fileName }: IconTopHelperProp): JSX.Element {
    return (
        <div className="bwi_content_icon_top">
            <ScannerIcon scanningState={scanningState} contentType={contentType} />
            <ScannerLabel scanningState={scanningState} fileName={fileName} fileSize={null} />
        </div>
    );
}

interface IconLeftHelperProps {
    scanningState: ScanningState;
    contentType: ContentType;
    fileName: string | null;
    fileSize: string | null;
}

function IconLeftHelper({ scanningState, contentType, fileName, fileSize }: IconLeftHelperProps): JSX.Element {
    return (
        <div className="bwi_content_icon_left">
            <ScannerIcon scanningState={scanningState} contentType={contentType} />
            <div className="bwi_content_icon_left_body">
                <div>{fileName ?? "No Filename"}</div>
                <ScannerLabel scanningState={scanningState} fileName={null} fileSize={fileSize} />
            </div>
        </div>
    );
}

export interface ScannerIconProps {
    scanningState: ScanningState;
    contentType: ContentType;
}

export function ScannerIcon({ scanningState, contentType }: ScannerIconProps): JSX.Element {
    return (
        <div
            className="bwi_content_icon_container"
            data-bwi_scanner_type={scanningState.scanningType}
            data-bwi_content_type={contentType}
        />
    );
}

export interface ScannerLabelProps {
    scanningState: ScanningState;
    fileName: string | null;
    fileSize: string | null;
}

export function ScannerLabel({ scanningState, fileName, fileSize }: ScannerLabelProps): JSX.Element | null {
    const message = scanningState.getScanMessage();
    let fileNameElement: JSX.Element | null = null;
    let fileSizeElement: JSX.Element | null = null;

    if (fileName !== null) {
        fileNameElement = <span>{fileName}</span>;
    }

    if (fileSize !== null) {
        fileSizeElement = <div className="bwi_content_label">{fileSize}</div>;
    }

    switch (scanningState.scanningType) {
        case SCANNING:
        case CONTENT_NOT_AVAILABLE:
        case SCANNER_NOT_AVAILABLE:
        case UNKNOWN_REQ_ERR:
            return <div className="bwi_content_label">{message}</div>;
        case BLOCKED:
            return (
                <div className="bwi_content_label">
                    {fileNameElement}
                    <span className="bwi_content_label_dark"> {message}</span>
                </div>
            );
        case SUCCESS:
            return fileSizeElement;
    }
}
