// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { ClientEvent, MatrixClient, Method, SyncState, TypedEventEmitter } from "matrix-js-sdk/src/matrix";
import { isNullOrUndefined } from "matrix-js-sdk/src/utils";
import { useState } from "react";

import { formatFullDateWithTZ, formatSameDayTimeSpan } from "../../DateUtils";
import { MatrixClientPeg } from "../../MatrixClientPeg";
import SdkConfig from "../../SdkConfig";
import { _t } from "../../languageHandler";
import SettingsStore from "../../settings/SettingsStore";
import { BwiFeature } from "../settings/BwiFeature";
import { useTypedEventEmitter } from "../../hooks/useEventEmitter";
import { useSettingValue } from "../../hooks/useSettings";
import { SettingKey } from "../../settings/Settings.tsx";

const ENDPOINT = "/cmaintenance";

export const REFRESH_INTERVAL = 1000 * 60 * 10;

const MAINTENANCE_TYPES = ["MAINTENANCE", "ADHOC_MESSAGE", "MAINTENANCE_STANDARD", "UNAVAILABLE"] as const;
export type MaintenanceType = (typeof MAINTENANCE_TYPES)[number];

export type ActiveDowntimeInfo = {
    active: boolean;
    downtime?: DowntimeInfo;
};

export type DowntimeInfo = {
    start_time: number;
    end_time: number;
    warning_start_time: number;
    type?: MaintenanceType;
    description?: string;
    blocking?: boolean;
};

export type ServerDowntimeInfo = {
    start_time: string;
    end_time: string;
    warning_start_time: string;
    type?: MaintenanceType;
    description?: string;
    blocking?: boolean;
};

export type MaintenanceInfo = {
    downtime: DowntimeInfo[];
    versions: {
        android: {
            update_before: string;
            warn_before: string;
        };
        ios: {
            update_before: string;
            warn_before: string;
        };
    };
};

export type ServerMaintenanceInfo = {
    downtime: ServerDowntimeInfo[];
    versions: {
        android: {
            update_before: string;
            warn_before: string;
        };
        ios: {
            update_before: string;
            warn_before: string;
        };
    };
};

/**
 * Class that retrieves the version and downtime information from a backend endpoint in the MaintenanceInfo format
 * Handles the logic to show only the nearest downtime and stores if the downtime notification has already been seen
 * in this browser in the local storage.
 */
export class MaintenanceManager extends TypedEventEmitter<
    "update",
    {
        update: (manager: MaintenanceManager, downtime: DowntimeInfo | null) => void;
    }
> {
    private static _instance?: MaintenanceManager;
    private static bypassBlocking = false;
    private static currentClient: MatrixClient;
    private isInitialized = false;
    private initializing = false;
    private maintenanceInfo: MaintenanceInfo | null = null;
    public downtimeInfo: DowntimeInfo | null = null;
    private MaintenanceStorageKey = "bwi.downTimeSeen";
    private downtimeSeen: { [x: string]: boolean } = {};
    private runningQuery: Promise<void> | undefined;
    private interval = 0;
    private serverNotReachable = false;

    public constructor() {
        super();
    }

    public static get instance(): MaintenanceManager {
        if (!MaintenanceManager._instance) {
            MaintenanceManager._instance = new MaintenanceManager();
            MaintenanceManager._instance.interval = setInterval(
                MaintenanceManager._instance.refreshQuery,
                REFRESH_INTERVAL,
            ) as unknown as number;
            MatrixClientPeg.safeGet().on(ClientEvent.Sync, MaintenanceManager._instance.onSync);
        }
        return MaintenanceManager._instance;
    }

    /**
     * Takes API Maintenance Info with ISO8601 dates and converts them to UNIX timestamps
     * @param serverInfo ServerMaintenanceInfo with ISO8601 dates
     */
    public static parse = (serverInfo: ServerMaintenanceInfo): MaintenanceInfo => {
        const parsedDates: DowntimeInfo[] = [];
        serverInfo.downtime.forEach((downtime) => {
            try {
                // Only accept valid types
                if (!downtime.type || !MAINTENANCE_TYPES.includes(downtime.type)) return;

                if (downtime.type === "ADHOC_MESSAGE" && !downtime.description) return;

                const end = +new Date(downtime.end_time);
                const start = +new Date(downtime.start_time);
                const warning = +new Date(downtime.warning_start_time);

                // Validate date types
                if (isNaN(end) || isNaN(start) || isNaN(warning)) return;

                parsedDates.push({
                    description: downtime.description,
                    type: downtime.type,
                    end_time: end,
                    start_time: start,
                    warning_start_time: warning,
                    blocking: downtime.blocking,
                });
            } catch {
                console.debug(downtime);
            }
        });

        return { ...serverInfo, downtime: parsedDates };
    };

    /**
     * Before logging in we don't have an actual MatrixClient ready so we use the tempclient
     * and create the MaintenanceManager Singleton later
     */
    public static shouldShowBeforeLogin = async (client: MatrixClient): Promise<ActiveDowntimeInfo> => {
        MaintenanceManager.currentClient = client;
        const result: ActiveDowntimeInfo = { active: false };
        try {
            const maintenanceData = await client.http.request<ServerMaintenanceInfo>(
                Method.Get,
                ENDPOINT,
                undefined,
                undefined,
                {
                    prefix: "/_matrix",
                    avoidBlocking: true,
                },
            );

            if (maintenanceData) {
                const parsedData = MaintenanceManager.parse(maintenanceData);
                const info = MaintenanceManager.findClosestDowntime(parsedData, true);

                let isMaintenanceActive = false;
                if (info) isMaintenanceActive = MaintenanceManager.checkIfSelectedDowntimeActive(Date.now(), info);

                if (info && info.blocking && isMaintenanceActive && !MaintenanceManager.bypassBlocking) {
                    client.http.enableBlocking();
                } else {
                    client.http.disableBlocking();
                }

                if (!info) return result;
                const currentTime = Date.now();
                if (currentTime >= info.start_time && currentTime < info.end_time) {
                    result.active = true;
                    result.downtime = info;
                    return result;
                }
            }
        } catch (error) {
            if (client) client.http.disableBlocking();
            console.debug(error);
        }

        return result;
    };

    /**
     * Format messages that are returned to users
     */
    public static getDowntimeMessage = (downtime: DowntimeInfo): string => {
        const downtimeType = downtime.type;
        const isMaintenanceType = downtimeType === "MAINTENANCE" || downtimeType === "MAINTENANCE_STANDARD";
        const isUnavailable = downtimeType === "UNAVAILABLE";
        const isAdHoc = downtimeType === "ADHOC_MESSAGE";

        let dtMessage = "";

        if (downtime && isMaintenanceType) {
            dtMessage = MaintenanceManager.getMaintenanceMessage(downtime);
        }

        let maintenanceMessage = "";
        let descriptionMessage = "";

        const noEventsText = `${_t("Messages may not be sent or received during that time.")}`;

        if (isMaintenanceType && downtime) {
            maintenanceMessage = `${dtMessage}
            ${noEventsText}`;
        }

        if (isUnavailable) {
            const unAvailableText = _t("The %(brand)s is currently unavailable.", {
                brand: SdkConfig.get().brand,
            });
            maintenanceMessage = `${unAvailableText}
            ${noEventsText}`;
        }

        if (downtime && downtime.description) {
            if (isMaintenanceType) {
                descriptionMessage = `
            ${downtime.description}`;
            }
            if (isAdHoc) {
                descriptionMessage = downtime.description;
            }
        }
        return `${maintenanceMessage}${descriptionMessage}`;
    };

    public static getMaintenanceMessage = (downtime: DowntimeInfo): string => {
        let dtMessage = "";
        const config = SdkConfig.get();
        const fromDate = new Date(downtime.start_time);
        const toDate = new Date(downtime.end_time);

        // Same Day
        if (fromDate.toDateString() === toDate.toDateString()) {
            dtMessage = _t("The %(brand)s is not available on %(dateSpan)s.", {
                brand: config.brand,
                dateSpan: formatSameDayTimeSpan(fromDate, toDate, false),
            });
        } else {
            dtMessage = _t("The %(brand)s is not available from %(startDate)s to %(endDate)s.", {
                brand: config.brand,
                startDate: formatFullDateWithTZ(fromDate, false),
                endDate: formatFullDateWithTZ(toDate, false),
            });
        }
        return dtMessage;
    };

    /**
     * Lookup nearest downtime that has not already ended
     */
    public static findClosestDowntime = (
        maintenance: MaintenanceInfo | ServerMaintenanceInfo,
        isParsed: boolean,
    ): DowntimeInfo | null => {
        let closest: DowntimeInfo | null = null;
        let info: MaintenanceInfo;
        if (!isParsed && maintenance.downtime) {
            info = MaintenanceManager.parse(maintenance as any as ServerMaintenanceInfo);
        } else {
            info = maintenance as any as MaintenanceInfo;
        }
        if (info.downtime) {
            info.downtime.forEach((downtime) => {
                const isActive = downtime.end_time > Date.now();
                const isFirstAndActive = !closest && isActive;
                const isCloserThanCurrent = closest && downtime.start_time < closest.start_time && isActive;

                if (isFirstAndActive || isCloserThanCurrent) {
                    closest = downtime;
                }
            });
            return closest;
        } else return null;
    };

    public static setBypassBlocking = (shouldBypass: boolean): void => {
        if (SettingsStore.getValue(BwiFeature.BypassBlockingButton)) {
            MaintenanceManager.bypassBlocking = shouldBypass;
        }
        const currentClient = MaintenanceManager.currentClient;
        if (shouldBypass && currentClient) {
            currentClient.http.disableBlocking();
        }
    };

    public static getBypassBlocking = (): boolean => MaintenanceManager.bypassBlocking;

    private onSync = (state: SyncState): void => {
        if (state === "ERROR") {
            this.serverNotReachable = true;
        } else {
            this.serverNotReachable = false;
        }
    };

    public static checkIfSelectedDowntimeActive = (currentTime: number, downtimeToCheck: DowntimeInfo): boolean => {
        return currentTime >= downtimeToCheck.start_time && currentTime < downtimeToCheck.end_time;
    };

    /**
     * Retrieve account data of Maintenance API.
     * Store result as state in manager.
     * @async
     */
    private queryApi = async (): Promise<void> => {
        this.initializing = true;
        const cli = MatrixClientPeg.safeGet();
        MaintenanceManager.currentClient = cli;
        await cli.http
            .request<ServerMaintenanceInfo>(Method.Get, ENDPOINT, undefined, undefined, {
                prefix: "/_matrix",
                avoidBlocking: true,
            })
            .then(async (maintenanceData) => {
                if (maintenanceData) {
                    this.maintenanceInfo = MaintenanceManager.parse(maintenanceData);
                    this.downtimeInfo = MaintenanceManager.findClosestDowntime(this.maintenanceInfo, true);

                    this.emit("update", this, this.downtimeInfo);

                    const isDowntimeActive = this.shouldSeeActiveDowntime().active;

                    if (
                        this.downtimeInfo &&
                        this.downtimeInfo.blocking &&
                        isDowntimeActive &&
                        !MaintenanceManager.bypassBlocking
                    ) {
                        cli.http.enableBlocking();
                    } else {
                        cli.http.disableBlocking();
                    }

                    this.isInitialized = true;
                }
            })
            .catch((e) => {
                if (cli) cli.http.disableBlocking();

                this.isInitialized = true;
            });
    };

    /**
     * Check if API is being called or already called or needs to be called
     * wait for API call to finish and retrieve if notification already seen
     * @async
     */
    private runQuery = async (): Promise<void> => {
        if ((!this.isInitialized && !this.initializing) || this.runningQuery) {
            if (!this.isInitialized && !this.initializing) {
                this.runningQuery = this.queryApi();
            }

            await this.runningQuery;
            this.runningQuery = undefined;
        }
        this.downtimeSeen = JSON.parse(localStorage.getItem(this.MaintenanceStorageKey) ?? "null");
    };

    private refreshQuery = async (): Promise<void> => {
        if (!this.runningQuery) {
            await this.queryApi();
            this.downtimeSeen = JSON.parse(localStorage.getItem(this.MaintenanceStorageKey) ?? "null");
            this.runningQuery = undefined;
        }
    };

    private checkIfDowntimeValid = (currentTime: number): boolean => {
        if (!this.downtimeInfo) return false;
        // eslint-disable-next-line camelcase
        const { warning_start_time, start_time, end_time } = this.downtimeInfo;
        // eslint-disable-next-line camelcase
        if (warning_start_time > start_time) return false;
        // eslint-disable-next-line camelcase
        if (warning_start_time > end_time) return false;
        // eslint-disable-next-line camelcase
        if (start_time > end_time) return false;
        return currentTime > this.downtimeInfo.warning_start_time && currentTime < this.downtimeInfo.end_time;
    };

    private checkIfDowntimeActive = (currentTime: number): boolean => {
        if (!this.downtimeInfo) return false;
        return currentTime >= this.downtimeInfo.start_time && currentTime < this.downtimeInfo.end_time;
    };

    /**
     * If the information has not been retrieved, query the API
     * Else just return the stored information
     * @async
     * @return MaintenanceInfo
     */
    public getMaintenanceInfo = async (): Promise<MaintenanceInfo | null> => {
        await this.runQuery();
        return this.maintenanceInfo;
    };

    /**
     * If the information has not been retrieved, query the API
     * Else just return the stored information
     * @async
     * @return DowntimeInfo
     */
    public getDowntimeInfo = async (): Promise<DowntimeInfo | null> => {
        await this.runQuery();
        return this.downtimeInfo;
    };

    /**
     * Store if downtime has been seen in localstorage and in manager state
     */
    public setSeenDowntime = (hasSeen: boolean): void => {
        if (this.downtimeInfo) {
            const savedDowntime: Record<string, boolean> = {};
            savedDowntime[`${this.downtimeInfo.start_time}-downtime`] = hasSeen;
            this.downtimeSeen = savedDowntime;
            localStorage.setItem(this.MaintenanceStorageKey, JSON.stringify(savedDowntime));
        }
    };

    /**
     * If next downtime should be warned about
     */
    public shouldSeeDowntimeNotification = (): boolean => {
        if (!this.downtimeInfo) return false;
        const date = Date.now();
        return this.checkIfDowntimeValid(date);
    };

    /**
     * True if current downtime has already been seen
     * either cached or fetched from local storage
     */
    public hasSeenDowntimeNotification = (): boolean => {
        if (!this.downtimeInfo) return false;
        const date = Date.now();
        const startTime = this.downtimeInfo.start_time;

        const checkIfValid = (): boolean => {
            if (this.checkIfDowntimeValid(date)) {
                return !!this.downtimeSeen[`${startTime}-downtime`];
            } else return false;
        };

        // localstorage already queried
        if (!isNullOrUndefined(this.downtimeSeen)) {
            return checkIfValid();
        } else {
            const fromStorage = JSON.parse(localStorage.getItem(this.MaintenanceStorageKey) ?? "null");
            if (fromStorage) {
                this.downtimeSeen = fromStorage;
                return checkIfValid();
            }
            return false;
        }
    };

    /**
     * Check if the nearest downtime is still valid and return true
     * Otherwise false
     */
    public shouldSeeDowntimeBanner = (): boolean => {
        if (isNullOrUndefined(this.downtimeInfo)) return false;

        if (this.checkIfDowntimeValid(Date.now())) return true;
        else return false;
    };

    /**
     * Check if a downtime is active right now and
     * if we should see the banner for it or if the server is currently unavailable
     */
    public shouldSeeActiveDowntime = (): ActiveDowntimeInfo => {
        const serverUnavailable: DowntimeInfo = {
            type: "UNAVAILABLE",
            warning_start_time: 0,
            start_time: 0,
            end_time: 0,
        };

        if (isNullOrUndefined(this.downtimeInfo)) {
            if (this.serverNotReachable) return { active: true, downtime: serverUnavailable };
            else return { active: false };
        }
        // Downtime info has precedence over generic downtime info
        if (this.checkIfDowntimeActive(Date.now())) {
            return { active: true, downtime: this.downtimeInfo! };
        } else if (this.serverNotReachable) return { active: true, downtime: serverUnavailable };
        else return { active: false };
    };

    public isBlocking(): boolean {
        return this.downtimeInfo?.blocking === true;
    }

    /**
     * For testing purposes to restore initial state
     */
    public static resetManager = (): void => {
        if (MaintenanceManager._instance) {
            if (typeof MaintenanceManager._instance.interval === "number") {
                clearInterval(MaintenanceManager._instance.interval);
            }
            delete MaintenanceManager._instance;
        }
    };
}

export function useMaintenanceManager(manager: MaintenanceManager = MaintenanceManager.instance): {
    enabled: string;
    downtime: DowntimeInfo | null;
    manager: MaintenanceManager;
    showNotification: boolean;
    showBanner: boolean;
    isBlocking: boolean;
} {
    const [showBanner, setShowBanner] = useState(manager.shouldSeeDowntimeBanner());
    const [showNotification, setShowNotification] = useState(manager.shouldSeeDowntimeNotification());
    const [downtime, setDowntime] = useState(manager.downtimeInfo);
    const [isBlocking, setIsBlocking] = useState(manager.isBlocking());
    const enabled = useSettingValue(BwiFeature.ShowMaintenanceInfo as SettingKey) as string;

    useTypedEventEmitter(manager, "update", () => {
        manager.shouldSeeDowntimeBanner();
        setShowBanner(manager.shouldSeeDowntimeBanner());
        setShowNotification(manager.shouldSeeDowntimeNotification());
        setIsBlocking(manager.isBlocking());
        setDowntime(manager.downtimeInfo);
    });

    return { enabled, downtime, manager, showNotification, showBanner, isBlocking };
}
