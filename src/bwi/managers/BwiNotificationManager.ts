// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { MatrixError, Method } from "matrix-js-sdk/src/matrix";
import { encodeUri } from "matrix-js-sdk/src/utils";

import { MatrixClientPeg } from "../../MatrixClientPeg";
import { BwiNotifications, NOTIFICATION_DATA } from "../components/notifications/BwiNotifications";
import { BwiNotificationDialog } from "../components/notifications/BwiNotificationRenderer";

/**
 * Class to handle the modification of Account Data related to BWI Notifications
 * Singleton with internal state to reliably sync what should be shown
 * and what was already shown to the user
 */
export class BwiNotificationManager {
    private static _instance: BwiNotificationManager | null = null;
    private isInitialized = false;
    private initializing = false;
    private notificationState: BwiNotifications = {};

    public static get instance(): BwiNotificationManager {
        if (!BwiNotificationManager._instance) {
            BwiNotificationManager._instance = new BwiNotificationManager();
        }
        return BwiNotificationManager._instance;
    }

    public static isNotificationVersionized = (dialog: BwiNotificationDialog): boolean =>
        typeof dialog.version === "string";

    private async ensureAccountDataIsRetrieved(): Promise<void> {
        let initPromise: Promise<void> | null = null;
        if (!this.isInitialized) {
            initPromise = this.queryNotificationAccountData();
        }

        if (initPromise) await initPromise;
    }

    /**
     * Retrieve account data of BWI Notifications once directly from Backend.
     * Store result as state in manager.
     * @async
     */
    private async queryNotificationAccountData(): Promise<void> {
        if (!this.isInitialized && !this.initializing) {
            this.initializing = true;
            const cli = MatrixClientPeg.safeGet();
            const path = encodeUri(`/user/$userId/account_data/$type`, {
                $userId: cli.credentials.userId,
                $type: NOTIFICATION_DATA,
            });

            try {
                const response = await cli.http.authedRequest<BwiNotifications>(Method.Get, path, undefined);
                if (response) {
                    this.notificationState = response;
                }
                this.isInitialized = true;
            } catch (err) {
                // no data in notifications stored yet
                if (err instanceof MatrixError && err.data && err.data.errcode === "M_NOT_FOUND") {
                    this.notificationState = {};
                    this.isInitialized = true;
                }
            }
            this.initializing = false;
        }
    }

    private mapAsync<T, U>(array: T[], callbackfn: (value: T, index: number, array: T[]) => Promise<U>): Promise<U[]> {
        return Promise.all(array.map(callbackfn));
    }

    private async filterAsync<T>(
        array: T[],
        callbackfn: (value: T, index: number, array: T[]) => Promise<boolean>,
    ): Promise<T[]> {
        const filterMap = await this.mapAsync(array, callbackfn);
        return array.filter((value, index) => filterMap[index]);
    }

    /**
     * Check account data for input notifications to see if they have already been seen.
     * @async
     * @param notificationsToCheck Notifications to show if not already shown
     * @return Notifications that can be shown
     */
    public async filterAlreadySeen(notificationsToCheck: BwiNotificationDialog[]): Promise<BwiNotificationDialog[]> {
        await this.ensureAccountDataIsRetrieved();

        // accountdata request failed so return empty response and try again later
        if (!this.isInitialized) return [];

        return await this.filterAsync(notificationsToCheck, async (notification) => {
            // local only true false and in local storage
            if (notification.customFilterFunction) {
                return await notification.customFilterFunction();
            } else {
                const val = this.notificationState[notification.notificationType];
                if (BwiNotificationManager.isNotificationVersionized(notification) && typeof val !== "boolean") {
                    // notification is expected to have a version and to be {[key: string]: boolean} and not boolean
                    // the notification is shown when it is either not in the account data or true in the account data
                    const version = notification.version || "";
                    return !val?.hasOwnProperty(version) || val[version];
                } else {
                    // notification is true or false so show only when true or not defined
                    return typeof val === "undefined" || !!val;
                }
            }
        });
    }

    /**
     * Mark Notification as shown.
     * Stores result in Account Data.
     *
     * @async
     * @param {BwiNotificationDialog} notification Notification that was shown
     * @memberof BwiNotificationManager
     */
    public async hasSeenNotification(notification: BwiNotificationDialog): Promise<void> {
        await this.ensureAccountDataIsRetrieved();

        if (notification.customFinishFunction) {
            await notification.customFinishFunction();
            return;
        }

        const newState: BwiNotifications = { ...this.notificationState };
        if (BwiNotificationManager.isNotificationVersionized(notification)) {
            // no prior versions in account data so initialize the object first
            if (typeof newState[notification.notificationType] === "undefined") {
                newState[notification.notificationType] = {};
            }

            (newState[notification.notificationType] as { [key: string]: boolean })[notification.version || ""] = false;
        } else {
            newState[notification.notificationType] = false;
        }

        await MatrixClientPeg.safeGet()
            .setAccountData(NOTIFICATION_DATA, newState)
            .then(() => {
                this.notificationState = newState;
            });
    }

    /**
     * For testing purposes to restore initial state
     */
    public static resetManager = (): void => {
        if (BwiNotificationManager._instance) {
            BwiNotificationManager._instance = null;
        }
    };
}
