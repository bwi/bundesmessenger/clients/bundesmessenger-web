// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// eslint-disable-next-line no-restricted-imports
import { IHierarchyRoom } from "matrix-js-sdk/src/@types/spaces";

import { MatrixClientPeg } from "../../MatrixClientPeg";

export type PublicRoomManagerState = {
    crawledRooms: Record<string, IHierarchyRoom>;
};

export class PublicRoomManagerClass {
    private crawledRooms: Record<string, IHierarchyRoom> = {};

    private async crawlRoom(roomId: string): Promise<IHierarchyRoom | undefined> {
        try {
            const result = await MatrixClientPeg.safeGet().getRoomHierarchy(roomId, 1, 1);
            if (result.rooms && result.rooms.length > 0) {
                // Spaces API returns an array of rooms but since we query the id there should only be one result
                return result.rooms[0];
            }
        } catch {
            return undefined;
        }
    }

    /**
     *  check if room already queried and if not fetch from the Spaces hierarchy endpoint
     *  @param roomId id of the room we are looking for
     *
     * @memberof DirectoryStore
     */
    public async findRoom(roomIdentifier: string): Promise<IHierarchyRoom | undefined> {
        // check the directory we already fetched
        const cached = this.crawledRooms[roomIdentifier];
        if (cached) {
            return cached;
        } else if (roomIdentifier.startsWith("#")) {
            try {
                const aliasInfo = await MatrixClientPeg.safeGet().getRoomIdForAlias(roomIdentifier);
                const result = await this.crawlRoom(aliasInfo.room_id);
                if (result) {
                    this.crawledRooms[roomIdentifier.toString()] = result;
                }
                return result;
            } catch {
                return undefined;
            }
        } else if (roomIdentifier.startsWith("!")) {
            const result = await this.crawlRoom(roomIdentifier);
            if (result) {
                this.crawledRooms[roomIdentifier.toString()] = result;
            }
            return result;
        }
        return undefined;
    }
}

export default class PublicRoomManager {
    private static internalInstance?: PublicRoomManagerClass;

    public static get instance(): PublicRoomManagerClass {
        if (!PublicRoomManager.internalInstance) {
            PublicRoomManager.internalInstance = new PublicRoomManagerClass();
        }

        return PublicRoomManager.internalInstance;
    }

    public static resetManager = (): void => {
        if (PublicRoomManager.internalInstance) {
            PublicRoomManager.internalInstance = undefined;
        }
    };
}
