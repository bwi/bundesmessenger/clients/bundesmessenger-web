// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { KNOWN_POLL_KIND, M_POLL_KIND_DISCLOSED, M_POLL_KIND_UNDISCLOSED } from "matrix-events-sdk";
import { IContent, JoinRule, MatrixEvent, MsgType, Room, RoomEvent } from "matrix-js-sdk/src/matrix";
import { logger } from "matrix-js-sdk/src/logger";

import { MatrixClientPeg } from "../../MatrixClientPeg";
import BwiAnalyticsManager, { Dimension, DimensionKey } from "../../bwi/analytics/BwiAnalyticsManager";
import SettingsStore from "../../settings/SettingsStore";
import { bwiIsRoomFederated } from "../functions";
import { BwiFeature } from "../settings/BwiFeature";
import {
    BwiAnalyticsActions,
    BwiAnalyticsCategories,
    BwiAnalyticsEvents,
    FeatureAnalyticsEvents,
    FederationAnalyticsActions,
    FederationAnalyticsEvents,
} from "./BwiAnalyticsEvents";

export type HuddleFeatureEvents =
    | FeatureAnalyticsEvents.Huddle_Created
    | FeatureAnalyticsEvents.Huddle_Joined
    | FeatureAnalyticsEvents.Huddle_Full;
type AnalyticsMessageType = "text" | "image" | "video" | "voice_message" | "file" | "location" | "audio" | "unknown";
type AnalyticsPollType =
    | "disclosed"
    | "undisclosed"
    | "disclosed_show_participants"
    | "undisclosed_show_participants"
    | "unknown"
    | "unknown_show_participants";

export const getTimestamp = (): number => {
    return Math.floor(new Date().getTime());
};

export const getDeviceDimensionCluster = (count: number): string => {
    if (isNaN(count)) return "Unbekannt";
    if (count <= 0) return "Undefiniert";
    if (count <= 10) return "1 bis 10";
    if (count <= 100) return "11 bis 100";
    if (count <= 200) return "101 bis 200";
    if (count <= 500) return "201 bis 500";
    if (count <= 1000) return "501 bis 1000";
    if (count <= 2500) return "1001 bis 2500";
    return "mehr als 2500";
};

const getAnalyticsEventType = (content: IContent): AnalyticsMessageType => {
    const msgType = content.msgtype!.toString();
    let analyticsType: AnalyticsMessageType = BwiAnalyticsEvents.ForwardMessage_unknown;
    switch (msgType) {
        case MsgType.Text.toString():
            analyticsType = BwiAnalyticsEvents.ForwardMessage_text;
            break;
        case MsgType.File.toString():
            analyticsType = BwiAnalyticsEvents.ForwardMessage_file;
            break;
        case MsgType.Image.toString():
            analyticsType = BwiAnalyticsEvents.ForwardMessage_image;
            break;
        case MsgType.Video.toString():
            analyticsType = BwiAnalyticsEvents.ForwardMessage_video;
            break;
        case MsgType.Location.toString():
            analyticsType = BwiAnalyticsEvents.ForwardMessage_location;
            break;
        case MsgType.Audio.toString():
            if (!!content["org.matrix.msc2516.voice"] || !!content["org.matrix.msc3245.voice"]) {
                analyticsType = BwiAnalyticsEvents.ForwardMessage_voice;
            } else {
                analyticsType = BwiAnalyticsEvents.ForwardMessage_audio;
            }
            break;
    }

    return analyticsType;
};

const getRoomDevicesDimension = async (roomId: string): Promise<Dimension[]> => {
    let dimensions: Dimension[] = [];
    const cli = MatrixClientPeg.safeGet();
    if (!cli || !cli.getRoom) return dimensions;
    const room = cli.getRoom(roomId);
    if (!room) return dimensions;

    const memberIds = room.getJoinedMembers().map((member) => member.userId);
    let deviceSum = 0;
    try {
        const deviceMap = await cli.getCrypto()?.getUserDeviceInfo(memberIds, true);

        if (!deviceMap) return [];

        for (const [, devicesOfUser] of deviceMap) {
            deviceSum += devicesOfUser.size;
        }
        const deviceCountKey: DimensionKey = "RoomDevicesCount";
        if (BwiAnalyticsManager.isDimensionConfigured(deviceCountKey)) {
            const deviceCountID = BwiAnalyticsManager.configuredDimensions![deviceCountKey];

            const devicesCountOutput: Dimension = {
                id: deviceCountID,
                value: getDeviceDimensionCluster(deviceSum),
                name: deviceCountKey,
            };
            dimensions = [devicesCountOutput];
        }

        return dimensions;
    } catch {
        logger.error("Matomo dimensions could not be configured");
        return [];
    }
};

export const sendAnalyticsEncryptionViewMessage = async (
    errorCode: string,
    count: number,
    roomId?: string,
): Promise<void> => {
    let devices;

    if (roomId) {
        devices = await getRoomDevicesDimension(roomId);
    }

    return BwiAnalyticsManager.trackEvent(
        BwiAnalyticsCategories.Encryption,
        BwiAnalyticsActions.Encryption_ViewMessage,
        errorCode,
        count,
        devices,
    );
};

export const sendAnalyticsGeneralConsentGiven = async (source: "popup" | "settings"): Promise<void> => {
    return BwiAnalyticsManager.trackEvent(
        BwiAnalyticsCategories.General,
        BwiAnalyticsActions.General_ConsentGiven,
        source,
        undefined,
        undefined,
        true,
    );
};

export const sendAnalyticsGeneralLogout = async (): Promise<void> => {
    return BwiAnalyticsManager.trackEvent(
        BwiAnalyticsCategories.General,
        BwiAnalyticsActions.General_Logout,
        BwiAnalyticsEvents.Logout_Default,
    );
};

export const sendAnalyticsPerformanceSendMessage = async (
    startTime: number,
    sendPromise: Promise<{ event_id: string }>,
    roomId: string,
): Promise<void> => {
    if (BwiAnalyticsManager.disabled) return;
    const cli = MatrixClientPeg.safeGet();
    const room = cli.getRoom(roomId);

    const eventId = (await sendPromise).event_id;
    let endTime = getTimestamp();

    if (!room?.findEventById(eventId)) {
        await new Promise<void>((resolve) => {
            const handler = (ev: MatrixEvent): void => {
                if (ev.getId() === eventId) {
                    room!.off(RoomEvent.LocalEchoUpdated, handler);
                    resolve();
                }
            };

            room!.on(RoomEvent.LocalEchoUpdated, handler);
        });

        endTime = getTimestamp();
    }
    const duration = endTime - startTime;
    // message is slower than 5 seconds, send error to matomo
    if (duration >= 5000) {
        const devices = await getRoomDevicesDimension(roomId);
        return BwiAnalyticsManager.trackEvent(
            BwiAnalyticsCategories.Performance,
            BwiAnalyticsActions.Performance_SendMessage,
            BwiAnalyticsEvents.SendMessage_Default,
            duration,
            devices,
        );
    }
};

export const sendAnalyticsFeatureSendPoll = async (
    pollKind: KNOWN_POLL_KIND,
    votingOptions: number,
    roomId: string,
    showParticipants?: boolean,
): Promise<void> => {
    const devices = await getRoomDevicesDimension(roomId);
    let pollType: AnalyticsPollType = "unknown";
    if (M_POLL_KIND_DISCLOSED.matches(pollKind?.name)) {
        if (showParticipants) pollType = BwiAnalyticsEvents.SendPoll_disclosed_show_participants;
        else pollType = BwiAnalyticsEvents.SendPoll_disclosed;
    } else if (M_POLL_KIND_UNDISCLOSED.matches(pollKind?.name)) {
        if (showParticipants) pollType = BwiAnalyticsEvents.SendPoll_undisclosed_show_participants;
        else pollType = BwiAnalyticsEvents.SendPoll_undisclosed;
    }

    return BwiAnalyticsManager.trackEvent(
        BwiAnalyticsCategories.Feature,
        BwiAnalyticsActions.Feature_SendPoll,
        pollType,
        votingOptions,
        devices,
    );
};

export const sendAnalyticsFeatureSendVoiceMessage = async (recordLength: number, roomId: string): Promise<void> => {
    const devices = await getRoomDevicesDimension(roomId);
    const messageLengthSeconds = typeof recordLength === "number" && recordLength > 0 ? Math.round(recordLength) : 0;

    return BwiAnalyticsManager.trackEvent(
        BwiAnalyticsCategories.Feature,
        BwiAnalyticsActions.Feature_SendVoiceMessage,
        BwiAnalyticsEvents.SendVoiceMessage_default,
        messageLengthSeconds,
        devices,
    );
};

export const sendAnalyticsFeatureForwardMessage = async (content: IContent, roomId: string): Promise<void> => {
    const devices = await getRoomDevicesDimension(roomId);
    const eventType = getAnalyticsEventType(content);

    return BwiAnalyticsManager.trackEvent(
        BwiAnalyticsCategories.Feature,
        BwiAnalyticsActions.Feature_ForwardMessage,
        eventType,
        undefined,
        devices,
    );
};

export const sendHuddleFeatureMessage = async (type: HuddleFeatureEvents): Promise<void> => {
    return BwiAnalyticsManager.trackEvent(BwiAnalyticsCategories.Feature, BwiAnalyticsActions.Feature_Huddle, type);
};

export async function sendE2EEDecryptionTime(
    eventCount: number,
    maxTimeToDecrypt: number,
    avgTimeToDecrypt: number,
): Promise<void> {
    await Promise.all([
        BwiAnalyticsManager.trackEvent(
            BwiAnalyticsCategories.Encryption,
            BwiAnalyticsActions.Decryption_Time,
            "unknown_inbound_sessionid_count",
            eventCount,
        ),
        BwiAnalyticsManager.trackEvent(
            BwiAnalyticsCategories.Encryption,
            BwiAnalyticsActions.Decryption_Time,
            "unknown_inbound_sessionid_max",
            maxTimeToDecrypt,
        ),
        BwiAnalyticsManager.trackEvent(
            BwiAnalyticsCategories.Encryption,
            BwiAnalyticsActions.Decryption_Time,
            "unknown_inbound_sessionid_avg",
            avgTimeToDecrypt,
        ),
    ]);
}

export function bwiTrackRoomCreate(
    _room: Room,
    opts: {
        federated: boolean;
        direct: boolean;
    },
): void {
    if (SettingsStore.getValue(BwiFeature.Federation)) {
        federationTracker.sendCreateRoom(opts.federated, opts.direct);
    }
}

export function bwiTrackJoinRoom(room: Room): void {
    if (SettingsStore.getValue(BwiFeature.Federation)) {
        federationTracker.sendJoinRoom(room);
    }
}

export const federationTracker = {
    async sendFederationDecision(accepts: boolean, adminsInRoom: number) {
        await BwiAnalyticsManager.trackEvent(
            BwiAnalyticsCategories.Federation,
            FederationAnalyticsActions.ShowSettingFederateRoom,
            accepts ? FederationAnalyticsEvents.AcceptFederation : FederationAnalyticsEvents.DeclineFederation,
            adminsInRoom,
        );
    },
    async sendCreateRoom(federated: boolean, direct: boolean) {
        const action = direct ? FederationAnalyticsActions.CreateDM : FederationAnalyticsActions.CreateRoom;
        const event = federated ? FederationAnalyticsEvents.FederatedRoom : FederationAnalyticsEvents.LocalRoom;

        await BwiAnalyticsManager.trackEvent(BwiAnalyticsCategories.Federation, action, event);
    },
    async sendJoinRoom(room: Room) {
        const joinRule = room.getJoinRule();
        if (bwiIsRoomFederated(room) && [JoinRule.Public, JoinRule.Invite].includes(joinRule)) {
            await BwiAnalyticsManager.trackEvent(
                BwiAnalyticsCategories.Federation,
                FederationAnalyticsActions.JoinRoom,
                room.getJoinRule() === JoinRule.Public
                    ? FederationAnalyticsEvents.JoinPublicFederatedRoom
                    : FederationAnalyticsEvents.JoinFederatedRoom,
            );
        }
    },
};
