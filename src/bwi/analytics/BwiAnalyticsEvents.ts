// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

export enum BwiAnalyticsCategories {
    "General" = "General",
    "Performance" = "Performance",
    "Feature" = "Feature",
    "Encryption" = "Encryption",
    "Federation" = "Federation",
}

export enum GeneralAnalyticsActions {
    "General_ConsentGiven" = "ConsentGiven",
    "General_Logout" = "Logout",
}

export enum GeneralAnalyticsEvents {
    "ConsentGiven_Popup" = "popup",
    "ConsentGiven_Settings" = "settings",
    "Logout_Default" = "logout_default",
}

export enum PerformanceAnalyticsActions {
    "Performance_SendMessage" = "SendMessage",
}

export enum PerformanceAnalyticsEvents {
    "SendMessage_Default" = "send_message_default",
}

export enum FeatureAnalyticsActions {
    "Feature_Huddle" = "Huddle",
    "Feature_SendPoll" = "SendPoll",
    "Feature_SendVoiceMessage" = "SendVoiceMessage",
    "Feature_ForwardMessage" = "ForwardMessage",
}

export enum FeatureAnalyticsEvents {
    "Huddle_Created" = "created_huddle",
    "Huddle_Joined" = "joined_huddle",
    "Huddle_Full" = "huddle_is_full",
    "ForwardMessage_text" = "text",
    "ForwardMessage_image" = "image",
    "ForwardMessage_video" = "video",
    "ForwardMessage_file" = "file",
    "ForwardMessage_voice" = "voice_message",
    "ForwardMessage_audio" = "audio",
    "ForwardMessage_location" = "location",
    "ForwardMessage_unknown" = "unknown",
    "SendPoll_disclosed" = "disclosed",
    "SendPoll_undisclosed" = "undisclosed",
    "SendPoll_disclosed_show_participants" = "disclosed_show_participants",
    "SendPoll_undisclosed_show_participants" = "undisclosed_show_participants",
    "SendVoiceMessage_default" = "send_voice_message_default",
}

export enum EncryptionAnalyticsActions {
    "Encryption_ViewMessage" = "ViewMessage",
    "Decryption_Time" = "DecryptionTime",
}

export enum FederationAnalyticsActions {
    "ShowSettingFederateRoom" = "ShowSettingFederateRoom",
    "CreateRoom" = "CreateRoom",
    "CreateDM" = "CreateDM",
    "JoinRoom" = "JoinRoom",
}
export enum FederationAnalyticsEvents {
    "AcceptFederation" = "accept",
    "DeclineFederation" = "decline",
    "FederatedRoom" = "federated",
    "LocalRoom" = "non_federated",
    "JoinFederatedRoom" = "federated_invite",
    "JoinPublicFederatedRoom" = "federated_invite_public",
}

export const BwiAnalyticsActions = {
    ...GeneralAnalyticsActions,
    ...PerformanceAnalyticsActions,
    ...FeatureAnalyticsActions,
    ...EncryptionAnalyticsActions,
    ...FederationAnalyticsActions,
};

export const BwiAnalyticsEvents = {
    ...GeneralAnalyticsEvents,
    ...PerformanceAnalyticsEvents,
    ...FeatureAnalyticsEvents,
    ...FederationAnalyticsEvents,
};
