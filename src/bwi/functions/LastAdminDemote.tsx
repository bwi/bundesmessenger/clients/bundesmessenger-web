// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { MatrixEvent, Room } from "matrix-js-sdk/src/matrix";
import React from "react";

import ErrorDialog from "../../components/views/dialogs/ErrorDialog";
import { _t } from "../../languageHandler";
import Modal from "../../Modal";

export const showLastAdminWarning = async (): Promise<boolean> => {
    const { finished } = Modal.createDialog(ErrorDialog, {
        title: _t("Demotion not allowed"),
        description: (
            <div>{_t("You are not allowed to demote yourself, as you are the only administrator in this room")}</div>
        ),
        button: _t("OK"),
    });

    const [confirmed] = await finished;
    return confirmed ?? false;
};

export const isLastRoomAdmin = (myPower: string, room: Room, powerLevelEvent: MatrixEvent): boolean => {
    if (parseInt(myPower) !== 100) return false;

    const invitedMembers = room.getMembersWithMembership("invite");
    // MESSENGER-3932 only allow demotion if > 2 members with powerlevel > 100 and not in invite state
    const admins = Object.entries(powerLevelEvent.getContent().users).filter(
        (user) => user[1] === 100 && invitedMembers.findIndex((invited) => invited.userId === user[0]) === -1,
    );
    return admins.length <= 1;
};
