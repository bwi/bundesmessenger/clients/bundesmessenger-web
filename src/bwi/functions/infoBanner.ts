// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import SdkConfig from "../../SdkConfig";

interface BannerConfig {
    text: string;
    bgColor: string;
    fgColor: string;
}

/**
 * Checks the config for an active branding.banner configuration and displays it at the top of the page.
 */
export function showInfoBanner(): void {
    const { banner } = (SdkConfig.get("branding") || {}) as { banner?: BannerConfig };
    if (!banner) return;

    const bannerElement = document.getElementById("infoBanner");
    if (!bannerElement) throw new Error(`Can not set banner. Missing #infoBanner element`);

    const { text, bgColor, fgColor } = banner;
    bannerElement.innerText = text;
    bannerElement.style.backgroundColor = bgColor;
    bannerElement.style.color = fgColor;
    bannerElement.style.display = "flex";

    // adjust matrixchat root height, otherwise with an active banner the chat root would be to large and causes an overflow
    const chatRoot = document.getElementById("matrixchat")!;
    chatRoot.style.height = `calc(100% - ${bannerElement.clientHeight}px)`;
}
