// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { EventType, MatrixEvent } from "matrix-js-sdk/src/matrix";
import { M_POLL_START, M_POLL_END } from "matrix-events-sdk";

import SettingsStore from "../../settings/SettingsStore";
import { BwiFeature } from "../settings/BwiFeature";

const ACTIONBAR_FEATURES = [BwiFeature.HideRedactedMessageSharing, BwiFeature.HideDebug, BwiFeature.HideOSMLink];

const EVENTS_WITH_ACTIONBAR: string[] = [
    EventType.RoomMessage,
    EventType.RoomMessageEncrypted,
    M_POLL_START.name,
    M_POLL_END.name,
];

/**
 * Check if a Message can be shared
 * @returns false if the feature is enabled and the message is redacted; otherwise true
 */
export function canShareMessage(event: MatrixEvent): boolean {
    return SettingsStore.getValue(BwiFeature.HideRedactedMessageSharing) ? !event.isRedacted() : true;
}

/**
 * Check if the MessageActionBar can be hidden if all 'hide features' are enabled and the context of the event allows it.
 * @returns true if the action bar can be hidden; otherwise false
 */
export function disableMessageActionBar(event: MatrixEvent): boolean {
    // Only allow some events to render a ActionBar

    if (!EVENTS_WITH_ACTIONBAR.includes(event.getType())) {
        // element is not in the whitelist
        return true;
    }

    // disable by all items are hidden
    const allowDisable = ACTIONBAR_FEATURES.every((feature) => {
        const value = SettingsStore.getValue(feature);
        if (typeof value === "boolean") return value;
        return false;
    });
    if (!allowDisable) {
        return false;
    }
    return event.isRedacted();
}
