/*
Copyright 2019-2024 New Vector Ltd.

SPDX-License-Identifier: AGPL-3.0-only OR GPL-3.0-only OR LicenseRef-Element-Commercial
Please see LICENSE files in the repository root for full details.
*/

import classNames from "classnames";
import React, { PropsWithChildren } from "react";

import { isTransparencyConfigured } from "../../../bwi/functions/LoginBackground";

interface Props {
    className?: string;
    flex?: boolean;
}

export default function AuthBody({ flex, className, children }: PropsWithChildren<Props>): JSX.Element {
    // MESSENGER-4459 if transparency configured make this bg here transparent
    const bwiBgTransparency = isTransparencyConfigured();

    return (
        <main
            data-testid="mx_AuthBody"
            style={bwiBgTransparency ? { backgroundColor: "transparent" } : undefined}
            className={classNames("mx_AuthBody", className, { mx_AuthBody_flex: flex })}
        >
            {children}
        </main>
    );
}
