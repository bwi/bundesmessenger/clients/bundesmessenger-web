/*
Copyright 2024 New Vector Ltd.
Copyright 2020 The Matrix.org Foundation C.I.C.

SPDX-License-Identifier: AGPL-3.0-only OR GPL-3.0-only OR LicenseRef-Element-Commercial
Please see LICENSE files in the repository root for full details.
*/

import React from "react";

import SetupEncryptionBody from "../../../structures/auth/SetupEncryptionBody";
import BaseDialog from "../BaseDialog";
import { _t } from "../../../../languageHandler";
import { SetupEncryptionStore } from "../../../../stores/SetupEncryptionStore";

function iconFromPhase(): JSX.Element {
    // BWI: Use a fixed icon for every phase.
    return <span className="mx_CompleteSecurity_headerIcon mx_BwiEncIcon" />;
}

interface IProps {
    hasCancel?: boolean;
    onFinished(): void;
}
interface IState {
    icon: React.ReactNode;
}

export default class SetupEncryptionDialog extends React.Component<IProps, IState> {
    private store: SetupEncryptionStore;

    public constructor(props: IProps) {
        super(props);

        this.store = SetupEncryptionStore.sharedInstance();
        this.state = {
            ...this.state,
            icon: iconFromPhase(),
        };
    }

    public componentDidMount(): void {
        this.store.on("update", this.onStoreUpdate);
    }

    public componentWillUnmount(): void {
        this.store.removeListener("update", this.onStoreUpdate);
    }

    private onStoreUpdate = (): void => {
        this.setState({ icon: iconFromPhase() });
    };

    public render(): React.ReactNode {
        return (
            <BaseDialog
                className="mx_SetupEncryptionDialog"
                customHeaderImage={this.state.icon}
                onFinished={this.props.onFinished}
                hasCancel={this.props.hasCancel}
                title={_t("encryption|verify_toast_title")}
            >
                <SetupEncryptionBody onFinished={this.props.onFinished} />
            </BaseDialog>
        );
    }
}
