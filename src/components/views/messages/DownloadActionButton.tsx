/*
Copyright 2024 New Vector Ltd.
Copyright 2021 The Matrix.org Foundation C.I.C.

SPDX-License-Identifier: AGPL-3.0-only OR GPL-3.0-only OR LicenseRef-Element-Commercial
Please see LICENSE files in the repository root for full details.
*/

import { MatrixEvent } from "matrix-js-sdk/src/matrix";
import React from "react";
import classNames from "classnames";
import { DownloadIcon } from "@vector-im/compound-design-tokens/assets/web/icons";

import { MediaEventHelper } from "../../../utils/MediaEventHelper";
import { RovingAccessibleButton } from "../../../accessibility/RovingTabIndex";
import Spinner from "../elements/Spinner";
import { _t, _td, TranslationKey } from "../../../languageHandler";
import { FileDownloader } from "../../../utils/FileDownloader";
import Modal from "../../../Modal";
import ErrorDialog from "../dialogs/ErrorDialog";
// BWI
import ContentScanner, { ContentScanError } from "../../../bwi/content/ContentScanner";
import { ScanningState } from "../../../bwi/content/ScanningState";

interface IProps {
    mxEvent: MatrixEvent;

    // XXX: It can take a cycle or two for the MessageActionBar to have all the props/setup
    // required to get us a MediaEventHelper, so we use a getter function instead to prod for
    // one.
    mediaEventHelperGet: () => MediaEventHelper | undefined;
}

interface IState {
    loading: boolean;
    blob?: Blob;
    tooltip: TranslationKey;
    isScanning: boolean;
    error?: unknown;
}

export default class DownloadActionButton extends React.PureComponent<IProps, IState> {
    private downloader = new FileDownloader();

    public constructor(props: IProps) {
        super(props);

        this.state = {
            loading: false,
            tooltip: _td("timeline|download_action_downloading"),
            // actual scanning takes place when trying to download,
            isScanning: false,
        };
    }

    public async componentDidMount(): Promise<void> {
        const mediaEventHelper = this.props.mediaEventHelperGet();
        if (ContentScanner.instance.canEnable && mediaEventHelper?.media) {
            this.setState({ isScanning: true });
            const scanResult = await mediaEventHelper.media.scanSource();
            if (scanResult !== "safe") {
                this.setState({ isScanning: false, error: new ContentScanError(scanResult) });
            } else {
                this.setState({ isScanning: false });
            }
        }
    }

    private onDownloadClick = async (): Promise<void> => {
        if (this.state.error) return;
        try {
            await this.doDownload();
        } catch (e) {
            Modal.createDialog(ErrorDialog, {
                title: _t("timeline|download_failed"),
                description: (
                    <>
                        <div>{_t("timeline|download_failed_description")}</div>
                        <div>{e instanceof Error ? e.toString() : ""}</div>
                    </>
                ),
            });
            this.setState({ loading: false });
        }
    };

    private async doDownload(): Promise<void> {
        const mediaEventHelper = this.props.mediaEventHelperGet();
        if (this.state.loading || !mediaEventHelper) return;

        if (mediaEventHelper.media.isEncrypted) {
            this.setState({ tooltip: _td("timeline|download_action_decrypting") });
        }

        this.setState({ loading: true });

        const blob = await mediaEventHelper.sourceBlob.value;
        this.setState({ blob });
        await this.downloadBlob(blob);
    }

    private async downloadBlob(blob: Blob): Promise<void> {
        await this.downloader.download({
            blob,
            name: this.props.mediaEventHelperGet()!.fileName,
        });
        this.setState({ loading: false });
    }

    public render(): React.ReactNode {
        let disabled: boolean;
        let tooltip: string;
        let spinner: JSX.Element | undefined;

        if (this.state.loading || this.state.isScanning) {
            spinner = <Spinner w={18} h={18} />;
        }

        if (this.state.error instanceof ContentScanError) {
            disabled = true;
            tooltip = ScanningState.from(this.state.error.reason).getScanMessage() ?? "";
        } else if (this.state.isScanning) {
            tooltip = _t("Scanning...");
        } else if (this.state.loading) {
            tooltip = _t("Decrypting");
        } else {
            tooltip = _t("action|download");
        }

        disabled ??= !!spinner;

        const classes = classNames({
            mx_MessageActionBar_iconButton: true,
            mx_MessageActionBar_downloadButton: true,
            mx_MessageActionBar_downloadSpinnerButton: !!spinner,
        });

        return (
            <RovingAccessibleButton
                className={classes}
                title={tooltip}
                onClick={this.onDownloadClick}
                disabled={disabled}
                placement="left"
            >
                <DownloadIcon />
                {spinner}
            </RovingAccessibleButton>
        );
    }
}
