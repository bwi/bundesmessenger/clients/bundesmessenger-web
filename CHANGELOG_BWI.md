# Changes in BWI project 2.22.0 (2025-02-13)
Upstream merge ✨:
1.11.90

Features ✨:
- message pinning
- BuM Birthday screen

Improvements 🙌:
- reworked poll details layout
- reworked share dialog
- force_verification flag enabled by default
- react sdk integrated into web, only js sdk and web now
- content scanner fit for authenticated media
- 

Bugfix 🐛:
- Fixes for CVE-2024-51750 / GHSA-w36j-v56h-q9pc
- Fixes for CVE-2024-51749 / GHSA-5486-384g-mcx2
- Fixes for CVE-2024-50336 / GHSA-xvg8-m4x3-w6xr
- fixed passphrase reset flow
- dialogs/notifications trigger now after using cross signing

# Changes in BWI project 2.21.0 (2024-09-03)
Upstream merge ✨:
1.11.73

Features ✨:
- removed Huawei App Gallery

Bugfix 🐛:
- Password change error wording improved
- fixed clipping of user role in user list
- fixed disallowing admins to abandon non-empty room
- fixed matomo deviceAmount dimension
- fix for CVE-2024-42369

# Changes in BWI project 2.18.0 (2024-07-02)
Upstream merge ✨:
v1.11.68

Features ✨:
- DATAV number config and view in Legal tab

Improvements 🙌:
- Rust enabled by default
- Theming reworked with new accent (compound) colors

Bugfix 🐛:
- Rust migration fixed
- Rust mandatory passphrase fixed

# Changes in BWI project 2.17.0 (2024-04-29)
Upstream merge ✨:
v1.11.59

Features ✨:
- Federation: Onboarding modal
- Federation: Add Matomo tracking for some federation events
- Add wellknown configuration provider
- Add PhasedModal component
- Add AvatarStack component

Improvements 🙌:
- Add custom text for Rust migration loading screen

Bugfix 🐛:
- No longer create ACL event for DMs

# Changes in BWI project 2.16.0 (2024-03-21)
Upstream merge ✨:
v1.11.58

Features ✨:
- Federation: Announcement modal
- Federation: Federation pill in room header
- Federation: Do not allow federated admins
- Federation: Public avatar information
- Federation: Feedback after migrating a non federated room
- Trim user name after pasting into login field

Improvements 🙌:
- Alias removed from room summary
- Hide the option to verify another users session
- Federation: Remove 'Remind me later' option from room migration modal
- Federation: Text changes for federation invites errors
- Removed Element actions from Spotlight

# Changes in BWI project 2.15.0 (2024-02-12)
Upstream merge ✨:
v1.11.53

Features ✨:
- Federation: add icon to timeline avatars=
- Federation: add decision dialog what to do with a pre federation room
- Federation: add validation to the InviteDialog to only allow federated invites if room federation is enabled

Improvements 🙌:
- Set web client name to app name in sessions
- Set showAvatarChanges feature to always true and hide option in settings
- Federation: migrated federation pill in RoomSummary to the new layout

Bugfix 🐛:
- Remove e2e error in new DM
- Translation and Text fixes
- Add ip_literals to ACL event object

# Changes in BWI project 2.14.0 (2024-01-15)
Upstream merge ✨:
v1.11.51

Features ✨:
- Federation: add 'Enable federation' option to room creation options
- Federation: add 'Enable federation' option to room settings
- Federation: add federation icon to member avatars

Improvements 🙌:
- Remove 'Spaces' from Spotlight

Bugfix 🐛:
- Allow leaving a DM if the other party has not joined
- Translation and Text fixes

# Changes in BWI project 2.12.0 (2023-12-4)
Upstream merge ✨:
v1.11.49

Features ✨:
- Federation: add federation icons to federated rooms and users

Improvements 🙌:
- Remove "validate user" action from UserInfo
- Remove e2ee shields in some places to makes space for federation icons
- Add new composer (wysiwyg) to user preferences to allow switching between composers
- Synchronize date formatting in poll details with other formats used

# Changes in BWI project 2.11.0 (2023-10-24)
Upstream merge ✨:
v1.11.44

Features ✨:
- Add key backup restore to security settings to fix some decryption issues

Improvements 🙌:
- Add additional tracking for decryption issues.
- Improved handling for invalid maintenance data

Bugfix 🐛:
- Fix a bug where to context menu will reopen again after sending a voice message

# Changes in BWI project 2.10.0 (2023-09-26)
Upstream merge ✨:
v1.11.39

Improvements 🙌:
- Personal notes are now in favorites and no longer have their own category
- Readme improved

Bugfix 🐛:
- Fix mentions with local permalinks for WYSIWYG editor
- Avatars for shared users are now displayed correctly
- Missing translation for voice recordings text placeholder

# Changes in BWI project 2.9.0 (2023-08-28)
Upstream merge ✨:
v3.76.0

Features ✨:
- maintenance can block backend requests
- supporting maintenance api v2
- huddle matomo events
- optional login hint to help users with their username

Bugfix 🐛:
- well known links fixed
- permission settings reset for room fixed

# Changes in BWI project 2.8.0 (2023-07-31)
Upstream merge ✨:
v1.11.34

Features ✨:
- huddle labeling for video calls
- huddle permissions required set to 0 on room creation

Bugfix 🐛:
- Functionlabels room settings fixed

# Changes in BWI project 2.7.0 (2023-06-30)
Upstream merge ✨:
v1.11.31

Features ✨:

- New option for polls to show users votes
- Added a11y declaration
- Added imprint referenze

Improvements 🙌:

- Both login verification dialogs now look the same
- Privacy data url and imprint are now fetched from the well known file

Bugfix 🐛:

- It is no longer possible to change your power level as the last remaining admin in a room
- Fixed some missing translations

# Changes in BWI project 2.6.0 (2023-05-04)
Upstream merge ✨:
v1.11.28

Improvements 🙌:

- hide ips and improve wording for device manager
- improved wording for analytics setting
- migrated unit/integration tests from enzyme to @testing-library/react
- removed source maps

Features ✨:

- LocationPicker now has a fullscreen option for better usability
- external_user_profile to link to external profile from userprofile that can be configured via config.json

Bugfix 🐛:

- GHSA-xv83-x443-7rmw CVE-2023-30609
- user selection in InviteDialog now using Enter instead of Space to allow for first and last name input again
- csp policy hardened again

# Changes in BWI project 2.5.0 (2023-04-11)
Upstream merge ✨:
v1.11.25

Improvements 🙌:

- room id/alias as tooltip in room list instead of as list element to have more space for room name
- altered dialog for key backup restore/reset finish
- several BITV improvements

Features ✨:

- Permalink button to profile in own user profile
- Scrolling floating date in Timeline

Bugfix 🐛:

- no password confirmation needed anymore after passphrase reset
- CVE-2023-28103 / GHSA-6g43-88cp-w5gv
- CVE-2023-28427 / GHSA-mwq8-fjpf-c2gr

# Changes in BWI project 2.4.0 (2023-03-10)
Upstream merge ✨:
v1.11.22

Improvements 🙌:

-   changed the maintenance wording
-   the welcome screen is now shown as first thing instead of the personal notes room
-   minor BITV additions for passphrase dialogs
-   secure backup: enabled is default now for well-known responses with mandatorycrosssigning feature
-   cleanup of unused files

Features ✨:

-   permalink sharing now includes qr codes
-   created a customizable lab tab to show only specific features

Bugfix 🐛:

-   changing the powerlevel in the userprofile is now working again
-   fixed german translation for download

# Changes in BWI project 2.3.0 (2023-02-10)
Upstream merge ✨:
v1.11.17

Improvements 🙌:

-   added minor BITV compliance fixes
-   disabled context menu for status events
-   improved the timing of the appearance of the BwiNotification Dialogs
-   added flag to hide individual sounds for room notifications
-   improved wording and cross-platform congruence for the cross-signing feature

Bugfix 🐛: well-known request generated from base url in a correct way again

# Changes in BWI project 2.2.0 (2023-01-13)

Upstream merge ✨:
v1.11.16

Bugfix 🐛:

-   Fixed wording of error message page

# Changes in BWI project 2.2.0 (2023-01-13)

Upstream merge ✨:
v3.62.0

Improvements 🙌:

-   BWI Permalink previews for rooms, messages and users reworked
-   Added legal tab
-   hide unintuitive notification targets

Bugfix 🐛:

-   Location attribution from style.json fixed
-   Changing themes between dark and light and system fixed
-   fixed bug that prevented users from self demoting when other admins are in room
-   corrected onenote: and outlook: permalink scheme

# Changes in BWI project 2.1.0 (2022-12-13)

Upstream merge ✨:
v1.11.14

Improvements 🙌:

-   further improvements to onboarding experience
-   user functions labels now appear below usernames

# Changes in BWI project 2.0.0 (2022-11-18)

Upstream merge ✨:
v1.11.12

Bugfix 🐛:

-   faulty icons replaced
-   overlapping categories in room list fixed

Improvements 🙌:

-   make-icons script fixed
-   opencode pipeline step added
-   onboarding experience adapted
-   sharing deleted messages is hidden now

# Changes in BWI project 1.26.0 (2022-10-25)

Upstream merge ✨:
1.11.8

Bugfix 🐛:

-   CVE-2022-39249
-   CVE-2022-39250
-   CVE-2022-39251
-   CVE-2022-39236

Improvements 🙌:

-   changed project to use yarn link instead of workspaces due to compatibility issues
-   location sharing configuration changed to better select which features to use
-   login screen and help and support pages adapted
-   increased usability of search popup (scrolling)
-   better explanation of privileged users

Features ✨:

-   added option to disable browser geolocation query for location sharing
-   onboarding slider enabled
-   custom error pages for BWI

# Changes in BWI project 1.25.0 (2022-09-20)

Upstream merge ✨:
v1.11.4

Improvements 🙌:

-   removed old nv e2e test suite
-   MESSENGER-3538: added feature flag "BwiFeature.hideLiveLocationSharing" to hide live location sharing in composer context

Features ✨:

-   MESSENGER-3609: added unit test to validate configs

Bugfix 🐛:

-   MESSENGER-3629: fixed missing maintenance notification hint when user has a custom avatar

# Changes in BWI project 1.24.0 (2022-08-29)

Upstream merge ✨:
v1.11.1

Improvements 🙌:
MESSENGER-3591: create sha256sum for each platform package after archiving it

Bugfix 🐛:
fixed docker builds for release candidates

Improvements 🙌:
MESSENGER-3421: renamed standard role to member
MESSENGER-3544: notes room image deletion on startup
MESSENGER-3243: added SdkConfig flag 'suppressNotification' to suppress certain notification events by their message type
MESSENGER-3464: fixed compatibility issues with certain features while running nv e2e cypress tests
MESSENGER-3584: hide personal notes room feature when explicitly configured

Features ✨:
MESSENGER-3523: added feature flag 'BwiFeature.preferLocationSharingConfig' to set preference which location sharing config should be prioritized
MESSENGER-3532: added SdkConfig flag 'enabledLanguages' to set available languages the user can pick from

Bugfix 🐛:
fixed an issue when trying to uproot the users session in the login screen which led to a race condition initializing the mxClient twice
fixed contentscanner failing on startup when we were still waiting for the contentscanners public key
CVE-2022-36059 (https://github.com/matrix-org/matrix-js-sdk/commit/8716c1ab9ba93659173b806097c46a2be115199f)

# Changes in BWI project 1.23.0 (2022-08-02)

Upstream merge ✨:
v1.11.0

Improvements 🙌:
MESSENGER-3309: always show static image for notes room, disallow notes room image upload
MESSENGER-3316: improved ci pipeline for faster feedback
MESSENGER-3402: reenabled nv e2e tests in release branches & merge requests

Features ✨:
MESSENGER-3414 & 3415: added feature flag: 'BwiFeature.mandatoryCrossSigning' to avoid user being able to skip the crosssigning process

# Changes in BWI project 1.22.0 (2022-07-01)

Upstream merge ✨:
v1.10.13

# Changes in BWI project 1.21.1 (2022-06-20)

Bump react sdk to 1.21.1

Improvements 🙌:
MESSENGER-3227: automatically login and redirect an active session from login screen
MESSENGER-3225: Improved password related wording

Features ✨:
MESSENGER-3290: automatically set a room alias on room creation

# Changes in BWI project 1.21.0 (2022-06-03)

Upstream merge ✨:
v1.10.12

Improvements 🙌:
added feature flag to hide layout switcher
MESSENGER-3147: history sharing should be disabled by default when a new room is created
MESSENGER-3151: improved wording in user/room settings regarding mentions
MESSENGER-3168: fixed "ensure_translation.sh" script to properly handle plurals

Features ✨:
MESSENGER-3165: enabled certain custom protocols to be linkified in timeline
MESSENGER-1475: added functionality to paste lotus notes snippets directly into composer as url
MESSENGER-3159: show ignored users in security usersettings tab
