# BWI Docker Container

## Arguments that are needed to build the container:

### Required

ARG GITLAB_USERNAME  
ARG GITLAB_PASSWORD  
ARG WEB_REPO  
ARG WEB_BRANCH  
ARG REACT_SDK_REPO  
ARG REACT_SDK_BRANCH  
ARG JS_SDK_REPO  
ARG JS_SDK_BRANCH

### Optional

ARG CONFIG_REPO  
ARG CONFIG_BRANCH  
ARG CONFIG_FILE

### Example

`docker build --build-arg GITLAB_USERNAME=gitlab-user --build-arg GITLAB_PASSWORD=gitlab-password ./`

## External Config File

The docker container has to be run with an external config file and port 80 exposed like so:

`docker run -p 8081:80 -v $(pwd)/config.json:/usr/share/nginx/html/config.json CONTAINER`
